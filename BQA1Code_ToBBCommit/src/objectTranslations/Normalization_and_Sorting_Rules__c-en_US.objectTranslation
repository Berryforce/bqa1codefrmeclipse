<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Normalization and Sorting Rule</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Normalization and Sorting Rules</value>
    </caseValues>
    <fields>
        <help><!-- Placeholder field. This will likely be dis-aggregated to allow for one &quot;apply this rule&quot; field for each and every address field that is subject to normalization or sorting. --></help>
        <label><!-- Apply to Address Field --></label>
        <name>Apply_to_Address_Field__c</name>
    </fields>
    <fields>
        <help><!-- Should this rule apply to listings that will appear in a CenturyTel directory.  All records should always be unchecked, as no sorting of any CenturyTel listing should ever take place. --></help>
        <label><!-- Apply to CenturyTel --></label>
        <name>Apply_to_CenturyTel__c</name>
    </fields>
    <fields>
        <help><!-- If yes, apply this rule to character(s) at the end of the input string.  Some rules are only applied to characters that occur at the beginning, middle or end of a string. --></help>
        <label><!-- Apply to End of String --></label>
        <name>Apply_to_End_of_String__c</name>
    </fields>
    <fields>
        <label><!-- Apply to Frontier --></label>
        <name>Apply_to_Frontier__c</name>
    </fields>
    <fields>
        <help><!-- Should this rule be applied to listings that will appear in Hawaiian Telecom directories? --></help>
        <label><!-- Apply to Hawaiian Telecom --></label>
        <name>Apply_to_Hawaiian_Telecom__c</name>
    </fields>
    <fields>
        <label><!-- Apply to Listing Type 1 --></label>
        <name>Apply_to_Listing_Type_1__c</name>
    </fields>
    <fields>
        <help><!-- If yes, apply this rule to character(s) in the middle of the input string.  Some rules are only applied to characters that occur at the beginning, middle or end of a string. --></help>
        <label><!-- Apply to Middle of String --></label>
        <name>Apply_to_Middle_of_String__c</name>
    </fields>
    <fields>
        <label><!-- Apply to North State --></label>
        <name>Apply_to_North_State__c</name>
    </fields>
    <fields>
        <label><!-- Apply to Standard --></label>
        <name>Apply_to_Standard__c</name>
    </fields>
    <fields>
        <help><!-- If yes, apply this rule to character(s) at the beginning of the input string.  Some rules are only applied to characters that occur at the beginning, middle or end of a string. --></help>
        <label><!-- Apply to Start of String --></label>
        <name>Apply_to_Start_of_String__c</name>
    </fields>
    <fields>
        <help><!-- Placeholder field. This will likely be dis-aggregated to allow for one &quot;apply this rule&quot; field for each and every telephone number field that is subject to normalization or sorting. --></help>
        <label><!-- Apply to Telephone Number Fields --></label>
        <name>Apply_to_Telephone_Number_Fields__c</name>
    </fields>
    <fields>
        <label><!-- Apply to Whole String --></label>
        <name>Apply_to_Whole_String__c</name>
    </fields>
    <fields>
        <help><!-- Placeholder field.  This will likely be dis-aggregated to allow for one &quot;apply this rule&quot; field for each and every name field that is subject to normalization or sorting. --></help>
        <label><!-- Apply to Name Field --></label>
        <name>Apply_to_name_field__c</name>
    </fields>
    <fields>
        <help><!-- What input text needs to be evaluated or transformed? --></help>
        <label><!-- Input Text --></label>
        <name>Input_Text__c</name>
    </fields>
    <fields>
        <help><!-- Is this rule Active?  Remove the checkmark to prevent it from being enforced. --></help>
        <label><!-- Is Active --></label>
        <name>Is_Active__c</name>
    </fields>
    <fields>
        <help><!-- This field will be TRUE if this is a Numerical Normalization Rule meant to substitute a Number for its Text equivalent. A number with TH, RD, ND, or ST, such as 30TH, 33RD, 32ND, or 31ST, is NOT considered a pure Numerical Normalization Rule --></help>
        <label><!-- Numerical Normalization Rule --></label>
        <name>Numerical_Normalization_Rule__c</name>
    </fields>
    <fields>
        <help><!-- What text string should the transformation process insert into the Sort As string to be used for sorting purposes? --></help>
        <label><!-- Output Text --></label>
        <name>Output_Text__c</name>
    </fields>
    <fields>
        <label><!-- Rule Sequence --></label>
        <name>Rule_Sequence__c</name>
    </fields>
    <fields>
        <help><!-- This field helps to identify the information that will be normalized among Name, Address &amp; Phone. --></help>
        <label><!-- Rule for --></label>
        <name>Rule_for__c</name>
        <picklistValues>
            <masterLabel>Address</masterLabel>
            <translation><!-- Address --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Name</masterLabel>
            <translation><!-- Name --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation><!-- Phone --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Short, natural language description of rule.  Example, &quot;1 is converted to one&quot; --></help>
        <label><!-- Short Description --></label>
        <name>Short_Description__c</name>
    </fields>
    <layouts>
        <layout>Admin Normalization and Sorting Rule Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Directory Level Switches --></label>
            <section>Directory Level Switches</section>
        </sections>
        <sections>
            <label><!-- Listing Part Switches --></label>
            <section>Listing Part Switches</section>
        </sections>
        <sections>
            <label><!-- Listing Type Switches --></label>
            <section>Listing Type Switches</section>
        </sections>
        <sections>
            <label><!-- String Part Switches --></label>
            <section>String Part Switches</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Normalization and Sorting Rule Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
        <sections>
            <label><!-- Directory Level Switches --></label>
            <section>Directory Level Switches</section>
        </sections>
        <sections>
            <label><!-- Listing Part Switches --></label>
            <section>Listing Part Switches</section>
        </sections>
        <sections>
            <label><!-- Listing Type Switches --></label>
            <section>Listing Type Switches</section>
        </sections>
        <sections>
            <label><!-- String Part Switches --></label>
            <section>String Part Switches</section>
        </sections>
    </layouts>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
