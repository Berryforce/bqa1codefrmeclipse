<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PAR2AdminQueue</fullName>
        <description>Assign the Admin queue as the owner of the new Portal Access Request</description>
        <field>OwnerId</field>
        <lookupValue>Berry_Admin_Team</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PAR2AdminQueue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RequestToAdminQueue</fullName>
        <actions>
            <name>PAR2AdminQueue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Portal_Access_Request__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Routes each Portal Access Request to the Admin queue for disposition.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
