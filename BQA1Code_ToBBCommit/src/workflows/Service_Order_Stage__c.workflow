<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Action_Code</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Action Not Identified</literalValue>
        <name>Action Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fall_Out_Check</fullName>
        <field>Fallout__c</field>
        <literalValue>1</literalValue>
        <name>Fall Out Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fall_Out_Phone_Format</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Not Appropriate Phone Format</literalValue>
        <name>Fall Out Phone Format</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fallout_Caption_Display_Text</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Caption Display Text</literalValue>
        <name>Fallout - Caption Display Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fallout_Flag</fullName>
        <field>Fallout__c</field>
        <literalValue>1</literalValue>
        <name>Fallout Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flag_as_Fallout</fullName>
        <field>Fallout__c</field>
        <literalValue>1</literalValue>
        <name>Flag as Fallout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Header_Text</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Header Text</literalValue>
        <name>Header Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_CLEC_Provider</fullName>
        <description>If the CLEC Provider is null, flag record as Fallout from Informatica import process (Informatica import process triggers this fallout, WF populates Fallout Reason).</description>
        <field>Fallout_Reason__c</field>
        <literalValue>Header Text</literalValue>
        <name>SO Fallout - CLEC Provider</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Caption_Header</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Caption Header</literalValue>
        <name>SO Fallout - Caption Header</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Caption_Member</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Caption Member</literalValue>
        <name>SO Fallout - Caption Member</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Directory</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Not Connected to a Directory</literalValue>
        <name>SO Fallout - Directory</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Directory_Heading</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Directory Heading is Pending</literalValue>
        <name>SO Fallout - Directory Heading</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Directory_Section</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Directory Section Not Identified</literalValue>
        <name>SO Fallout - Directory Section</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Disconnected_Phone</fullName>
        <field>Fallout__c</field>
        <literalValue>1</literalValue>
        <name>SO Fallout - Disconnected Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Disconnected_Phone_2</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Phone not identified for Disconnect</literalValue>
        <name>SO Fallout - Disconnected Phone 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Indent_Level_Missing</fullName>
        <description>Caption Header and Caption Member records require Indent Level and Indent Order for all file loads except Daily Service Order.</description>
        <field>Fallout_Reason__c</field>
        <literalValue>Indent Level is Required</literalValue>
        <name>SO Fallout - Indent Level Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Indent_Order_Missing</fullName>
        <description>Caption Header and Caption Member records require Indent Level and Indent Order for all file loads except Daily Service Order.</description>
        <field>Fallout_Reason__c</field>
        <literalValue>Indent Order is Required</literalValue>
        <name>SO Fallout - Indent Order Missing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Informatica_Import</fullName>
        <description>If there any fallouts detected by the Informatica Import process set the Fallout Reason. The Informatica process will flag the record as Fallout and set the Fallout Reason Description.</description>
        <field>Fallout_Reason__c</field>
        <literalValue>Fallout during Informatica Import</literalValue>
        <name>SO Fallout - Informatica Import</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Resolved_Date</fullName>
        <field>Fallout_Resolved_Date__c</field>
        <formula>TODAY()</formula>
        <name>SO Fallout - Resolved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Under_Caption</fullName>
        <field>Fallout__c</field>
        <literalValue>1</literalValue>
        <name>SO Fallout - Under Caption</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Fallout_Under_Caption_2</fullName>
        <field>Fallout_Reason__c</field>
        <literalValue>Not Identified - Caption Member - Under Caption</literalValue>
        <name>SO Fallout - Under Caption 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Normalization_Phone</fullName>
        <field>Normalized_Phone__c</field>
        <formula>IF( BEGINS(Phone__c, &quot;(&quot;) , 
(MID(Phone__c, 2,3)+ MID(Phone__c, 7, 3)+MID(Phone__c, 11,4)),
(MID(Phone__c, 1,3)+ MID(Phone__c, 4, 3)+MID(Phone__c, 7,4)))</formula>
        <name>SO Normalization Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SO_Phone_Update</fullName>
        <field>Phone__c</field>
        <formula>OUT_PHONE_NUMBER__c</formula>
        <name>SO - Phone Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Order_Batch_Process_Complete_Dat</fullName>
        <field>Scheduled_Process_Complete_Date__c</field>
        <formula>TODAY()</formula>
        <name>Service Order Batch Process Complete Dat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Order_Disconnected</fullName>
        <field>Disconnected__c</field>
        <literalValue>1</literalValue>
        <name>Service Order - Disconnected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Order_Stage_Indent_Level</fullName>
        <description>If the Indent Level of the SO record is blank and the Indent Order is populated - populate the Indent Level with value = &quot;0&quot;</description>
        <field>Indent_Level__c</field>
        <formula>0</formula>
        <name>Service Order Stage Indent Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Match_on_DL_to_TRUE</fullName>
        <description>When the SO record has an initial match on a Directory Listing, this will be set.</description>
        <field>Initial_Match_on_DL__c</field>
        <literalValue>1</literalValue>
        <name>Set Initial Match on DL to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Match_on_Listing_to_TRUE</fullName>
        <field>Initial_Match_on_Listing__c</field>
        <literalValue>1</literalValue>
        <name>Set Initial Match on Listing to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SO - Phone Update</fullName>
        <actions>
            <name>SO_Phone_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Phone__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.OUT_PHONE_NUMBER__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the Service Order does NOT have a phone listed but the OUT Phone field is populated - copy the out to the inbound</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Action Code</fullName>
        <actions>
            <name>Action_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Action__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Action Code on imported Annual and Daily Data was not identified</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - CLEC Provider</fullName>
        <actions>
            <name>SO_Fallout_CLEC_Provider</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Reason_Description__c</field>
            <operation>contains</operation>
            <value>CLEC FallOut</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>If the CLEC Provider is null, flag record as Fallout from Informatica import process (Informatica import process triggers this fallout, WF populates Fallout Reason).</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Caption Display Text</fullName>
        <actions>
            <name>Fallout_Caption_Display_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fallout_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Display_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type_2__c</field>
            <operation>equals</operation>
            <value>Daily Service Orders,DSO,SODE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type__c</field>
            <operation>equals</operation>
            <value>Daily Service Order,DSO,SODE</value>
        </criteriaItems>
        <description>If Caption Display Text is populated &gt; Flag as Fallout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Caption Header</fullName>
        <actions>
            <name>Fallout_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Caption_Header</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 and (3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Header__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type__c</field>
            <operation>equals</operation>
            <value>Daily Service Orders,SODE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type_2__c</field>
            <operation>equals</operation>
            <value>Daily Service Orders,SODE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Caption Indent Level</fullName>
        <actions>
            <name>Flag_as_Fallout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Indent_Level_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Header__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Indent_Level__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Daily Service Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Caption Header and Caption Member records require Indent Level and Indent Order values, otherwise the record is set to Fallout (this applies to all file imports except DSO - caption records are not created from DSO import).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Caption Indent Order</fullName>
        <actions>
            <name>Flag_as_Fallout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Indent_Order_Missing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Header__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Indent_Order__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Daily Service Orders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Caption Header and Caption Member records require Indent Level and Indent Order values, otherwise the record is set to Fallout (this applies to all file imports except DSO - caption records are not created from DSO import).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Caption Member</fullName>
        <actions>
            <name>Fallout_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Caption_Member</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 or 4)</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type_2__c</field>
            <operation>equals</operation>
            <value>Daily Service Orders,SODE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Data_Feed_Type__c</field>
            <operation>equals</operation>
            <value>Daily Service Order,SODE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Directory</fullName>
        <actions>
            <name>SO_Fallout_Directory</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If Directory is blank &gt; Flag record as Fallout</description>
        <formula>AND( Directory__c = &quot;&quot;,  Fallout__c = TRUE,  Fallout_Resolved__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Directory Heading</fullName>
        <actions>
            <name>SO_Fallout_Directory_Heading</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If the Directory Heading is Blank or Invalid</description>
        <formula>AND(   Directory_Heading__r.Directory_Heading_Name__c = &quot;&quot;,    Fallout__c = TRUE,    Fallout_Resolved__c = FALSE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Directory Section</fullName>
        <actions>
            <name>SO_Fallout_Directory_Section</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( Directory_Section__c = &quot;&quot;,  Fallout__c = TRUE,  Fallout_Resolved__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Disconnected Phone</fullName>
        <actions>
            <name>SO_Fallout_Disconnected_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Disconnected_Phone_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Action__c</field>
            <operation>equals</operation>
            <value>Disconnect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.OUT_PHONE_NUMBER__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the SO Action = Disconnect and NO phone number has been provided - Flag as Fallout = TRUE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Header Text</fullName>
        <actions>
            <name>Flag_as_Fallout</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Header_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Header_text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>If Header Text is populated &gt; Flag as Fallout</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Informatica Import</fullName>
        <actions>
            <name>SO_Fallout_Informatica_Import</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Reason_Description__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>If there any fallouts detected by the Informatica Import process set the Fallout Reason.  The Informatica process will flag the record as Fallout and set the Fallout Reason Description.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Phone Number Validation</fullName>
        <actions>
            <name>Fall_Out_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fall_Out_Phone_Format</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow verifies if any of the THREE Phone fields are populated
Phone
Phone Override
OUT PHONE
If any are populated then it does not trigger, If they are blank / null or in the wrong format the workflow flags the records as fallout</description>
        <formula>AND(    NOT(REGEX(Phone__c, &quot;(\\D?[0-9]{3}\\D?)[\\s][0-9]{3}-[0-9]{4}&quot;)),    (NOT(Phone_Override__c = NULL)),    (NOT(ISBLANK(OUT_PHONE_NUMBER__c))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Resolved Date</fullName>
        <actions>
            <name>SO_Fallout_Resolved_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Fallout_Resolved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When was this fallout item resolved?</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Under Caption</fullName>
        <actions>
            <name>SO_Fallout_Under_Caption</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Under_Caption_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.HIDDEN_FIELDS_Under_Caption__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If So record is a Caption Member and the Under Caption lookup is NOT populated then Fallout = TRUE - Berryforce does not know how to build the Caption Arrangement</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Fallout - Under Caption2</fullName>
        <actions>
            <name>SO_Fallout_Under_Caption</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SO_Fallout_Under_Caption_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>If So record is a Caption Member and the Under Caption lookup is NOT populated then Fallout = TRUE - Berryforce does not know how to build the Caption Arrangement</description>
        <formula>AND(Caption_Member__c = TRUE, ISBLANK( Under_Caption__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SO Normalization Phone</fullName>
        <actions>
            <name>SO_Normalization_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate the Phone in the Normalized Phone field</description>
        <formula>AND( NOT(Phone__c =&quot;&quot;), (Normalized_Phone__c = &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Service Order - Disconnected</fullName>
        <actions>
            <name>Service_Order_Disconnected</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 or 3)</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Disconnect_Reason__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Disconnected__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Disconnected__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Should the Disconnected box be checked? If the Disconnected text field is populated the Disconnect checkbox should be checked.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Service Order Batch Process Complete Date</fullName>
        <actions>
            <name>Service_Order_Batch_Process_Complete_Dat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Scheduled_Process_Complete__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Scheduled_Process_Complete_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Date stamp the SO Record when the Service Order Batch Process has been completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Service Order Stage Indent Level</fullName>
        <actions>
            <name>Service_Order_Stage_Indent_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 2) AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Header__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Caption_Member__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Indent_Level__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Service_Order_Stage__c.Indent_Order__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the Indent Level of the SO record is blank and the Indent Order is populated - populate the Indent Level with value = &quot;0&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Initial Match on DL to TRUE</fullName>
        <actions>
            <name>Set_Initial_Match_on_DL_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Current_Match_On_DL__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Initial Match on Listing to TRUE</fullName>
        <actions>
            <name>Set_Initial_Match_on_Listing_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Service_Order_Stage__c.Current_Match_On_Listing__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
