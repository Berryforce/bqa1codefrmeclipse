public class TBChDigitalForBTHandlerController{
    public TBChDigitalForBTHandlerController(){   
    }
    /**** this method  RetroTransfer will use when OLI has been gone live and does have invoices  
    What we will do:
    1. we will create SCN for old billing partner.
    2. we will create invoices for new billing partner.
    2. we will update OLIs Information .
    3. we will handle recurring CS claims if it exists.
    *****/     

    public static void RetroTransfer(boolean isValidate,Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForSalesInvoiceWrapper,
                Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,
                list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim , 
                list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
                map<Integer, list<c2g__codaCreditNoteLineItem__c>> mapSCNLI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,
                list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaInvoice__c> insertSalesInvoice,List<c2g__codaInvoiceLineItem__c> SILIListForUpdate,
                Id accountid,String selectedReasonForTransfer,String selectedPartner, String selectedPartnerName, String selectedPayment,String selectedPaymentMethodId,
                map<Id,Date> mapOLIMinimumInvoiceDt, String strBillingParnterAccountId,List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,List<Opportunity> lstNewOpportunity,
                List<Line_Item_History__c> lstOLIHistory, map<Id, Integer> mapOLINoOfMonthTransfer) {
        Map<String ,id> dimensionsMapValues;
        map<String, Id> mapPeriod = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        dimensionsMapValues=DimensionsValues.getDimensionByOLI(mapForUpdatedOLis.values());

        System.debug('Diane %%%%%%%%%%%%%%%%%%%%mapForSalesInvoiceWrapper '+mapForSalesInvoiceWrapper);
        System.debug('Diane %%%%%%%%%%%%%%%%%%%%mapForSalesInvoiceWrapper size '+mapForSalesInvoiceWrapper.size());

        System.debug('====>>>lstorderlineForUpdate '+isValidate);
        System.debug('====>>>mapForUpdatedOLis.values()'+mapForUpdatedOLis.values());
        // Here we will update the OLIs.
        if(!isValidate) {
            list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id> {accountid});
            List<order_line_items__c> lstorderlineForUpdate = TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformation(mapForUpdatedOLis.values(), lstAccount[0], selectedReasonForTransfer, selectedPartner, selectedPartnerName, selectedPayment, selectedPaymentMethodId, 'retro',lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory, mapOLINoOfMonthTransfer);
            //List<order_line_items__c> lstorderlineForUpdate=TBChOrderLineItemBTHandlerController.UpdateOlisDigitalRetro(mapForUpdatedOLis.values(), accountId, selectedReasonForTransfer,selectedPartner,selectedPartnerName,selectedPayment,selectedPaymentMethodId);
            //System.debug('====>>>after lstorderlineForUpdate'+lstorderlineForUpdate);
            update lstorderlineForUpdate;
            dimensionsMapValues= DimensionsValues.getDimensionByOLI(lstorderlineForUpdate);
        }

        Integer iCount = 0;
        Integer iCountSCN=0;
        map<Id, Date> mapSIInvoiceDate = new map<Id, Date>();
        map<Id, Date> mapSIDueDate = new map<Id, Date>();
        map<Id, Date> mapSIServiceStartDate = new map<Id, Date>();
        map<Id, Date> mapSIServiceEndDate = new map<Id, Date>();  
        set<Id> SILIsetOfIds =new Set<Id>();
        Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForExtraInvoice = new Map<Id,List<c2g__codaInvoiceLineItem__c>>();
        Map<Id,Date> mapForExtraInvoiceNextDate = new Map<Id,Date>();
        order_line_items__c objoliNew ;               
        for(ID iterator : mapForSalesInvoiceWrapper.keySet()) {
            iCount++;
            objoliNew =null;
            integer successPayments;
            integer paymentsRemaining;
            for(c2g__codaInvoiceLineItem__c iteratorChild:mapForSalesInvoiceWrapper.get(iterator)){
                iCountSCN++;               
                if(iteratorChild.c2g__NetValue__c>0 || Test.isRunningTest()){
                lstInsertSCN.add(TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceToCreditNoteForDigitalRetro(mapForUpdatedOLis.get(iteratorChild.order_line_item__c),iteratorChild,iCountSCN,mapPeriod ));
                TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceLineItemToSalesCreditNoteLineItemForDigitalRetro(new list<c2g__codaInvoiceLineItem__c>{iteratorChild}, iCountSCN, mapSCNLI,mapForUpdatedOLis);
                }
                if(!SILIsetOfIds.contains(iteratorChild.id)) {
                    System.debug('###### in if 1 sili update');
                    SILIsetOfIds.add(iteratorChild.Id);
                    SILIListForUpdate.add(new c2g__codaInvoiceLineItem__c(id=iteratorChild.Id,Offset__c=true));
                    System.debug('Testingg SILIListForUpdate 3 '+SILIListForUpdate);
                }
                if(iteratorChild.Order_Line_Item__c != null) {
                    objoliNew = mapForUpdatedOLis.get(iteratorChild.Order_Line_Item__c);
                }
                if(!mapSIInvoiceDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIInvoiceDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.c2g__InvoiceDate__c);
                }
                if(!mapSIDueDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIDueDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.c2g__DueDate__c);
                }
                if(!mapSIServiceStartDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIServiceStartDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.Service_Start_Date__c);
                }                           
                if(!mapSIServiceEndDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIServiceEndDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.Service_End_Date__c);
                }                           
                if(!mapSalesInvoiceLineItem.containsKey(iCount)) {
                    mapSalesInvoiceLineItem.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                }
                mapSalesInvoiceLineItem.get(iCount).add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceLineItemForDigitalRetro(iteratorChild, objoliNew,dimensionsMapValues));  
                successPayments=Integer.valueOf(iteratorChild.Successful_Payments__c);
                paymentsRemaining=Integer.valueOf(iteratorChild.Payments_Remaining__c) ;                 
            }

            if(objoliNew!=null) {
                Date duedate=mapSIDueDate.get(iterator);
                Date invoicedate=mapSIInvoiceDate.get(iterator);
                System.debug('Diane %%%%%%%%%%%%%%%%%%%%duedate '+duedate +'invoicedate '+invoicedate);
                if(Test.isRunningTest()) {
                    if(duedate==null)
                        duedate=date.today().addmonths(1);
                    if(invoicedate==null)
                        invoicedate=date.today().addmonths(1);
                }
                if(mapSalesInvoiceLineItem.size()>0)
                    insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForDigitalRetro(objoliNew, iCount, mapPeriod, 0,duedate,invoicedate,mapSIServiceStartDate.get(iterator),mapSIServiceEndDate.get(iterator),successPayments,paymentsRemaining)); 
            }
            /*** End Creating Sales Invoices  for new billing partner ***/
        }
        // below logic is for handle the recurring CS Claim 
        //Map<Id,c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet());
        list<c2g__codaCreditNoteLineItem__c> lstSCN = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet(), strBillingParnterAccountId, false);
        if(lstSCN.size()>0){
        RecurringCSClaimHandlerMethod(mapPeriod,lstSCN,mapForUpdatedOLis,dimensionsMapValues,mapSCNLIForCSClaim,lstInsertSCNForCSClaim,mapSalesInvoiceLineItemForCSClaim,insertSalesInvoiceForCSClaim,SCNLIListForUpdateForCSClaim,mapOLIMinimumInvoiceDt);
        }
    }

    /***** this method FutureTransfer will use if there is no need to create the invoices. 
    What we will do:
    1. we will update OLIs Information .
    2. we will handle recurring CS claims if it exists.
    *****/
        
    public static void FutureTransfer(boolean isValidate,Map<id,order_line_Items__c> mapForUpdatedOLis, 
                Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, 
                map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim, 
                list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
                Id accountid,String selectedReasonForTransfer,String selectedPartner, String selectedPartnerName, String selectedPayment,
                String selectedPaymentMethodId,map<Id,Date> mapOLIMinimumInvoiceDt, String strBillingParnterAccountId,List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,
                List<Opportunity> lstNewOpportunity,List<Line_Item_History__c> lstOLIHistory, map<Id, Integer> mapOLINoOfMonthTransfer){
        Map<String ,id> dimensionsMapValues;
        dimensionsMapValues=DimensionsValues.getDimensionByOLI(mapForUpdatedOLis.values()); 
        map<String, Id> mapPeriod = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        // Here we will update the OLIs.
        system.debug('Step 2 : ');
        if(!isValidate){
            system.debug('Step 3 : ');
            list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id> {accountid});
            //List<order_line_items__c> lstorderlineForUpdate=TBChOrderLineItemBTHandlerController.UpdateOlisDigitalFuture(mapForUpdatedOLis.values(), accountId, selectedReasonForTransfer,selectedPartner,selectedPartnerName,selectedPayment,selectedPaymentMethodId);
            List<order_line_items__c> lstorderlineForUpdate = TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformation(mapForUpdatedOLis.values(), lstAccount[0], selectedReasonForTransfer, selectedPartner, selectedPartnerName, selectedPayment, selectedPaymentMethodId, 'future',lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory, mapOLINoOfMonthTransfer);
            System.debug('====>>>after lstorderlineForUpdate'+lstorderlineForUpdate); 
            update lstorderlineForUpdate;
            dimensionsMapValues = DimensionsValues.getDimensionByOLI(lstorderlineForUpdate);
            system.debug('Step 4 : ');
        }
        // below logic is for handle the recurring CS Claim
        //Map<Id,c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap = SalesCreditNoteLineItemSOQLMethods.getSCNLIMapByOLiIds(mapForUpdatedOLis.keySet());
        list<c2g__codaCreditNoteLineItem__c> lstSCN = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet(), strBillingParnterAccountId, true);
        if(lstSCN.size()>0){
            RecurringCSClaimHandlerMethod(mapPeriod,lstSCN,mapForUpdatedOLis,dimensionsMapValues,mapSCNLIForCSClaim,lstInsertSCNForCSClaim,
            mapSalesInvoiceLineItemForCSClaim,insertSalesInvoiceForCSClaim,SCNLIListForUpdateForCSClaim,mapOLIMinimumInvoiceDt);
        }
    }

    // this method will use for recurring CS claim for whole digital process
    private static void RecurringCSClaimHandlerMethod(Map<String,Id> mapOfPeriod,list<c2g__codaCreditNoteLineItem__c>  CreditNoteLineItemsSOQLMap, 
                Map<id,order_line_Items__c> mapForUpdatedOLis,Map<String, Id> dimensionsMapValuesNew, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim, 
                list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim, 
                list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,Map<id,Date> MapOfSCNDate) {
        Map<id, List<c2g__codaCreditNoteLineItem__c>> mapForCreditNote = new Map<id, List<c2g__codaCreditNoteLineItem__c>>();
        integer iCountSCN=0;
        integer iCountSI=0;
        Set<Id> SCNLIsetOfIds =new Set<Id>();
        Set<Id> SCNsetOfIds =new Set<Id>();
        if(CreditNoteLineItemsSOQLMap.size()>0) {
            for(c2g__codaCreditNoteLineItem__c  creditNoteLineItem : CreditNoteLineItemsSOQLMap) {
            System.debug('====>>>iterator.order_line_Item__c '+MapOfSCNDate);
            System.debug('====>>>iterator.order_line_Item__c '+creditNoteLineItem.Order_Line_Item__c);
                Date dtInvoiceDate = MapOfSCNDate.get(creditNoteLineItem.Order_Line_Item__c);
                System.debug('====>>>iterator.order_line_Item__c '+dtInvoiceDate );
                 System.debug('====>>>iterator.order_line_Item__c '+creditNoteLineItem.c2g__CreditNote__r.c2g__CreditNoteDate__c);
                 if(Test.isRunningTest() && dtInvoiceDate==null)
                     dtInvoiceDate=creditNoteLineItem.c2g__CreditNote__r.c2g__CreditNoteDate__c.addMonths(-1);
                if(creditNoteLineItem.c2g__CreditNote__r.c2g__CreditNoteDate__c >= dtInvoiceDate) {
                    if(!mapForCreditNote.containsKey(creditNoteLineItem.c2g__CreditNote__c))
                        mapForCreditNote.put(creditNoteLineItem.c2g__CreditNote__c,new List<c2g__codaCreditNoteLineItem__c>());
                    mapForCreditNote.get(creditNoteLineItem.c2g__CreditNote__c).add(creditNoteLineItem);
                }
            }
            System.debug('====>>>mapForCreditNote '+MapOfSCNDate);
            for(Id creditnoteId : mapForCreditNote.keySet()) {
                iCountSCN++;
                iCountSI++; 
                for(c2g__codaCreditNoteLineItem__c iterator : mapForCreditNote.get(creditnoteId)) {
                    System.debug('====>>>iterator.order_line_Item__c '+iterator.order_line_Item__c);
                    System.debug('====>>>iterator.c2g__CreditNote__r.c2g__CreditNoteDate__c '+iterator.c2g__CreditNote__r.c2g__CreditNoteDate__c);
                    //if(MapOfSCNDate.containsKey(iterator.order_line_Item__c) && MapOfSCNDate.get(iterator.order_line_Item__c)<=iterator.c2g__CreditNote__r.c2g__CreditNoteDate__c){
                    if(!SCNLIsetOfIds.contains(iterator.id)) {
                        SCNLIsetOfIds.add(iterator.Id);
                        SCNLIListForUpdateForCSClaim.add(new c2g__codaCreditNoteLineItem__c(id=iterator.Id,Offset__c=true));
                        if(iterator.c2g__UnitPrice__c > 0) {
                            if(!mapSalesInvoiceLineItemForCSClaim.containsKey(iCountSI)) {
                                mapSalesInvoiceLineItemForCSClaim.put(iCountSI,new List<c2g__codaInvoiceLineItem__c>());
                            }
                            mapSalesInvoiceLineItemForCSClaim.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.copyDataFromSCNLIToSILIForDigitalCSClaim(iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c)));

                            if(!mapSCNLIForCSClaim.containsKey(iCountSCN)) {
                                mapSCNLIForCSClaim.put(iCountSCN, new list<c2g__codaCreditNoteLineItem__c>());
                            }
                            mapSCNLIForCSClaim.get(iCountSCN).add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineItemForDigitalCSClaim(mapForUpdatedOLis,iterator,dimensionsMapValuesNew));
                        }
                    }

                    // Below logic will create invoices and credit notes data.
                    if(!SCNsetOfIds.contains(iterator.c2g__CreditNote__c)) {
                        SCNsetOfIds.add(iterator.c2g__CreditNote__c);
                        if(mapSCNLIForCSClaim.size()>0)
                            lstInsertSCNForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesCreditNoteForDigitalCSClaim(mapOfPeriod,iterator,iCountSCN,mapForUpdatedOLis.get(iterator.Order_Line_Item__c),false));                   
                        if(mapSalesInvoiceLineItemForCSClaim.size()>0)
                            insertSalesInvoiceForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForDigitalCSClaim(mapOfPeriod,iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c),iCountSI,false));                 
                    }
                    //}
                }
            }
        }
    }
}