@isTest
private class OpportunityRollUpPaymentsTest {

    static testMethod void OpportunityRollUpPaymentsTestMethod() {
        Test.startTest();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        TmpOpportunityLineItem__c newTmpOLI = TestMethodsUtility.generateTmpOpportunityLineItem(newAccount);
        newTmpOLI.Opportunityid__c = newOpportunity.Id;
        insert newTmpOLI;
        delete newTmpOLI;
        Test.stopTest();
    }
}