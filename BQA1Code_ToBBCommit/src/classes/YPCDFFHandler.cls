public with sharing class YPCDFFHandler {
	public static void onBeforeUpdate(list<YPC_Graphics__c> listYPCGraphics, map<Id, YPC_Graphics__c> oldMap) {
        YPCGraphicURLUpdate(listYPCGraphics, oldMap);
    }
    
    public static void onAfterUpdate(list<YPC_Graphics__c> listYPCGraphics, map<Id, YPC_Graphics__c> oldMap) {
        updateParentDFFForChildCompletion(listYPCGraphics, oldMap);
    }
    
    private static void YPCGraphicURLUpdate(list<YPC_Graphics__c> listYPCGraphics, map<Id, YPC_Graphics__c> oldMap) {
        for (YPC_Graphics__c ypc : listYPCGraphics) {
            if(ypc.Miles_Status__c == 'Complete') {
                ypc.YPC_Graphics_URL__c = ypc.LoResCopy__c;
            }
        }
    }
    
    private static void updateParentDFFForChildCompletion(list<YPC_Graphics__c> listYPCGraphics, map<Id, YPC_Graphics__c> oldMap) {
        map<Id, Digital_Product_Requirement__c> mapDFF = new map<Id, Digital_Product_Requirement__c>();
        set<Id> setDFFId = new set<Id>();
        for (YPC_Graphics__c iterator : listYPCGraphics) {
        	setDFFId.add(iterator.DFF__c);
        }
        
        if(setDFFId.size() > 0) {
        	list<Digital_Product_Requirement__c> lstDFF = [Select Id, Miles_Status__c, Graphic_Pao_Url__c, Logo_Graphic_Url__c, Diamond_Graphic_URL__c, Banner_Url__c, Diamond_Graphic_Dist_URL__c, Banner_Dist_Url__c, Logo_Graphic_Dist_Url__c , (Select Id, DFF__c, Miles_Status__c, YPC_Graphics_Type__c, YPC_Graphics_URL__c From YPC_Graphics__r) From Digital_Product_Requirement__c where ID IN:setDFFId];
        	if(lstDFF.size() > 0) {
				boolean bFlag = false;
				for(Digital_Product_Requirement__c iterator : lstDFF) {
					set<Id> setYPCCount = new set<Id>();
					for(YPC_Graphics__c iteratorYPC : iterator.YPC_Graphics__r) {
						if(iteratorYPC.Miles_Status__c == 'Complete') {
							setYPCCount.add(iteratorYPC.Id);
							bFlag = true;
							if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c.contains('PAO'))
		                    	iterator.Graphic_Pao_Url__c = iteratorYPC.YPC_Graphics_URL__c;
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Logo' )
		                    	iterator.Logo_Graphic_Url__c = iteratorYPC.YPC_Graphics_URL__c;
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Diamond Logo')    
		                    	iterator.Diamond_Graphic_URL__c = iteratorYPC.YPC_Graphics_URL__c;
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Banner') 
		                    	iterator.Banner_Url__c = iteratorYPC.YPC_Graphics_URL__c;
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c.contains('Diamond Distribution'))
		                    	iterator.Diamond_Graphic_Dist_URL__c = iteratorYPC.YPC_Graphics_URL__c;    
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c == 'Banner Distribution')
		                    	iterator.Banner_Dist_Url__c = iteratorYPC.YPC_Graphics_URL__c;
		                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Logo Distribution')
		                    	iterator.Logo_Graphic_Dist_Url__c = iteratorYPC.YPC_Graphics_URL__c;
						}
					}
					if(iterator.YPC_Graphics__r.size() == setYPCCount.size()) {
						iterator.Miles_Status__c = 'Complete';
					}
					else {
						iterator.Miles_Status__c = 'In Progress';
					}
				}
				if(bFlag) {
					update lstDFF;
				}
			}
        }
        
        /*for (YPC_Graphics__c ypc : listYPCGraphics) {
           if(ypc.Miles_Status__c != oldMap.get(ypc.Id).Miles_Status__c && ypc.DFF__c != null) {
                Boolean checkFlag = false;     
                Digital_Product_Requirement__c DFF;              
                if(!mapDFF.containsKey(ypc.DFF__c)) {
                    mapDFF.put(ypc.DFF__c, new Digital_Product_Requirement__c(Id = ypc.DFF__c));    
                    DFF = mapDFF.get(ypc.DFF__c);     
                    checkFlag = true;
                } else if(mapDFF.get(ypc.DFF__c).Miles_Status__c != 'Complete') {   
                    DFF = mapDFF.get(ypc.DFF__c);     
                    checkFlag = true;
                }
                if(checkFlag) {
                    if(ypc.Miles_Status__c == 'Complete')
                        DFF.Miles_Status__c = 'Complete';
                    else
                        DFF.Miles_Status__c = 'In Progress';
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c.contains('PAO'))
                    DFF.Graphic_Pao_Url__c = ypc.YPC_Graphics_URL__c;
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c =='Logo' )
                    DFF.Logo_Graphic_Url__c = ypc.YPC_Graphics_URL__c;
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c =='Diamond Logo')    
                    DFF.Diamond_Graphic_URL__c = ypc.YPC_Graphics_URL__c;
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c =='Banner') 
                    DFF.Banner_Url__c = ypc.YPC_Graphics_URL__c;
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c.contains('Diamond Distribution'))
                    DFF.Diamond_Graphic_Dist_URL__c = ypc.YPC_Graphics_URL__c;    
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c == 'Banner Distribution')
                    DFF.Banner_Dist_Url__c = ypc.YPC_Graphics_URL__c;
                    if(ypc.YPC_Graphics_Type__c != null && ypc.YPC_Graphics_Type__c =='Logo Distribution')
                    DFF.Logo_Graphic_Dist_Url__c = ypc.YPC_Graphics_URL__c;
                }
           }
        }
        if(!Test.isRunningTest())
            update mapDFF.values();*/
   }
}