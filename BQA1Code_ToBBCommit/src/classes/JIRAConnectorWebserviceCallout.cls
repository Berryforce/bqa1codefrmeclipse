global class JIRAConnectorWebserviceCallout {
    @future (callout=true)
    WebService static void createIssue(String caseId, String recTypeId) { 
        //Set your username and password here
        JIRAConnector__c JIRAConnector = JIRAConnector__c.getInstance(recTypeId);
        String username = JIRAConnector.Username__c;// 'SFDC Admin'
        String password = JIRAConnector.Password__c;// 'Berryadmin1asUtYaSsEuvITIW75vptllaS';
 
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
 
        //Construct Authorization and Content header
        Blob headerValue = Blob.valueOf(username+':'+password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
 
        //Construct Endpoint
        String endpoint = JIRAConnector.JIRA_URL__c +'/rest/customware/connector/1.0/'+ JIRAConnector.System_Id__c +'/'+ JIRAConnector.Object_Type__c +'/'+ caseId +'/issue/create.json';
 
        //Set Method and Endpoint and Body
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setBody('{"project":"'+ JIRAConnector.Project_Key__c +'", "issueType":"'+ JIRAConnector.Issue_Type__c +'"}');
  
        //try {
           //Send endpoint to JIRA
           if(!Test.isRunningTest()) {
			res = http.send(req);
           }
        //} //catch(System.CalloutException e) {
            System.debug(res.toString());
        //}
    }
 	
 	@future (callout=true) 
    WebService static void synchronizeWithJIRAIssue(String caseId, String recTypeId) {
        //Modify these variables:
        JIRAConnector__c JIRAConnector = JIRAConnector__c.getInstance(recTypeId);
        String username = JIRAConnector.Username__c;// 'SFDC Admin'
        String password = JIRAConnector.Password__c;// 'Berryadmin1asUtYaSsEuvITIW75vptllaS';
 
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Content-Length', '0');
        req.setEndpoint(JIRAConnector.JIRA_URL__c + '/rest/customware/connector/1.0/' + JIRAConnector.System_Id__c + '/' + JIRAConnector.Remote_Object__c + '/' + caseId + '/issue/synchronize.json');
        req.setMethod('PUT');
  
        try {
        	if(!Test.isRunningTest()) {
            	res = http.send(req);   
        	}        
        } catch(System.CalloutException e) {         
            System.debug(res.toString());
        }
    }     
}