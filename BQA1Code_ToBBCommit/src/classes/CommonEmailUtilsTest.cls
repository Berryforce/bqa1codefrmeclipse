//CommonEmailUtils  Testclass
@isTest(SeeAllData=True)
public class CommonEmailUtilsTest{
static testmethod void CEUTest(){
Test.starttest();
list<String> recipients = new list<string>();
recipients.add('pmakamgovind@csc.com');
recipients.add('tkakarla@csc.com');

list<string> CCrecipients = new list<string>();
CCrecipients.add('pmakamgovind@csc.com');
CCrecipients.add('tkakarla@csc.com');

Account Acc = TestmethodsUtility.createaccount('customer');
Contact ct = TestmethodsUtility.createcontact(Acc.id);

//Email Template query
Set<ID> FolderIds = new Set<ID>();
ID emailtemplateid;
/*
List<Folder> Folders = [Select Id, Name From Folder Where Type = 'Email'];
for(Folder F : Folders) { FolderIds.add(F.Id); } 

EmailTemplate Templates = [Select Id, Name, IsActive, Folder.Name From EmailTemplate  Where Name = 'DFF_Reminder' and IsActive = true And Folder.Id IN :FolderIds ORDER BY Folder.Name, Name limit 1]; 
emailtemplateid = Templates.id;
*/
/*
EmailTemplate Templates = [Select Id, Name, IsActive From EmailTemplate  Where IsActive = true ORDER BY  Name limit 1]; 
emailtemplateid = Templates.id;
*/
id targetid = ct.id;

list<Messaging.EmailFileAttachment> fileAttachments = new list<Messaging.EmailFileAttachment>();

//CommonEmailUtils.sendEmailWithEmailTemplate(CCrecipients,null,targetid,null);
//CommonEmailUtils.sendEmailWithEmailTemplateAttachement(CCrecipients,null,targetid,null);
CommonEmailUtils.sendHTMLEmail(CCrecipients,'subject','body');
CommonEmailUtils.sendHTMLEmailWithAttachment(CCrecipients,'subject','body',null);
boolean useHTML = false;
CommonEmailUtils.sendEmail(recipients,CCrecipients,'subject','body',useHTML,null,targetid,null,null);

string attachment = 'name';

CommonEmailUtils.generateEmailFileAttachment(attachment,null);

Test.stoptest();
}
}