@IsTest
 public class NewOpportunityOverriddenControllerTest{
    static testMethod void CreateFulfillmentProfileTest() { 
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        Account objAccount = TestMethodsUtility.createAccount('cmr');        
        
        Contact objContact = TestMethodsUtility.createContact(objAccount.Id);
        
        NewOpportunityAutoPopulate__c objOppAuto = new NewOpportunityAutoPopulate__c();
        objOppAuto.name='opportunity';
        objOppAuto.Opportunity_Fields__c='opp3,CF00NG0000009H2Cs_lkid,CF00NG0000009H2Cs,CF00NZ0000000wOtB_lkid,CF00NZ0000000wOtB,CF00NG0000009H2Cv_lkid,CF00NG0000009H2Cv,00NG0000009H2Ct';
        insert objOppAuto;
        
        ApexPages.currentPage().getParameters().put('accid', objAccount.ID);        
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(new Opportunity()); 
        NewOpportunityOverriddenController overiddenOpportunity = new NewOpportunityOverriddenController(controller);
        Test.startTest();
       // Test.setCurrentPage(overiddenOpportunity.onLoad());
        overiddenOpportunity.onLoad();
        ApexPages.currentPage().getParameters().put('accid', null);
        controller = new Apexpages.Standardcontroller(new Opportunity());
        Test.setCurrentPage(overiddenOpportunity.onLoad()); 
        Test.stopTest();
    }
}