@isTest(seeAllData=true)
public with sharing class Email2Case_SOAP_WebserviceTest {

    public static testMethod void e2C () {
    
    test.startTest();

    Canvass__c c = CommonUtility.createCanvasTest();
    insert c;

    Account a = CommonUtility.createAccountTest(c);
    insert a;

    Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
    insert dff;

    Case cs = CommonUtility.createCaseTest(a);
    insert cs;
            
        Email2Case_SOAP_Webservice.createTalusCase('TeamTrackCase', 'TestNotes', String.valueof(dff.Id), '1234', '1234');
        Email2Case_SOAP_Webservice.reOpenCase('TeamTrackCase', 'TestNotes', cs.Id, 'TestCaseReason');
    
    test.stopTest();    
    
    }

}