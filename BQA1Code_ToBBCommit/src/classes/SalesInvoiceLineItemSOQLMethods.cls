public with sharing class SalesInvoiceLineItemSOQLMethods {
    
    public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItemByOLI(set<Id> setOLI) {
        return [Select c2g__UnitPrice__c,Billing_Frequency__c,Successful_Payments__c,Payments_Remaining__c , c2g__Quantity__c, 
            c2g__Product__c, c2g__NetValue__c, c2g__LineNumber__c, c2g__Invoice__r.Customer_Name__c, c2g__LineDescription__c, 
            c2g__Invoice__r.c2g__NetTotal__c,c2g__Invoice__r.Service_End_Date__c,c2g__Invoice__r.Service_Start_Date__c,c2g__Invoice__r.Telco_Reversal__c,c2g__Invoice__r.c2g__Transaction__c, c2g__Invoice__r.c2g__PaymentStatus__c, c2g__Invoice__r.c2g__Opportunity__c,   
            c2g__Invoice__r.c2g__NumberOfPayments__c, c2g__Invoice__r.c2g__InvoiceTotal__c, c2g__Invoice__r.c2g__InvoiceStatus__c, c2g__Invoice__r.c2g__Period__c,
            c2g__Invoice__r.c2g__InvoiceCurrency__c, c2g__Invoice__r.c2g__InvoiceDate__c,c2g__Invoice__r.c2g__DueDate__c, 
            c2g__Invoice__r.c2g__Account__c,c2g__Invoice__r.c2g__Account__r.name, c2g__Invoice__r.Name, c2g__Invoice__c, 
            c2g__Invoice__r.SI_Payments_Remaining__c,c2g__Invoice__r.SI_Successful_Payments__c, Order_Line_Item__r.CMR_Name__c,
            c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,c2g__Dimension4__c, Order_Line_Item__r.Product2__r.RGU__c, Order_Line_Item__r.Directory_Code__c, 
            Order_Line_Item__r.Edition_Code__c, Order_Line_Item__r.Canvass__r.Canvass_Code__c,
            Order_Line_Item__c, Name, Id, Order_Line_Item__r.UnitPrice__c, Order_Line_Item__r.Talus_Go_Live_Date__c, Order_Line_Item__r.ProductCode__c, 
            Directory__c, Directory_Edition__c,Directory_Edition__r.name, c2g__Invoice__r.Total_Month_Invoice__c, Order_Line_Item__r.Billing_Partner__c, Order_Line_Item__r.Is_P4P__c, Order_Line_Item__r.P4P_Billing__c,
            Order_Line_Item__r.Payments_Remaining__c,c2g__Invoice__r.CreatedDate,c2g__Invoice__r.c2g__InvoiceDescription__c, Order_Line_Item__r.Canvass__c, Order_Line_Item__r.Name, 
            Order_Line_Item__r.Successful_Payments__c,Order_Line_Item__r.Directory__c,order_line_item__r.Package_ID__c,Order_Line_Item__r.Directory_Edition__c,Order_Line_Item__r.Next_Billing_Date__c, 
            Total_Credit_Amount__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c,Order_Line_Item__r.Directory_Edition__r.Book_Status__c,Order_Line_Item__r.Media_Type__c
            From c2g__codaInvoiceLineItem__c where Order_Line_Item__c IN :setOLI AND OffSet__c=false AND Is_Manually_Created__c = false AND
            (c2g__Invoice__r.c2g__InvoiceStatus__c = 'In Progress' OR c2g__Invoice__r.c2g__InvoiceStatus__c = 'Complete') AND c2g__Invoice__r.Telco_Reversal__c=false order by c2g__Invoice__r.c2g__InvoiceDate__c ASC] ;   
            //From c2g__codaInvoiceLineItem__c where Order_Line_Item__c IN :setOLI and c2g__Invoice__r.c2g__PaymentStatus__c = 'Unpaid' and c2g__Invoice__r.c2g__InvoiceStatus__c = 'Reversed' order by Order_Line_Item__c];
    }

    public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItemByOLI(set<Id> setOLI, string strPartnerAccountId, String strFrequency) {
        set<String> strStatus = new set<String>{'In Progress','Complete'};
        String strQuery = 'Select c2g__UnitPrice__c,Billing_Frequency__c,Successful_Payments__c,Payments_Remaining__c , c2g__Quantity__c, c2g__Product__c, c2g__NetValue__c, c2g__LineNumber__c, '+
            'c2g__Invoice__r.Customer_Name__c, c2g__LineDescription__c, c2g__Invoice__r.c2g__NetTotal__c, c2g__Invoice__r.Service_End_Date__c,c2g__Invoice__r.Service_Start_Date__c, '+
            'c2g__Invoice__r.Telco_Reversal__c,c2g__Invoice__r.c2g__Transaction__c, c2g__Invoice__r.c2g__PaymentStatus__c, c2g__Invoice__r.c2g__Opportunity__c, c2g__Invoice__c, '+
            'c2g__Invoice__r.c2g__NumberOfPayments__c, c2g__Invoice__r.c2g__InvoiceTotal__c, c2g__Invoice__r.c2g__InvoiceStatus__c, c2g__Invoice__r.c2g__Period__c, c2g__Invoice__r.Name, '+
            'c2g__Invoice__r.c2g__InvoiceCurrency__c, c2g__Invoice__r.c2g__InvoiceDate__c,c2g__Invoice__r.c2g__DueDate__c, c2g__Invoice__r.c2g__Account__c, c2g__Invoice__r.c2g__Account__r.name, '+ 
            'c2g__Invoice__r.SI_Payments_Remaining__c,c2g__Invoice__r.SI_Successful_Payments__c, Order_Line_Item__r.CMR_Name__c, c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c, '+
            'c2g__Dimension4__c, Order_Line_Item__r.Product2__r.RGU__c, Order_Line_Item__r.Directory_Code__c, Order_Line_Item__r.Edition_Code__c, Order_Line_Item__r.Canvass__r.Canvass_Code__c, '+
            'Order_Line_Item__c, Name, Id, Order_Line_Item__r.UnitPrice__c, Order_Line_Item__r.Talus_Go_Live_Date__c, Order_Line_Item__r.ProductCode__c, Directory__c, '+
            'Directory_Edition__c,Directory_Edition__r.name, c2g__Invoice__r.Total_Month_Invoice__c, Order_Line_Item__r.Billing_Partner__c, Order_Line_Item__r.Is_P4P__c, '+
            'Order_Line_Item__r.P4P_Billing__c, Order_Line_Item__r.Payments_Remaining__c,c2g__Invoice__r.CreatedDate,c2g__Invoice__r.c2g__InvoiceDescription__c, Order_Line_Item__r.Canvass__c, '+ 
            'Order_Line_Item__r.Name, Order_Line_Item__r.Billing_Frequency__c, Order_Line_Item__r.Successful_Payments__c,Order_Line_Item__r.Directory__c,order_line_item__r.Package_ID__c,Order_Line_Item__r.Directory_Edition__c, '+
            'Order_Line_Item__r.Next_Billing_Date__c, Total_Credit_Amount__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c, '+
            'Order_Line_Item__r.Directory_Edition__r.Book_Status__c,Order_Line_Item__r.Media_Type__c From c2g__codaInvoiceLineItem__c '+
            'Where Order_Line_Item__c IN :setOLI AND OffSet__c=false AND Is_Manually_Created__c = false AND c2g__Invoice__r.c2g__InvoiceStatus__c IN:strStatus AND c2g__Invoice__r.Telco_Reversal__c=false';
        if(String.isNotEmpty(strPartnerAccountId)) {
            strQuery += ' AND c2g__Invoice__r.c2g__Account__c =:strPartnerAccountId';
        }
        if(String.isNotEmpty(strFrequency)) {
            strFrequency = strFrequency == 'single' ? 'Single Payment' : 'Monthly';
            strQuery += ' AND Billing_Frequency__c =:strFrequency';
        }
        strQuery += ' order by c2g__Invoice__r.c2g__InvoiceDate__c ASC';
        return Database.query(strQuery);
    }
    
    public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItembyDocNumber(set<string> setDocNumber) {
    	set<String> strStatus = new set<String>{'In Progress','Complete'};
    	set<String> strTransactionType = new set<String>{'I - Invoice','WR - Recovery','MW - Migration Write-off Invoice','FC - Frequency Change','F - Fee','TD - Billing Transfer Invoice','BB - Bulk Billing'};
    	return [Select Id,Name,c2g__Invoice__c,c2g__Invoice__r.Name,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__Invoice__r.Transaction_Type__c,c2g__Invoice__r.c2g__InvoiceStatus__c,c2g__Invoice__r.c2g__NetTotal__c,c2g__NetValue__c 
    			from c2g__codaInvoiceLineItem__c where c2g__Invoice__r.Name IN : setDocNumber AND c2g__Invoice__r.c2g__InvoiceStatus__c IN : strStatus AND c2g__Invoice__r.Transaction_Type__c IN : strTransactionType
    			ORDER BY  c2g__Invoice__r.c2g__InvoiceDate__c ASC limit 1000];
    }
    
    public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItemById(set<Id> setSILIId) {
    	return [SELECT Successful_Payments__c, ADJ_Amt__c, Billing_Frequency__c, Billing_Partner__c, CMR_Commission__c, CMR_Commission_Amount_CR__c, CMR_Commission_Value__c, ffbilling__CalculateIncomeSchedule__c, ffbilling__CalculateTaxValue2FromRate__c, ffbilling__CalculateTaxValue3FromRate__c, 
    	ffbilling__CalculateTaxValue1FromRate__c, Cancellation_Reason__c, Is_Cancelled__c, Comm_Amt__c, c2g__OwnerCompany__c, CreatedById, CreatedDate, IsDeleted, c2g__Scheduled__c, ffbilling__DeriveLineNumber__c, ffbilling__DeriveTaxRate2FromCode__c, ffbilling__DeriveTaxRate3FromCode__c, ffbilling__DeriveTaxRate1FromCode__c, 
    	ffbilling__DeriveUnitPriceFromProduct__c, c2g__Dimension1__c, c2g__Dimension2__c, c2g__Dimension3__c, c2g__Dimension4__c, Directory__c, Directory_Edition__c, ffps_berry__DisableCustomTaxOverride__c, Discount_Dollars__c, Edition_Code__c, c2g__ExternalId__c, From_SCN__c, c2g__GeneralLedgerAccount__c, Gross_Amt__c, 
    	c2g__IncomeScheduleGroup__c, Hawaii_Island__c, ISS_Line_Item__c, c2g__IncomeSchedule__c, ffbilling__InternalCalculateIS__c, c2g__Invoice__c, Invoice_Reason__c, Is_Manually_Created__c, LastModifiedById, LastModifiedDate, c2g__LineDescription__c, c2g__LineNumber__c, Migrated_Edition__c, SIL_NAT_Net_Amt__c, c2g__NetValue__c, 
    	c2g__NumberofJournals__c, OLI_Line_Number__c, OLI_Product_Name__c, Offset__c, OpCo__c, Order_Line_Item__c, P4P_Current_Billing_Period_Clicks_Leads__c, P4P_Invoice_Line_Item__c, Payments_Remaining__c, c2g__PeriodInterval__c, Previous_URL__c, Previous_URN_Number__c, c2g__Product__c, c2g__Quantity__c, Id, SCN_AutoNum__c, 
    	SILI_OLI_Price_Difference__c, Name, ffbilling__ScheduleNetTotal__c, ffbilling__SetTaxCode1ToDefault__c, ffbilling__SetTaxCode2ToDefault__c, ffbilling__SetTaxCode3ToDefault__c, c2g__StartDate__c, Is_Statement_Generated__c, SystemModstamp, c2g__TaxCode1__c, c2g__TaxCode2__c, c2g__TaxCode3__c, c2g__TaxRate1__c, 
    	c2g__TaxRate2__c, c2g__TaxRate3__c, Tax_Rate_City__c, Tax_Rate_County__c, Tax_Rate_Mapped__c, Tax_Rate_Mapped_2__c, Tax_Rate_Mapped_3__c, Tax_Rate_State__c, c2g__TaxRateTotal__c, c2g__TaxValue1__c, c2g__TaxValue2__c, c2g__TaxValue3__c, c2g__TaxValueTotal__c, Total_Credit_Amount__c, c2g__UnitPrice__c, c2g__UnitOfWork__c, 
    	c2g__UsePartPeriods__c, ffbilling__UseProductInformation__c, Write_Off__c FROM c2g__codaInvoiceLineItem__c where Id IN : setSILIId];
    }
    public static list<c2g__codaInvoiceLineItem__c> getInvoiceLineItembyDocNumInvoiceDate(set<string> setDocNumber,Date InvoiceStDate,Date InvoiceEdDate) {
    	set<String> strStatus = new set<String>{'In Progress','Complete'};
    	set<String> strTransactionType = new set<String>{'I - Invoice','WR - Recovery','MW - Migration Write-off Invoice'};
    	return [Select Id,Name,c2g__Invoice__c,c2g__Invoice__r.Name,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__Invoice__r.Transaction_Type__c,c2g__Invoice__r.c2g__InvoiceStatus__c,c2g__Invoice__r.c2g__NetTotal__c,c2g__NetValue__c 
    			from c2g__codaInvoiceLineItem__c where c2g__Invoice__r.Name IN : setDocNumber AND (c2g__Invoice__r.c2g__InvoiceDate__c >: InvoiceStDate AND c2g__Invoice__r.c2g__InvoiceDate__c <:InvoiceEdDate) AND 
    			c2g__Invoice__r.c2g__InvoiceStatus__c IN : strStatus AND c2g__Invoice__r.Transaction_Type__c IN : strTransactionType
    			ORDER BY  c2g__Invoice__r.c2g__InvoiceDate__c ASC limit 1000];
    }
}