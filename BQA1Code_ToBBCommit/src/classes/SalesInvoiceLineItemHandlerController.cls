public with sharing class SalesInvoiceLineItemHandlerController 
{
    public static void onAfterInsert(list<c2g__codaInvoiceLineItem__c> lstSILI) 
    {
        List<order_line_Items__c> OLIForUpdate=new List<order_line_Items__c>();
        Map<Id,Order_Line_Items__c> oliMap=new Map<Id,Order_Line_Items__c>();
        for(c2g__codaInvoiceLineItem__c SILIIterator:[Select c2g__UnitPrice__c,From_SCN__c, c2g__Quantity__c, c2g__Product__c, c2g__NetValue__c, c2g__LineNumber__c, c2g__Invoice__r.Customer_Name__c, c2g__LineDescription__c, 
        c2g__Invoice__r.c2g__NetTotal__c, c2g__Invoice__r.c2g__Transaction__c, c2g__Invoice__r.c2g__PaymentStatus__c, c2g__Invoice__r.c2g__Opportunity__c,   
        c2g__Invoice__r.c2g__NumberOfPayments__c, c2g__Invoice__r.c2g__InvoiceTotal__c, c2g__Invoice__r.c2g__InvoiceStatus__c, c2g__Invoice__r.Transaction_Type__c,
        c2g__Invoice__r.c2g__InvoiceCurrency__c, c2g__Invoice__r.c2g__InvoiceDate__c, c2g__Invoice__r.c2g__Account__c, c2g__Invoice__r.Name, c2g__Invoice__c,  
        Order_Line_Item__c, Name, Id, Order_Line_Item__r.Current_Invoice__c, Order_Line_Item__r.Last_Invoice__c,Order_Line_Item__r.Name, c2g__Invoice__r.Is_Manually_Created__c 
        From c2g__codaInvoiceLineItem__c where ID IN  : lstSILI AND c2g__Invoice__r.Is_Manually_Created__c=false])
        {
            if(!SILIIterator.From_SCN__c && SILIIterator.Order_Line_Item__c != null)
            {
                    Order_Line_Items__c obj=new Order_Line_Items__c(Id=SILIIterator.Order_Line_Item__c );
                    if(SILIIterator.Order_Line_Item__r.Current_Invoice__c!=null)
                          obj.Last_Invoice__c=SILIIterator.Order_Line_Item__r.Current_Invoice__c;
                          obj.Current_Invoice__c=SILIIterator.c2g__NetValue__c;
                    oliMap.put(SILIIterator.Order_Line_Item__c,obj);  
            }
        }
        System.debug('############OLIForUpdate '+OLIForUpdate );
        System.debug('############oliMap '+oliMap);
        if(oliMap.size()>0)
            update oliMap.values();
    }
}