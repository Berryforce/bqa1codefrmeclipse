public with sharing class TestController {

    //Getter and Setters
    public static List < Package_ID__c > lstPkgs {
        get;
        set;
    }
    public static Set < Id > setPrdtIds = new Set < Id > ();
    public static List < Package_Product__c > lstPkgPrdts {
        get;
        set;
    }
    public static List < Product_Properties__c > lstPkgPrpts {
        get;
        set;
    }
    public static Map < String, List < Product_Properties__c >> mapPkgPrpts {
        get;
        set;
    }
    public static List < String > lstPickLst {
        get;
        set;
    }

    public TestController() {

    }

    public static void allPkgs() {

        lstPkgs = [SELECT Id, Name, UDAC_Package_ID__c, Product_Information__c, Description__c, (SELECT Id, Name, Product_Code__c, Product_Name__c, Properties_Count__c from Package_Products__r) from Package_ID__c];

        //lstPkgPrdts = [SELECT Id, Product_Name__c, (SELECT Id, Name, Name__c, Type__c from Package_Product__c.Product_Properties__r) from Package_Product__c];
        
    }
}