global with sharing class bSure_ManagePaymentPlan implements Database.Batchable<sObject>
{
    global string strConvertsId15;   
    global date sdate; 
    global date edate;
       global Database.QueryLocator start(Database.BatchableContext BC)
    {
            sdate = system.today()-1;
            edate = system.today()-7;
            String query ='';
            System.debug('Test==============');
            query='SELECT Id,Vertex_Berry__Amount_Due__c,Vertex_Berry__Amount_to_be_paid__c, '+
                      ' Vertex_Berry__Customer_Name__c,Vertex_Berry__Customer_Name__r.Vertex_Berry__ExternalId__c,Vertex_Berry__Customer_Name_F__c, '+
                      ' Vertex_Berry__Financial_Fee__c,Vertex_Berry__Financial_Fees__c,Vertex_Berry__Payment_Date__c,Vertex_Berry__Payment_Plan_ID__c, '+
                      ' Vertex_Berry__Status_c__c,Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Total_Paid_Amount__c,Vertex_Berry__isUpdate__c, '+
                      ' Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Amount_Paid__c,Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Monthly_EMI__c, '+
                      ' Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Status__c from Vertex_Berry__Bsure_Payment_Schedules__c '+
                      ' WHERE Vertex_Berry__Payment_Date__c >=: edate and  Vertex_Berry__Payment_Date__c <=: sdate '+
                      ' AND Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Status__c=\'Active\' '+
                      ' AND Vertex_Berry__Payment_Plan_ID__r.Vertex_Berry__Payment_Plan_Status__c=\'Active\' AND Vertex_Berry__isUpdate__c = false '; 
           System.debug('query==========='+query);
           return Database.getQueryLocator(query);            
    }
    global void execute(Database.BatchableContext BC, List<Vertex_Berry__Bsure_Payment_Schedules__c> scope)
    {
       sdate = system.today()-1;
       string strActive = 'Active';
       system.debug('scope==========='+scope);
       system.debug('scope=====size======'+scope.size());
       set<String> setaccountids = new set<String>();
       map<String,Date> mapduedate = new map<String,Date>();
       //payment plan schedule
       map<string,Vertex_Berry__Bsure_Payment_Schedules__c> mapPymntschedule =new map<string,Vertex_Berry__Bsure_Payment_Schedules__c>();
       map<String,Vertex_Berry__BSure_Payment_Plan__c> mappaymentplan = new map<String,Vertex_Berry__BSure_Payment_Plan__c>();
       map<string,pymt__PaymentX__c> mappayemtx = new map<string,pymt__PaymentX__c>();
       list<Vertex_Berry__BSure_Payment_Plan__c> listPayPlan = new list<Vertex_Berry__BSure_Payment_Plan__c>();
       set<String> setpplnids = new set<String>();
       map<String,Decimal> mapEmivalue = new map<String,Decimal>();
       if(scope != null && scope.size() > 0){ 
              for(Vertex_Berry__Bsure_Payment_Schedules__c pschl : scope){
              		
                     system.debug('ExternalId============='+pschl.Vertex_Berry__Customer_Name__r.Vertex_Berry__ExternalId__c);
                     if(pschl.Vertex_Berry__Customer_Name__r.Vertex_Berry__ExternalId__c != null && pschl.Vertex_Berry__Customer_Name__r.Vertex_Berry__ExternalId__c != ''){
                           
                           String strAccid = string.valueof(pschl.Vertex_Berry__Customer_Name__r.Vertex_Berry__ExternalId__c);
                           setaccountids.add(strAccid.subString(0,15));
                           mapPymntschedule.put(strAccid.subString(0,15),pschl);
                           String pplanId = String.valueOf(pschl.Vertex_Berry__Payment_Plan_ID__c);
                           setpplnids.add(pplanId.subString(0,15));
                           
                           mapduedate.put(strAccid.subString(0,15),pschl.Vertex_Berry__Payment_Date__c);
                           decimal dpaymentvalue = pschl.Vertex_Berry__Amount_to_be_paid__c;
                           mapEmivalue.put(strAccid.subString(0,15),dpaymentvalue.setScale(2));
                     }
                     system.debug('setaccountids=======123========='+setaccountids);      
              }
       }
       system.debug('mapPymntschedule====keys==='+mapPymntschedule.keyset()); 
       system.debug('setaccountids============='+setaccountids);
       system.debug('setpplnids============'+setpplnids);
       //payment plan
       list<Vertex_Berry__BSure_Payment_Plan__c> lstpaymentplan = new list<Vertex_Berry__BSure_Payment_Plan__c>();
       String strpayemtplnquery = ' SELECT Id,Name,Vertex_Berry__Total_Paid_Amount__c,Vertex_Berry__Amount_Paid__c,'+
                                   ' Vertex_Berry__Status__c,Vertex_Berry__Monthly_EMI__c,Vertex_Berry__External_ID__c, '+
                                   ' Vertex_Berry__Amount_Due__c FROM Vertex_Berry__BSure_Payment_Plan__c '+
                                   ' WHERE Vertex_Berry__External_ID__c != null and Id IN: setpplnids   AND Vertex_Berry__Status__c =: strActive ';                                  
        //Vertex_Berry__External_ID__c IN: setaccountids
        system.debug('strpayemtplnquery=============='+strpayemtplnquery); 
        lstpaymentplan = Database.query(strpayemtplnquery);
        system.debug('lstpaymentplan=============='+lstpaymentplan);
        system.debug('lstpaymentplan====size=========='+lstpaymentplan.size()); 
        set<String> setIds = new set<String>(); 
        if(lstpaymentplan != null && lstpaymentplan.size() > 0){
              for(Vertex_Berry__BSure_Payment_Plan__c pplanobj : lstpaymentplan){
                     //String pplnid = pplanobj.Id;
                     //if(setpplnids.contains(pplnid.subString(0,15))){
                           if(pplanobj.Vertex_Berry__External_ID__c != null && pplanobj.Vertex_Berry__External_ID__c != ''){
                                  String strAccs = String.valueof(pplanobj.Vertex_Berry__External_ID__c);
                                  mappaymentplan.put(strAccs.subString(0,15) ,pplanobj);
                                  setIds.add(strAccs.subString(0,15));
                            }
                     //}
              } 
        }
        system.debug('mappaymentplan============='+mappaymentplan);
        //payment sales      
        //date startDate = Date.newInstance(sdate.year(),sdate.month(), 1);     
              Integer numberOfDays = Date.daysInMonth(sdate.year(), sdate.month());
              //Date enddate = Date.newInstance(sdate.year(), sdate.month(), numberOfDays);
              Date startDate = System.today().addDays(-40);
              Date enddate = System.today();
              system.debug('setaccountids-----------'+setaccountids);
              system.debug('startDate============'+startDate);
              system.debug('enddate=========='+enddate);  
              
              map<String,list<pymt__PaymentX__c>> mappaymentx = new map<String,list<pymt__PaymentX__c>>(); //new
              map<String,decimal> mapInteger = new map<String,decimal>();
              
              
              list<pymt__PaymentX__c> lstpaymentx = new list<pymt__PaymentX__c>();
              String strPaymentX = 'SELECT Id,pymt__Account__c,pymt__Amount__c,pymt__Date__c  FROM pymt__PaymentX__c where pymt__Amount__c != null and pymt__Date__c >=: startDate '+
                                                 ' and pymt__Date__c  <=: enddate and pymt__Account__c in: setaccountids ';
              system.debug('strPaymentX============='+strPaymentX);
              lstpaymentx = Database.query(strPaymentX);             
              
              if(lstpaymentx != null && lstpaymentx.size() > 0){
                     for(pymt__PaymentX__c payobj : lstpaymentx){
                     	   String accId = payobj.pymt__Account__c;
                     	   decimal decval = null;
                     	   //setIds.add(accId.subString(0,15));
                     	   Date paymentdays = mapduedate.get(accId.subString(0,15)); 	
                     	   Date paymentpast30  = paymentdays.addDays(-30);
                     	   //Date paymentpast1 = paymentdays.addDays(-1);
                     	   
                     	   System.debug('System.today()==========='+System.today());
                     	   System.debug('paydate============'+paymentdays);
                     	   System.debug('paymentpast30======='+paymentpast30);
                     	   System.debug('payobj.pymt__Date__c============='+payobj.pymt__Date__c);
                     	   
                     	   if(payobj.pymt__Date__c >  paymentpast30 && payobj.pymt__Date__c <= paymentdays ){
                     	   	  
                     	   		   String straccid = payobj.pymt__Account__c;
		                           if(mapInteger.containskey(straccid.subString(0,15))){
		                                  if(payobj.pymt__Amount__c != null){
		                                         decval = mapInteger.get(straccid.subString(0,15)) + payobj.pymt__Amount__c ;
		                                         mapInteger.put(straccid.subString(0,15), decval.setScale(2));
		                                  }      
		                           }
		                           else{
		                                  if(payobj.pymt__Amount__c != null){
		                                         decval = payobj.pymt__Amount__c;
		                                         mapInteger.put(straccid.subString(0,15), decval.setScale(2));
		                                  }
		                           }
                     	   }
                           
                     }
              }
              System.debug('mapInteger=============='+mapInteger);
              System.debug('mapInteger====**========='+mapInteger);
              //System.debug('mapInteger===**===new===='+mapInteger.get('001Z000000i0DkG'));
              
              list<Vertex_Berry__BSure_Payment_Plan__c> lstobjPyPlan = new list<Vertex_Berry__BSure_Payment_Plan__c>();
              for(String sid :mappaymentplan.keyset()){
              	Vertex_Berry__BSure_Payment_Plan__c objPyPlan = new Vertex_Berry__BSure_Payment_Plan__c();
              	objPyPlan  = mappaymentplan.get(sid);
              	if(mapInteger.containsKey(sid))
              	{
	              		if(mapInteger.get(sid) != null)
	              		{
	              			   if(objPyPlan.Vertex_Berry__Amount_Paid__c == null){
	              			   		objPyPlan.Vertex_Berry__Amount_Paid__c = 0;
	              			   }
	              			   if(objPyPlan.Vertex_Berry__Total_Paid_Amount__c == null){
	              			   		objPyPlan.Vertex_Berry__Total_Paid_Amount__c = 0;
	              			   }
	                           objPyPlan.Vertex_Berry__Amount_Paid__c = mapInteger.get(sid);
	                           objPyPlan.Vertex_Berry__Total_Paid_Amount__c += mapInteger.get(sid);
	                    }      
                        if(objPyPlan.Vertex_Berry__Amount_Paid__c != null)
                        {
                        	if((objPyPlan.Vertex_Berry__Total_Paid_Amount__c != null && objPyPlan.Vertex_Berry__Amount_Due__c != null ) && objPyPlan.Vertex_Berry__Total_Paid_Amount__c >= objPyPlan.Vertex_Berry__Amount_Due__c)
                            {
                                system.debug('objPyPlan.Vertex_Berry__Amount_Due__c==========='+objPyPlan.Vertex_Berry__Amount_Due__c);
                                objPyPlan.Vertex_Berry__Status__c =  'Completed';
                            }
                            else
                            {
                                system.debug('mapEmivalue.get(sid):::::::'+mapEmivalue.get(sid)+'AmountPaid:::::'+objPyPlan.Vertex_Berry__Amount_Paid__c);
                                if( (objPyPlan.Vertex_Berry__Amount_Paid__c != null && mapEmivalue.get(sid) != null) && mapEmivalue.get(sid) > objPyPlan.Vertex_Berry__Amount_Paid__c)
                                {
                                    objPyPlan.Vertex_Berry__Status__c =  'Delinquent';
                                    system.debug('Delinquent'+objPyPlan.Vertex_Berry__Status__c);
                                }
                                else
                                {
                                       system.debug('objPyPlan.Vertex_Berry__Status__c================'+objPyPlan.Vertex_Berry__Status__c);
                                    objPyPlan.Vertex_Berry__Status__c =  'Active';
                                }   
                            }
                        }
                		else
                		{
                			objPyPlan.Vertex_Berry__Amount_Paid__c = 0;   
                		}
              	}
              	else
              	{
              		if(objPyPlan.Vertex_Berry__Total_Paid_Amount__c  == null){
              			objPyPlan.Vertex_Berry__Total_Paid_Amount__c = 0;
              		}
              		objPyPlan.Vertex_Berry__Amount_Paid__c =0;
                	objPyPlan.Vertex_Berry__Status__c =  'Delinquent';
              	}
              	if(objPyPlan != null){
  					lstobjPyPlan.add(objPyPlan);
  				}
              }
              if(lstobjPyPlan != null && lstobjPyPlan.size() > 0){
	      			Database.SaveResult[] dbstatus = Database.update(lstobjPyPlan, false);
	      	  }
             
              if(setIds != null && setIds.size() > 0){
  				list<Vertex_Berry__Bsure_Payment_Schedules__c> listpplanshobj = new list<Vertex_Berry__Bsure_Payment_Schedules__c>();
  				for(String schobj: setIds){
  					Vertex_Berry__Bsure_Payment_Schedules__c pplanshobj = new Vertex_Berry__Bsure_Payment_Schedules__c();
  					 if(mapPymntschedule.containskey(schobj)){
                             pplanshobj = mapPymntschedule.get(schobj);
                             pplanshobj.Vertex_Berry__isUpdate__c = true;
                      }
                      if(pplanshobj != null){
                              listpplanshobj.add(pplanshobj); 
                      }
  				}
  				if(listpplanshobj != null && listpplanshobj.size() > 0){
  					update listpplanshobj;
  				}
  			}
              
              /*
              if(listPayPlan != null && listPayPlan.size() > 0){
                     list<Vertex_Berry__Bsure_Payment_Schedules__c> listpplanshobj = new list<Vertex_Berry__Bsure_Payment_Schedules__c>();
                     for(Vertex_Berry__BSure_Payment_Plan__c pplnobj : listPayPlan){
                           
                           Vertex_Berry__Bsure_Payment_Schedules__c pplanshobj = new Vertex_Berry__Bsure_Payment_Schedules__c();
                           if(pplnobj.Vertex_Berry__External_ID__c != null){
                                  String strrec = pplnobj.Vertex_Berry__External_ID__c;
                                  if(mapPymntschedule.containskey(strrec.subString(0,15))){
                                         pplanshobj = mapPymntschedule.get(strrec.subString(0,15));
                                  }
                                  pplanshobj.Vertex_Berry__isUpdate__c = true;
                           }
                           system.debug('pplanshobj================'+pplanshobj);
                           if(pplanshobj != null){
                                  listpplanshobj.add(pplanshobj); 
                           }
                     }
                     update listPayPlan;
                     if(listpplanshobj != null && listpplanshobj.size() > 0){
                           update listpplanshobj;
                     }
              }*/
                                                       
    }       
    global void finish(Database.BatchableContext BC)
    {
       
    }     
}