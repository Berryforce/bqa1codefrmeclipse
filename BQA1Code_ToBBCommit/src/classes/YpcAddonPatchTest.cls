@isTest(seeAllData=true)
public with sharing class YpcAddonPatchTest {

    public static testMethod void YpcAddonPatchTest() {

        List<YPC_AddOn__c> lstYPCAddons = new List<YPC_AddOn__c>();

        Test.startTest();

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.UDAC__c = 'BRZ';
        dff.add_ons__c = 'CSTLK;YPCP';
        dff.udac_points__c = 'VCTLK';
        dff.Talus_Subscription_Id__c = '2525';
        dff.Account__c = a.Id;
        insert dff;

        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'CSTLK', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'CSTLK', 'Cancel'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'YPCP', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'YPCP', 'Cancel'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'VCTLK', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'RB5', 'New'));

        insert lstYPCAddons;

        String query = 'SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Account__r.TalusAccountId__c != null and Talus_Subscription_Id__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)';
        String sched = '0 00 00 * * ?'; //batch will run every night

        //ScheduleYpcAddonPatch SC = new ScheduleYpcAddonPatch();
        //String jobId = System.schedule('Ypc Addon Patch Job', sched, sc);

        
        List<Digital_Product_Requirement__c> lstDffs = [SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Account__r.TalusAccountId__c != null and Talus_Subscription_Id__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)];

        YpcAddonPatch objBatch = new YpcAddonPatch(query);
        ID batchprocessid = Database.executeBatch(objBatch);
        objBatch.execute(null, lstDffs);

        Test.stopTest();
    }

}