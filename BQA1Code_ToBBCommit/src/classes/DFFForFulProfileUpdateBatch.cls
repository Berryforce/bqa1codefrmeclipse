global class DFFForFulProfileUpdateBatch implements Database.Batchable<sObject>{
	
	 global Database.QueryLocator start(Database.BatchableContext bc) {
	 	String userId='005K0000002MSSp';
	 	String query='Select id,name,Account__c from Digital_Product_Requirement__c where CreatedById=:userId and Fulfillment_Profile__c=null';
	 	return Database.getQueryLocator(query);
	 }
	 
	 global void execute(Database.BatchableContext bc, List<Digital_Product_Requirement__c> lstDFF) {
	 	set<Id> accIdSet=new Set<Id>();
	 	map<id,Account> reqdAccMap=null;
	 	for(Digital_Product_Requirement__c dp:lstDFF){
	 		accIdSet.add(dp.Account__c);
	 	}
	 	if(accIdSet.size()>0){
	 		reqdAccMap=new Map<Id,Account>([select id, (select id from Fulfillment_Profiles__r) from Account where id IN :accIdSet]);
	 	}
	 	if(reqdAccMap!=null && reqdAccMap.size()>0){
	 		for(Digital_Product_Requirement__c dp:lstDFF){
	 			dp.Fulfillment_Profile__c=reqdAccMap.get(dp.Account__c).Fulfillment_Profiles__r[0].Id;
	 		}
	 	}
	 	update lstDFF;
	 }
	 
	 global void finish(Database.BatchableContext bc) {
        
    }
}