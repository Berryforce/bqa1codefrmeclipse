global class  BatchClassDataLoad implements Database.Batchable<sObject>{
    
    global BatchClassDataLoad(){
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select name, SL_Last_Name_Business_Name__c From Directory_Listing__c where SL_Last_Name_Business_Name__c = null order by Listing__c';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Directory_Listing__c> dirListingList) {
        Savepoint sp = Database.setSavepoint();
        try{
            if(dirListingList!=null && dirListingList.size()>0){
                
                //AppearanceCountSHM.IncreaseDecreaseAppearancecount(dirListingList,null);
                List<Directory_Listing__c> dirListingListUpdt = new List<Directory_Listing__c>();
                for(Directory_Listing__c dirList :dirListingList){
                    
                    dirList.SL_Last_Name_Business_Name__c = dirList.name;
                    /*if(dirList.Phone_Number__c != null && dirList.Directory__c != null && dirList.Directory_Section__c != null){                        
                        dirList.SL_StrPhoneDLDS__c = dirList.Phone_Number__c +''+String.valueOf(dirList.Directory__c).substring(0, 15)+''+String.valueOf(dirList.Directory_section__c).substring(0, 15);
                    }
                    //dirList.SL_strScopedlistingMatched__c = dirList.Directory__c+''+dirList.Directory_Section__c+''+dirList.Name+''+dirList.Phone_Number__c+''+dirList.Listing_City__c+''+dirList.Listing_PO_Box__c+''+dirList.Caption_Header__c+''+dirList.Caption_Member__c;
                    dirList.SL_strScopedlistingMatched__c = dirList.Directory__c+''+dirList.Directory_Section__c+''+dirList.Name+''+dirList.Phone_Number__c+''+ dirList.Listing_PO_Box__c+''+dirList.Caption_Header__c+''+dirList.Caption_Member__c;
                    */
                    dirListingListUpdt.add(dirList);
                }
                if(dirListingListUpdt.size() > 0) {
                    update dirListingListUpdt;
                }
            }
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of Scoped Listing name');
        } 
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Scoped Listing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}