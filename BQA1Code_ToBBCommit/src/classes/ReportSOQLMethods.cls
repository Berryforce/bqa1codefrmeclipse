public class ReportSOQLMethods {
    
    public static List<Report> getReportsByDevName(List<String> lstReportDevName) {
        return [SELECT DeveloperName,Id,Name FROM Report where DeveloperName in :lstReportDevName];
    }
}