global class BatchClassDataLoadTesting implements Database.Batchable<sObject>{
    
    //global string filtercondition;
    
    global BatchClassDataLoadTesting(){
        //filtercondition = condition;
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select Id from Order_Line_Items__c limit 1';
        return Database.getQueryLocator(query);
     }
     
     global void execute(Database.BatchableContext bc, List<Order_Group__c> lstOS) {
        
     } 
      
    global void finish(Database.BatchableContext bc) {
        }
}