@isTest
public with sharing class Reference_HeadingTest {
	static testMethod void refHeadTest(){
		Test.startTest();
		List<Directory_Heading__c> dirHeadingList = new List<Directory_Heading__c>();
		Directory_Heading__c sourceDirHeading = TestMethodsUtility.generateDirectoryHeading();
		dirHeadingList.add(sourceDirHeading);
		Directory_Heading__c crossRefDirHeading = TestMethodsUtility.generateDirectoryHeading();
		dirHeadingList.add(crossRefDirHeading);
		insert dirHeadingList;
		Cross_Reference_Headings__c crossRefHeading = TestMethodsUtility.createCrossRefHeading(sourceDirHeading, crossRefDirHeading);
		Reference_Heading RH = new Reference_Heading(new ApexPages.StandardController(sourceDirHeading));
		RH.delID = crossRefHeading.Id;
		RH.onLoad();
		RH.deleteCross();
		Test.stopTest();
	}
}