@isTest
/*
    This is the test class for the controller PortalMessageController of
    MyMessages and Message Centre pages
*/
private class TestPortalMessageController 
{
  public static testMethod void testPortalBroadcastMessages()
  {
    
    Portal_Message__c  PMobj=new Portal_Message__c();
    
     PMobj.Portal_User__c=UserInfo.getUserId();
     PMobj.Message_Spanish__c='test';
     PMobj.Message_English__c='teset';  
     PMobj.Apply_to_the_following_portals__c='null';
     PMobj.Publish_Date__c= Date.Today().addDays(-2);
     PMobj.End_Date__c= Date.Today().addDays(+2);
     insert PMobj;                                            
    
    
    PortalMessageController pmc = new PortalMessageController();
    
    pmc.getUserDetails();
    pmc.getAllBroadcastMessages();
    pmc.getIndividualMessages();
    pmc.getEnglishIndiMessage();
    pmc.getSpanishIndiMessage();
    
  }
}