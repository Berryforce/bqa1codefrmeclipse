global class GalleyReportStageDeleteBatch Implements Database.Batchable <sObject> {
	global Database.queryLocator start(Database.BatchableContext bc){
		String SOQL = 'SELECT Id FROM Galley_Report_Stage__c';
		return Database.getQueryLocator(SOQL);
	}

	global void execute(Database.BatchableContext bc, list<Galley_Report_Stage__c> listGalleyReportStage) {
		delete listGalleyReportStage;
		Database.emptyRecycleBin(listGalleyReportStage);
	}

	global void finish(Database.BatchableContext bc) {
		GalleyReportHeaderDeleteBatch obj = new GalleyReportHeaderDeleteBatch(); 
        Database.executeBatch(obj); 
	}
}