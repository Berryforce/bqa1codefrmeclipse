Public class DFFNotification{
    public static void DFFRemainder(List<Digital_Product_Requirement__c> lstDFF) {
        set<Id> objdffId = new set<id>();
        map<Id,list<Digital_Product_Requirement__c>> mapActDff = new map<Id,list<Digital_Product_Requirement__c>>();

        for(Digital_Product_Requirement__c DFF:lstDFF)
        {   
            if((DFF.Miles_Status__c=='New' || DFF.Miles_Status__c=='In Progress') && (DFF.RecordType.DeveloperName=='iYP_Bronze' || DFF.RecordType.DeveloperName=='iYP_Bronze_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Silver' || DFF.RecordType.DeveloperName=='iYP_Silver_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Gold' || DFF.RecordType.DeveloperName=='iYP_Gold_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Priority' || DFF.RecordType.DeveloperName=='iYP_Priority'))
            {
                objdffId.add(DFF.Id);
            }
            else
            if(DFF.Fulfillment_Submit_Status__c==Null)
            {
                objdffId.add(DFF.Id);
            }
        }
        system.debug('**********Scope Lst SET ids********'+objdffId);
        list<Digital_Product_Requirement__c> dffLst = new list<Digital_Product_Requirement__c>();
        if(objdffId.size()>0){
            dffLst=[select id,Account__r.Name ,UDAC__c,Account__r.owner.Email,DFFcloseddate__c,UnitPrice__c,Account__r.Account_Manager__r.Email from Digital_Product_Requirement__c where id IN:objdffId];
        }
        system.debug('**********Scope Lst dff lst *******'+dffLst);
        system.debug('**********Scope Lst dff lst *******'+dffLst.size());
        if(dffLst.size()>0){
            for(Digital_Product_Requirement__c objDff : dffLst)
            {
                if(objDff.Account__c != null){
                    if(!mapActDff.containsKey(objDff.Account__c)){
                        mapActDff.put(objDff.Account__c,new list<Digital_Product_Requirement__c>());
                    }
                    mapActDff.get(objDff.Account__c).add(objDff);
                }
            }
        }
        system.debug('**********Scope Lst dff map******'+mapActDff);
        system.debug('**********Scope Lst dff map******'+mapActDff.size());
        if(mapActDff.size()>0)
        {
            sendmail(mapActDff);
        }
    }
    
    public static void sendmail(map<Id,list<Digital_Product_Requirement__c>> mapDffEmail){
       
        List<Messaging.SingleEmailMessage> lstEmail= new list<Messaging.SingleEmailMessage>();
        BerryLogo__c berryLg = BerryLogo__c.getInstance('Berryurl');
        for(ID acctId : mapDffEmail.keyset()){
            if(mapDffEmail.get(acctId) != null){
                for(Digital_Product_Requirement__c objDFFEmail : mapDffEmail.get(acctId)){
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    string [] toaddress;
                    string [] ccAddresses;
                    system.debug('**********Scope account owner email********'+objDFFEmail.Account__r.Owner.Email);
                    system.debug('**********Scope account owner manager email********'+objDFFEmail.Account__r.Account_Manager__r.Email);
                    if(objDFFEmail.Account__r.Owner.Email!=Null){
                        toaddress= New string[]{objDFFEmail.Account__r.Owner.Email};
                    }
                    if(objDFFEmail.Account__r.Account_Manager__r.Email!=Null){
                        ccAddresses= New string[]{objDFFEmail.Account__r.Account_Manager__r.Email};
                    }
                    email.setToAddresses(toaddress);
                    email.setccAddresses(ccAddresses);
                    email.setWhatID(objDFFEmail.Id); 
                    email.setSubject('Reminder Notification for DFF ' );    
                    email.setBccSender(false);
                    email.setPlainTextBody('Message – Urgent: The completion of the fulfillment process for the above product requires your immediate attention.');
                    if(objDFFEmail.Account__r.Name != null && objDFFEmail.UDAC__c != null){
                    email.setHtmlBody('<html> <b><img width="120" src="\"'+berryLg.BerryURL__c+'\"> <br>  Message – Urgent:</b> The completion of the fulfillment process for the Below product requires your immediate attention.<br> <br> <b>Account Name</b>:' + objDFFEmail.Account__r.Name +' <br><br>  ' + '<b>Product Code  </b>:' + objDFFEmail.UDAC__c +' </b> <br><br> ' + '<b>Opportunity closed date </b>:' + objDFFEmail.DFFcloseddate__c +' </b>  <br>'  + '<br> <b> Sold Amount  </b>:' + objDFFEmail.UnitPrice__c +' </b> <br> <br> Please complete the fulfillment process for the above product. Another reminder will be sent 24 hours from today. <br> <img width="120" src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006QIz&oid=00DZ0000001C48o&lastMod=1384862875000"></html> ');
                    }
                    email.setSaveAsActivity(false);
                    lstEmail.add(email);
                }
            }
        }
        system.debug('********email list size*********'+lstEmail.size());
        Messaging.sendEmail(lstEmail);

    }
}