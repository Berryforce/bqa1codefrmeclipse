@isTest
private class ListingSOQLMethodsTest {
    static testMethod void DirectoryListingSOQLMethodsCoverage() {
        set<Id> setLstId = new set<Id>();
        set<string> setMatch= new set<string>{'TestMatching'};
        set<Id> setListRT = new set<Id>{system.label.TestListingMainRT};
        Listing__c objLst = TestMethodsUtility.generateMainListing();
        objLst.Lst_StrListingMatch__c='TestMatching';
        insert objLst;
        setLstId.add(objLst.id);
        test.starttest();
        ListingSOQLMethods.getNationalDisconnectedListing(setLstId);
        ListingSOQLMethods.fetchListing(setLstId);
        ListingSOQLMethods.fetchAdditionalListings(setLstId);
        ListingSOQLMethods.fetchListingforSync(setLstId);
        ListingSOQLMethods.fetchListingforSLDFFupdate(objLst.id);
        ListingSOQLMethods.getMatchedListings(setMatch,setListRT);
        test.stoptest();
    }
}