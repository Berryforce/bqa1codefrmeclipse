@IsTest
public class CreateNOSOListingfromLeadTestClass {
	private static Id LeadId;
	  
	static testmethod void CreateNOSOListingfromLead(){
		set<Id> setLeadId = new set<Id>();
		Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
		Canvass__c objCanvass = TestMethodsUtility.createCanvass(telco);
		    
		Lead objLead = new Lead(FirstName = 'Test24', LastName = 'Test Unit12', Company = 'Test Berry12', Primary_Canvass__c = objCanvass.Id, Phone = '9998877009', Status = 'Open',City='Texas',Country='US',State='TX',Create_Listing__c='Yes',street='st 12');
		insert objLead;
		LeadId = objLead.Id;
		set<Id> convertedLeadId = new set<Id>();
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(LeadId);
		lc.setDoNotCreateOpportunity(true);
		lc.setConvertedStatus('Qualified');
		        
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
		convertedLeadId.add(lcr.getLeadID());
		        
		CreateNOSOListingfromLead.createNosoListing(convertedLeadId);
	     
	}
}