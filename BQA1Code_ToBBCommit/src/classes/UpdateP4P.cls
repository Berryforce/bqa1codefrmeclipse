public class UpdateP4P {

    
        public string dffId;
        public Digital_Product_Requirement__c objDFF {get;set;}
        public Canvass__c objCanvass {get;set;}
        public Boolean bolEditFlag {get;set;}
        public UpdateP4P (ApexPages.StandardController controller) {
            dffId = Apexpages.currentPage().getParameters().get('id');
            fetchRecord();
        }
        
        public PageReference redirectToDFF() {
            PageReference objPageRef = new PageReference('/'+dffId);
            objPageRef.setRedirect(true);
            return objPageRef;
        }
        
        public void edit() {
            bolEditFlag = true;
        }
        
        public void cancel() {
            bolEditFlag = false;
        }
        
        public void fetchRecord() {
            List<Digital_Product_Requirement__c> lstDFF = [SELECT Canvass__c, P4P_Forwarded_Number__c, business_phone_number_office__c, P4P_Billing__c, P4P_Number_Type__c, P4P_Requested_Area_Code__c, P4P_Requested_Prefix__c, Call_Recording__c FROM Digital_Product_Requirement__c where id =: dffId];
            bolEditFlag = false;
            if(lstDFF.size()>0){
                list<Canvass__c> lstCanvass = [Select Id, P4P_Call_Time__c, P4P_Threshold__c, P4P_Frequency__c from Canvass__c where Id =:lstDFF[0].Canvass__c];
                if(lstCanvass != null && lstCanvass.size() > 0) {
                    objCanvass = lstCanvass[0];
                }
                objDFF = lstDFF[0];
            }
        }
        
        public Pagereference Submit() {
            Boolean bolOLIFlag = true;
            Boolean bolAccFlag = true;
            Boolean bolDFFFlag = true;
            Boolean bolValDirFlag = true;
            Boolean bolValDirHeadFlag = true;
            Boolean bolFlag = true;
            String strDirError = 'The below fields need to be populated in the Directory to get tracking number<br/>';
            String strDirHeadError = 'The below fields need to be populated in the Directory Heading record to get tracking number<br/>';
            String strDFFError = 'The below fields need to be populated in the Data Fulfillment record to get tracking number<br/>';
            String strAccountError = 'The below fields need to be populated in the Account record to get tracking number<br/>';
            String strOLIError = 'The below fields need to be populated in the Order Line Item record to get tracking number<br/>';
            Digital_Product_Requirement__c objDFFVal;
            PageReference objPageRef;
            
            try{
                List<Digital_Product_Requirement__c> lstDFF = [SELECT UDAC__c,OrderLineItemID__r.Last_Billing_Date__c,business_phone_number_office__c,OrderLineItemID__r.Order_Group__c, OrderLineItemID__r.Name, Account__r.Media_Trax_Client_ID__c, Account__c, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry, P4P_Forwarded_Number__c,
                                                            OrderLineItemID__c, OrderLineItemID__r.Talus_Go_Live_Date__c, OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c, OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c, OrderLineItemID__r.Billing_Start_Date__c, 
                                                            OrderLineItemID__r.P4P_Tracking_Number_ID__c, OrderLineItemID__r.Billing_End_Date__c, OrderLineItemID__r.Product2__c, OrderLineItemID__r.P4P_Price_Per_Click_Lead__c, P4P_Billing__c, P4P_Number_Type__c, P4P_Requested_Area_Code__c, 
                                                            P4P_Requested_Prefix__c, Call_Recording__c FROM Digital_Product_Requirement__c where id =: dffId];
                objDFFVal = lstDFF[0];
                
                /*if(objDFFVal.OrderLineItemID__r.Last_Billing_Date__c == null){
                    strOLIError = strOLIError+'Last Billing Date<br/>';
                    bolOLIFlag = false;
                }
                if(objDFFVal.OrderLineItemID__r.Talus_Go_Live_Date__c == null){
                    strOLIError = strOLIError+'Go Live Date<br/>';
                    bolOLIFlag = false;
                }*/
                
                if(objDFFVal.OrderLineItemID__r.Talus_Go_Live_Date__c != null){
                    CommonMethods.addError('Go Live Date is already present for this record');
                    bolFlag = false;
                }
                if(objDFFVal.OrderLineItemID__r.P4P_Tracking_Number_ID__c != null){
                    CommonMethods.addError('P4P Tracking Number ID is already present for this record');
                    bolFlag = false;
                }
                if(objDFFVal.OrderLineItemID__r.Directory__r == null){
                    strOLIError = strOLIError+'Directory<br/>';
                    bolOLIFlag = false;
                }
                else{
                    if(objDFFVal.OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c == null){
                        strDirError = strDirError+'Media Trax Directory Id';
                        bolValDirFlag = false;
                    }
                }
                if(objDFFVal.OrderLineItemID__r.Directory_Heading__c == null){
                    strOLIError = strOLIError+'Directory Heading<br/>';
                    bolOLIFlag = false;
                }
                else{
                    if(objDFFVal.OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c == null){
                        strDirHeadError = strDirHeadError+'Media Trax Directory Heading Id';
                        bolValDirHeadFlag = false;
                    }
                }
                if(objDFFVal.Account__r == null){
                    strOLIError = strOLIError+'Account<br/>';
                    bolDFFFlag = false;
                }
                else{
                    if(objDFFVal.Account__r.Media_Trax_Client_ID__c == null){
                        strAccountError = strAccountError+'Media Trax Client Id';
                        bolAccFlag = false;
                    }
                    if(objDFFVal.Account__r.BillingCity == null){
                        strAccountError = strAccountError+'Directory<br/>';
                        bolAccFlag = false;
                    }
                    if(objDFFVal.Account__r.BillingState == null){
                        strAccountError = strAccountError+'Directory<br/>';
                        bolAccFlag = false;
                    }
                    if(objDFFVal.Account__r.BillingCountry == null){
                        strAccountError = strAccountError+'Directory<br/>';
                        bolAccFlag = false;
                    }
                }
                if(objDFFVal.business_phone_number_office__c == null){
                    strDFFError = strDFFError+'Main Listed Number<br/>';
                    bolDFFFlag = false;
                }
                if(objDFFVal.P4P_Requested_Area_Code__c == null){
                    strDFFError = strDFFError+'P4P Requested Area Code<br/>';
                    bolDFFFlag = false;
                }
                if(objDFFVal.P4P_Requested_Prefix__c == null){
                    strDFFError = strDFFError+'P4P Requested Prefix<br/>';
                    bolDFFFlag = false;
                }
                if(objDFFVal.UDAC__c == null){
                    strDFFError = strDFFError+'UDAC<br/>';
                    bolDFFFlag = false;
                }
                if(objDFFVal.P4P_Number_Type__c == null){
                    strDFFError = strDFFError+'P4P Number Type<br/>';
                    bolDFFFlag = false;
                }
                
                if(!bolAccFlag){
                    CommonMethods.addError(strAccountError);
                }
                if(!bolOLIFlag){
                    CommonMethods.addError(strOLIError);
                }
                if(!bolValDirHeadFlag){
                    CommonMethods.addError(strDirHeadError);
                }
                if(!bolDFFFlag){
                    CommonMethods.addError(strDFFError);
                }
                if(!bolValDirFlag){
                    CommonMethods.addError(strDirError);
                }           
                
                if(bolValDirHeadFlag && bolValDirFlag && bolOLIFlag  && bolAccFlag && bolDFFFlag && bolFlag){
                    set<String> setHeadingActionName = new set<String>{'AddTrackingNumber'};
                    System.debug('ProcessMediaTraxForGetHeading Method starts');
                    list<MediaTrax_API_Configuration__c> lstMediaConf = [SELECT API_URL__c, AuthToken__c, Callout_Method__c, Content_Type__c, Endpoint_URL__c, Header_SOAPAction__c, 
                                                                        Name, Password__c, PredicateField__c, PredicateOperator__c, PredicateValue__c, Response_Method__c, Return_Fields__c, Return_Method__c, SOAP_URL__c, UserName__c 
                                                                        FROM MediaTrax_API_Configuration__c where Name IN:setHeadingActionName];
                    map<String, MediaTrax_API_Configuration__c> mapMediaTracConf = new map<String, MediaTrax_API_Configuration__c>();
                    
                    for(MediaTrax_API_Configuration__c iterator : lstMediaConf) {
                        mapMediaTracConf.put(iterator.Name, iterator);
                        System.debug('Testingg mapMediaTracConf '+mapMediaTracConf);
                    }
                    String tracingNumber;
                    String trackingId;
                
                    DOM.Document objDOMResult = MediaTraxHTTPCalloutHandlerController_V1.TrackingInMediaTraxCall(lstDFF[0], objCanvass, mapMediaTracConf.get('AddTrackingNumber'));
                    MediaTrax_API_Configuration__c objMedia = mapMediaTracConf.get('AddTrackingNumber');
                    System.debug('Testingg 111 objDOMResult '+objDOMResult);
                    if(objDOMResult != null) {                
                        map<String, dom.XmlNode> mapNodeResult = MediaTraxHTTPCalloutHandlerController_V1.parseDirectoryXML(objDOMResult, objMedia.SOAP_URL__c, objMedia.API_URL__c, objMedia.Return_Method__c, objMedia.Response_Method__c);
                       System.debug('Testingg 111 mapNodeResult '+mapNodeResult);
                        if(mapNodeResult.size() > 0) {
                            System.debug('Testingg 111 mapNodeResult.get(result) '+mapNodeResult.get('result'));
                            System.debug('Testingg 111 mapNodeResult.get(Error) '+mapNodeResult.get('Error'));
                            if(mapNodeResult.get('result') != null) {
                                for(dom.XmlNode iterator1: mapNodeResult.get('result').getChildElements()) {
                                    system.debug('Testingg 111 Iterator : '+ iterator1);
                                    system.debug('Testingg 111 Iterator.getName() : '+ iterator1.getName());
                                    if(iterator1.getName() == 'trackingnumberid') {
                                        system.debug('Track Id : ' + iterator1.getText());
                                        trackingId = iterator1.getText();
                                        System.debug('Testingg trackingId '+trackingId);
                                    }
                                    else if(iterator1.getName() == 'trackingnumber') {
                                        tracingNumber = iterator1.getText();
                                        System.debug('Testingg tracingNumber '+tracingNumber);
                                    }
                                }
                            }
                            else if(mapNodeResult.get('Error') != null){
                                String strError = 'Error message from MediaTrax : ';
                                String strText;
                                String strDisplayError;
                                String strErrorUI;
                                dom.XmlNode objError = mapNodeResult.get('Error');
                                System.debug('Testingg mapNodeResult.get(Error) '+mapNodeResult.get('Error'));
                                System.debug('Testingg mapNodeResult.get(Error).getChildElements() '+mapNodeResult.get('Error').getChildElements());
                                for(dom.XmlNode iterator1: mapNodeResult.get('Error').getChildElements()) {
                                    System.debug('Testingg error iterator1.getName()'+iterator1.getName());
                                    System.debug('Testingg error iterator1'+iterator1);
                                    if(iterator1.getName() == 'message'){
                                        //System.debug('Testingg text iterator1.getChildElement(message, https://ct.mediatrax.com/api) '+objError.getChildElement('message', 'https://ct.mediatrax.com/api').getText());
                                        System.debug('Testingg Error objError.getChildElement(message) ' +objError.getChildElement('message',null));
                                        System.debug('Testingg Error objError.getChildElement(message, https://ct.mediatrax.com/api) ' +objError.getChildElement('message', 'https://ct.mediatrax.com/api'));
                                        System.debug('Testingg Error objError.getChildElement(message, https://ct.mediatrax.com/api).getText() ' +objError.getChildElement('message', 'https://ct.mediatrax.com/api').getText());
                                        strText = objError.getChildElement('message', 'https://ct.mediatrax.com/api').getText();
                                        strError = strError + iterator1.getName() +' = '+strText+'. ';
                                        if(strText != null && strText.contains('NPA has no rate centers')){
                                            strDisplayError = 'The Area Code and Prefix combination you selected is unavailable. Please provide a new Area Code and Prefix combination and try again.';
                                            strErrorUI = 'User has been notified with following error message : '+strDisplayError;
                                        }
                                        else {
                                            strDisplayError = strText+'. ';
                                        }
                                    }
                                    else{
                                        strError = strError + iterator1.getName() +' = '+objError.getChildElement(iterator1.getName(), 'https://ct.mediatrax.com/api').getText()+'. ';
                                    }
                                }
                                if(strErrorUI == null) {
                                    futureCreateErrorLog.createErrorRecord('MediaTrax returned error message while fetching tracking number. ',strError,'MediaTrax');
                                }
                                else {
                                    futureCreateErrorLog.createErrorRecord('MediaTrax returned error message while fetching tracking number. ',strError+strErrorUI,'MediaTrax');
                                }
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while performing the requested operation. '+strDisplayError));
                                System.debug('Testingg strError '+strError);
                                System.debug('Testingg strText '+strText);
                            }                    
                        }
                    }                
                    if(String.isNotBlank(trackingId) && String.isNotBlank(tracingNumber)) {
                        list<Order_Line_Items__c> lstOLI = [Select Id, P4P_Tracking_Number_ID__c, (Select Id,business_phone_number_office__c,P4P_Forwarded_Number__c from Digital_Product_Requirements__r) from Order_Line_Items__c where Order_Group__c =:lstDFF[0].OrderLineItemID__r.Order_Group__c and (P4P_Billing__c = true OR Is_P4P__c = true)];
                        list<Order_Line_Items__c> lstUpdateOLI = new list<Order_Line_Items__c>();
                        list<Digital_Product_Requirement__c> lstUpdateDFF = new list<Digital_Product_Requirement__c>();
                        for(Order_Line_Items__c iterator : lstOLI) {
                            iterator.P4P_Tracking_Number_ID__c = trackingId;
                            iterator.P4P_Tracking_Number_Status__c = 'Activated';
                            //iterator.Service_Start_Date__c = System.today();
                            //iterator.Service_End_Date__c = System.today().addMonths(13);
                            //iterator.Last_Billing_Date__c = System.today().addMonths(13);
                            for(Digital_Product_Requirement__c iteratorDff : iterator.Digital_Product_Requirements__r) {
                                iteratorDff.P4P_Forwarded_Number__c = iteratorDff.business_phone_number_office__c;
                                iteratorDff.business_phone_number_office__c = tracingNumber;
                                //iteratorDff.Advertised_Phone_Number__c = tracingNumber;
                                lstUpdateDFF.add(iteratorDff);
                            }
                            lstUpdateOLI.add(iterator);
                        }
                        
                        if(lstUpdateOLI.size() > 0) {
                            update lstUpdateOLI;
                            System.debug('Testingg lstUpdateOLI '+lstUpdateOLI);
                        }
                        
                        if(lstUpdateDFF.size() > 0) {
                            update lstUpdateDFF;
                            System.debug('Testingg lstUpdateDFF '+lstUpdateDFF);
                        }
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'P4P Tracking Number has been provisioned successfully.'));
                    }
                }
                //PageReference objPageRef = new PageReference('/'+objDFFVal.id);
                //objPageRef.setRedirect(true);
            }
            catch(CustomException objExp){
                System.debug('Exception occured in UpdateP4P class in method Submit - Message '+objExp.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while performing the requested operation. Please try after some time or contact administrator if error persist.'));
                futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.strStackTrace,'MediaTrax');
            }catch(Exception objExp){
                System.debug('Exception occured in UpdateP4P class in method Submit - Message '+objExp.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while performing the requested operation. Please try after some time or contact administrator if error persist.'));
                futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.getStackTraceString(),'MediaTrax');
            }
            return null;
        }
        
        public Pagereference save() {
            bolEditFlag = false;
            objDFF.P4P_Forwarded_Number__c = objDFF.business_phone_number_office__c;
            update objDFF;
            
            //PageReference PageRef = (new ApexPages.StandardController(objDFF)).view();
            //PageRef.setRedirect(true);
            return null;
        }
    }