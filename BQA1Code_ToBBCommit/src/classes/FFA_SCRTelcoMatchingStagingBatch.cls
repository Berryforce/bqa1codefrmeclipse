/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class FFA_SCRTelcoMatchingStagingBatch implements Database.Batchable<SObject>, Database.Stateful
{
    private List<String> errorMessages;
    private List<String> successMessages;
    private List<String> recordTypes;
    private Set<id> accountIds;
    private Set<String> processedAccountIds;

    /*
    public void execute (SchedulableContext ctx)
    {
        List<String> classNames = new List<String>{'FFA_SCRTelcoMatchingStagingQueueable'}; 
        List<String> batchStatus = new List<String>{'Queued', 'Holding', 'Preparing', 'Processing'};
        Integer jobs = [SELECT count() FROM AsyncApexJob WHERE ApexClass.Name IN :classNames AND Status IN :batchStatus LIMIT 1];

        if(jobs > 0)
        {
            Datetime sysTime = System.now().addminutes( 10 );
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            FFA_SCRTelcoMatchingStagingBatch batch = new FFA_SCRTelcoMatchingStagingBatch(accountIds, recordTypes, berryMatchingType, enforceLineLimit);
            System.schedule('FFA_SCRTelcoMatchingStagingBatch' + sysTime.getTime() + '' + berryMatchingType, chron_exp, batch);
        }
        else
        {
            if(berryMatchingType != '' && accountIds != null && recordTypes != null)
            {
                Database.executeBatch( new FFA_SCRTelcoMatchingStagingBatch(accountIds, recordTypes, berryMatchingType, enforceLineLimit) , 1 );
            }
            else
            {
                Database.executeBatch( new FFA_SCRTelcoMatchingStagingBatch() , 1 );
            }
        }
    }
    */

    public FFA_SCRTelcoMatchingStagingBatch() 
    {
        initialise(new set<id>());
    }

    public FFA_SCRTelcoMatchingStagingBatch(Set<id> acctIds)
    {
        initialise(acctIds);
    } 

    private void initialise(Set<id> accIds)
    {
        accountIds = accIds;
        processedAccountIds = new Set<String>();
        recordTypes = new List<String>{'Telco Partner'};
        errorMessages = new List<String>();
        successMessages = new List<String>(); 
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date matchingDate = System.today();

        String query = FFA_SCRTelcoMatchingStagingSOQL.getStartQuery(accountIds, matchingDate);

        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext BC, List<SObject> unTypedScope)
    {
        for(c2g__codaTransactionLineItem__c scopeItem : (List<c2g__codaTransactionLineItem__c>)unTypedScope)
        {
            String key = scopeItem.c2g__Account__c + '' + (String)scopeItem.getsObject('c2g__Transaction__r').get('Customer_Name__c');
            if(!processedAccountIds.contains(key))
            {
                processedAccountIds.add(key); 
                System.enqueueJob(new FFA_SCRTelcoMatchingStagingQueueable(true, scopeItem.c2g__Account__c, (Id)scopeItem.getsObject('c2g__Transaction__r').get('Customer_Name__c')));
            }
        }
    }

    public void finish( Database.BatchableContext BC )
    {
        String numberOfAccounts = string.valueof(processedAccountIds.size());
        successMessages.add(numberOfAccounts);

        Datetime sysTime = System.now().addminutes( 10 );
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        FFA_TelcoCashMatchingDefineGroupsBatch batch = new FFA_TelcoCashMatchingDefineGroupsBatch();
        System.schedule('FFA_TelcoCashMatchingDefineGroupsBatch' + sysTime.getTime(), chron_exp, batch);

        AsyncApexJob batchJob = [
            Select
                Id, 
                Status, 
                NumberOfErrors, 
                ExtendedStatus, 
                JobItemsProcessed, 
                TotalJobItems, 
                CreatedBy.Email 
            From 
                AsyncApexJob 
            Where 
                Id = :bc.getJobId()
        ];
        
        if(!Test.isRunningTest()) sendEmail( batchJob.CreatedBy.Email, successMessages, errorMessages );

    }

    private void sendEmail( String address, List<String>successMessages, List<String>errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = '';
        body += 'Results of matching (SCR Matched to SIN by Order Line Item):\n\n';
    
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Matching Sales Credit matched to Sales Invoices (FFA_SCRTelcoMatchingStagingBatch)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }

}