@isTest
public with sharing class TestFAQ {
    static testMethod void testConstructor()
    {
      
      
      List<FAQ__c> lstFaq = new List<FAQ__c>();
      
      FAQ__c objFaq = new FAQ__c();
      
      objFaq.Category__c ='About Berry';
      objFaq.Question__c='what is Berry?';
      objFaq.Answer__c='Brand';
      objFaq.Active__c=true;
      
      FAQ__c objFaq1 = new FAQ__c();
      
      objFaq1.Category__c ='About Berry';
      objFaq1.Question__c='where it is located?';
      objFaq1.Answer__c='US';
      objFaq1.Active__c=true;
      
      lstFaq.add(objFaq);
      lstFaq.add(objFaq1);
      
      insert lstFaq;
      
      FAQs faq = new FAQs();
      
      faq.getCategories();
      faq.selectedCategory='About Berry';
      faq.fetchCategoryDetails();
      //faq.getFAQs();
    }
       
}