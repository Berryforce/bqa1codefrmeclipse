public with sharing class TBCsBillingTransferStatusSOQLMethods {
  public static List<Billing_Transfer_Status__c> fetchBillingTransferStatusByAccountId(String AccountId) {
    return [SELECT Id,CreatedBy.Id, BTS_Billing_Frequency__c,CreatedBy.Name,BTS_Billing_Partner_Name__c,BTS_Account__c,BTS_Billing_Partner__c,BTS_Canvass__c,
        BTS_Directory_Edition_Id__c,BTS_Directory_Id__c,BTS_Go_Live__c,BTS_Migrated_OLI__c,BTS_Media_Type__c,BTS_Mode_of_Transfer__c,BTS_Type_of_Transfer__c,Name, BTS_Billing_Partner_ID__c  
        from Billing_Transfer_Status__c where BTS_Account__c=:AccountId];
  }
}