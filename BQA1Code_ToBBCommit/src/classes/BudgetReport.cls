public class BudgetReport {
public string directories;
public string directoriesEdition;
public boolean showreport{get;set;}
public String URLdir{get;set;}
public String URLdEdition{get;set;}
public boolean ContexValue{get;set;}
public boolean showyearpanel{get;set;}
public boolean showExportBtn{get;set;}
public boolean actionpanel{get;set;}
public boolean Selectionpanel{get;set;}
public String selecteddirectories{get;set;}
public List<Budget__c> bdgt{get;set;}
public string DirectoryName{get;set;}
public string DirEdition{get;set;}
public string DirCode{get;set;}
public string DirVendor{get;set;}
public string Telco{get;set;}
public string PubMonth {get;set;}
public string YPPA {get;set;}
public string Complex {get;set;}
public string Copies{get;set;}
public string TotalPrintQuantity{get;set;}
public string Division{get;set;}
public string Printer {get;set;}
public string PubYear {get;set;}
public string YellowPages {get;set;}
public string WhitePages {get;set;}
public string TotalPages {get;set;}
public string exporttype{get;set;}
public boolean showCompanyReport {get;set;}
public AggregateResult[] BudgetData{get;set;}
public List<BudgetWrapper> BudgetWrap{get;set;}
public List<PagesWrapper> PagesWrap{get;set;}
public Boolean showDirectoryLevelReport {get;set;}
public Boolean showAllDirectoryReport {get;set;}
public List<CompanyReportWrapper> CompanyReportWrap{get;set;}
public List<MainCompanyReportWrapper> MainCompanyReportWrap {get;set;}
public string yearforreport;
public BudgetReport(ApexPages.StandardController controller) 
{
    bdgt=new List<Budget__c>  ();
    // showreport=false;
    //showDirectoryLevelReport =false;
    showAllDirectoryReport =false;
    showCompanyReport=false;
    exporttype='text/html';
    actionpanel=true;
    // showedit=false;
    showyearpanel=false;
    URLdir=Apexpages.currentpage().getparameters().get('dir');
    URLdEdition=Apexpages.currentpage().getparameters().get('dirE');
    System.debug(URLdEdition+'##############'+URLdir);
    if(URLdir!=null && URLdEdition!=null)
    {
      System.debug(URLdEdition+'##############'+URLdir);
      directories=URLdir;
      directoriesEdition=URLdEdition;
      generateBudgetReport();
      showreport=true;
      Selectionpanel=false;
      ContexValue=true;
    }
    
    }
    
    public void showReport()
    {
    showAllDirectoryReport =true;
    showDirectoryLevelReport=false;
    }
public void showcompanypanel()
{
showyearpanel=true;
showDirectoryLevelReport=false;
showAllDirectoryReport=false;
yearforreport='0';
}  
public void FetchCompanyData()
{
System.debug('@@@@@@@@@@@@@'+yearforreport);
if(yearforreport!='0')
{
GenerateComapanyLevelReport(yearforreport);
}
else
{
MainCompanyReportWrap =new List<MainCompanyReportWrapper>();
}
}
    
public List<SelectOption> getreportyear() {
            Integer currentyear=date.today().year();
            List<SelectOption> options = new List<SelectOption>();
             options.add(new SelectOption('0','--None--'));
             for(integer i=currentyear;i<currentyear+10;i++)
             {
             options.add(new SelectOption(String.valueof(i),String.valueof(i)));
            }
            
            return options;
        }
       public String getyearforreport() {
           return yearforreport;
        }  
    public void setyearforreport(String yearforreport) {
            this.yearforreport= yearforreport;
                    
        }
public void exportReport()
{
  actionpanel=false;
  exporttype='application/vnd.ms-excel#CompanyLevelReport.xls';
}
public void saveforecast()
{
  Set<Budget__c> bgSet=new Set<Budget__c>();
  List<Budget__c> bgList=new List<Budget__c>();
  if(BudgetWrap.size()>0)
  {
   
    for(BudgetWrapper bgwrap:BudgetWrap)
    {
       bgSet.add(bgwrap.bdgtObj);
       /*
        for(Budget__c bg:bgwrap.bdgtObj)
        {
          bgList.add(bg);
        }*/
    }
    bgList.addall(bgSet);
     System.debug('@@@@@@@@@@@@@@'+bgList.size());
    if(bgList.size()>0)
    {
     update bgList;
    }
  }
}

public void showdirectoryLevelReport()
{

    URLdir=Apexpages.currentpage().getparameters().get('dir');
    URLdEdition=Apexpages.currentpage().getparameters().get('dirE');
    System.debug(URLdEdition+'##############'+URLdir);
    if(URLdir!=null && URLdEdition!=null)
    {
      directories=URLdir;
      directoriesEdition=URLdEdition;
      Selectionpanel=false;
      showDirectoryLevelReport=true;
      showreport=true;
      ContexValue=true;
    }
    else
    {
     showDirectoryLevelReport =true;
     Selectionpanel=true;
     ContexValue=false;
    }
    
showAllDirectoryReport =false;
showyearpanel=false;
}
public void showCompanyLevelReport()
{
showDirectoryLevelReport =false;
showAllDirectoryReport =false;
showyearpanel=true;
//GenerateComapanyLevelReport();
}
public List<SelectOption> getdirectory() 
{
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('--None--','--None--'));
            for(Directory__c tl:[Select id,Name from Directory__c Order By Name ASC Limit 999])
            {
             options.add(new SelectOption(tl.id,tl.Name));
            }
            
            return options;
}
       
public String getdirectories() 
{
           return directories;
}
            
public void setdirectories(String directories) 
{
            this.directories= directories;
            this.selecteddirectories=directories;
           
}

public List<SelectOption> getdirectoryEdition() 
{
            List<SelectOption> options = new List<SelectOption>();
            showExportBtn=false;
            options.add(new SelectOption('--None--','--None--'));
            for(Directory_Edition__c tl:[Select id,Name from Directory_Edition__c where Directory__c=:this.selecteddirectories Order By Name ASC])
            {
             options.add(new SelectOption(tl.id,tl.Name));
            }
            
            return options;
}
public String getdirectoriesEdition() 
{
           return directoriesEdition;
}  

public void setdirectoriesEdition(String directoriesEdition) 
{
            this.directoriesEdition= directoriesEdition;
         
           
}
public void populateEdition()
{
  system.debug('&&&&&&&&&&&&&&&&&&&&&&'+directories);
  getdirectoryEdition();
  showExportBtn=false;
}

public void generateBudgetReport()
   {
      
      DirectoryName= null;
      Telco= null;
      PubMonth = null;
      YPPA = null;
      Complex = null;
      Copies= null;
      Division= null;
      Printer = null;
      PubYear = null;
      YellowPages = null;
      WhitePages = null;
      TotalPages = null;
      DirEdition=null;
      DirCode=null;
      DirVendor=null;
      showExportBtn=true;
      System.debug('@@@@@@@@@@@@@@'+directories +'############************'+directoriesEdition);
     if(directories !=null  && directoriesEdition != '--None--' && directoriesEdition !='')
     {
      bdgt=new List<Budget__c>  ();
       bdgt=[SELECT Directory_Edition__c,Directory_Name__r.Directory_Code__c,Directory_Name__r.Vendor__c,Edition_Code__c,Directory_Edition__r.Name,Directory_Edition__r.Pub_Date__c,Directory_Name__c,Directory_Name__r.Name,Directory_Name__r.Pub_Date__c,Division__c,Freight_Local_Logistics_B__c,Freight_Local_Logistics__c,Front_of_Book__c,Front_of_the_Book_B__c,Id,Initial_Distribution_Vendor_Cost_B__c,Initial_Distribution_Vendor_Cost__c,Miscellaneous_Distribution_Cost_B__c,Miscellaneous_Initial_Distribution_Cost__c,Name,Paper_Cost_B__c,Paper_Cost__c,Printer_Invoice_Distribution_Cost_B__c,Printer_Invoice_Distribution_Cost__c,Printer__c,Printing_Cost_B__c,Printing_Cost__c,Publication_Company__c,Sales_Tax_B__c,Sales_Tax__c,Secondary_Distribution_B__c,Secondary_Distribution__c,SystemModstamp,Telco_Provider__c,Text_Lbs__c,Total_Cost_B__c,Total_Cost__c,Total_Distribution_Cost_B__c,Total_Distribution_Cost__c,Total_Manufacturing_Cost_B__c,Total_Manufacturing_Cost__c,Total_Page_Count_B__c,Total_Page_Count__c,Total_Print_Quantity_B__c,Total_Print_Quantity__c,Vendor__c,WhitePages__c,White_Pages_B__c,YellowPages__c,Yellow_Pages_B__c FROM Budget__c where Directory_Name__c=:directories AND Directory_Edition__c=:directoriesEdition];
       if(bdgt.size()>0)
       {
         DirCode=bdgt[0].Directory_Name__r.Directory_Code__c;
         DirVendor=bdgt[0].Directory_Name__r.Vendor__c;
         DirectoryName=bdgt[0].Directory_Name__r.Name;
        // String DEdition=String.valueof(bdgt[0].Directory_Edition__r.Pub_Date__c.year()).Right(2)+String.valueof(bdgt[0].Directory_Edition__r.Pub_Date__c.month());
         DirEdition=bdgt[0].Edition_Code__c;
         Telco=bdgt[0].Telco_Provider__c;
         PubMonth =String.valueof(bdgt[0].Directory_Name__r.Pub_Date__c.Month());
         YPPA =null;
         Complex =null;
         Copies=null;
         Division=bdgt[0].Division__c;
         Printer =bdgt[0].Printer__c;
         PubYear =String.valueof(bdgt[0].Directory_Name__r.Pub_Date__c.year());
         YellowPages =String.valueof(bdgt[0].YellowPages__c);
         WhitePages =String.Valueof(bdgt[0].WhitePages__c);
         TotalPages =String.valueof(bdgt[0].Total_Page_Count__c);
         TotalPrintQuantity=String.valueof(bdgt[0].Total_Print_Quantity__c);
         showreport=true;
       }
       else
       {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Budget records found for the Directory & Edition.');
        ApexPages.AddMessage(myMsg);
        showExportBtn=false;
        showreport=false;
       }       
       BudgetWrap=new List<BudgetWrapper>(); 
       PagesWrap=new List<PagesWrapper>();
       for(Budget__c bdgtIterator:[Select Name,Paper_Cost__c,Total_Print_Quantity__c,Total_Print_Quantity_B__c,WhitePages__c,White_Pages_B__c,YellowPages__c,Yellow_Pages_B__c,Total_Page_Count__c,Total_Page_Count_B__c,Forecast_Manufacturing_Cost__c, Paper_Cost_B__c,Forecast_Distribution_Cost__c,Forecast_Paper_Cost__c,Printing_Cost__c,Printing_Cost_B__c,Forecast_Printing_Cost__c,Total_Manufacturing_Cost__c,Total_Manufacturing_Cost_B__c,Total_Distribution_Cost__c,Total_Distribution_Cost_B__c,Total_Cost__c,Total_Cost_B__c from Budget__c where Directory_Name__c=:directories AND Directory_Edition__c=:directoriesEdition])
       {
          PagesWrap.add(new PagesWrapper('White Pages',bdgtIterator.WhitePages__c,bdgtIterator.White_Pages_B__c,bdgtIterator));
          PagesWrap.add(new PagesWrapper('Yellow Pages',bdgtIterator.YellowPages__c,bdgtIterator.Yellow_Pages_B__c,bdgtIterator));
          PagesWrap.add(new PagesWrapper('Total Pages',bdgtIterator.Total_Page_Count__c,bdgtIterator.Total_Page_Count_B__c,bdgtIterator));
          PagesWrap.add(new PagesWrapper('Total Print Quantity',bdgtIterator.Total_Print_Quantity__c,bdgtIterator.Total_Print_Quantity_B__c,bdgtIterator));
          BudgetWrap.add(new BudgetWrapper('Paper',bdgtIterator.Paper_Cost__c,bdgtIterator.Paper_Cost_B__c,bdgtIterator));
          BudgetWrap.add(new BudgetWrapper('Print',bdgtIterator.Printing_Cost__c,bdgtIterator.Printing_Cost_B__c,bdgtIterator));
          BudgetWrap.add(new BudgetWrapper('Total Manufacturing',bdgtIterator.Total_Manufacturing_Cost__c,bdgtIterator.Total_Manufacturing_Cost_B__c,bdgtIterator));
          BudgetWrap.add(new BudgetWrapper('Total Distribution',bdgtIterator.Total_Distribution_Cost__c,bdgtIterator.Total_Distribution_Cost_B__c,bdgtIterator));
          BudgetWrap.add(new BudgetWrapper('Total Cost',bdgtIterator.Total_Cost__c,bdgtIterator.Total_Cost_B__c,bdgtIterator ));
       }
     }
   }
   
   public void GenerateComapanyLevelReport(String yearforreport)
   {
      String ReportYear=yearforreport.substring(2,yearforreport.length());
      String SearchPattern=ReportYear+'%';
      Set<Id> BudgetIds=new Set<Id>();
      System.debug('@@@@@BudgetIds@@@@@@@@@@@@@@@'+SearchPattern);
      for(Budget__c bdgtIterator:[Select Id,Edition_Code__c from Budget__c where Edition_Code__c like:SearchPattern ])
      {
         BudgetIds.add(bdgtIterator.id);
      }
      if(BudgetIds.size()>0)
     { 
      System.debug('@@@@@BudgetIds@@@@@@@@@@@@@@@'+BudgetIds);
      BudgetData=new AggregateResult[]{};
      showAllDirectoryReport =true;
      CompanyReportWrap=new List<CompanyReportWrapper>();
      Map<String,Map<String,List<String>>> testmap=new Map<String,Map<String,List<String>>> ();
     BudgetData=[SELECT SUM(Forecast_Distribution_Cost__c) DForecast,SUM(Forecast_Paper_Cost__c) PaperForecast,SUM(Forecast_Printing_Cost__c) PrintForecast, SUM(Paper_Cost__c) APaperCost,SUM(Paper_Cost_B__c) BPaperCost,SUM(Printing_Cost__c) APrintCost,SUM(Printing_Cost_B__c) BPrintCost,
     SUM(Total_Distribution_Cost__c) ADCost,SUM(Total_Distribution_Cost_B__c) BDCost,CALENDAR_MONTH(Directory_Name__r.Pub_Date__c) PrintedMonth FROM Budget__c Where ID in: BudgetIds GROUP BY CALENDAR_MONTH(Directory_Name__r.Pub_Date__c) ORDER BY CALENDAR_MONTH(Directory_Name__r.Pub_Date__c)];
    
      
    for(AggregateResult agr:BudgetData)
    {
      
     if(String.valueof(agr.get('PrintedMonth'))!=null && String.valueof(agr.get('PrintedMonth'))!='')
     {
       CompanyReportWrap.add(new CompanyReportWrapper(String.valueof(agr.get('PrintedMonth')),Double.Valueof(agr.get('APaperCost')),Double.Valueof(agr.get('BPaperCost')),Double.Valueof(agr.get('PaperForecast')),Double.Valueof(agr.get('APrintCost')),Double.Valueof(agr.get('BPrintCost')),Double.Valueof(agr.get('PrintForecast')),Double.Valueof(agr.get('ADCost')),Double.Valueof(agr.get('BDCost')),Double.Valueof(agr.get('DForecast'))));
     }
    }
    Map<Integer,CompanyReportWrapper> mapCRW=new Map<Integer,CompanyReportWrapper>();
    
    for(CompanyReportWrapper CRWIterator: CompanyReportWrap)
    {
          mapCRW.put(Integer.Valueof(CRWIterator.BudgetMonth),CRWIterator);
    
    }
    
    Set<Integer> MonthSetNew=new SET<Integer>{1,2,3,4,5,6,7,8,9,10,11,12};
    for(Integer monthvar : MonthSetNew)
       {
         if(!mapCRW.containsKey(monthvar))
         {
           mapCRW.put(monthvar,new CompanyReportWrapper(String.valueof(monthvar),0,0,0,0,0,0,0,0,0));
         }
         
       }
     List<Integer> MonthListIter=new List<Integer>();
     MonthListIter.addall(mapCRW.keyset());
     MonthListIter.sort();
     CompanyReportWrap.clear();
     for(Integer crwIter : MonthListIter)
     {
       CompanyReportWrap.add(mapCRW.get(crwIter));
     }
    
    Double BQ1=0;
    Double AQ1=0;
    Double FQ1=0;
    Double VQ1=0;
    Double BQ2=0;
    Double AQ2=0;
    Double FQ2=0;
    Double VQ2=0;
    Double BQ3=0;
    Double AQ3=0;
    Double FQ3=0;
    Double VQ3=0;
    Double BQ4=0;
    Double AQ4=0;
    Double FQ4=0;
    Double VQ4=0;
       
    Double BQ11=0;
    Double AQ11=0;
    Double FQ11=0;
    Double VQ11=0;
    Double BQ21=0;
    Double AQ21=0;
    Double FQ21=0;
    Double VQ21=0;
    Double BQ31=0;
    Double AQ31=0;
    Double FQ31=0;
    Double VQ31=0;
    Double BQ41=0;
    Double AQ41=0;
    Double FQ41=0;
    Double VQ41=0;
    
    Double BQ12=0;
    Double AQ12=0;
    Double FQ12=0;
    Double VQ12=0;
    Double BQ22=0;
    Double AQ22=0;
    Double FQ22=0;
    Double VQ22=0;
    Double BQ32=0;
    Double AQ32=0;
    Double FQ32=0;
    Double VQ32=0;
    Double BQ42=0;
    Double AQ42=0;
    Double FQ42=0;
    Double VQ42=0;
    
    Double BQ13=0;
    Double AQ13=0;
    Double FQ13=0;
    Double VQ13=0;
    Double BQ23=0;
    Double AQ23=0;
    Double FQ23=0;
    Double VQ23=0;
    Double BQ33=0;
    Double AQ33=0;
    Double FQ33=0;
    Double VQ33=0;
    Double BQ43=0;
    Double AQ43=0;
    Double FQ43=0;
    Double VQ43=0;
    
    for(CompanyReportWrapper CRW:CompanyReportWrap)
    {
      if(CRW.BudgetPaperCost==null)
      {
      CRW.BudgetPaperCost=0;
      } 
      if(CRW.ActualPaperCost==null)
      {
      CRW.ActualPaperCost=0;
      }
      if(CRW.PaperForecast==null)
      {
      CRW.PaperForecast=0;
      }
      if(CRW.VariancePaperCost==null)
      {
       CRW.VariancePaperCost=0;
      }
      if(CRW.BudgetPrintCost==null)
      {
      CRW.BudgetPrintCost=0;      
      }
      if(CRW.ActualPrintCost==null)
      {
      CRW.ActualPrintCost=0;
      }
      if(CRW.PrintForecast==null)
      {
      CRW.PrintForecast=0;      
      }
      if(CRW.VariancePrintCost==null)
      {
      CRW.VariancePrintCost=0;
      } 
      if(CRW.BudgetDCost==null)
      {
      CRW.BudgetDCost=0;
      } 
      if(CRW.ActualDCost==null)
      {
      CRW.ActualDCost=0;
      }
      if(CRW.DForecast==null)
      {
      CRW.DForecast=0;
      }
      if(CRW.VarianceDCost==null)
      {
         CRW.VarianceDCost=0; 
      }    
          
          if(Integer.Valueof(CRW.BudgetMonth)<4)
          {
            BQ1=BQ1+CRW.BudgetPaperCost;
            AQ1=AQ1+CRW.ActualPaperCost;
            FQ1=FQ1+CRW.PaperForecast;
            VQ1=VQ1+CRW.VariancePaperCost;
            
            BQ2=BQ2+CRW.BudgetPrintCost;
            AQ2=AQ2+CRW.ActualPrintCost;
            FQ2=FQ2+CRW.PrintForecast;
            VQ2=VQ2+CRW.VariancePrintCost;
            
            BQ3=BQ3+CRW.BudgetDCost;
            AQ3=AQ3+CRW.ActualDCost;
            FQ3=FQ3+CRW.DForecast;
            VQ3=VQ3+CRW.VarianceDCost;
          }
          if(Integer.Valueof(CRW.BudgetMonth)>3 && Integer.Valueof(CRW.BudgetMonth)<7)
          {
            BQ11=BQ11+CRW.BudgetPaperCost;
            AQ11=AQ11+CRW.ActualPaperCost;
            FQ11=FQ11+CRW.PaperForecast;
            VQ11=VQ11+CRW.VariancePaperCost;
            
            BQ21=BQ21+CRW.BudgetPrintCost;
            AQ21=AQ21+CRW.ActualPrintCost;
            FQ21=FQ21+CRW.PrintForecast;
            VQ21=VQ21+CRW.VariancePrintCost;
            
            BQ31=BQ31+CRW.BudgetDCost;
            AQ31=AQ31+CRW.ActualDCost;
            FQ31=FQ31+CRW.DForecast;
            VQ31=VQ31+CRW.VarianceDCost;
          }
          if(Integer.Valueof(CRW.BudgetMonth)>6 && Integer.Valueof(CRW.BudgetMonth)<10)
          {
            BQ12=BQ12+CRW.BudgetPaperCost;
            AQ12=AQ12+CRW.ActualPaperCost;
            FQ12=FQ12+CRW.PaperForecast;
            VQ12=VQ12+CRW.VariancePaperCost;
            
            BQ22=BQ22+CRW.BudgetPrintCost;
            AQ22=AQ22+CRW.ActualPrintCost;
            FQ22=FQ22+CRW.PrintForecast;
            VQ22=VQ22+CRW.VariancePrintCost;
            
            BQ32=BQ32+CRW.BudgetDCost;
            AQ32=AQ32+CRW.ActualDCost;
            FQ32=FQ32+CRW.DForecast;
            VQ32=VQ32+CRW.VarianceDCost;
          }
          if(Integer.Valueof(CRW.BudgetMonth)>9 && Integer.Valueof(CRW.BudgetMonth)<13)
          {
            BQ13=BQ13+CRW.BudgetPaperCost;
            AQ13=AQ13+CRW.ActualPaperCost;
            FQ13=FQ13+CRW.PaperForecast;
            VQ13=VQ13+CRW.VariancePaperCost;
            
            BQ23=BQ23+CRW.BudgetPrintCost;
            AQ23=AQ23+CRW.ActualPrintCost;
            FQ23=FQ23+CRW.PrintForecast;
            VQ23=VQ23+CRW.VariancePrintCost;
            
            BQ33=BQ33+CRW.BudgetDCost;
            AQ33=AQ33+CRW.ActualDCost;
            FQ33=FQ33+CRW.DForecast;
            VQ33=VQ33+CRW.VarianceDCost;
          }
         
    }
    MainCompanyReportWrap =new List<MainCompanyReportWrapper>();
   for(CompanyReportWrapper CRWNEW:CompanyReportWrap)
    {
      if(Integer.Valueof(CRWNEW.BudgetMonth)==3)
      {
        MainCompanyReportWrap .add(new MainCompanyReportWrapper(CRWNEW.BudgetMonth,CRWNEW.BudgetPaperCost,CRWNEW.ActualPaperCost,CRWNEW.PaperForecast,CRWNEW.VariancePaperCost,CRWNEW.BudgetPrintCost,CRWNEW.ActualPrintCost,CRWNEW.PrintForecast,CRWNEW.VariancePrintCost,CRWNEW.BudgetDCost,CRWNEW.ActualDCost,CRWNEW.DForecast,CRWNEW.VarianceDCost));
        MainCompanyReportWrap .add(new MainCompanyReportWrapper('13',BQ1,AQ1,FQ1,VQ1,BQ2,AQ2,FQ2,VQ2,BQ3,AQ3,FQ3,VQ3));
      }
      else if(Integer.Valueof(CRWNEW.BudgetMonth)==6)
      {
        MainCompanyReportWrap .add(new MainCompanyReportWrapper(CRWNEW.BudgetMonth,CRWNEW.BudgetPaperCost,CRWNEW.ActualPaperCost,CRWNEW.PaperForecast,CRWNEW.VariancePaperCost,CRWNEW.BudgetPrintCost,CRWNEW.ActualPrintCost,CRWNEW.PrintForecast,CRWNEW.VariancePrintCost,CRWNEW.BudgetDCost,CRWNEW.ActualDCost,CRWNEW.DForecast,CRWNEW.VarianceDCost));
        MainCompanyReportWrap .add(new MainCompanyReportWrapper('14',BQ11,AQ11,FQ11,VQ11,BQ21,AQ21,FQ21,VQ21,BQ31,AQ31,FQ31,VQ31));
      }
      else if(Integer.Valueof(CRWNEW.BudgetMonth)==9)
      {
       MainCompanyReportWrap .add(new MainCompanyReportWrapper(CRWNEW.BudgetMonth,CRWNEW.BudgetPaperCost,CRWNEW.ActualPaperCost,CRWNEW.PaperForecast,CRWNEW.VariancePaperCost,CRWNEW.BudgetPrintCost,CRWNEW.ActualPrintCost,CRWNEW.PrintForecast,CRWNEW.VariancePrintCost,CRWNEW.BudgetDCost,CRWNEW.ActualDCost,CRWNEW.DForecast,CRWNEW.VarianceDCost));
       MainCompanyReportWrap .add(new MainCompanyReportWrapper('15',BQ12,AQ12,FQ12,VQ12,BQ22,AQ22,FQ22,VQ22,BQ32,AQ32,FQ32,VQ32));
      }
      else if(Integer.Valueof(CRWNEW.BudgetMonth)==12)
      {
       MainCompanyReportWrap .add(new MainCompanyReportWrapper(CRWNEW.BudgetMonth,CRWNEW.BudgetPaperCost,CRWNEW.ActualPaperCost,CRWNEW.PaperForecast,CRWNEW.VariancePaperCost,CRWNEW.BudgetPrintCost,CRWNEW.ActualPrintCost,CRWNEW.PrintForecast,CRWNEW.VariancePrintCost,CRWNEW.BudgetDCost,CRWNEW.ActualDCost,CRWNEW.DForecast,CRWNEW.VarianceDCost));
       MainCompanyReportWrap .add(new MainCompanyReportWrapper('16',BQ13,AQ13,FQ13,VQ13,BQ23,AQ23,FQ23,VQ23,BQ33,AQ33,FQ33,VQ33));   
      }
      else
      {
       MainCompanyReportWrap .add(new MainCompanyReportWrapper(CRWNEW.BudgetMonth,CRWNEW.BudgetPaperCost,CRWNEW.ActualPaperCost,CRWNEW.PaperForecast,CRWNEW.VariancePaperCost,CRWNEW.BudgetPrintCost,CRWNEW.ActualPrintCost,CRWNEW.PrintForecast,CRWNEW.VariancePrintCost,CRWNEW.BudgetDCost,CRWNEW.ActualDCost,CRWNEW.DForecast,CRWNEW.VarianceDCost));
        }
    }
     MainCompanyReportWrap .add(new MainCompanyReportWrapper('17',BQ1+BQ11+BQ12+BQ13,AQ1+AQ11+AQ12+AQ13,FQ1+FQ11+FQ12+FQ13,VQ1+VQ11+VQ12+VQ13,BQ2+BQ21+BQ22+BQ23,AQ2+AQ21+AQ22+AQ23,FQ2+FQ21+FQ22+FQ23,VQ2+VQ21+VQ22+VQ23,BQ3+BQ31+BQ32+BQ33,AQ3+AQ31+AQ32+AQ33,FQ3+FQ31+FQ32+FQ33,VQ3+VQ31+VQ32+VQ33));   
     System.debug('#################'+MainCompanyReportWrap.size());
   }
   else
   {
     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Budget records found for the selected year.');
     ApexPages.AddMessage(myMsg);
     showAllDirectoryReport =false;
   }  
     
}
    
    
public class MainCompanyReportWrapper
{
       
        public String BudgetMonth{get;set;}
        public Decimal ActualPaperCost{get;set;}
        public Decimal BudgetPaperCost{get;set;}
        public Decimal PaperForecast{get;set;}
        public Decimal VariancePaperCost{get;set;}
        public Decimal ActualPrintCost{get;set;}
        public Decimal BudgetPrintCost{get;set;}
        public Decimal PrintForecast{get;set;}
        public Decimal VariancePrintCost{get;set;}
        public Decimal ActualDCost{get;set;}
        public Decimal BudgetDCost{get;set;}
        public Decimal DForecast{get;set;}
        public Decimal VarianceDCost{get;set;}
        public Decimal TotalBudget {get;set;}
        public Decimal TotalActual {get;set;}
        public Decimal TotalVariance {get;set;}
        public Decimal TotalForecast {get;set;}
        public MainCompanyReportWrapper(String BudgetMonth, Decimal BudgetPaperCost,Decimal ActualPaperCost,Decimal PaperForecast,Decimal VariancePaperCost,Decimal BudgetPrintCost,Decimal ActualPrintCost,Decimal PrintForecast,Decimal VariancePrintCost,Decimal BudgetDCost,Decimal ActualDCost,Decimal DForecast,Decimal VarianceDCost)
        {
         if(ActualPaperCost==null)
         ActualPaperCost=0; 
         if(BudgetPaperCost==null)
         BudgetPaperCost=0;
         if(VariancePaperCost==null)
         VariancePaperCost=0;
         if(ActualPrintCost==null)
         ActualPrintCost=0;
         if(BudgetPrintCost==null)
         BudgetPrintCost=0;
         if(VariancePrintCost==null)
         VariancePrintCost=0;
         if(ActualDCost==null)
         ActualDCost=0;
         if(BudgetDCost==null)
         BudgetDCost=0;
         if(VarianceDCost==null)
         VarianceDCost=0;
         this.BudgetMonth=BudgetMonth;
         this.ActualPaperCost=Math.Round(ActualPaperCost);
         this.BudgetPaperCost=Math.Round(BudgetPaperCost);
         this.PaperForecast=PaperForecast;
         this.VariancePaperCost=Math.Round(VariancePaperCost);
         this.ActualPrintCost=Math.Round(ActualPrintCost);
         this.BudgetPrintCost=Math.Round(BudgetPrintCost);
         this.PrintForecast=PrintForecast;
         this.VariancePrintCost=Math.Round(VariancePrintCost);
         this.ActualDCost=Math.Round(ActualDCost);
         this.BudgetDCost=Math.Round(BudgetDCost);
         this.DForecast=DForecast;
         this.VarianceDCost=Math.Round(VarianceDCost);
         this.TotalBudget =Math.Round(BudgetPaperCost+BudgetPrintCost+BudgetDCost);
         this.TotalActual=Math.Round(ActualPaperCost+ActualPrintCost+ActualDCost);
         this.TotalVariance =Math.Round(VariancePaperCost+VariancePrintCost+VarianceDCost);
         this.TotalForecast=Math.Round(PaperForecast+PrintForecast+DForecast);        
        }    
}
       
public class CompanyReportWrapper
{
       
        public String BudgetMonth{get;set;}
        public Decimal ActualPaperCost{get;set;}
        public Decimal BudgetPaperCost{get;set;}
        public Decimal PaperForecast{get;set;}
        public Decimal VariancePaperCost{get;set;}
        public Decimal ActualPrintCost{get;set;}
        public Decimal BudgetPrintCost{get;set;}
        public Decimal PrintForecast{get;set;}
        public Decimal VariancePrintCost{get;set;}
        public Decimal ActualDCost{get;set;}
        public Decimal BudgetDCost{get;set;}
        public Decimal DForecast{get;set;}
        public Decimal VarianceDCost{get;set;}
        public Decimal TotalBudget {get;set;}
        public Decimal TotalActual {get;set;}
        public Decimal TotalVariance {get;set;}
        public CompanyReportWrapper(String BudgetMonth, Decimal ActualPaperCost,Decimal BudgetPaperCost,Decimal PaperForecast,Decimal ActualPrintCost,Decimal BudgetPrintCost,Decimal PrintForecast,Decimal ActualDCost,Decimal BudgetDCost,Decimal DForecast)
        {
         if(ActualPaperCost==null){ActualPaperCost=0;}
         if(BudgetPaperCost==null){BudgetPaperCost=0;}
         if(ActualPrintCost==null){ActualPrintCost=0;}
         if(BudgetPrintCost==null){BudgetPrintCost=0;}
         if(ActualDCost==null){ActualDCost=0;}
         if(BudgetDCost==null){BudgetDCost=0;}
         this.BudgetMonth=BudgetMonth;
         this.ActualPaperCost=Math.Round(ActualPaperCost);
         this.BudgetPaperCost=Math.Round(BudgetPaperCost);
         this.PaperForecast=PaperForecast;
         this.VariancePaperCost=Math.Round(BudgetPaperCost-ActualPaperCost);
         this.ActualPrintCost=Math.Round(ActualPrintCost);
         this.BudgetPrintCost=Math.Round(BudgetPrintCost);
         this.PrintForecast=PrintForecast;
         this.VariancePrintCost=Math.Round(BudgetPrintCost-ActualPrintCost);
         this.ActualDCost=Math.Round(ActualDCost);
         this.BudgetDCost=Math.Round(BudgetDCost);
         this.DForecast=DForecast;
         this.VarianceDCost=Math.Round(BudgetDCost-ActualDCost);
         this.TotalBudget =Math.Round(BudgetPaperCost+BudgetPrintCost+BudgetDCost);
         this.TotalActual=Math.Round(ActualPaperCost+ActualPrintCost+ActualDCost);
         this.TotalVariance =Math.Round(VariancePaperCost+VariancePrintCost+VarianceDCost);       
        }    
}

public class PagesWrapper
{
       
        public String Type{get;set;}
        public Decimal Actual{get;set;}
        public Decimal Budget{get;set;}
        public Decimal Variance{get;set;}
        public Decimal Forecast{get;set;}
        public Budget__c bdgtObj{get;set;}
        public PagesWrapper(String Type,Decimal Actual,Decimal Budget,Budget__c bdgtObj)
        {
          if(Actual==null){Actual=0;}
          if(Budget==null){Budget=0;}
          this.Type=Type;
          this.Actual=Math.round(Actual);
          
          this.Budget=Math.round(Budget);
          if(this.Actual!=null && this.Budget!=null)
          {
            this.Variance=Math.Round(this.Budget-this.Actual);
          }
          this.Forecast=Forecast;
          this.bdgtObj=bdgtObj;
        }    
}

public class BudgetWrapper
{
       
        public String Type{get;set;}
        public Decimal Actual{get;set;}
        public Decimal Budget{get;set;}
        public Decimal Variance{get;set;}
        public Decimal Forecast{get;set;}
        public Budget__c bdgtObj{get;set;}
        public BudgetWrapper(String Type,Decimal Actual,Decimal Budget,Budget__c bdgtObj)
        {
          if(Actual==null){Actual=0;}
          if(Budget==null){Budget=0;}
          this.Type=Type;
          this.Actual=Math.round(Actual);
          
          this.Budget=Math.round(Budget);
          if(this.Actual!=null && this.Budget!=null)
          {
            this.Variance=Math.Round(this.Budget-this.Actual);
          }
          this.Forecast=Forecast;
          this.bdgtObj=bdgtObj;
        }    
}
}