@IsTest (SeeAllData=true)
public class BoostAccountRenewalControllerTest{
    static testmethod void BoostRenewalTest(){
         
        Id RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityLockRTName, CommonMessages.opportunityObjectName);    
        Database.BatchableContext bc;

        Account newCustomerAccount = TestMethodsUtility.generateAccount('customer');
        newCustomerAccount.Open_claim__c = false;
        newCustomerAccount.Delinquency_Indicator__c= false;
        newCustomerAccount.Open_or_Closed_Claim_Past_18_Months__c = false;
        insert newCustomerAccount;
        
       
        Contact newContact = TestMethodsUtility.generateContact(newCustomerAccount.Id);        
        insert newContact;
        
        
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newCustomerAccount);
        
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        
        List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
        
        Directory_Edition__c newDE1 = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newCustomerAccount.Primary_Canvass__c, null, null);
        newDE1.Book_Status__c = 'BOTS';
        newDE1.Pub_date__c = system.today().adddays(1);
        newDE1.Bill_prep__c = system.today().adddays(1);
        newDE1.New_Print_Bill_Date__c = system.today().adddays(1);
        newDE1.Final_Auto_Renew_Job__c= System.today();
        lstDE.add(newDE1);
        insert lstDE;
        
        Directory_Edition__c newDE2 = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newCustomerAccount.Primary_Canvass__c, null, null);
        newDE1.Book_Status__c = 'BOTS-1';
        insert newDE2;
        
        Product2 newProduct = TestMethodsUtility.createproduct();
        
        Order__c newOrder = TestMethodsUtility.createOrder(newCustomerAccount.Id);
        
        Order_Group__c newOG = TestMethodsUtility.createOrderSet(newCustomerAccount, newOrder, newOpportunity);
        
        List<Order_Line_Items__c> lstOLI = new List<Order_Line_Items__c>();
        Order_Line_Items__c newOLI1 = TestMethodsUtility.generateOrderLineItem(newCustomerAccount, newContact, newOpportunity, newOrder, newOG);
        newOLI1.Product2__c=newProduct.Id;
        newOLI1.Order_Anniversary_Start_Date__c = date.today();
        newOLI1.is_p4p__c=false;
        newOLI1.Directory_Edition__c = newDE2.Id;
        newOLI1.UnitPrice__c=100;
        newOLI1.Is_Handled__c = false;
        
        lstOLI.add(newOLI1);
        
        insert lstOLI;
        //system.assertequals(null,lstOLI[0].Opportunity__c);
        Set<Id> setOLIId = new Set<Id>();
        for(Order_Line_Items__c OLI : lstOLI){
            setOLIId.add(OLI.Id);   
        }
        
        
        map<Id, list<Order_Line_Items__c>> mapTempOLI = new map<Id, list<Order_Line_Items__c>>();
        
        Test.startTest();
        BoostRenewalAccountsBatchController batchBR = new BoostRenewalAccountsBatchController(new set<Id>{newOpportunity.Id});
        batchBR.start(bc);
        batchBR.execute(bc,new list<Opportunity>{newOpportunity});
        
        BoostRenewalScheduler lrRS= new BoostRenewalScheduler();
        schedulableContext sc;
        lrRS.execute(sc);
        BoostRenewalAccountManualSchedule brms = new BoostRenewalAccountManualSchedule();
        brms.doRun();
        Test.stopTest();
        }
}