public with sharing class coOpCustomSearch{
 
  public String countMessage {get;set;}
  // the soql without the order and limit
  private String soql {get;set;}
  // the collection of contacts to display
  public List<Co_Ops__c> coOpList {get;set;}
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
 
  // init the controller and display some sample data when the page loads
  public coOpCustomSearch() {
    //soql = 'select Id,Name, Company__c, Classified_Headings__c,Status__c from Co_Ops__c ';
    //runQuery();
  }
  
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
 
   // try {
    system.debug('ee======'+soql + ' order by ' + sortField + ' ' + sortDir + ' limit 50');
      coOpList = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 50');
   // } catch (Exception e) {
   //   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
   // }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String coOpname = Apexpages.currentPage().getParameters().get('coOpname');
    String countSql ='select Id FROM Co_Ops__c';
    soql = 'select Id,Name, Company__c, Classified_Headings__c,Status__c,StatusFormula__c from Co_Ops__c ';
    if (coOpname!=null && coOpname!='')
    {
      soql += ' WHERE (Name LIKE \'%'+String.escapeSingleQuotes(coOpname)+'%\' OR Classified_Headings__c LIKE \'%'+String.escapeSingleQuotes(coOpname)+'%\')';
      countSql+=' WHERE (Name LIKE \'%'+String.escapeSingleQuotes(coOpname)+'%\' OR Classified_Headings__c LIKE \'%'+String.escapeSingleQuotes(coOpname)+'%\')';
    }
    system.debug('countSql====='+countSql);
    List<Co_Ops__c> lstCoOp = Database.query(countSql);
    Integer counter = 0;
    if(lstCoOp!=null && lstCoOp.size()>0)
    {
        system.debug('lstCoOp.size()=========='+lstCoOp.size());
        counter=lstCoOp.size();
    }
    system.debug('counter====='+counter);
    if(counter!=null && counter>50)
    {
        countMessage='First 50 matches displayed only. Please refine your search or see the All list view to see other results.';
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,countMessage));
    }
    // run the query again
    runQuery();
 
    return null;
  }
  
  //return to co-op__c object list view pagae
  public PageReference doRedirect()
  {
      PageReference ref = new PageReference('/a3D?fcf=00BZ0000000a4VB');
      return ref;
  }
}