public class OrderLineItemUpdateTriggerBatch implements Database.Batchable<sObject>{
    
    public Boolean bolStopExecution;
    
    public OrderLineItemUpdateTriggerBatch(){
    }
    
    public OrderLineItemUpdateTriggerBatch(Boolean bolStopExec ){
        bolStopExecution = bolStopExec;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
       
        //String query = 'Select o.Account__c, o.OLI_Account_Dashboard_Pending__c From Order_Line_Items__c o where o.Core_Opportunity_Line_ID__c!=null and o.Media_Type__c =\'Digital\' and o.Talus_Go_Live_Date__c != null and o.Account__r.Dashboard_Account__c=false AND o.Account__r.Dashboard_Pending__c = false';
        //String query = 'Select o.action_code__c, o.Talus_Go_Live_Date__c,o.Media_Type__c, o.Payment_Duration__c, o.Successful_Payments__c, o.Continious_Billing__c, o.Payments_Remaining__c,o.Account__c, o.OLI_Account_Dashboard_Pending__c,o.Account__r.Dashboard_Account__c, o.Account__r.Dashboard_Pending__c From Order_Line_Items__c o where o.Core_Opportunity_Line_ID__c!=null and o.Media_Type__c =\'Digital\'';
        String query = 'Select o.action_code__c, o.Talus_Go_Live_Date__c,o.Media_Type__c, o.Payment_Duration__c, o.Successful_Payments__c, o.Continious_Billing__c, o.Payments_Remaining__c,o.Account__c, o.OLI_Account_Dashboard_Pending__c,o.Account__r.Dashboard_Account__c, o.Account__r.Dashboard_Pending__c From Order_Line_Items__c o where o.Core_Opportunity_Line_ID__c!=null and o.Media_Type__c =\'Digital\'  and ((o.Talus_Go_Live_Date__c != null and o.Account__r.Dashboard_Account__c=false AND o.Account__r.Dashboard_Pending__c = false) or (o.action_code__c != \'Cancel\' and o.Payments_Remaining__c = 0 and o.Media_Type__c = \'Digital\' and o.Continious_Billing__c = false))';
        return Database.getQueryLocator(query);
    }
    
     public void execute(Database.BatchableContext bc, List<Order_Line_Items__c> listOLI) {
        List<Order_Line_Items__c> listUpdtOLI = new List<Order_Line_Items__c>();
        if(listOLI!=null && listOLI.size()>0){
            Map<Id, Account> mapAccount = new Map<Id, Account>();
            for (Order_Line_Items__c iterator : listOLI) {
                if(iterator.Talus_Go_Live_Date__c != null && iterator.Account__r.Dashboard_Account__c==false && iterator.Account__r.Dashboard_Pending__c == false && !iterator.OLI_Account_Dashboard_Pending__c) {
                    if(!mapAccount.containsKey(iterator.Account__c)) {
                        mapAccount.put(iterator.Account__c, new Account(Id = iterator.Account__c, Dashboard_Pending__c = true));
                    }
                }
                
                if(iterator.action_code__c != 'Cancel' && iterator.Payments_Remaining__c == 0 && iterator.Media_Type__c == 'Digital' && iterator.Continious_Billing__c == false) {
                    listUpdtOLI.add(new Order_Line_Items__c(id=iterator.id, Continious_Billing__c = true));
                }
            }
            
            if(mapAccount!=null && mapAccount.size()>0){ 
                update mapAccount.values();
            }
            
            if(listUpdtOLI!=null && listUpdtOLI.size()>0){ 
                update listUpdtOLI;
            }
                        
            /*Map<Id, Account> mapAccount = new Map<Id, Account>();
                         
            for (Order_Line_Items__c oli : listOLI) {
                mapAccount.put(oli.Account__c, new Account(Id = oli.Account__c, Dashboard_Pending__c = true));  
            }
            
            if(mapAccount!=null && mapAccount.size()>0){ 
                update mapAccount.values();
            }*/
        }     
            
     }
     
     public void finish(Database.BatchableContext bc) {
        if(bolStopExecution != true) {
            OrderLineItemUpdateTriggerBatch objBatch = new OrderLineItemUpdateTriggerBatch(true);
            Database.executeBatch(objBatch, 500);
        }
    }

}