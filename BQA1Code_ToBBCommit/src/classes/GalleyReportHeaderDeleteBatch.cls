global class GalleyReportHeaderDeleteBatch Implements Database.Batchable <sObject> {
	global Database.queryLocator start(Database.BatchableContext bc){
		String SOQL = 'SELECT Id FROM Galley_Report_Header__c';
		return Database.getQueryLocator(SOQL);
	}

	global void execute(Database.BatchableContext bc, list<Galley_Report_Header__c> listGalleyReportHeader) {
		delete listGalleyReportHeader;
		Database.emptyRecycleBin(listGalleyReportHeader); 
	}

	global void finish(Database.BatchableContext bc) {		 
	}
}