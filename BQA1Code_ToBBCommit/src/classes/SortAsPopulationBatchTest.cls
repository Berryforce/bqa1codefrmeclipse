@isTest
public class SortAsPopulationBatchTest {
    static testMethod void sortAsBatchTest() {
        Test.StartTest();   
        Account a = TestMethodsUtility.generateCustomerAccount();
        a.Sensitive_Heading__c = false;
        insert a;             
        Contact cnt = TestMethodsUtility.createContact(a);               
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', a);                      
        Order__c ord = TestMethodsUtility.createOrder(a.Id);                
        Order_Group__c og = TestMethodsUtility.createOrderSet(a, ord, oppty);     
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Print_Specialty_Product_Type__c = 'Banner';
        product2.Family = 'Print';
        product2.Trademark_Product__c = 'Trademark Finding Line';
        insert product2;   
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Order_Line_Items__c OLI = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        oln.Product2__c = product2.Id;
        //oln.Sort_As__c = 'test';
        oln.Listing__c = listing.Id;
        oln.Sort_As__c = 'Test';
        oln.Anchor_Listing_Caption_Header__c = OLI.Id;
        insert oln;        
        oln.Sort_As__c = 'Test1';
        update oln;
                
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        String jobId = System.schedule('ScheduleSortAsPopulationBatch', CRON_EXP, new ScheduleSortAsPopulationBatch() );
        Test.StopTest(); 
    }
}