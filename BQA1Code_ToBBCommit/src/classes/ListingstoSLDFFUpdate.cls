global class ListingstoSLDFFUpdate{
	webservice static void LsttoSLDFFUpdate(string lstId){
		ListingstoSLDFFUpdateBatchController lstSLDFFbatch = new ListingstoSLDFFUpdateBatchController(lstId);
		database.executebatch(lstSLDFFbatch);	
	}
}