@isTest
private class FulfillmentCronJobsControllerTest {
	
    public static testMethod void flmntCronJobTest() {

        List<YPC_AddOn__c> lstYPCAddons = new List<YPC_AddOn__c>();

        Test.startTest();

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.UDAC__c = 'BRZ';
        dff.add_ons__c = 'CSTLK;YPCP';
        dff.udac_points__c = 'VCTLK';
        dff.Talus_Subscription_Id__c = '2525';
        dff.Account__c = a.Id;
        insert dff;

        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'CSTLK', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'CSTLK', 'Cancel'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'YPCP', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'YPCP', 'Cancel'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'VCTLK', 'New'));
        lstYPCAddons.add(CommonUtility.createYPCAddonTest(String.valueof(dff.Id), 'RB5', 'New'));

        insert lstYPCAddons;
        
        FulfillmentCronJobsController fpCronJobs = new FulfillmentCronJobsController();
        fpCronJobs.ypcBatch();
        fpCronJobs.ypcDMBatch();
        fpCronJobs.fulmntCncl();

        Test.stopTest();
    }
	
}