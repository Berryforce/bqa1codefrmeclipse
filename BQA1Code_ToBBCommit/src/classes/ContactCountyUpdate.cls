global class ContactCountyUpdate implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts{
    
    public static Contact con{get;set;}
      public Integer count=0;                       // Vairable for count
      
    // smart street response fields
      public static string street_name;             //for capture stree_name       
      public static string primary_number;          //for capture primarynumber
      public static string treet_suffix;            //for capture treetsuffix
      public static string city_name;               //for capture cityname
      public static string zipcode1;                //for capture zipcode
      public static string input_index;             //for capture inputindex
      public static string candidate_index;         //for capture candidateindex
      public static string delivery_line_1;         //for capture deliveryline1
      public static string last_line;               //for capture lastline
      public static string delivery_point_barcode;  //for capture delivery point barcode
      public static string county_name;             //for capture county name
      public static string county_fips;             //for capture county fips
      public static string plus4_code;              //for capture plus4code
      public static string active;                  //for capture active
      public static string validaddress;            //for capture validaddress
      public static string state_abbreviation;      //for capture state abbreviation
      public static string record_type;             //for capture record type
      public static string zip_type;                //for capture zip type
      public static string carrier_route;           //for capture carrier route
      public static string rdi;                     //for capture rdi
      public static double latitude;                //for capture latitude
      public static double longitude;               //for capture longitude
      public static string precision;               //for capture precision
      public static string time_zone;               //for capture timezone
      public static string utc_offset;              //for capture utc offset
      public static string dst;                     //for capture dst
      public static string dpv_match_code;          //for capture dpvmatchcode
      public static string dpv_footnotes;           //for capture dpvfootnotes
      public static string dpv_cmra;                //for capture dpvcmra
      public static string dpv_vacant;              //for capture dpvvacant
      public static string footnotes;               //for capture footnotes
      public static string URLForPage;              //for capture urlforpage
      public static string delivery_point_check_digit;//for capture delivery point check digit
      public static string delivery_point;          //for capture delivery point
      public static string congressional_district;  //for capture congressiondistrict
      public static string elot_sequence;           //for capture elotsequence
      public static string elot_sort;               //for capture elotsort
      public static JSONParser parser;              //Parsing the response from smart street
      public static string ews_match;                // for capture ews_match  
      public static string addressee;               // for capture addressess
      public static string LACSLink_Code;           //for capture LACSLink_Code
      public static string LACSLink_Match_Indicator;//for capture LACSLink_Match_Indicator
      public static string SuiteLink_Match;         //for capture SuiteLink_Match
      public static string building_default_indicator;//for capture building_default_indicator
      public static string pmb_number;              //for capture pmb_number
      public static string pmb_designator;          //for capture pmb_designator
      public static string delivery_line_2;         //for captue delivery_line_2
      
      global ContactCountyUpdate(){
    }
    
    
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String strStateLabel = label.State_ContactCountyUpdate;
        //String SOQL = 'select id from Contact where County_Name__c = null and createdbyid = \'005G0000005hhf4\' and Do_Not_Validate_Address__c = false and MailingState in : strStateLabel and id = \'003Z000001OxFL9\'';
        String SOQL = 'select id from Contact where County_Name__c = null and PrimaryBilling__c = true and Do_Not_Validate_Address__c = false and MailingState = \'OK\'';
        //String SOQL = 'select id from Contact where  id = \'003Z000001OxFL9\'';
        
        System.debug('Testingg SOQL '+SOQL);
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> lstContact){
        try {
            for(Contact objContact : lstContact) {
                Id id = objContact.id;
                con =[select id,name,mailingstate,mailingcity,mailingStreet,mailingpostalcode,mailingCountry,Street_Status__c ,description from contact where id =:id];
        System.debug('Testingg con '+con);
        Boolean bolValid = false;
        //construct an HTTP request
        HttpRequest req = new HttpRequest();
        HttpResponse res;
        String Url = 'https://api.smartystreets.com/street-address?street='+con.mailingStreet+'&city='+con.mailingcity+'&state='+con.mailingState+'&zipcode='+con.mailingpostalcode+'&candidates=5&auth-id=ad36b28c-2b30-d1e0-4d25-e28af101b7a7&auth-token=jLI2rTAr4NFNJeKK8QG3p7J2mNGLqDCFQ8PxOZZ7sPPI3s9FqILlQ%2FQvdZFMtNZJollBnhDHvfLWmotF2x8fNA%3D%3D';
        Url = Url.replace(' ','+');
        String urll = Url;
        req.setEndpoint(Url);
        req.setMethod('GET');
        
        //send the request
        Http http = new Http();
        System.debug('Testingg req '+req);
        
        res = http.send(req);
        
        System.debug('Testingg res '+res);
         updateHelper.inFutureContext = true;
         if(res.getStatusCode()==200)
          {
            System.debug('Testingg res.getBody() '+res.getBody());
            /*if(res.getBody()== null) {
                System.debug('Testingg Inside Iff');
            }
            else if(res.getBody()== '') {
                System.debug('Testingg Inside elsee 1');
            }
            else if(String.isBlank(res.getBody())){
                System.debug('Testingg Inside elsee 2');
            }
            else if(res.getBody().equals('[]')){
                System.debug('Testingg Inside elsee 4'+res.getBody()+'Inside');
            }
            else {
                System.debug('Testingg Inside elsee');
            }*/
            
            //System.debug('Testingg res.getBodyDocument() '+res.getBodyDocument());
            parser = JSON.createParser(res.getBody());
            System.debug('Testingg parser.nextToken() '+parser.nextToken());
            /*if(res.getBody()!='[]')
            {
             */
             System.debug('Testingg parser '+parser);
             System.debug('Testingg parser hasCurrentToken '+parser.hasCurrentToken());
              while (parser.nextToken() != null) {
                
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    bolValid = true;
                    String text = parser.getText();
                    parser.nextToken();
                    System.debug('Testingg text '+text);
                    System.debug('text*************1'+text);
                        if (text == 'input_index') {
                            input_index = parser.getText();
                        }
                        if (text == 'candidate_index') {
                            candidate_index = parser.getText();
                        } 
                        if (text == 'delivery_line_1') {
                            delivery_line_1 = parser.getText();
                        } 
                         if(text=='last_line'){
                            last_line=parser.getText();
                         }
                         if(text=='delivery_point_barcode'){
                            delivery_point_barcode=parser.getText();
                         }
                         if (text == 'primary_number') {
                            primary_number = parser.getText();
                         }
                        if(text=='street_name'){
                            street_name=parser.getText();
                         }
                         if(text=='street_suffix'){
                            treet_suffix=parser.getText();
                        }
                          if(text=='city_name'){
                              city_name=parser.getText();
                         }
                         if(text=='state_abbreviation'){
                            state_abbreviation=parser.getText();
                            
                        }
                          if(text=='zipcode'){
                                    zipcode1=parser.getText();
                        }
                        if(text=='plus4_code'){
                                plus4_code=parser.getText();               
                        }
                        if(text=='delivery_point'){
                            delivery_point=parser.getText();
                        }
                        if(text=='delivery_point_check_digit'){
                            delivery_point_check_digit=parser.getText();
                        } 
                         if(text=='record_type'){
                            record_type=parser.getText();
                        }
                        if(text=='zip_type'){
                            zip_type=parser.getText();
                        }
                         if(text=='county_fips'){
                            county_fips=parser.getText();
                        }
                        if(text=='county_name'){
                               county_name=parser.getText();
                        }
                        if(text=='carrier_route'){
                            carrier_route=parser.getText();
                        }
                        if(text=='congressional_district'){
                            congressional_district=parser.getText();
                        }
                         if(text=='rdi'){
                            rdi=parser.getText();
                        }         
                        if(text=='elot_sequence'){
                            elot_sequence=parser.getText(); 
                        }
                        if(text=='elot_sort'){
                            
                            elot_sort=parser.getText();
                        }
                         if(text=='latitude'){
                            latitude=parser.getDoubleValue();
                        }
                        if(text=='longitude'){
                            longitude=parser.getDoubleValue();
                        }
                        if(text=='precision'){
                            precision=parser.getText();
                        }
                         if(text=='time_zone'){
                            time_zone=parser.getText();
                        }
                         if(text=='utc_offset'){
                                utc_offset=parser.getText();
                        }
                          if(text=='dst')
                        {
                            dst=parser.getText();
                        } 
                        if(text=='dpv_match_code'){
                            dpv_match_code=parser.getText();
                        }
                        if(text=='dpv_footnotes'){
                            dpv_footnotes=parser.getText();
                        }
                        if(text=='dpv_cmra'){
                            dpv_cmra=parser.getText();
                        }
                        if(text=='dpv_vacant'){
                            dpv_vacant=parser.getText();
                        }
                         if(text=='active'){
                            active=parser.getText();
                        }
                        if(text=='footnotes'){    
                            footnotes=parser.getText();
                        }
                        if(text=='ews_match'){
                             ews_match =parser.getText();
                        }
                        if(text=='addressee')
                        {
                             addressee=parser.getText();
                        }
                        if(text=='LACSLink_Code'){    
                           LACSLink_Code=parser.getText();
                        }
                 
                        if(text=='LACSLink_Match_Indicator'){    
                            LACSLink_Match_Indicator=parser.getText();
                                        }
                        if(text=='SuiteLink_Match'){    
                            SuiteLink_Match=parser.getText();
                                        }
                        if(text=='building_default_indicator'){    
                           building_default_indicator=parser.getText();
                                        }
                        if(text=='pmb_number'){    
                           pmb_number=parser.getText();
                                        }
                        if(text=='pmb_designator'){    
                            pmb_designator=parser.getText();
                                        }
                        if(text=='delivery_point_barcode'){    
                            delivery_point_barcode=parser.getText();
                                        }
                        if(text=='delivery_line_2'){
                        
                        delivery_line_2 =parser.getText();
                        }                
                          
                }
           }
                
                if(bolValid) {
                    System.debug('Vertex-SmartyStreets**county_name**********'+county_name+'county_fips********'+county_fips+'active**********'+active+'state_abbreviation************'+state_abbreviation+'record_type**************'+record_type+'zip_type********'+zip_type+'carrier_route***********'+carrier_route+'rdi***********'+rdi+'latitude*********'+latitude+'time_zome*********'+time_zone+'delivery_point_check_digit***********'+delivery_point_check_digit);
                    System.debug('Vertex-SmartyStreets**utc_offset*********'+utc_offset+'dst***********'+dst+'dpv_match_code**********'+dpv_match_code+'dpv_footnotes**********'+dpv_footnotes+'dpv_cmra************'+dpv_cmra+'dpv_vacant************'+dpv_vacant+'footnotes************'+footnotes);
                    zipcode1=zipcode1+'-'+plus4_code;
                
                    con.Latitude__latitude__s=Double.valueOf(latitude); 
                    con.Latitude__longitude__s=Double.valueOf(longitude);
                    con.County_FIPS_Code__c= county_fips;
                    con.County_Name__c =county_name;
                    con.Geolocation_Precision__c =precision;
                    con.Daylight_Savings_Observed__c = dst ;
                    con.Delivery_Point_Validation_Code__c = dpv_match_code;
                    con.Type_of_Zip_Code__c=zip_type;
                    con.Delivery_Point_Vacant__c=dpv_vacant;
                    con.Address_Changes_Made_Codes__c =footnotes;
                    con.Residential_Delivery_Indicator__c=rdi; 
                    con.LACSLink_Code__c=LACSLink_Code;
                    con.LACSLink_Match_Indicator__c=LACSLink_Match_Indicator;
                    con.SuiteLink_Match__c=SuiteLink_Match;
                    con.Building_Default_Indicator__c=building_default_indicator;
                    con.POSTNET_Barcode__c=delivery_point_barcode;
                    con.Private_Mailbox_Number__c=pmb_number;
                    con.Private_Mailbox_Unit_Designator__c=pmb_designator;
                    con.Uses_Commercial_Mail_Receiving_Agency__c=dpv_cmra;
                    con.DPV_Reason_Code_s__c=dpv_footnotes;
                    con.Type_of_Address__c=record_type;
                    con.Address_is_Active__c =active;
                    con.Address_Not_Ready_for_Mail__c=ews_match;
                    con.UTC_Offset__c =utc_offset;
                    con.Carrier_Route__c = carrier_route; 
                    con.Time_Zone__c=time_zone;
                    con.Addressee__c=addressee;
                    con.mailingStreet=delivery_line_1;
                    con.mailingCity=city_name;
                    con.mailingstate=state_abbreviation;
                    con.mailingPostalCode=zipcode1; 
                     
                    //these filds for validation purpose
                    con.Street_Status__c ='valid';
                
                
                
                /*  
                  Missing filed from response
               
                con.Address_Changes_Made_Codes__c = footnotes;
                con.Address_Not_Ready_for_Mail__c = ews_match; 
                con.Addressee__c=addressee
                */
                if(delivery_line_2!=null){
                    con.MailingStreet=delivery_line_1+delivery_line_2;
                 }
                
            
             
                    updateHelper.inFutureContext = true;   
                    update con;
                
             }
            //}
          
           else{
               
                con.Latitude__latitude__s=Double.valueOf(00.000); 
                con.Latitude__longitude__s=Double.valueOf(00.000);
                con.County_FIPS_Code__c= '';
                con.County_Name__c ='';
                con.Geolocation_Precision__c ='';
                con.Daylight_Savings_Observed__c ='' ;
                con.Delivery_Point_Validation_Code__c = '';
                con.Type_of_Zip_Code__c='';
                con.Delivery_Point_Vacant__c='';
                con.Address_Changes_Made_Codes__c ='';
                con.Residential_Delivery_Indicator__c=''; 
                con.LACSLink_Code__c='';
                con.LACSLink_Match_Indicator__c='';
                con.SuiteLink_Match__c='';
                con.Building_Default_Indicator__c='';
                con.POSTNET_Barcode__c='';
                con.Private_Mailbox_Number__c='';
                con.Private_Mailbox_Unit_Designator__c='';
                con.Uses_Commercial_Mail_Receiving_Agency__c='';
                con.DPV_Reason_Code_s__c='';
                con.Type_of_Address__c='';
                con.Address_is_Active__c ='';
                con.Address_Not_Ready_for_Mail__c='';
                con.UTC_Offset__c ='';
                con.Carrier_Route__c =''; 
                con.Time_Zone__c='';
                /*con.mailingStreet='';
                con.mailingCity='';
                con.mailingstate='';
                con.mailingPostalCode='';
                con.mailingCountry=''; */
                
                 //these filds for validation purpose
               
                con.Street_Status__c ='invalid';
                
            
                    updateHelper.inFutureContext = true;   
                    update con;
                
          }
    }
                    else
                    {
                        con.Street_Status__c = 'BadRequest';    
                        
                                updateHelper.inFutureContext = true;   
                                update con;
                            
    
                   }
                //SmartStreetContactVerification.updateContact(con.Id);
            }
        }catch(Exception objExp){
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+objExp.getTypename()+'. Error Message : '+objExp.getMessage(), objExp.getStackTraceString(), 'Contact County Update');
            
        }
    }
    
    global void finish(Database.BatchableContext bc){
        String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        /*String[] toAddresses = new String[] {a.CreatedBy.Email, 'msureshkuma2@csc.com'};*/
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Migrated Opportunity Records processing status ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'. Please check Exception records for any other errors that might have occured while processing Opportunities.');
        
    }
}