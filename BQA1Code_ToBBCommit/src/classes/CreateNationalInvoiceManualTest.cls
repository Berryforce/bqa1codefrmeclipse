@IsTest
public class CreateNationalInvoiceManualTest{
    public static testmethod void CreateNationalInvoiceManualTest(){
    	Directory__c dir = TestMethodsUtility.generateDirectory();
    	dir.Months_for_National__c = 12;
    	insert dir;
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        objNSOS.CMR_Name__c = objCMRAcc.id;
        
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = dir.Id;
        insert objNSOS;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        insert objNSLI;        
    
       /* list<National_Staging_Order_Set__c> lstNSOS = [Select CMR_Name__r.c2g__CODACreditStatus__c,CMR_Name__r.Manual_Delinquency_Indicator__c, ChildReadyForProcess__c,CMR_Name__r.Delinquency_Indicator__c,CMR_Name__r.Bankruptcy__c,CMR_Name__r.Is_Active__c,Child_Total__c,Transaction_Version__c, Transaction_Version_ID__c, Transaction_Unique_ID__c, Transaction_ID__c,  TRANS_Code__c, Publication_Date__c, 
                Publication_Company__c, Publication_Code__c, Name, NAT__c, NAT_Client_Id__c, Id, Directory__c, Directory_Number__c, Directory_Edition__c, 
                Directory_Edition_Number__c, Date__c, Client_Number__c, Client_Name__c, CMR_Number__c, CMR_Name__c, CMR_Name__r.Name, Auto_Number__c, Opportunity__c, 
                (Select Id, Name, Line_Number__c, Action__c, UDAC__c, Product2__c, BAS__c, DAT__c, SPINS__c, Advertising_Data__c, Full_Rate_f__c, Sales_Rate_f__c, 
                Row_Type__c, Type__c, Discount__c, Ready_for_Processing__c, Line_Error_Description__c, National_Staging_Header__c, 
                Parent_ID__c, Transaction_Unique_ID__c, Auto_Number__c, Listing__c, Scoped_Listing__c, Directory_Section__c, Directory_Heading__c From National_Staging_Line_Items__r 
                Where UDAC__c != null order by Line_Number__c)
                From National_Staging_Order_Set__c where id =:objNSOS.id];
        
        objNSOS = lstNSOS[0];
        System.debug('Testingg objNSOS '+objNSOS.CMR_Name__r.Is_Active__c);
        System.debug('Testingg objNSOS '+objNSOS.CMR_Name__r.Bankruptcy__c);
        System.debug('Testingg objNSOS '+objNSOS.CMR_Name__r.Delinquency_Indicator__c);
        System.debug('Testingg objNSOS '+objNSOS.CMR_Name__r.Manual_Delinquency_Indicator__c );
        System.debug('Testingg objNSOS '+objNSOS.CMR_Name__r.c2g__CODACreditStatus__c);
        System.debug('Testingg objNSOS '+objNSOS.Child_Total__c);
        System.debug('Testingg objNSOS '+objNSOS.ChildReadyForProcess__c);*/
        PageReference pageRef = Page.CreateNationalInvoiceManualPage_V4;
        Test.setCurrentPage(pageRef);
        Order_Group__c objOS = new Order_Group__c(name = 'TestOS');
        ApexPages.StandardController oppCtrl = new ApexPages.standardController(objOS);
        
        CreateNationalInvoiceManual_V5 objController = new CreateNationalInvoiceManual_V5(oppCtrl);
        objController.createOpportunityAndOrder();
    }
}