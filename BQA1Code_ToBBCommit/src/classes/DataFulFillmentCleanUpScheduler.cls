global class DataFulFillmentCleanUpScheduler implements Schedulable {
   public Interface DataFulFillmentCleanUpSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('DataFulFillmentCleanUpSchedulerHndlr');
        if(targetType != null) {
            DataFulFillmentCleanUpSchedulerInterface obj = (DataFulFillmentCleanUpSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}