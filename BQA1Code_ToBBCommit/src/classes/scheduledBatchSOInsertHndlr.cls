global class scheduledBatchSOInsertHndlr implements scheduledBatchSOInsert.scheduledBatchSOInsertInterface {
     global void execute(SchedulableContext sc) {
      ServiceOrderBatchController  b = new ServiceOrderBatchController(); 
      database.executebatch(b);
   } 
}