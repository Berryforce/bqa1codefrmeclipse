public with sharing class LocalBillingCreateStatement {
	public Dummy_Object__c objDummy {get;set;}
	public boolean bShowLateFee{get;set;}
	public boolean bShowStmtSummary {get;set;}
	@Testvisible set<Id> setAccountID;
	string[] selectedcheckboxes=new string[]{};
	@Testvisible Id batchId;
	@Testvisible Id SCNbatchId;
	@Testvisible Id lateFeeBatchId;
	@Testvisible Id createStmtBatchId;
	@Testvisible Set<Id> setBatchIds = new set<Id>();
	public String strStmtCreationBatchStatus {get;set;}
	public boolean bStmtCreationEnabledPoller {get;set;}
	//Report Ids Custom Settings 
	public Id rptStmtReportId {get;set;}
	public Id StmtSINFalloutReportId {get;set;}
	public Id StmtSCRFalloutReportId {get;set;}
	public Id StmtCELIFalloutReportId {get;set;}

	public LocalBillingCreateStatement() {
		objDummy = new Dummy_Object__c();
		Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
		if(reportIds.get('StatementReport')!=null) rptStmtReportId = reportIds.get('StatementReport').Report_Id__c;
		if(reportIds.get('StmtSCRFallout')!=null) StmtSCRFalloutReportId = reportIds.get('StmtSCRFallout').Report_Id__c;
		if(reportIds.get('StmtSINFallout')!=null) StmtSINFalloutReportId = reportIds.get('StmtSINFallout').Report_Id__c;
		if(reportIds.get('StmtCELIFallout')!=null) StmtCELIFalloutReportId = reportIds.get('StmtCELIFallout').Report_Id__c;		
		clearDatas();
	}

	private void clearDatas() {
		bShowLateFee=false;
		setAccountID=new set<Id>();
		strStmtCreationBatchStatus ='';
		bStmtCreationEnabledPoller =false;
		bShowStmtSummary = false;
	}

	public void clickGoNew() {
		if(objDummy.Date__c != null) {
			clearDatas();
			Integer DayofStatement = objDummy.Date__c.day();
			setAccountID = new set<Id>();
			List<Account> LstforCreateStatement =[Select Id, Name, Collection_Status__c, Final_Statement_Sent_Out__c From Account WHERE DAY_IN_MONTH(Billing_Anniversary_Date__c) = : DayofStatement AND RecordTypeId =:System.Label.TestAccountCustomerRT AND AR_Balance__c > 10.00 AND Statement_Suppression__c = false limit 10000];
			if(LstforCreateStatement.size() > 0) {
				bShowLateFee=true;
			}
		}
	}

	public void invokecreateStatementBatch() {
		if(!Test.isRunningTest()){
			setBatchIds = new set<Id>();
			String createStmtBatchIdNew;
			//added by Mythili for ticket CC-2200. 
			//Splitting single statement batch job into 3 jobs.
			List<StatementSplitBatch__c> statementBatchList=StatementSplitBatch__c.getall().values();
			if(statementBatchList!=null && statementBatchList.size()>0){
				for(StatementSplitBatch__c stmntBatch:statementBatchList){
					//String accStartNumber=stmntBatch.Account_Start_Number__c;
					//String accEndNumber=stmntBatch.Account_End_Number__c;
					//createStmtBatchId=Database.executeBatch(new LocalBillingCreateStmtBatch(objDummy.Date__c), 1);
					createStmtBatchIdNew = Database.executeBatch(new LocalBillingCreateStmtBatch(objDummy.Date__c,stmntBatch.Account_Start_Number__c,stmntBatch.Account_End_Number__c), 1);
					setBatchIds.add(createStmtBatchIdNew);
				} 
			}else{
				createStmtBatchIdNew = Database.executeBatch(new LocalBillingCreateStmtBatch(objDummy.Date__c),1);
				setBatchIds.add(createStmtBatchIdNew);
			}		  	
		}
		strStmtCreationBatchStatus ='Status of Statement Creation Batch : Queued';
		bStmtCreationEnabledPoller = true;		
	}

	public void apexCreateStmtJobStatus() {
		//Set<Id> setBatchIds = new Set<id>();
		//Boolean bStatus = false;
		Map<id,AsyncApexJob> mapBatchFFJob;
		/*if(createStmtBatchId!= null) {
			setBatchIds.add(createStmtBatchId);
		}*/
		
		boolean bStatus = LocalBillingCommonMethods.apexBatchJobStatus(setBatchIds);
    	if(bStatus) {
    		strStmtCreationBatchStatus= 'Status of Statement Creation Batch : Completed';
            bStmtCreationEnabledPoller = false;
			bShowStmtSummary = true;
        }
        else {
        	strStmtCreationBatchStatus= 'Status of Statement Creation Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
		/*if(setBatchIds.size() > 0) {
			mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID IN:setBatchIds]);
			integer iCount = 0;
			for(Id idBatch : setBatchIds) {
				if(mapBatchFFJob.get(idBatch) != null) {
					strStmtCreationBatchStatus = 'Status of Statement Creation Batch : ' + mapBatchFFJob.get(idBatch).Status;
					if(mapBatchFFJob.get(idBatch).Status == 'Completed') {
						//bStatus = true;
						iCount++;
					}
				}
			}
			/*if(mapBatchFFJob.containsKey(createStmtBatchId)) {
				strStmtCreationBatchStatus = 'Status of Statement Creation Batch : ' + mapBatchFFJob.get(createStmtBatchId).Status;
				if(mapBatchFFJob.get(createStmtBatchId).Status == 'Completed') {
					bStatus = true;
				}
			}
			else {
				bStatus = true;
			}*//*
			if(iCount == mapBatchFFJob.size()) {
				bStmtCreationEnabledPoller = false;
				bShowStmtSummary = true;
			}
		}*/
	}

	public void SendStmtFiletoWFOne() {
		List<Statement_Header__c> UpdateStmtHeaderList=new List<Statement_Header__c>();
		for(Statement_Header__c stmtIterator : [SELECT Account__c,Send_to_WF1__c, id FROM Statement_Header__c WHERE CreatedDate = TODAY]) {
			UpdateStmtHeaderList.add(new Statement_Header__c(id=stmtIterator.Id,Send_to_WF1__c=true));
		}
		if(UpdateStmtHeaderList.size()>0) {
			update UpdateStmtHeaderList;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Statements are sent to Workflow One');
			ApexPages.addMessage(myMsg);
		}
	}

	public List<SelectOption> getStmntCheckboxVal() {
		List<SelectOption> options = new List<SelectOption>(); 
		options.add(new SelectOption('0','Statement Summary (Balanced)')); 
		options.add(new SelectOption('1','Workflow One Summary (Balanced)')); 
		options.add(new SelectOption('2','Marketing Message Validated')); 
		return options; 
	}

	public String[] getcheckboxvalues() {
		return selectedcheckboxes;
	}

	public void setcheckboxvalues(String[] selectedcheckboxes) { 
		this.selectedcheckboxes= selectedcheckboxes;
	}
}