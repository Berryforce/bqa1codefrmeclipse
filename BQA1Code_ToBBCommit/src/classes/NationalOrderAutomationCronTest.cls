@IsTest(SeeAlldata=true)
public class NationalOrderAutomationCronTest {

  public static testMethod void NationalOrderAutomationCrontestMethod(){
        Test.startTest(); 
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        Recordtype rtId = [SELECT DeveloperName,Id,Name FROM RecordType where Name = 'Daily Service Orders'];
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        objCMRAcc.National_Credit_Status__c='Approved';
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        objDirEd.Directory__c = objDir.Id;
        insert objDirEd;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
       // Pricebook2 newPriceBook=[Select Id from Pricebook2 where isActive=true AND IsStandard=true Limit 1];
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        /*PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;*/
        
        Directory_Product_Mapping__c dirPM=new Directory_Product_Mapping__c(Directory__c=objDir.Id,Product2__c=objProd.Id);
        insert dirPM;
        
        service_order_stage__c objSO = new service_order_stage__c();
        objSO.Name = 'Test SO XXX';
        objSO.Action__c = 'New';
        objSO.Phone__c = '(288) 451-1021';
        objSo.Disconnected__c = false;
        objSO.Bus_Res_Gov_Indicator__c = 'Business';
        objSO.BOID__c = 1234;
        objSO.Directory__c = objDir.Id;
        objSO.Indent_Order__c=0;
        objSO.Indent_Level__c=3;
        objSO.Caption_Header__c = true;
        objSO.Directory_Section__c = objDS.Id;
        objSO.recordtypeId = rtId.Id;
        objSO.Telco_Provider__c=objTelco .id;
        objSO.BEX__c=2;
        objSO.Cross_Reference_Text__c='Test';
        objSO.Designation__c='SEO';
        objSO.Honorary_Title__c='TEST';
        objSO.Lineage_Title__c='TEST123';
        objSO.Listing_City__c='NY';
        objSO.Listing_State__c='TX';
        objSO.Listing_Street_Number__c='311';
        objSO.Listing_Street__c='St. Lane';
        objSO.Listing_Country__c='USA';
        objSO.Listing_PO_Box__c='232';
        objSO.Listing_Postal_Code__c='101';
        objSO.Listing_Street__c='NX';
        objSO.BOID__c=4;
        objSO.Bus_Res_Gov_Indicator__c='Government';
        objSO.Effective_Date__c=Date.parse('01/01/2013');
        objSO.First_Name__c='Test';
        objSO.SOS_LastName_BusinessName__c='test';
        insert objSO;      
        
        Listing__c objL = new Listing__c();
        objL.Name = objSO.name; 
        objL.Phone__c = objSO.Phone__c; 
        objL.Telco_Provider__c = objSO.Telco_Provider__c;
        objL.BEX__c = objSO.BEX__c;
        objL.Cross_Reference_Text__c = objSO.Cross_Reference_Text__c;
        objL.Designation__c = objSO.Designation__c;
        objL.Honorary_Title__c = objSO.Honorary_Title__c;
        objL.Lineage_Title__c = objSO.Lineage_Title__c;
        objL.Listing_City__c = objSO.Listing_City__c;
        objL.Listing_Country__c = objSO.Listing_Country__c;
        objL.Listing_PO_Box__c = objSO.Listing_PO_Box__c;
        objL.Listing_Postal_Code__c = objSO.Listing_Postal_Code__c;
        objL.Listing_State__c= objSO.Listing_State__c;
        objL.Listing_Street_Number__c= objSO.Listing_Street_Number__c;
        objL.Listing_Street__c= objSO.Listing_Street__c;
        objL.BOID__c= objSO.BOID__c;
        objL.Company__c= objSO.name;
        objL.Bus_Res_Gov_Indicator__c= objSO.Bus_Res_Gov_Indicator__c;
        objL.Effective_Date__c = objSO.Effective_Date__c;
        objL.First_Name__c= objSO.First_Name__c;
        objL.Manual_Sort_As_Override__c = objSO.Manual_Sort_As_Override__c;
        objL.Phone_Type__c = objSO.Phone_Type__c;
        objL.Primary_Canvass__c= objSO.Primary_Canvass__c;
        objL.Region__c= objSO.Region__c;
        objL.Secondary_Surname__c = objSO.Secondary_Surname__c;
        objL.Service_Order_Stage_Item__c = objSO.Id;
        objL.Service_Order__c = objSO.Service_Order__c;
        objL.Year__c= objSO.Year__c;
        objL.Phone_Override__c = objSO.Phone_Override__c;
        objL.Omit_Address_OAD__c= objSO.Omit_Address_OAD__c;
        objL.Right_Telephone_Phrase__c=objSO.Right_Telephone_Phrase__c;
        objL.Left_Telephone_Phrase__c=objSO.Left_Telephone_Phrase__c;
        objL.Telco_Sort_Order__c=objSO.Telco_Sort_Order__c;
        objL.Disconnected__c=true;
        insert objL; 
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        objNSOS.IsMigratedXtrans__c=true;
        objNSOS.Client_Name__c=newAccount.Id;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        objNSLI.Directory_Section__c=objDS.Id;
        objNSLI.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        objNSLI1.Directory_Section__c=objDS.Id;
        objNSLI1.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        objNSLI2.Directory_Section__c=objDS.Id;
        objNSLI2.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        
        NationalOrderAutomationBatchEmails__c noemail=new NationalOrderAutomationBatchEmails__c(Email__c='anigam23@csc.com',Name='Ankit');
        insert noemail;
        NationalOrderAutomationProcessBatch  sh1 = new NationalOrderAutomationProcessBatch();      
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        
        Test.StopTest();
  }
  
  public static testMethod void NationalOrderAutomationCrontestNegativeMethod(){
        Test.startTest(); 
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        objCMRAcc.National_Credit_Status__c='Approved';
        insert objCMRAcc;
        
        Account objCMRAccNegative = TestMethodsUtility.generateCMRAccount();
        objCMRAccNegative.Is_Active__c = false;
        objCMRAccNegative.National_Credit_Status__c='In Progress';
        insert objCMRAccNegative;
        
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        objDirEd.Directory__c = objDir.Id;
        insert objDirEd;
        
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        //Pricebook2 newPriceBook=[Select Id from Pricebook2 where isActive=true AND IsStandard=true Limit 1];
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        /*PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;*/
        
        Directory_Product_Mapping__c dirPM=new Directory_Product_Mapping__c(Directory__c=objDir.Id,Product2__c=objProd.Id);
        insert dirPM;
        
        Recordtype rtId = [SELECT DeveloperName,Id,Name FROM RecordType where Name = 'Daily Service Orders'];
        
        service_order_stage__c objSO = new service_order_stage__c();
        objSO.Name = 'Test SO XXX';
        objSO.Action__c = 'New';
        objSO.Phone__c = '(288) 451-1021';
        objSo.Disconnected__c = false;
        objSO.Bus_Res_Gov_Indicator__c = 'Business';
        objSO.BOID__c = 1234;
        objSO.Directory__c = objDir.Id;
        objSO.Indent_Order__c=0;
        objSO.Indent_Level__c=3;
        objSO.Caption_Header__c = true;
        objSO.Directory_Section__c = objDS.Id;
        objSO.recordtypeId = rtId.Id;
        objSO.Telco_Provider__c=objTelco .id;
        objSO.BEX__c=2;
        objSO.Cross_Reference_Text__c='Test';
        objSO.Designation__c='SEO';
        objSO.Honorary_Title__c='TEST';
        objSO.Lineage_Title__c='TEST123';
        objSO.Listing_City__c='NY';
        objSO.Listing_State__c='TX';
        objSO.Listing_Street_Number__c='311';
        objSO.Listing_Street__c='St. Lane';
        objSO.Listing_Country__c='USA';
        objSO.Listing_PO_Box__c='232';
        objSO.Listing_Postal_Code__c='101';
        objSO.Listing_Street__c='NX';
        objSO.BOID__c=4;
        objSO.Bus_Res_Gov_Indicator__c='Government';
        objSO.Effective_Date__c=Date.parse('01/01/2013');
        objSO.First_Name__c='Test';
        objSO.SOS_LastName_BusinessName__c='test';
        insert objSO;      
        
        Listing__c objL = new Listing__c();
        objL.Name = objSO.name; 
        objL.Phone__c = objSO.Phone__c; 
        objL.Telco_Provider__c = objSO.Telco_Provider__c;
        objL.BEX__c = objSO.BEX__c;
        objL.Cross_Reference_Text__c = objSO.Cross_Reference_Text__c;
        objL.Designation__c = objSO.Designation__c;
        objL.Honorary_Title__c = objSO.Honorary_Title__c;
        objL.Lineage_Title__c = objSO.Lineage_Title__c;
        objL.Listing_City__c = objSO.Listing_City__c;
        objL.Listing_Country__c = objSO.Listing_Country__c;
        objL.Listing_PO_Box__c = objSO.Listing_PO_Box__c;
        objL.Listing_Postal_Code__c = objSO.Listing_Postal_Code__c;
        objL.Listing_State__c= objSO.Listing_State__c;
        objL.Listing_Street_Number__c= objSO.Listing_Street_Number__c;
        objL.Listing_Street__c= objSO.Listing_Street__c;
        objL.BOID__c= objSO.BOID__c;
        objL.Company__c= objSO.name;
        objL.Bus_Res_Gov_Indicator__c= objSO.Bus_Res_Gov_Indicator__c;
        objL.Effective_Date__c = objSO.Effective_Date__c;
        objL.First_Name__c= objSO.First_Name__c;
        objL.Manual_Sort_As_Override__c = objSO.Manual_Sort_As_Override__c;
        objL.Phone_Type__c = objSO.Phone_Type__c;
        objL.Primary_Canvass__c= objSO.Primary_Canvass__c;
        objL.Region__c= objSO.Region__c;
        objL.Secondary_Surname__c = objSO.Secondary_Surname__c;
        objL.Service_Order_Stage_Item__c = objSO.Id;
        objL.Service_Order__c = objSO.Service_Order__c;
        objL.Year__c= objSO.Year__c;
        objL.Phone_Override__c = objSO.Phone_Override__c;
        objL.Omit_Address_OAD__c= objSO.Omit_Address_OAD__c;
        objL.Right_Telephone_Phrase__c=objSO.Right_Telephone_Phrase__c;
        objL.Left_Telephone_Phrase__c=objSO.Left_Telephone_Phrase__c;
        objL.Telco_Sort_Order__c=objSO.Telco_Sort_Order__c;
        objL.Disconnected__c=false;
        insert objL; 
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        objNSOS.IsMigratedXtrans__c=true;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        objNSLI.Directory_Section__c=objDS.Id;
        objNSLI.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        objNSLI1.Directory_Section__c=objDS.Id;
        objNSLI1.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        objNSLI2.Directory_Section__c=objDS.Id;
        objNSLI2.Directory_Heading__c=objDH.Id;
        ListNSLI.add(objNSLI2);
        
        //Alternative Conditions
        
        National_Staging_Order_Set__c objNSOS1 = TestMethodsUtility.generateManualNSRT();
        objNSOS1.Is_Ready__c = true;
        objNSOS1.Directory__c = objDir.id;
        objNSOS1.Directory_Edition__c =objDirEd.Id;
        objNSOS1.IsMigratedXtrans__c=true;
        objNSOS1.CMR_Name__c = objCMRAccNegative .id;
        objNSOS1.Client_Name__c=newAccount.Id;
        objNSOS1.Override__c=false;
        insert objNSOS1;
        
        National_Staging_Line_Item__c objNSLI10 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS1);
        objNSLI10.Ready_for_Processing__c = true;
        objNSLI10.UDAC__c='SS';
        objNSLI10.Standing_Order__c=true;
        objNSLI10.National_Staging_Header__c = objNSOS1.id;
        objNSLI10.Is_Changed__c=true;
        objNSLI10.Product2__c=newProduct.Id;
        objNSLI10.Directory_Section__c=objDS.Id;
        objNSLI10.Directory_Heading__c=objDH.Id;
        objNSLI10.Line_Number__c='1121';
        objNSLI10.Listing__c=objL.Id;
        ListNSLI.add(objNSLI10);
        
        National_Staging_Line_Item__c objNSLI11 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS1);
        objNSLI11.Ready_for_Processing__c = false;
        objNSLI11.UDAC__c='SS';
        objNSLI11.Standing_Order__c=true;
        objNSLI11.National_Staging_Header__c = objNSOS1.id;
        objNSLI11.Is_Changed__c=true;
        objNSLI11.Product2__c=newProduct.Id;
        objNSLI11.Discount__c=null;
        ListNSLI.add(objNSLI11);
        
        National_Staging_Line_Item__c objNSLI20 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS1);
        objNSLI20.Ready_for_Processing__c = true;
        objNSLI20.New_Transaction__c =true;
        objNSLI20.Standing_Order__c=true;
        objNSLI20.National_Staging_Header__c = objNSOS1.id;
        objNSLI20.Is_Changed__c=true;
        objNSLI20.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI20);
        
        National_Staging_Line_Item__c objNSLI13 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS1);
        objNSLI13.Ready_for_Processing__c = true;
        objNSLI13.UDAC__c='XYZ';
        objNSLI13.Standing_Order__c=true;
        objNSLI13.National_Staging_Header__c = objNSOS1.id;
        objNSLI13.Is_Changed__c=true;
        objNSLI13.Product2__c=newProduct.Id;
        objNSLI13.Directory_Section__c=objDS.Id;
        objNSLI13.Directory_Heading__c=objDH.Id;
        objNSLI13.Line_Number__c='1121';
        objNSLI13.Listing__c=objL.Id;
        ListNSLI.add(objNSLI13);
        
        insert ListNSLI;
        
        NationalOrderAutomationProcessBatch  sh1 = new NationalOrderAutomationProcessBatch();      
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        
        Test.StopTest();
  }
}