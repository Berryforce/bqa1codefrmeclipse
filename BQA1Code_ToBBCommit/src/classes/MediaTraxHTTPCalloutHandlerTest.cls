@isTest
public class MediaTraxHTTPCalloutHandlerTest{
    public static testmethod void MediaTraxHTTPCalloutTrackingTest() {
        Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generateDataFulfillmentForm();
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        Account acc = TestMethodsUtility.generateAccount('customer');
        acc.Media_Trax_Client_ID__c = '12345';
        lstAccount.add(acc);
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        objDFF.Account__c = newAccount.id;
        objDFF.business_phone_number_office__c = '8975642310';
        objDFF.P4P_Requested_Area_Code__c = '232';
        objDFF.P4P_Requested_Prefix__c = '1';
        objDFF.Call_Recording__c = true;
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        objDir.Media_Trax_Directory_ID__c = '12345';
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        objDH.Media_Trax_Heading_ID__c = '12345';
        insert objDH;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;
        
        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        insert lstProduct;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        //list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstDPM;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        
        Order_Line_Items__c objOLI = new Order_Line_Items__c(Quote_Signed_Date__c = System.Today()+1, 
                                 Order_Anniversary_Start_Date__c = System.Today()-5,   Billing_Frequency__c = CommonMessages.singlePayment, 
                                 Billing_Partner__c = 'THE BERRY COMPANY', Talus_Go_Live_Date__c = System.Today()+30, Payment_Duration__c = 10,
                                 Account__c = newAccount.Id,Order__c = newOrder.Id, Opportunity__c = newOpportunity.Id,
                                 Billing_Contact__c = newContact.Id, Order_Group__c = newOrderSet.Id, Canvass__c = newAccount.Primary_Canvass__c);

        objOLI.Package_ID__c = '';
        objOLI.Package_Item_Quantity__c = 2;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.UnitPrice__c = 30.00;
        objOLI.P4P_Price_Per_Click_Lead__c = 1;
        objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        objOLI.Product2__c = lstProduct[0].id;
        objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
        
        insert objOLI;
        objOLI.Talus_Go_Live_Date__c = system.today();
        objOLI.Last_Billing_Date__c=system.today();
        objOLI.Directory_Edition__c = objDirEd1.Id;
        update objOLI;
        objDFF.OrderLineItemID__c = objOLI.id;
        objDFF.P4P_Number_Type__c = '1';
        objDFF.DFF_Product__c = lstProduct[0].id;
        insert objDFF;
        List<Digital_Product_Requirement__c> lstDFF = [select DFF_Product__c, OrderLineItemID__r.Last_Billing_Date__c, OrderLineItemID__r.Name, Account__r.Media_Trax_Client_ID__c, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry, business_phone_number_office__c, P4P_Requested_Area_Code__c, P4P_Requested_Prefix__c, OrderLineItemID__r.Directory__r.Media_Trax_Directory_ID__c, OrderLineItemID__r.Directory_Heading__r.Media_Trax_Heading_ID__c, UDAC__c, P4P_Number_Type__c, OrderLineItemID__r.P4P_Price_Per_Click_Lead__c, Call_Recording__c from Digital_Product_Requirement__c where id = :objDFF.id];
        Canvass__c objCanvass = TestMethodsUtility.generateCanvass(telco);
        objCanvass.P4P_Call_Time__c = '1';
        objCanvass.P4P_Threshold__c = '1';
        objCanvass.P4P_Frequency__c = '1';
        insert objCanvass;
        
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='DeactivateTrackingNumber',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/trackingNumbers.cfc',HEADER_SOAPACTION__C='DeactivateTrackingNumber',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='Response',RETURN_METHOD__C='Return',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='Test123',OPERATION__C='');
        insert objMedia;
        
        MediaTraxHTTPCalloutHandlerController_V1.generateDOMObjectForTrackingDeacivate('12345', System.today(), objMedia);
        Test.startTest();
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        MediaTraxHTTPCalloutHandlerController_V1.generateDOMObjectForTracking(lstDFF[0], objCanvass, objMedia);
        
        Test.stoptest();
    }
}