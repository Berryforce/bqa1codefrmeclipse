@isTest
public class NationalStagingOrderSetDetailV1Test{
    public static testmethod void NationalStagingOrderSetDetailTest(){
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateEliteRT();
        objNSOS.CMR_Number__c = '123';
        objNSOS.Client_Number__c = '123';
        objNSOS.Directory__c = objDir.id;
        insert objNSOS;
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Standing_Order__c = true;
        objNSLI.New_Transaction__c = true;
        insert objNSLI;
        
        Advice_Query__c objAQ = new Advice_Query__c();
        objAQ.National_Staging_Line_Item__c = objNSLI.id;
        objAQ.Process_Completed__c = false;
        objAQ.Frequent_Comments_Questions__c = 'Advice';
        objAQ.Advice_Query_Options__c = 'Empty Telephone Number';
        objAQ.Free_text__c = 'Testing';
        insert objAQ;
        
        National_History_Order_Set__c objNH = TestMethodsUtility.generateNationalHistoryOrderSet();
        objNH.CMR_Number__c = '123';
        objNH.Client_Number__c = '123';
        insert objNH;
        
        ApexPages.StandardController objA = new ApexPages.StandardController(objNSOS);
        ApexPages.currentPage().getParameters().put('NSLIID', objNSLI.Id);
                
        NationalStagingOrderSetDetail_V1 objASHC = new NationalStagingOrderSetDetail_V1(objA);
        
        
        objASHC.onLoad();
        objASHC.updateNewTransaction();
        objASHC.backtoOrderSet();
        objASHC.deleteLateOrder();
    }
}