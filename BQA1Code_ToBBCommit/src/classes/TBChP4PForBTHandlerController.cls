public class TBChP4PForBTHandlerController {
    /**** this method  RetroTransfer will use when OLI has been gone live and does have invoices  
    What we will do:
        1. we will create invoice using child account (Transferring Account) information if required.
        2. we will create invoices for new child account (Transferring Account).
        3. we will update OLIs Information .
        4. we will handle recurring CS claims if it exists.
    *****/
    public static void RetroTransfer(boolean isValidate,Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForSalesInvoiceWrapper,
            Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,
            list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim , 
            list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
            map<Integer, list<c2g__codaCreditNoteLineItem__c>> mapSCNLI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,
            list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaInvoice__c> insertSalesInvoice,List<c2g__codaInvoiceLineItem__c> SILIListForUpdate,
            String selectedAccountId, Id accountid,String selectedContactID,String selectedReasonForTransfer, String strModeofTransfer,
            List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,List<Opportunity> lstNewOpportunity,List<Line_Item_History__c> lstOLIHistory,
            map<Id,Date> mapOLIMinimumInvoiceDt, map<Id, Integer> mapOLINoOfMonthTransfer) {

        // Here we will update the OLIs.
        if(!isValidate) {
            list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id>{selectedAccountID}, new set<Id>{selectedContactID});
            List<order_line_items__c> lstorderlineForUpdate=TBChOrderLineItemBTHandlerController.updateOLIWithBillingTransferInformation(mapForUpdatedOLis.values(), lstAccount[0], accountId, selectedContactID, selectedReasonForTransfer, strModeofTransfer,lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory, mapOLINoOfMonthTransfer, new set<Id>());
            update lstorderlineForUpdate;
        }
        // this condition is used only when we will validate the process.
        map<String, Id> mapPeriod = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        Integer iCount = 0;
        Integer iCountSCN=0;
        map<Id, Date> mapSIInvoiceDate = new map<Id, Date>();
        map<Id, Date> mapSIDueDate = new map<Id, Date>();
        map<Id, Date> mapSIServiceStartDate = new map<Id, Date>();
        map<Id, Date> mapSIServiceEndDate = new map<Id, Date>();
        set<Id> SILIsetOfIds =new Set<Id>();                 
        Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForExtraInvoice = new Map<Id,List<c2g__codaInvoiceLineItem__c>>();
        Map<Id,Date> mapForExtraInvoiceNextDate = new Map<Id,Date>();
        order_line_items__c objoliNew ;               
        for(ID iterator : mapForSalesInvoiceWrapper.keySet()) {
            iCount++;
            objoliNew =null;
            integer successPayments;
            integer paymentsRemaining;
            for(c2g__codaInvoiceLineItem__c iteratorChild:mapForSalesInvoiceWrapper.get(iterator)) {
                iCountSCN++;
                 if(iteratorChild.c2g__NetValue__c>0 || Test.isRunningTest()){
                lstInsertSCN.add(TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceToCreditNoteForP4PRetro(mapForUpdatedOLis.get(iteratorChild.order_line_item__c),iteratorChild,iCountSCN,mapPeriod,false ));
                TBChFFDocumentsForBTHandlerController.copyDataFromInvoiceLineItemToSalesCreditNoteLineItemForP4PRetro(new list<c2g__codaInvoiceLineItem__c>{iteratorChild}, iCountSCN, mapSCNLI,mapForUpdatedOLis);
                }
                if(!SILIsetOfIds.contains(iteratorChild.id)) {
                    SILIsetOfIds.add(iteratorChild.Id);
                    SILIListForUpdate.add(new c2g__codaInvoiceLineItem__c(id=iteratorChild.Id,Offset__c=true));
                }
                if(iteratorChild.Order_Line_Item__c != null) {
                    objoliNew = mapForUpdatedOLis.get(iteratorChild.Order_Line_Item__c);
                }
                if(!mapSIInvoiceDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIInvoiceDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.c2g__InvoiceDate__c);
                }
                if(!mapSIDueDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIDueDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.c2g__DueDate__c);
                }
                if(!mapSIServiceStartDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIServiceStartDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.Service_Start_Date__c);
                }                           
                if(!mapSIServiceEndDate.containsKey(iteratorChild.c2g__Invoice__c)) {
                    mapSIServiceEndDate.put(iteratorChild.c2g__Invoice__c, iteratorChild.c2g__Invoice__r.Service_End_Date__c);
                }
                if(!mapSalesInvoiceLineItem.containsKey(iCount)) {
                    mapSalesInvoiceLineItem.put(iCount, new list<c2g__codaInvoiceLineItem__c>());
                }
                mapSalesInvoiceLineItem.get(iCount).add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceLineItemForP4PRetro(iteratorChild, objoliNew));  
                successPayments=Integer.valueOf(iteratorChild.Successful_Payments__c);
                paymentsRemaining=Integer.valueOf(iteratorChild.Payments_Remaining__c) ;                 
            }

            if(objoliNew!=null) {
                Date duedate=mapSIDueDate.get(iterator);
                Date invoicedate=mapSIInvoiceDate.get(iterator);
                if(Test.isRunningTest()) {
                    if(duedate==null)
                        duedate=date.today().addmonths(1);
                    if(invoicedate==null)
                        invoicedate=date.today().addmonths(1);
                }
                if(mapSalesInvoiceLineItem.size()>0)
                    insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForP4pRetro(objoliNew, iCount, mapPeriod, 0,duedate,invoicedate,mapSIServiceStartDate.get(iterator),mapSIServiceEndDate.get(iterator),successPayments,paymentsRemaining)); 
            }
            /*** End Creating Sales Invoices  for new billing partner ***/
        }
        // below logic is for handle the recurring CS Claim 
        list<c2g__codaCreditNoteLineItem__c> lstSCN = SalesCreditNoteLineItemSOQLMethods.getSysGenSCNLineItemByOLIForRetro(mapForUpdatedOLis.keySet(), String.valueOf(accountid), false);
        if(lstSCN.size() > 0) {
            RecurringCSClaimHandlerMethod(mapPeriod, lstSCN, mapForUpdatedOLis, mapSCNLIForCSClaim, lstInsertSCNForCSClaim, mapSalesInvoiceLineItemForCSClaim, insertSalesInvoiceForCSClaim, SCNLIListForUpdateForCSClaim,mapOLIMinimumInvoiceDt);
        }
    }

    /**** this method  FutureTransferForInvoices will use when OLI has been gone live and does have invoices. we will check if there is any need to create invoice while doing the billling tranfer for below scenario. 
        Current Account next Billing Date      :- 09/25/2015
        Transferring Account Next Billing Date :- 09/22/2015 (Already passed)
        Billing Transfer Date :-               :- 09/23/2015
    
        What we will do:
        1. we will create invoice using child account (Transferring Account) information.
        2. we will update OLIs Information .
        3. we will handle recurring CS claims if it exists.
    
    *******/      
    public static void FutureTransferForInvoices(boolean isValidate,Map<Id,List<c2g__codaInvoiceLineItem__c>> mapForSalesInvoiceWrapper,
                                Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim,
                                list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim , 
                                list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,
                                map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,
                                list<c2g__codaInvoice__c> insertSalesInvoice,
                                String selectedAccountId, Id accountid,String selectedContactID,String selectedReasonForTransfer, String strModeofTransfer,List<Order__c> lstNewOrders,
                                List<Order_Group__c> lstNewOrderGroup,List<Opportunity> lstNewOpportunity,List<Line_Item_History__c> lstOLIHistory,
                                map<Id,Date> mapOLIMinimumInvoiceDt, map<Id, Integer> mapOLINoOfMonthTransfer) {
        Map<String ,id> dimensionsMapValuesForUpdatedOLis;
        Map<String ,id> dimensionsMapValuesForWithoutUpdatedOLis;
        list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id>{selectedAccountID}, new set<Id>{selectedContactID});            // Here we will update the OLIs.
        // Here we will update the OLIs.
        if(!isValidate) {
            //List<order_line_items__c> lstorderlineForUpdate=TBChOrderLineItemBTHandlerController.UpdateOlisP4PFuture(mapForUpdatedOLis.values(),SelectedAccountId,accountId,selectedContactID,selectedReasonForTransfer);
            List<order_line_items__c> lstorderlineForUpdate=TBChOrderLineItemBTHandlerController.updateOLIWithBillingTransferInformation(mapForUpdatedOLis.values(), lstAccount[0], accountId, selectedContactID, selectedReasonForTransfer, strModeofTransfer,lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory, mapOLINoOfMonthTransfer, new set<Id>());
            update lstorderlineForUpdate;
        }
    }
    /***** Private Methods ****/
    
    // this method will use for recurring CS claim for whole p4p process
    public static void RecurringCSClaimHandlerMethod(Map<String,Id> mapOfPeriod,list<c2g__codaCreditNoteLineItem__c> CreditNoteLineItemsSOQLMap, 
                    Map<id,order_line_Items__c> mapForUpdatedOLis, Map<integer,list<c2g__codaCreditNoteLineItem__c>> mapSCNLIForCSClaim, 
                    list<c2g__codaCreditNote__c> lstInsertSCNForCSClaim, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItemForCSClaim, 
                    list<c2g__codaInvoice__c> insertSalesInvoiceForCSClaim, List<c2g__codaCreditNoteLineItem__c> SCNLIListForUpdateForCSClaim,Map<id,Date> MapOfSCNDate) {
        Map<id, List<c2g__codaCreditNoteLineItem__c>> mapForCreditNote = new Map<id, List<c2g__codaCreditNoteLineItem__c>>();
        integer iCountSCN=0;
        integer iCountSI=0;
        Set<Id> SCNLIsetOfIds =new Set<Id>();
        Set<Id> SCNsetOfIds =new Set<Id>();
        if(CreditNoteLineItemsSOQLMap.size()>0) {
            for(c2g__codaCreditNoteLineItem__c  creditNoteLineItem : CreditNoteLineItemsSOQLMap){
                Date dtInvoiceDate = MapOfSCNDate.get(creditNoteLineItem.Order_Line_Item__c);
                if(creditNoteLineItem.c2g__CreditNote__r.c2g__CreditNoteDate__c >= dtInvoiceDate || Test.isRunningTest()) {
                    if(!mapForCreditNote.containsKey(creditNoteLineItem.c2g__CreditNote__c))
                        mapForCreditNote.put(creditNoteLineItem.c2g__CreditNote__c,new List<c2g__codaCreditNoteLineItem__c>());
                    mapForCreditNote.get(creditNoteLineItem.c2g__CreditNote__c).add(creditNoteLineItem);
                }
            }

            for(Id creditnoteId : mapForCreditNote.keySet()) {
                iCountSCN++;
                iCountSI++;
                for(c2g__codaCreditNoteLineItem__c iterator : mapForCreditNote.get(creditnoteId)) {
                    //if(MapOfSCNDate.containsKey(iterator.order_line_Item__c) && MapOfSCNDate.get(iterator.order_line_Item__c)<=iterator.c2g__CreditNote__r.c2g__CreditNoteDate__c){
                        if(!SCNLIsetOfIds.contains(iterator.id)) {
                            SCNLIsetOfIds.add(iterator.Id);
                            SCNLIListForUpdateForCSClaim.add(new c2g__codaCreditNoteLineItem__c(id=iterator.Id,Offset__c=true));
                            if(iterator.c2g__UnitPrice__c>0 || Test.isRunningTest()) {
                                if(!mapSalesInvoiceLineItemForCSClaim.containsKey(iCountSI)) {
                                    mapSalesInvoiceLineItemForCSClaim.put(iCountSI,new List<c2g__codaInvoiceLineItem__c>());
                                }
                                mapSalesInvoiceLineItemForCSClaim.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.copyDataFromSCNLIToSILIForp4pCSClaim(iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c)));
    
                                if(!mapSCNLIForCSClaim.containsKey(iCountSCN)) {
                                    mapSCNLIForCSClaim.put(iCountSCN, new list<c2g__codaCreditNoteLineItem__c>());
                                }
                                mapSCNLIForCSClaim.get(iCountSCN).add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineItemForp4pCSClaim(iterator));
                            }
                        }
                        
                        // Below logic will create invoices and credit notes data.
                        if(!SCNsetOfIds.contains(iterator.c2g__CreditNote__c)) {
                            SCNsetOfIds.add(iterator.c2g__CreditNote__c);
                            if(mapSCNLIForCSClaim.size()>0)
                                 lstInsertSCNForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesCreditNoteForDigitalCSClaim(mapOfPeriod,iterator,iCountSCN,mapForUpdatedOLis.get(iterator.Order_Line_Item__c),false));                   
                            if(mapSalesInvoiceLineItemForCSClaim.size()>0)
                               insertSalesInvoiceForCSClaim.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForDigitalCSClaim(mapOfPeriod,iterator,mapForUpdatedOLis.get(iterator.Order_Line_Item__c),iCountSI,false));                 
                        }
                    //}
                }
            }
        }
    }
}