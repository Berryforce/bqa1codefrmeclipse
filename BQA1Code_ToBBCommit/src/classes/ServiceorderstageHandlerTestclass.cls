@isTest
private class ServiceorderstageHandlerTestclass {

    static testMethod void ServiceorderstageHandlerTest() {
        list<service_order_stage__c> lstSO = new list<service_order_stage__c>();
        list<service_order_stage__c> lstSO1 = new list<service_order_stage__c>();
        Recordtype rtId = [SELECT DeveloperName,Id,Name FROM RecordType where Name = 'Daily Service Orders'];
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.BOTS_Edition_Publication_Date__c=system.today().adddays(-4);
        objDir.SO_NI_Blackout_Period_Begin_Date__c=system.today().adddays(-6);
        objDir.SO_NI_Blackout_Period_End_Date__c=system.today().adddays(4);
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Heading__c objDirH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDirS = TestMethodsUtility.createDirectorySection(objDir);
        
        Service_order_stage__c objSO1 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)222-2221',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business');
        Service_order_stage__c objSO2 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)244-4441',Out_Phone_Number__c='',
                                                                Action__c = 'Disconnect',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business');
        Service_order_stage__c objSO3 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)288-2881',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id);
        Service_order_stage__c objSO4 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)299-2991',SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id);
        Service_order_stage__c objSO5 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)211-2111',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id,Header_text__c='testHT',Fallout_resolved__c=false,Fallout__c=false);                                                        
        Service_order_stage__c objSO6= new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)233-2331',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id,Caption_member__c=true,Under_caption__c= null);
        
        Service_order_stage__c objSO7= new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)255-2551',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id,Caption_member__c=true,Fallout_Resolved__c =false,Recordtypeid=rtId.Id);
                                                                
        Service_order_stage__c objSO8 = new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)266-2661',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id,Caption_header__c=true,Fallout_Resolved__c =false,Recordtypeid=rtId.Id);
    
        Service_order_stage__c objSO9= new Service_order_stage__c(Name=TestMethodsUtility.generateRandomString(10),SOS_LastName_BusinessName__c=TestMethodsUtility.generateRandomString(10),Phone__c ='(888)266-2661',
                                                                Action__c = 'New',Listing_Type__c='Main',Bus_Res_Gov_Indicator__c='Business',Directory__c=objDir.Id,Directory_heading__c=objDirH.Id,Directory_section__c=objDirS.Id,Effective_date__c = system.today(),Recordtypeid=rtId.Id);
    
    
        lstSO.add(objSO1);
        lstSO.add(objSO2);
        lstSO.add(objSO3);
        lstSO.add(objSO4);
        lstSO.add(objSO5);
        lstSO.add(objSO6);
        lstSO.add(objSO7);
        lstSO.add(objSO8);
        lstSO.add(objSO9);
        insert lstSO;
        
        objSO4.Directory__c = null;
        objSO3.Directory_section__c = null;
        lstSO1.add(objSO3);
        lstSO1.add(objSO4);
        update lstSO1;
        map<Id,Service_order_stage__c> oldMap = new map<Id,Service_order_stage__c>();
        oldMap.put(objSO4.Id,objSO4);                                                     
        oldMap.put(objSO3.Id,objSO3);
        oldMap.put(objSO5.Id,objSO5);
        ServiceOrderstageHandlerController.SOFalloutLogic(lstSO1,oldMap);
    }
}