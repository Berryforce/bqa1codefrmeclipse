@isTest(seeAlldata=true)
public class ListingsLeadBatchControllerTestclass{
    static testmethod void ListingsLeadBatchControllerTest(){
    test.starttest();
    list< listing__c> lstList = new list<listing__c>();
    Database.BatchableContext bc;
    listing__c objlist = new listing__c(Name='TestLisbatch',Lst_Last_Name_Business_Name__c='TestLisbatch',Phone__c='(444)228-9999',Bus_Res_Gov_Indicator__c = 'Business',Listing_Type__c='Main') ;
    insert objlist;
    objlist.lead__c=null;
    update objlist;
    set<Id> setLstId = new set<Id>();
    setLstId.add(objlist.Id);
    lstList = [SELECT Id,Name,Lst_Last_Name_Business_Name__c,Listing_Type__c,Account__c,Lead__c,Bus_Res_Gov_Indicator__c ,First_Name__c,Phone__c,Listing_Street__c,Listing_City__c,Listing_State__c,Listing_Postal_Code__c,Listing_Country__c,Lead_Account_Source__c,Primary_Canvass__c,Telco_Provider_Account_ID__c from listing__c where ID =: setLstId];
    ListingsLeadBatchController llbc = new ListingsLeadBatchController(setLstId);
    llbc.start(bc);
    llbc.execute(bc,lstList);
    test.stoptest();
    }
}