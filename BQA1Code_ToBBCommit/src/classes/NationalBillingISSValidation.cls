public class NationalBillingISSValidation {
    public Integer intDirectorySize {get;set;}
    public boolean bolFetchDir {get;set;}
    public String strPubCode {get;set;}
    public String strEditionCode {get;set;}
    public Integer intNICount {get;set;}
    public boolean bolDirFound {get;set;}
    public boolean bolCreateISS {get;set;}
    public Integer intISSCount {get;set;}
    public Decimal TaxAmt {get;set;}
    public Decimal NetAmt {get;set;}
    public Decimal ISSAmt {get;set;}
    public Decimal ISSTaxAmt {get;set;}
    public Decimal ISSNetAmt {get;set;}
    public Decimal InvoiceAmt {get;set;}
    public Decimal Commission {get;set;}
    public boolean chkDirComplete {get;set;}
    public boolean chkValGross {get;set;}
    public boolean chkValTax {get;set;}
    public boolean chkValNetAmt {get;set;}
    public boolean chkValIssCMR {get;set;}
    public boolean chkValComission {get;set;}
    public boolean bolISS {get;set;}
    public boolean valCMRForBill {get;set;}
    public String selectedTabVal {get;set;}
    public Account objPubco {get;set;}
    public Directory_Edition__c objDE {get;set;}
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    Public National_Billing_Status__c natBilling;
    public list<ISS__c> lstISSInserted {get;set;}
    set<Id> setNationalCommissionAccountID;
    set<Id> setDirectory;
    list<NationalInvoice__c> lstNI;
    map<Id, String> mapCMRNumber;
    map<Id, String> mapPubCoCode;
    public Id commissionAndTaxReportId {get;set;}
    public Id ISSToCMRReportId {get;set;}
    public Id ISSReportId {get;set;}
    public Decimal totalISSTaxAmount {get;set;}
    public Decimal totalISSADJAmount {get;set;}
    public Decimal totalISSNetAmount {get;set;} 
    public Decimal totalISSGrossAmount {get;set;} 
    public Decimal totalISSCommission {get;set;}
    
    public NationalBillingISSValidation() { 
        lstISSInserted = new list<ISS__c>();
        intISSCount = 0;
        TaxAmt = 0.00;
        NetAmt = 0.00;
        ISSAmt = 0.00;
        ISSTaxAmt = 0.00;
        ISSNetAmt = 0.00;
        InvoiceAmt = 0.00;
        Commission = 0.00;
        intNICount = 0;
        bolISS = false;
        String strSfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        commissionAndTaxReportId = National_Billing_Reports__c.getInstance('CommTaxReport').Report_Id__c; 
        ISSToCMRReportId = National_Billing_Reports__c.getInstance('ISSCMRReport').Report_Id__c;
        ISSReportId = National_Billing_Reports__c.getInstance('ISSReport').Report_Id__c;
    }
    
    /*public PageReference redirectToISS() {
        return Page.NationalBillingISS;
    }*/
    
    public void displayISS() {
        calcualteTotalForISS();
        bolISS = true;
    }
    public void hideISS() {
        bolISS = false;
    }
    
    public void reportsTab(){
        selectedTabVal = 'Reports';
    }
     
    public void fetchDirectories(){
        fetchStatusTable();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
            bolCreateISS = false;
            chkDirComplete = false;
            intDirectorySize = 0;
        } else {    
            lstISSInserted = new list<ISS__c>();
            natBilling = listNatBill.get(0);
            chkDirComplete = false;
            intDirectorySize = 0;
            bolFetchDir = true;
            lstNI = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeWithISSNull(strPubCode, strEditionCode);
            set<id> setDirectoryId = new Set<id>();
            if(lstNI != null){
                intNICount = lstNI.size();
                if(intNICount > 0) {
                    NationalInvoice__c objNI = lstNI[0];
                    objPubco = new Account(id = objNI.Publication_Company__c, name = objNI.Publication_Company__r.name);
                    objDE = new Directory_Edition__c(id = objNI.Directory_Edition__c, name = objNI.Directory_Edition__r.name, National_Billing_Date__c = objNI.Directory_Edition__r.National_Billing_Date__c);
                }
                for(NationalInvoice__c objNI : lstNI){
                    if(objNI.Directory__c != null){
                        setDirectoryId.add(objNI.Directory__c);
                    }
                }            
                intDirectorySize = setDirectoryId.size();
                if(setDirectoryId.size() > 0){
                    bolDirFound = true;
                }
            }       
            assignValuesFromNatBill();
            if(lstISSInserted.size() == 0) {
                bolCreateISS = false;
            }
        } 
    }
    
    public map<Id, map<Id, map<Id, map<Id, list<NationalInvoice__c>>>>> GroupingBYCMRandPubCo(list<NationalInvoice__c> lstNI) {
        map<Id, map<Id, map<Id, map<Id, list<NationalInvoice__c>>>>> mapCMR = new map<Id, map<Id, map<Id, map<Id, list<NationalInvoice__c>>>>>();
        mapCMRNumber = new map<Id, String>();
        mapPubCoCode = new map<Id, String>();
        setNationalCommissionAccountID = new set<Id>();
        setDirectory = new set<Id>();
        set<Id> setNI = new set<Id>();
        //set<Id> setOrderSerId = new set<Id>();
        
        for(NationalInvoice__c objNI : lstNI) {
            setNationalCommissionAccountID.add(objNI.CMR__c);
            setNationalCommissionAccountID.add(objNI.Publication_Company__c);
            setNationalCommissionAccountID.add(objNI.Client_Name__c);
            setDirectory.add(objNI.Directory__c);
            setNI.add(objNI.Id);
            //setOrderSerId.add(objNI.Order_Group__c);
            if(!mapCMRNumber.containsKey(objNI.CMR__c)) {
                mapCMRNumber.put(objNI.CMR__c, objNI.CMR_Number__c);
            }
            if(!mapPubCoCode.containsKey(objNI.Publication_Company__c)) {
                mapPubCoCode.put(objNI.Publication_Company__c, objNI.Publication_Code__c);
            }
            if(!mapCMR.containsKey(objNI.CMR__c)) {
                mapCMR.put(objNI.CMR__c, new map<Id, map<Id, map<Id, list<NationalInvoice__c>>>>());
            }
            
            map<Id, map<Id, map<Id, list<NationalInvoice__c>>>> mapPubCo = mapCMR.get(objNI.CMR__c);
            if(!mapPubCo.containsKey(objNI.Publication_Company__c)) {
                mapPubCo.put(objNI.Publication_Company__c, new map<Id, map<Id, list<NationalInvoice__c>>>());
            }
            
            map<Id, map<Id, list<NationalInvoice__c>>> mapDIR = mapPubCo.get(objNI.Publication_Company__c);
            
            if(!mapDIR.containsKey(objNI.Directory__c)) {
                mapDIR.put(objNI.Directory__c, new map<Id, list<NationalInvoice__c>>());
            }
            
            map<Id, list<NationalInvoice__c>> mapClient = mapDIR.get(objNI.Directory__c);
            if(!mapClient.containsKey(objNI.Client_Name__c)) {
                mapClient.put(objNI.Client_Name__c, new list<NationalInvoice__c>());
            }
            mapClient.get(objNI.Client_Name__c).add(objNI);
        }
        return mapCMR;
    }
    
    public void createISS() {
        bolCreateISS = true;
        map<ID, String> mapNIISS = new map<Id, String>();
        list<Document> lstDocument = new list<Document>();
        list<NationalInvoice__c> listNatInv = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeWithISSNull(strPubCode, strEditionCode);
        //System.debug('Testingg listNatInv '+listNatInv);
        //System.debug('Testingg listNatInv size '+listNatInv.size());
        if(listNatInv.size() > 0) {
            map<Id, map<Id, map<Id, map<Id, list<NationalInvoice__c>>>>> mapCMR = GroupingBYCMRandPubCo(listNatInv);
            //System.debug('Testingg mapCMR '+mapCMR);
            list<ISSAutoNumber__c> lstAuto = ISSAutoNumber__c.getall().values();
            String strAutoNumber = '00000';
            ISSAutoNumber__c objAuto;
            if(lstAuto != null) {
                objAuto = lstAuto[0];
                strAutoNumber = objAuto.Name;
            }
            System.debug('Testingg setNationalCommissionAccountID '+setNationalCommissionAccountID);
            map<Id, Account> mapNCAccount = AccountSOQLMethods.getNationalCommissionById(setNationalCommissionAccountID);
            map<Id, Directory__c> mapNCDir = DirectorySOQLMethods.getNationalCommissionById(setDirectory);
            list<ISS__c> lstISS = new list<ISS__c>();
            map<String, list<ISS_Line_Item__c>> mapISSLI = new map<String, list<ISS_Line_Item__c>>(); 
            String ISSNumber;
            Decimal TaxAmtTemp;
            Decimal CommissionTemp;
            Decimal GrossAmt;
            intISSCount = 0;
            Set<String> setPubco;
            for(Id cmrID : mapCMR.keySet()) {
                map<Id, map<Id, map<Id, list<NationalInvoice__c>>>> mapPubCo = mapCMR.get(cmrID);
                //System.debug('Testingg cmrID '+cmrID);
                setPubco = new Set<String>();
                for(Id pubCoID : mapPubCo.keySet()) {
                    //System.debug('Testingg pubCoID '+pubCoID);                    
                    
                    strAutoNumber = ISSNumberIncrement(strAutoNumber);
                    //System.debug('Testingg strAutoNumber '+strAutoNumber);
                    ISS__c objISS = new ISS__c(Invoice_Date__c = objDE.National_Billing_Date__c, CMR__c = cmrID, Publication_Company__c = pubCoID);
                    Date pubDate;
                    map<Id, map<Id, list<NationalInvoice__c>>> mapDIR = mapPubCo.get(pubCoID);
                    for(Id dirID : mapDIR.keySet()) {
                        //System.debug('Testingg dirID '+dirID);
                        map<Id, list<NationalInvoice__c>> mapClient = mapDIR.get(dirID);
                        for(Id clientId : mapClient.keySet()) {
                            //System.debug('Testingg clientId '+clientId);
                            Decimal NCRate = 0;
                            
                            if(mapNCAccount.get(pubCoID) != null) {
                                if(mapNCAccount.get(pubCoID).National_Commission__r.Rate__c != null) {
                                    NCRate = mapNCAccount.get(pubCoID).National_Commission__r.Rate__c;
                                    System.debug('Testingg pubco NCRate '+NCRate);
                                }
                            }
                            
                            if(mapNCDir.get(dirID) != null) {
                                if(mapNCDir.get(dirID).National_Commission__r.Rate__c != null) {
                                    NCRate = mapNCDir.get(dirID).National_Commission__r.Rate__c;
                                    System.debug('Testingg Directory NCRate '+NCRate);
                                }
                            }
                            
                            if(mapNCAccount.get(cmrID) != null) {
                                System.debug('Testingg CMR mapNCAccount.get(cmrID).National_Commission__r.Rate__c '+mapNCAccount.get(cmrID).National_Commission__r.Rate__c);
                                if(mapNCAccount.get(cmrID).National_Commission__r.Rate__c != null) {
                                    NCRate = mapNCAccount.get(cmrID).National_Commission__r.Rate__c;
                                    System.debug('Testingg CMR NCRate '+NCRate);
                                }
                            }
                            
                            if(mapNCAccount.get(clientId) != null) {
                                if(mapNCAccount.get(clientId).National_Commission__r.Rate__c != null) {
                                    NCRate = mapNCAccount.get(clientId).National_Commission__r.Rate__c;
                                    System.debug('Testingg Client NCRate '+NCRate);
                                }
                            }
                            
                            ISS_Line_Item__c objISSLine = new ISS_Line_Item__c(Commission__c = NCRate, Client_Name__c = clientId, Directory__c = dirID, Inv_Type__c = 'IR', Product__c = CommonMessages.nationalProductID, Life__c = '200');                                              
                            
                            for(NationalInvoice__c iterator : mapClient.get(clientId)) {
                                if(!setPubco.contains(pubCoID) && iterator.Publication_Date__c != null) {
                                    setPubco.add(pubCoID);
                                    pubDate = iterator.Publication_Date__c;
                                    break;
                                }                         
                            }
                            
                            ISSNumber = ISSNumberGeneration(mapCMRNumber.get(cmrID), mapPubCoCode.get(pubCoID), strAutoNumber, pubDate);
                            
                            if(!mapISSLI.containsKey(ISSNumber)) {
                                mapISSLI.put(ISSNumber, new list<ISS_Line_Item__c>());
                            }                            
                            
                            objIss.Name = ISSNumber;
                            objIss.Edition_Code__c = strEditionCode;
                    
                            for(NationalInvoice__c iterator : mapClient.get(clientId)) {
                                objISS.From__c = iterator.From__c;
                                objISS.To__c = iterator.To__c;
                                objISS.Trans__c = 'IV';
                                objISS.Trans_Version__c = iterator.Trans_Version__c;
                                mapNIISS.put(iterator.id, ISSNumber);
                                //System.debug('Testingg mapNIISS '+mapNIISS);
                                objISSLine.Pubco_date__c = iterator.Publication_Date__c;
                                objISSLine.Directory_Edition__c = iterator.Directory_Edition__c;
                                objISSLine.Life__c = '1200';
                                pubDate = iterator.Publication_Date__c;
                                for(NationalInvoiceLineItem__c objNILI : iterator.National_Invoice_Line_Items__r){
                                    if(objNILI.Gross_Amount__c != null) {
                                        if(objISSLine.Gross_Amount__c == null) {
                                            objISSLine.Gross_Amount__c = objNILI.Gross_Amount__c;
                                        }
                                        else {
                                            objISSLine.Gross_Amount__c = objISSLine.Gross_Amount__c + objNILI.Gross_Amount__c;
                                        }
                                    }
                                }
                                objISSLine.Tax__c = iterator.Tax__c;
                                objISSLine.ADJ_Amount__c = 0;
                            }
                            mapISSLI.get(ISSNumber).add(objISSLine);
                        }
                    }
                    lstISS.add(objISS);
                }
            }
            
            if(lstISS.size() > 0) {
                insert lstISS;
                intISSCount = lstISS.size();
                list<ISS_Line_Item__c> lstISSLI = new list<ISS_Line_Item__c>();
                map<String, Id> mapISSIDs = new map<String, Id>();
                for(ISS__c iteratorISS : lstISS) {
                    mapISSIDs.put(iteratorISS.Name, iteratorISS.Id);
                    //System.debug('Testingg iteratorISS '+iteratorISS);
                    for(ISS_Line_Item__c iteratorISSLI : mapISSLI.get(iteratorISS.Name)){
                        iteratorISSLI.ISS__c = iteratorISS.Id;
                        lstISSLI.add(iteratorISSLI);
                    }
                }
                
                if(lstISSLI.size() > 0) {
                    insert lstISSLI;
                }
                
                if(listNatInv.size() > 0) {
                    NetAmt = 0.00;
                    InvoiceAmt = 0.00;
                    TaxAmt = 0.00;
                    //System.debug('Testingg listNatInv 2 '+listNatInv);
                    //System.debug('Testingg listNatInv size 2 '+listNatInv.size());
                    for(NationalInvoice__c iterator : listNatInv) {
                        TaxAmtTemp = 0;
                        GrossAmt = 0;
                        iterator.ISS__c = mapISSIDs.get(mapNIISS.get(iterator.id));
                        //System.debug('Testingg iterator.id '+iterator.id);
                        //System.debug('Testingg iterator.ISS__c '+iterator.ISS__c);
                        //System.debug('Testingg mapISSIDs.get(mapNIISS.get(iterator.id)) '+mapISSIDs.get(mapNIISS.get(iterator.id)));
                        //System.debug('Testingg mapNIISS.get(iterator.id) '+mapNIISS.get(iterator.id));
                        if(iterator.Tax_Amount_New__c != null){
                            TaxAmtTemp = iterator.Tax_Amount_New__c;
                            TaxAmt = TaxAmt + TaxAmtTemp;
                        }
                        /*if(iterator.Net_Amount__c != null){
                            intInvoiceAmt = intInvoiceAmt + integer.valueOf(iterator.Net_Amount__c);
                        }*/
                        if(iterator.TotalGrossAmount__c != null){
                            GrossAmt = iterator.TotalGrossAmount__c;
                            InvoiceAmt = InvoiceAmt + GrossAmt;
                        }
                        NetAmt = NetAmt + GrossAmt + TaxAmtTemp;
                    }
                    update listNatInv;
                    //System.debug('Testingg listNatInv final '+listNatInv);
                }
                
                lstISSInserted = ISSSOQLMethods.getISSByISS(lstISS);
                if(lstISSInserted != null){
                    intISSCount = lstISSInserted.size();
                }           
                ISSTaxAmt = 0.00;
                ISSAmt = 0.00;
                Commission = 0.00;
                ISSNetAmt = 0.00;
                for(ISS__c iterator : lstISSInserted) {
                    TaxAmtTemp = 0.00;
                    CommissionTemp = 0.00;
                    GrossAmt = 0.00;
                    if(iterator.Total_Tax__c != null){
                        TaxAmtTemp = iterator.Total_Tax__c;
                        ISSTaxAmt  = ISSTaxAmt  + TaxAmtTemp;
                    }
                    /*if(iterator.NetAmount__c != null){
                        ISSNetAmt = ISSNetAmt + iterator.NetAmount__c;
                    }*/
                    if(iterator.Total_Gross_Amount__c != null){
                        GrossAmt = iterator.Total_Gross_Amount__c;
                        ISSAmt = ISSAmt + GrossAmt;
                    }
                    if(iterator.TotalCommission__c != null){
                        CommissionTemp = iterator.TotalCommission__c;
                        Commission = Commission + CommissionTemp ;
                    }
                    
                    ISSNetAmt = ISSNetAmt + ((GrossAmt + TaxAmtTemp) - CommissionTemp);
                }
                
                objAuto.Name = strAutoNumber;
                if(!Test.isRunningtest()) {
                    update objAuto;
                }
            }
            
            if(InvoiceAmt == ISSAmt) {
                chkValGross = true;
            }
            /*iif(TaxAmt == ISSTaxAmt) {
                chkValTax = true;
            }
            f(intNetAmt == intISSNetAmt) {
                chkValNetAmt = true;
            }*/
        }
        updateStatusTable();
    }
    
     public String ISSNumberIncrement(String seqNo) {
        Integer autoNumber = Integer.valueOf(seqNo);        
        autoNumber = autoNumber + 1;
        seqNo = String.valueOf(autoNumber);
        if(seqNo.length() < 5) {
            if(seqNo.length() == 4) {
                seqNo = '0' + String.valueOf(autoNumber);
            }
            else if(seqNo.length() == 3) {
                seqNo = '00' + String.valueOf(autoNumber);
            }
            else if(seqNo.length() == 2) {
                seqNo = '000' + String.valueOf(autoNumber);
            }
            else if(seqNo.length() == 1) {
                seqNo = '0000' + String.valueOf(autoNumber);
            }
        }
        return seqNo;
    }
    
    public string ISSNumberGeneration(String cmrCode, String pubCoCode, String seqNO, Date pubDate) {
        Date myDate = pubDate;
        string retval = '';
        String month = String.valueOf(Date.Today().Month());
        String year = String.valueOf(pubDate.year());
        String day = String.valueOf(Date.Today().day());
        if(day.length() == 1) {
            day = '0' + day;
        }
    
        if(month.length() == 1) {
            month = '0' + month;
        }
    
        year = year.right(2);
    
        if(cmrCode != null) {
            if(cmrCode.length() < 4 && cmrCode.length() == 3) {
                cmrCode = '0' + cmrCode;
            }
            else if (cmrCode.length() < 4 && cmrCode.length() == 2) {
                cmrCode = '00' + cmrCode;
            }
            else if (cmrCode.length() < 4 && cmrCode.length() == 1) {
                cmrCode = '000' + cmrCode;
            }
        }
        else {
            cmrCode = '0000';
        }
    
        retval = retval+ month + day + cmrCode + year + seqNO;
        if(String.isEmpty(pubCoCode)) {
            retval = retval + '000';
        }
        else {
            retval = retval + pubCoCode.right(3);
        }
        return retval;
    }
    
    public void updateStatusTable() {        
        if(listNatBill.size() > 0) {
            for(National_Billing_Status__c NBS : listNatBill) {
                NBS.ISSVAL_VALIDATE_COMMISSION__c = chkValComission;
                NBS.ISSVAL_VALIDATE_ISS_TO_CRM__c = chkValIssCMR;
                NBS.ISSVAL_VALIDATE_NET_AMOUNT__c = chkValNetAmt;
                NBS.ISSVAL_VALIDATE_TAX__c = chkValTax;
                NBS.ISSVAL_VALIDATE_TOTAL_ISS_GROSS_AMOUNT__c = chkValGross;
                NBS.ISSVAL_DIRECTORIES_COMPLETE__c = chkDirComplete;
                NBS.IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c = valCMRForBill;
            }
            update listNatBill;
        }
    }
    
    public void fetchStatusTable() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionCode(strPubCode, strEditionCode);
        //System.debug('National Billing status size is ' + listNatBill.size());
    }
    
    public void assignValuesFromNatBill() {
        chkValComission = natBilling.ISSVAL_VALIDATE_COMMISSION__c;
        chkValIssCMR = natBilling.ISSVAL_VALIDATE_ISS_TO_CRM__c;
        chkValNetAmt = natBilling.ISSVAL_VALIDATE_NET_AMOUNT__c;
        chkValTax = natBilling.ISSVAL_VALIDATE_TAX__c;
        chkValGross = natBilling.ISSVAL_VALIDATE_TOTAL_ISS_GROSS_AMOUNT__c;
        chkDirComplete = natBilling.ISSVAL_DIRECTORIES_COMPLETE__c;
        valCMRForBill = natBilling.IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c;
        
        if(natBilling.ISSVAL_DIRECTORIES_COMPLETE__c) {
            fetchOldValues();
        }
    }
    
    public void fetchOldValues() {
        bolCreateISS = true;
        list<NationalInvoice__c> listNatInv = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeWithISSNotNull(strPubCode, strEditionCode);
        Set<Id> ISSIds = new Set<Id>();
        Decimal TaxAmtTemp;
        Decimal CommissionTemp;
        Decimal GrossAmt;
            
        if(listNatInv.size() > 0) {
            NetAmt = 0.00;
            InvoiceAmt = 0.00;
            TaxAmt = 0.00;
            intNICount = listNatInv.size();
            set<id> setDirectoryId = new Set<id>();
            for(NationalInvoice__c iterator : listNatInv) {
                TaxAmtTemp = 0;
                GrossAmt = 0;
                if(iterator.Tax_Amount_New__c != null){
                    TaxAmtTemp = iterator.Tax_Amount_New__c;
                    TaxAmt = TaxAmt + TaxAmtTemp;
                }
                if(iterator.TotalGrossAmount__c != null){
                    GrossAmt = iterator.TotalGrossAmount__c;
                    InvoiceAmt = InvoiceAmt + GrossAmt;
                }
                NetAmt = NetAmt + GrossAmt + TaxAmtTemp;
                ISSIds.add(iterator.ISS__c);
                setDirectoryId.add(iterator.Directory__c);
            }           
            intDirectorySize = setDirectoryId.size();
        
            lstISSInserted = ISSSOQLMethods.getISSByISSIds(ISSIds);
            if(lstISSInserted != null) {
                intISSCount = lstISSInserted.size();
            }           
            ISSTaxAmt = 0.00;
            ISSAmt = 0.00;
            Commission = 0.00;
            ISSNetAmt = 0.00;
            for(ISS__c iterator : lstISSInserted) {
                TaxAmtTemp = 0.00;
                CommissionTemp = 0.00;
                GrossAmt = 0.00;
                if(iterator.Total_Tax__c != null){
                    TaxAmtTemp = iterator.Total_Tax__c;
                    ISSTaxAmt  = ISSTaxAmt  + TaxAmtTemp;
                }
                if(iterator.Total_Gross_Amount__c != null){
                    GrossAmt = iterator.Total_Gross_Amount__c;
                    ISSAmt = ISSAmt + GrossAmt;
                }
                if(iterator.TotalCommission__c != null){
                    CommissionTemp = iterator.TotalCommission__c;
                    Commission = Commission + CommissionTemp ;
                }
                ISSNetAmt = ISSNetAmt + ((GrossAmt + TaxAmtTemp) - CommissionTemp);
                /*if(iterator.NetAmount__c != null){
                    ISSNetAmt = ISSNetAmt + iterator.NetAmount__c;
                }*/
            }
            NationalInvoice__c objNI = listNatInv[0];
            objPubco = new Account(id = objNI.Publication_Company__c, name = objNI.Publication_Company__r.name);
            objDE = new Directory_Edition__c(id = objNI.Directory_Edition__c, name = objNI.Directory_Edition__r.name);
        }
        if(InvoiceAmt == ISSAmt) {
            chkValGross = true;
        }
        if(TaxAmt == ISSTaxAmt) {
            chkValTax = true;
        }
    }
        
    public void calcualteTotalForISS() {
        totalISSTaxAmount = 0;
        totalISSADJAmount = 0;
        totalISSNetAmount = 0; 
        totalISSGrossAmount = 0; 
        totalISSCommission = 0;
        
        if(lstISSInserted.size() > 0) {
            for(ISS__c ISS : lstISSInserted) {
                if(ISS.NetAmount__c != null) {
                    totalISSNetAmount += ISS.NetAmount__c;
                }
                
                if(ISS.Total_ADJ_Amount__c != null) {
                    totalISSADJAmount += ISS.Total_ADJ_Amount__c;
                }
                
                if(ISS.Total_Tax__c != null) {
                    totalISSTaxAmount += ISS.Total_Tax__c;
                }
                
                if(ISS.Total_Gross_Amount__c != null) {
                    totalISSGrossAmount += ISS.Total_Gross_Amount__c;
                }
                
                if(ISS.TotalCommission__c != null) {
                    totalISSCommission += ISS.TotalCommission__c;
                }
            }
        }
    }
}