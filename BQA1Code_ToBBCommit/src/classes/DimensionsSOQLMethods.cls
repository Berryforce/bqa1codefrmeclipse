public class DimensionsSOQLMethods {
	
	public static List<c2g__codaDimension1__c> fetchDimensions1(Set<String> reportingCode) {
		return [SELECT Id, Name, c2g__ReportingCode__c FROM c2g__codaDimension1__c WHERE c2g__ReportingCode__c IN : reportingCode];
	}
	
	public static List<c2g__codaDimension2__c> fetchDimensions2(Set<String> reportingCode) {
		return [SELECT Id, Name, c2g__ReportingCode__c FROM c2g__codaDimension2__c WHERE Name IN : reportingCode];
	}
	
	public static List<c2g__codaDimension3__c> fetchDimensions3(Set<String> reportingCode) {
		return [SELECT Id, Name, c2g__ReportingCode__c FROM c2g__codaDimension3__c WHERE c2g__ReportingCode__c IN : reportingCode];
	}
	
	public static List<c2g__codaDimension4__c> fetchDimensions4(Set<String> reportingCode) {
		return [SELECT Id, Name, c2g__ReportingCode__c FROM c2g__codaDimension4__c WHERE c2g__ReportingCode__c IN : reportingCode];
	}
	
}