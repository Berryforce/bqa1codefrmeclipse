@isTest
public class PrintFirstMonthlyBillingBatchTest {
	@testSetup static void setup() {
		User_Ids_For_Email__c userId = TestMethodsUtility.createUserIdForEmail('LocalBilling');
		Batch_Size__c FMPBatchSize = TestMethodsUtility.createBatchSize('FMP', 5); 
		Batch_Size__c PMBBatchSize = TestMethodsUtility.createBatchSize('PMB', 5); 
		Batch_Size__c DigitalSize = TestMethodsUtility.createBatchSize('DigitalMonthly', 5); 
		TBC_Cron__c DigitalPrintCron = TestMethodsUtility.createTBCCron('DigitalPrintCron');
		TBC_Cron__c FMPCron = TestMethodsUtility.createTBCCron('FMPCron');
		TBC_Cron__c CleanUpCron = TestMethodsUtility.createTBCCron('CleanUpCron');
	}
	
    static testMethod void PrintFirstMonthBillingTest() {
        Test.startTest();                     
		Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Monthly_Cron__c monCron = TestMethodsUtility.createMonthlyCron(CommonMessages.PrintMediaType, oln.Billing_End_Date__c, og.Id);
        PrintFirstMonthBilling obj = new PrintFirstMonthBilling(oln.Billing_End_Date__c, CommonMessages.PrintMediaType);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }
    
    static testMethod void PrintFirstMonthBillingTest1() {
        Test.startTest();                     
		Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Monthly_Cron__c monCron = TestMethodsUtility.createMonthlyCron(CommonMessages.PrintMediaType, oln.Billing_End_Date__c, og.Id);
        PrintFirstMonthBilling obj = new PrintFirstMonthBilling(oln.Billing_End_Date__c, CommonMessages.PrintMediaType, true);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }
}