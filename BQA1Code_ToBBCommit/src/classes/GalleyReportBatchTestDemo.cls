global class GalleyReportBatchTestDemo Implements Database.Batchable <sObject>, Database.Stateful {

    global Database.queryLocator start(Database.BatchableContext bc) {
    
Database.QueryLocator q = Database.getQueryLocator(

    [SELECT Name FROM directory_Pagination__c LIMIT 51000]);

// Get an iterator

Database.QueryLocatorIterator it =  q.iterator();

// Iterate over the records

while (it.hasNext())

{

    directory_Pagination__c a = (directory_Pagination__c)it.next();

    System.debug(a);

}


        String SOQLQuery = 'select id from Directory_Pagination__c limit 5';
        return Database.getQueryLocator(SOQLQuery);
    
    }
    
    global void execute(Database.BatchableContext bc, list<Directory_Pagination__c> listDirPagId) {
    }
    
    global void finish(Database.BatchableContext bc) {
    }

}