@IsTest (SeeAllData=true)
public class Account_BIUAIUDTest {
    static testmethod void advertiserAlertsFlagsTest(){
        List<Account> lstAccount = new List<Account>();
        Test.startTest();
        Account Acc1 = TestMethodsUtility.generateAccount('cmr');
        Acc1.Collection_Status__c = '1st Party Bulk Bill';
        Acc1.Bankruptcy__c = false;
        lstAccount.add(Acc1);       
        Account Acc2 = TestMethodsUtility.generateAccount('customer');
        Acc2.Collection_Status__c = '1st Party Bulk Bill';
        Acc2.Bankruptcy__c = true;
        lstAccount.add(Acc2);
        insert lstAccount;
        Test.stopTest();        
    }
    
    static testmethod void calculateFlagDaysTest1(){
        List<Account> lstAccount = new List<Account>();
        List<Account> lstAccountUpdate = new List<Account>();
        Test.startTest();
        Account Acc1 = TestMethodsUtility.generateAccount('customer');
        Acc1.Bankruptcy__c = true;
        Acc1.Collection_Status__c = '1st Party Bulk Bill';
        lstAccount.add(Acc1);
        
        Account Acc2 = TestMethodsUtility.generateAccount('customer');
        Acc2.Bankruptcy__c = false;
        Acc2.First_Party_Hold__c = false;
        Acc2.Collection_Status__c = '1st Party Status';
        lstAccount.add(Acc2);
        
        Account Acc3 = TestMethodsUtility.generateAccount('customer');
        Acc3.Bankruptcy__c = false;
        Acc3.Collection_Status__c = '3rd Party Write-off';
        lstAccount.add(Acc3);
        
        Account Acc4 = TestMethodsUtility.generateAccount('customer');
        Acc4.Bankruptcy__c = false;
        Acc4.Collection_Status__c = 'OCA Closed';
        lstAccount.add(Acc4);                 

        Account Acc5 = TestMethodsUtility.generateAccount('customer');
        Acc5.Bankruptcy__c = false;
        Acc5.Collection_Status__c = 'OCA Settlement';
        lstAccount.add(Acc5); 
        
        insert lstAccount;
        
        Acc1.Collection_Status__c = '1st Party Status';
        Acc1.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc1);

        Acc2.Collection_Status__c = '1st Party Bulk Bill';
        Acc2.Bankruptcy__c = true;
        lstAccountUpdate.add(Acc2);
                
        Acc3.Collection_Status__c = 'OCA Closed';
        Acc3.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc3);
        
        Acc4.Collection_Status__c = 'OCA Settlement';
        Acc4.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc4);
        
        Acc5.Collection_Status__c = '3rd Party Write-off';
        Acc5.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc5);     
        
        update lstAccountUpdate;
                            
        Test.stopTest();            
    }
    static testmethod void calculateFlagDaysTest2(){
        List<Account> lstAccount = new List<Account>();
        List<Account> lstAccountUpdate = new List<Account>();
        Test.startTest();
        Account Acc1 = TestMethodsUtility.generateAccount('customer');
        Acc1.Bankruptcy__c = true;
        Acc1.Collection_Status__c = '1st Party Bulk Bill';
        Acc1.Last_Modification_Date__c = Date.Today()-5;
        lstAccount.add(Acc1);
        
        Account Acc2 = TestMethodsUtility.generateAccount('customer');
        Acc2.Bankruptcy__c = false;
        Acc2.First_Party_Hold__c = false;
        Acc2.Collection_Status__c = '1st Party Status';
        Acc2.Last_Modification_Date__c = Date.Today()-5;
        lstAccount.add(Acc2);
        
        Account Acc3 = TestMethodsUtility.generateAccount('customer');
        Acc3.Bankruptcy__c = false;
        Acc3.Collection_Status__c = '3rd Party Write-off';
        Acc3.Last_Modification_Date__c = Date.Today()-5;
        lstAccount.add(Acc3);
        
        Account Acc4 = TestMethodsUtility.generateAccount('customer');
        Acc4.Bankruptcy__c = false;
        Acc4.Collection_Status__c = 'OCA Closed';
        Acc4.Last_Modification_Date__c = Date.Today()-5;
        lstAccount.add(Acc4);                 

        Account Acc5 = TestMethodsUtility.generateAccount('customer');
        Acc5.Bankruptcy__c = false;
        Acc5.Collection_Status__c = 'OCA Settlement';
        Acc5.Last_Modification_Date__c = Date.Today()-5;
        lstAccount.add(Acc5); 
        
        insert lstAccount;
        
        Acc1.Collection_Status__c = '1st Party Status';
        Acc1.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc1);

        Acc2.Collection_Status__c = '1st Party Bulk Bill';
        Acc2.Bankruptcy__c = true;
        lstAccountUpdate.add(Acc2);
                
        Acc3.Collection_Status__c = 'OCA Closed';
        Acc3.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc3);
        
        Acc4.Collection_Status__c = 'OCA Settlement';
        Acc4.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc4);
        
        Acc5.Collection_Status__c = '3rd Party Write-off';
        Acc5.Bankruptcy__c = false;
        lstAccountUpdate.add(Acc5);     
        
        update lstAccountUpdate;
                            
        Test.stopTest();            
    }
}