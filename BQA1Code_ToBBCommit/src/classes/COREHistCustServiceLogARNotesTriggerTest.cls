@isTest
public with sharing class COREHistCustServiceLogARNotesTriggerTest {
	@isTest static void COREHistTest() {
		Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
		CORE_Hist_Cust_Service_Log_AR_Notes__c obj = TestMethodsUtility.createCOREHisCusSerLogNotes(acct);
        Account acct1 = TestMethodsUtility.generateAccount('cmr');
        acct1.X3l_External_ID__c = obj.Core_Account_ID__c;
        insert acct1;
        Account acct2 = TestMethodsUtility.generateAccount('cmr');
        acct2.Parent_3L_External_ID__c = obj.Core_Account_ID__c;
        insert acct2;
        update obj;
	}
}