global class DPSortAsFieldUpdateScheduler implements Schedulable {
   public Interface DPSortAsFieldUpdateSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('DPSortAsFieldUpdateSchedulerHndlr');
        if(targetType != null) {
            DPSortAsFieldUpdateSchedulerInterface obj = (DPSortAsFieldUpdateSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}