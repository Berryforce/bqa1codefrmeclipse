@isTest(seealldata=true)
public class NationalBillingISSValidationTest {
    public static testmethod void ISSValidationTest() {
    
        insert new ISSAutoNumber__c(name = '00001');
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        date myDate = 
        date.newInstance(2014, 12, 17);
        objDirEd.Pub_Date__c = myDate;
        insert objDirEd;
        
        objDirEd = [SELECT Id, Edition_Code__c, Directory__c, Directory__r.Directory_Code__c, Directory__r.Name FROM Directory_Edition__c WHERE Id = : objDirEd.Id];
        
        National_Commission__c objNC = new National_Commission__c(Rate__c=10);
        insert objNC;
        
        Account objPubcoAcct = TestMethodsUtility.generateAccount('publication');
        objPubcoAcct.Publication_Code__c = '0012';
        objPubcoAcct.National_Commission__c = objNC.id;
        insert objPubcoAcct;
        Account objCMRAcct = TestMethodsUtility.generateCMRAccount();
        objCMRAcct.CMR_Number__c= '1234';
        objCMRAcct.National_Commission__c = objNC.id;
        objCMRAcct.AccountNumber = '1234';
        insert objCMRAcct;
        Account objClientAcct = TestMethodsUtility.generateCustomerAccount();
        objClientAcct.Client_Number__c='2345';
        objClientAcct.National_Commission__c = objNC.id;
        insert objClientAcct;
        
        National_Billing_Status__c natBillStatus = TestMethodsUtility.generateNatBillStatus(objPubcoAcct.Id, objDirEd.Id); 
        natBillStatus.ISSVAL_DIRECTORIES_COMPLETE__c = true;
        insert natBillStatus;
        
        NationalInvoice__c objNI = new NationalInvoice__c(Directory_Edition__c = objDirEd.id, Publication_Company__c=objPubcoAcct.id, Directory__c = objDir.id, 
                                                         cmr__c=objCMRAcct.id, Client_name__c=objClientAcct.id, Publication_Date__c = system.today());
        insert objNI;
        NationalInvoiceLineItem__c objNILI = new NationalInvoiceLineItem__c(Gross_Amount__c=500, NationalInvoice__c=objNI.id, ADJ_Amount__c=100);
        insert objNILI;
        ISS__c objISS = TestMethodsUtility.generateISS();
        objISS.Invoice_Date__c = System.today();
        objISS.CMR__c = objCMRAcct.id;
        objISS.Publication_Company__c = objPubcoAcct.id;
        insert objISS;
        NationalBillingISSValidation objNBISS = new NationalBillingISSValidation();
        objNBISS.strPubCode = '0012';
        objNBISS.strEditionCode = objDirEd.Edition_Code__c;
        objNBISS.fetchDirectories();
        objNBISS.lstISSInserted = new List<ISS__c>{objISS};
        objNBISS.displayISS();
        objNBISS.hideISS();
        objNBISS.reportsTab();
        objNBISS.fetchDirectories();
        objNBISS.createISS();
        
        objNBISS.ISSNumberIncrement('1234');
        objNBISS.ISSNumberIncrement('123');
        objNBISS.ISSNumberIncrement('12');
        objNBISS.ISSNumberGeneration('123','123','123', system.today());
        objNBISS.ISSNumberGeneration('12','123','123', system.today());
        objNBISS.ISSNumberGeneration('1','','123', system.today());
        objNBISS.ISSNumberGeneration(null,'123','123', system.today());
        objNBISS.updateStatusTable();
    }
}