@isTest(seeAllData=True)
private class AttachmentSpecArtName_BITest {
 
    static testmethod void CreatespecArtAttachmentTest()
    {
    
        Test.startTest();
        
        /*User objUser = [Select UserRole.Name, UserRole.Id, UserRoleId, Id From User where UserRole.Name = 'Account Manager' limit 1];
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        Account objAccount = new Account(Primary_Canvass__c = objCanvass.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Account_Manager__c = objUser.Id, Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');        
        
        insert objAccount;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
       

       
        User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null limit 1];*/
        List<Attachment> lstAtt = new List<Attachment>();
        
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Spec_Art_Request__c newSAR = TestMethodsUtility.createSpecArtRequest(newAccount);
        /*
        Digital_Product_Requirement__c newDPR = TestMethodsUtility.createDataFulfillmentForm('iYP Gold');
        YPC_Graphics__c newYPCG = TestMethodsUtility.createYPCGraphics(newDPR);*/
        
        Attachment Att1 = TestMethodsUtility.generateAttachment(newSAR.Id);
        //Attachment Att2 = TestMethodsUtility.generateAttachment(newDPR.Id);
        //Attachment Att3 = TestMethodsUtility.generateAttachment(newYPCG.Id);
        lstAtt.add(Att1);
        //lstAtt.add(Att2);
        //lstAtt.add(Att3);
        insert lstAtt;
        /*Spec_Art_Request__c objSAR = new Spec_Art_Request__c();
        
        
        objSAR.Log_on_name__c='r276';
        objSAR.Colors__c='full';
        objSAR.Description_of_Artwork_to_be_completed__c='Test Art Work';
        objSAR.Account__c=newAccount.Id;
        
        insert objSAR;
        
        
        system.debug('********Spec Art*******'+objSAR);
        system.debug('********Spec Art*******'+objSAR.Id);
        String prefix;
        List<Attachment> atLst = new List<Attachment>();
        Attachment atPdf = new Attachment();
        atPdf.Name = 'Test1';
        atPdf.parentId=objSAR.id;
        atPdf.body=blob.topdf('test');
        prefix= String.valueOf(objSAR.id).substring(0, 3);
        atLst.add(atpdf);
        
        insert atLst;
        
        system.debug('********Spec Art Attachment*******'+atLst);*/
        
        Test.stopTest(); 
    }
}