public class Cancellation_close_case_controller {
    public Case Caseobj{get; set;}
    public String caseID{get; set;}
    
    public Cancellation_close_case_controller(ApexPages.StandardController controller) {
        caseID = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference closeCaseValidation() 
    {
        caseID = ApexPages.currentPage().getParameters().get('id');
        Caseobj = [SELECT  Id,Zero_Cancel_Fee_Requested__c, Zero_Cancel_Fee_Approved__c,Zero_Cancel_Fee_Request_Rejected__c
                   FROM Case where Id = :caseID limit 1];       
        String returnURL='/'+caseID+'/s?retURL='+caseID;
        PageReference  pageRef = new PageReference(returnURL);
        if(Caseobj.Zero_Cancel_Fee_Requested__c){
            IF(!(Caseobj.Zero_Cancel_Fee_Requested__c && (Caseobj.Zero_Cancel_Fee_Approved__c || Caseobj.Zero_Cancel_Fee_Request_Rejected__c)))
                pageRef = null;
        }
        return pageRef;
    }
}