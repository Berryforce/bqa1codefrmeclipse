@isTest
private class TermsAndConditionsControllerTest {
    static testMethod void TermsAndConditionsControllerTestMethod1() {
    	User newUser = TestMethodsUtility.generateUser();
    	newUser.Terms_And_Conditions_Agreed__c = true;
    	insert newUser;    	
    	system.currentPageReference().getParameters().put('ContactId', newUser.Id);
		TermsAndConditionsController TACC1 = new TermsAndConditionsController('Terms_And_Conditions');
		TermsAndConditionsController TACC2 = new TermsAndConditionsController();
		Boolean hasMessage = TACC1.hasMessages;
		TACC1.ok();
		TACC1.cancel();
    }
    static testMethod void TermsAndConditionsControllerTestMethod2() {
    	system.currentPageReference().getParameters().put('ContactId', 'testId');
		TermsAndConditionsController TACC1 = new TermsAndConditionsController('TestResource');
		TermsAndConditionsController TACC2 = new TermsAndConditionsController();
		TACC2.ok();
		TACC2.cancel();
    }
}