public with sharing class LocalBillingSendToWFOneController{
    public Dummy_Object__c objDummy {get;set;}
    @TestVisible  private string createStmtBatchId;
    public String strStmtCreationBatchStatus {get;set;}
    public boolean bStmtCreationEnabledPoller {get;set;}
    string[] selectedcheckboxes=new string[]{};
    //Report Ids Custom Settings 
    public Id rptStmtReportId {get;set;}
	public Id StmtSINFalloutReportId {get;set;}
	public Id StmtSCRFalloutReportId {get;set;}
	public Id StmtCELIFalloutReportId {get;set;}
    
    public LocalBillingSendToWFOneController(){
        bStmtCreationEnabledPoller = false;
        objDummy = new Dummy_Object__c();
        Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
        if(reportIds.get('StatementReport')!=null) rptStmtReportId = reportIds.get('StatementReport').Report_Id__c;
		if(reportIds.get('StmtSCRFallout')!=null) StmtSCRFalloutReportId = reportIds.get('StmtSCRFallout').Report_Id__c;
		if(reportIds.get('StmtSINFallout')!=null) StmtSINFalloutReportId = reportIds.get('StmtSINFallout').Report_Id__c;
		if(reportIds.get('StmtCELIFallout')!=null) StmtCELIFalloutReportId = reportIds.get('StmtCELIFallout').Report_Id__c;	 
    }
    
    public List<SelectOption> getStmntCheckboxVal() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('0','Statement Summary (Balanced)')); 
        options.add(new SelectOption('1','Workflow One Summary (Balanced)')); 
        options.add(new SelectOption('2','Marketing Message Validated')); 
        return options; 
    }
    
    public String[] getcheckboxvalues() {
        return selectedcheckboxes;
    }
                        
    public void setcheckboxvalues(String[] selectedcheckboxes) { 
        this.selectedcheckboxes= selectedcheckboxes;
    }
    
    public void sendStmtFiletoWFOne() {
       if(!Test.isRunningTest())
            createStmtBatchId = Database.executeBatch(new localBillingSendToWFOneBatch(objDummy.Date__c), 2000);
        strStmtCreationBatchStatus ='Status of Statement Creation Batch : Queued';
        bStmtCreationEnabledPoller = true;
    }
    
    public void apexCreateStmtJobStatus() {
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(createStmtBatchId!= null) {
            setBatchIds.add(createStmtBatchId);
        }
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(createStmtBatchId)) {
                strStmtCreationBatchStatus = 'Status of Statement Creation Batch : ' + mapBatchFFJob.get(createStmtBatchId).Status;
                if(mapBatchFFJob.get(createStmtBatchId).Status == 'Completed' || mapBatchFFJob.get(createStmtBatchId).Status == 'Failed') {
                    bStatus = true;
                }               
            }
            else {
                bStatus = true;
            }
            if(bStatus) {
                bStmtCreationEnabledPoller = false;
            }
        }
    }
}