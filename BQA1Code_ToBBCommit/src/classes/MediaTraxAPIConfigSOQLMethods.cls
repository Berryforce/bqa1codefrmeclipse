public class MediaTraxAPIConfigSOQLMethods {

	public static list<MediaTrax_API_Configuration__c> getMediaTraxOnAction(Set<String> setActionName) {
    	return [SELECT API_URL__c, AuthToken__c, Callout_Method__c, Content_Type__c, Endpoint_URL__c, Header_SOAPAction__c, 
                Name, Password__c, PredicateField__c, PredicateOperator__c, PredicateValue__c, Response_Method__c, Return_Fields__c, Return_Method__c, SOAP_URL__c, UserName__c, Operation__c, Fields_Mapping__c 
                FROM MediaTrax_API_Configuration__c where Name IN:setActionName];
    }
}