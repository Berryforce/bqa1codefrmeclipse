public class CreateNOSOListingfromLead{
    public static void createNosoListing(set<Id> ldonvertedId){
        map<string,Lead> mapleadStr = new map<string,Lead>();
        list<lead> leadLst = [Select Id,Name,FirstName,Company,LastName,Phone,City,State,Country,PostalCode,Street,ConvertedAccountId,Primary_Canvass__c from lead where Id IN :ldonvertedId];
        set<string> setName = new set<string>();
        set<string> setPhone = new set<string>();
        if(leadLst.size()>0){
            for(Lead objld : leadLst){
                string strLdNamePhone = objld.Name+''+objld.Phone;
                mapleadStr.put(strLdNamePhone,objld);
                if(objld.Name != null){
                    setName.add(objld.Name);
                }
                if(objld.Phone != null){
                    setPhone.add(objld.Phone);
                }
            }
        }
        map<string,Listing__c> maplistingStr = new map<string,Listing__c>();
        if(mapleadStr.size()>0){
            for(Listing__c objLst : [Select Id,Name,LST_Last_Name_Business_Name__c,Phone__c from Listing__c where LST_Last_Name_Business_Name__c IN :setName]){
                string strListNamePhone = objLst.LST_Last_Name_Business_Name__c+''+objLst.Phone__c;
                maplistingStr.put(strListNamePhone,objLst);
            }
        }
        list<listing__c> listingInsertLst = new list<Listing__c>();
        list<string> strLst = new list<string>();
        string street;
        string listingstreetName;
        string listingstreetNumber;
        
        for(string objldStr : mapleadStr.keyset()){
            if(maplistingStr.get(objldStr) == null){
                Lead objLead = mapleadStr.get(objldStr);
                Listing__c objListing = new Listing__c();
                objListing.LST_Last_Name_Business_Name__c = objLead.Company;
                objListing.Phone__c = objLead.Phone;
                objListing.Listing_Type__c = 'NOSO';
                objListing.Bus_Res_Gov_Indicator__c = 'Business';
                objListing.Listing_City__c = objLead.City;
                objListing.Listing_Country__c = objLead.Country;
                objListing.Listing_Postal_Code__c = objLead.PostalCode;
                objListing.Listing_State__c = objLead.State;
                //objListing.Listing_Street__c = objLead.Street;
                objListing.Account__c=objLead.ConvertedAccountId;
                objListing.Primary_Canvass__c = objLead.Primary_Canvass__c;
                street = objLead.Street;
                if(street != null){
                    listingstreetNumber = street.substring(0,street.indexOf(' '));
                    listingstreetName = street.substring(street.indexOf(' ')+1);
                }
                objListing.Listing_Street__c = listingstreetName;
                objListing.Listing_Street_Number__c = listingstreetNumber;
                
                listingInsertLst.add(objListing);
                
            }
        }
        if(listingInsertLst.size() >0){
            insert listingInsertLst;
        }
        
    }
}