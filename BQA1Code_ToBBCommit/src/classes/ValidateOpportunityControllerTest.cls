@isTest
public with sharing class ValidateOpportunityControllerTest {
	static testMethod void valTest() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        oppty.Billing_Frequency__c = 'Single Payment';
        insert oppty;
		apexpages.currentpage().getparameters().put('Id' , oppty.Id);
		ValidateOpportunityController valOppty = new ValidateOpportunityController(new ApexPages.StandardController(oppty));
		valOppty.opptyValidation();
		valOppty.backToOpportunity();
		valOppty.bFlag = true;
		valOppty.closeOppty();
        Test.StopTest();
	}
}