/**************************************************************************
Apex Batch class to send sensitive heading flag value to fulfillment vendor
Created By: Reddy/Magulan
Updated Date: 11/10/2014
$Id$
***************************************************************************/
global class SensitiveHeadingUpdateBatch Implements Database.Batchable < sObject > , Database.AllowsCallouts {
    global Database.Querylocator start(Database.BatchableContext bc) {
        String soql = 'SELECT Account__c, Id FROM Order_Line_Items__c WHERE Directory_Heading__r.Sensitive_Heading__c = true AND Account__r.Sensitive_Heading__c = false';
        return Database.getQueryLocator(soql);
    }

    global void execute(Database.BatchableContext bc, List < Order_Line_Items__c > listOLI) {
        Set < Id > acctIds = new Set < Id > ();
        List < Account > listAccount = new List < Account > ();
        List < Account > accToUpdt = new List < Account > ();
        String furl;

        //Get nigel endpoint url
        string url = Label.Talus_Nigel_Url;

        for (Order_Line_Items__c OLI: listOLI) {
            acctIds.add(OLI.Account__c);
        }

        listAccount = AccountSOQLMethods.getAccountsByAccountIdWithSensitiveHeadFalse(acctIds);

        //System.debug('************List of Accounts************' + listAccount);

        if (listAccount.size() > 0) {

            for (Account acc: listAccount) {

                furl = url + acc.TalusAccountId__c + '/';

                JSONGenerator jsonGen = JSON.createGenerator(true);
				String jsonString;

				jsonGen.writeStartObject();

				jsonGen.writeBooleanField('privacy_flag', true);

				jsonGen.writeEndObject();

				jsonString = jsonGen.getAsString();

                //System.debug('************JSON Body for Sensitive Heading************' + jsonString);

                if (String.isNotBlank(jsonString)) {

                    Http h = new Http();
                    HttpRequest req = new HttpRequest();

                    //Default time is in milliseconds - this is now set two minutes
                    req.setTimeout(120000);
                    req.setMethod('PUT');

                    if (Test.isRunningTest()) {
                        req.setEndpoint(furl + 'q?param=1 & param=2');
                    } else {
                        req.setEndpoint(furl);
                    }
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setBody(jsonString);
                    Oauth1 reuest = new Oauth1();
                    reuest.sign(req);
                    HttpResponse res = new HttpResponse();
                    if (!Test.isRunningTest()) {
                        res = h.send(req);
                    }
                    String newResponsestring = res.getBody();

                    System.debug('************Response Body for Sensitive Heading Update************' + res.getStatusCode() + newResponsestring);

                    if (Test.isRunningTest() && acc.Name == 'UpdateAccountTest') {
                        res.setStatusCode(202);
                    }

                    try {

                        if (String.isNotBlank(newResponsestring) && (res.getStatusCode() == 202 || res.getStatusCode() == 200)) {

                            //Updating account fields with JSON response    
                            acc.Talus_Updated_JSON_Body__c = string.valueof(newResponsestring);
                            acc.Sensitive_Heading__c = true;
                            accToUpdt.add(acc);

                        } 

                    } catch (Exception e) {

                        //Create Error Log record if there is an exception 
                        ErrorLog.CreateErrorLog(e.getmessage(), acc.id, res.getStatusCode(), 'Account');

                    }

                }

            }

            if(accToUpdt.size()>0){

            	update accToUpdt;
            
            }
            
        }
    }

    global void finish(Database.BatchableContext bc) {

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        //Send email to Admin on batch job completion
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {
            'reddy.yellanki@theberrycompany.com', 'Shelley.Glueckert@theberrycompany.com' 
        };
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchJob For Account Sensitive Heading ' + a.Status);
        mail.setPlainTextBody('The batch Apex job is processed ' + a.TotalJobItems +
            ' batches with ' + a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });

    }
}