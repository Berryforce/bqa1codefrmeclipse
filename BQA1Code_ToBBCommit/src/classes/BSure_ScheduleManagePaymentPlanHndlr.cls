global class BSure_ScheduleManagePaymentPlanHndlr implements BSure_ScheduleManagePaymentPlan.BSure_ScheduleManagePaymentPlanInterface {
    global void execute(SchedulableContext sc) {
      bSure_ManagePaymentPlan objBatchPayment = new bSure_ManagePaymentPlan(); 
      database.executebatch(objBatchPayment);
   }  
}