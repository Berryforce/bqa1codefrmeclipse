global class ListingTriggerForDataMigrBatch implements Database.Batchable<sObject>{
    
    global string filtercondition;
    global Boolean bolStopExecution;
    
    global ListingTriggerForDataMigrBatch(String condition){
        filtercondition=condition;
    }
    
    global ListingTriggerForDataMigrBatch(String condition, Boolean bolStopExec){
        filtercondition = condition;
        bolStopExecution = bolStopExec;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select l.Lst_Last_Name_Business_Name__c, l.Year__c, l.X3235_Status__c, l.Workflow_Fire_Field__c, l.Website__c, l.Telco_Sort_Order__c,'+
                       'l.Telco_Provider__c, l.Telco_Provider_Name__c, l.Telco_Provider_Account_ID__c, l.Telco_Name__c, l.SystemModstamp,'+
                       'l.Street_For_Ext_ID__c, l.StrDisconnectLstMatch__c, l.StrDisconnectLstMatchRes__c, l.Sort_As_Full__c, l.Service_Order__c,'+
                       'l.Service_Order_Stage_item__c, l.Section__c, l.Secondary_Surname__c, l.Salutation__c, l.Sales_Rep__c, l.Right_Telephone_Phrase__c,'+
                       'l.Requires_LCN_Telco_Notification__c, l.Region__c, l.RecordTypeId, l.Provided_By_CLEC__c, l.Primary_Canvass__c, l.Previous_Street_Number__c,'+
                       'l.Previous_Street_Name__c, l.Previous_State__c, l.Previous_Postal_Code__c, l.Previous_Phone__c, l.Previous_PO_Box__c,'+
                       'l.Previous_Last_Name_Business_Name__c, l.Previous_City__c, l.Previous_Address__c, l.Phone__c, l.Phone_Unformatted__c, l.Phone_Type__c,'+
                       'l.Phone_Override__c, l.Phone_Override_Indicator__c, l.Phone_For_Ext_ID__c,l.OwnerId,'+
                       'l.Omit_Phone_OTN__c, l.Omit_Address_OAD__c,  l.Normalized_Secondary_Surname__c, l.Normalized_Phone__c,'+
                       'l.Normalized_Listing_Street_Number__c, l.Normalized_Listing_Street_Name__c, l.Normalized_Listing_Postal_Code__c,'+
                       'l.Normalized_Listing_PO_Box__c, l.Normalized_Listing_City__c, l.Normalized_Lineage_Title__c, l.Normalized_Last_Name_Business_Name__c,'+
                       'l.Normalized_Honorary_Title__c, l.Normalized_First_Name__c, l.Normalized_Designation__c, l.Name, l.Mobile__c, l.Manual_Sort_As_Override__c,'+
                       'l.Main_Listing__c, l.Main_Listed_Number__c, l.Listing_Type__c, l.Listing_Street__c, l.Listing_Street_Number__c, l.Listing_State__c,'+
                       'l.Listing_Postal_Code__c, l.Listing_PO_Box__c, l.Listing_External_ID__c, l.Listing_Country__c, l.Listing_City__c, l.Lineage_Title__c, l.Line__c,'+
                       'l.Line_WF__c, l.Left_Telephone_Phrase__c, l.Lead__c, l.Lead_Status__c, l.Lead_Source__c, l.Lead_Record_Type__c, l.Lead_Owner__c, l.Lead_Is_Converted__c,'+
                       'l.Lead_Account_Source__c, l.Last_Name__c, l.LastViewedDate, l.LastReferencedDate, l.LastModifiedDate, l.LastModifiedById, l.LastActivityDate, l.LCN_Change_Date__c,'+
                       'l.Is_Opportunity__c, l.Is_Lead__c, l.IsDeleted, l.Internal_External__c, l.Industry__c, l.Include_Foreign_Listings__c, l.Id, l.Honorary_Title__c, l.First_Name__c, l.Fax__c,'+
                       'l.Fax_Opt_Out__c, l.Exchange__c, l.Exchange_WF__c, l.Enterprise_Customer_ID__c, l.Email__c, l.Email_Opt_Out__c, l.Effective_Date__c, l.Disconnected__c, l.Disconnected_Via_ABD__c,'+
                       'l.Disconnect_Verified__c, l.Disconnect_Reason__c, l.Designation__c, l.Description__c, l.Customer_Type__c, l.Customer_Advised_Charges_May_Apply__c, l.Customer_Advised_Charges_May_Apply_Y_N__c,'+
                       'l.Cross_Reference_Text__c, l.Cross_Reference_Indicator__c, l.CreatedDate, l.CreatedById, l.Company__c, l.Changes_Authorized_By__c, l.CORE_Migration_ID__c, l.CORE_Customer_Migration_ID__c,'+
                       'l.CLEC_Provider__c, l.CLEC_Provider_Name__c, l.Bus_Res_Gov_Indicator__c, l.Billing_Street__c, l.Billing_State__c, l.Billing_Postal_Code__c, l.Billing_Country__c, l.Billing_City__c, l.BOID__c,'+
                       'l.BEX__c, l.Area_Code__c, l.Area_Code_WF__c, l.Annual_Revenue__c, l.Address__c, l.Active_or_Disconnected__c, l.Account__c, l.Account_Type_Selection__c, l.Account_Lead_Owner_Name__c, l.Account_Lead_Owner_Email__c,'+
                       'l.LST_NormalisedLastName_LEFT_1_Char__c,l.Account_Lead_Name__c, l.Account_Lead_Main_Listed_Number__c, l.AccountManager__c, l.AccountLead_Address__c, l.ABD_Pending_Review__c,l.Lead__r.Telco_Partner_Account__c, l.Account__r.Telco_Partner__c,l.Listing_Phone_Unformatted_Substitute__c'+
                       ' From Listing__c l '+filtercondition+' Order By Account__c, Lead__c';
        return Database.getQueryLocator(query);  
    }
    
    global void execute(Database.BatchableContext bc, List<Listing__c> listingList) {
        Map<Id,Lead> leadMapToUpdate=new Map<Id,Lead>();
        Map<Id,Account> accMapToUpdate=new Map<Id,Account>();
        List<Listing__c> listingListToUpdate=new List<Listing__c>();
        //List<Order_Line_Items__c> oliListToUpdate=new List<Order_Line_Items__c>();
        Savepoint sp = Database.setSavepoint();
        try{
            for(Listing__c listObj:listingList){
                listObj.Lst_StrListingMatch__c = listObj.Lst_Last_Name_Business_Name__c+''+listObj.First_Name__c+''+listObj.Phone__c+''+listObj.Listing_Street_Number__c+''+listObj.Listing_Street__c+''+listObj.Listing_PO_Box__c;
                if(listObj.Phone__c!=null && String.isNotBlank(listObj.Phone__c)){
                        listObj.Listing_Phone_Unformatted_Substitute__c=listObj.Phone__c.remove('(');
                        listObj.Listing_Phone_Unformatted_Substitute__c=listObj.Listing_Phone_Unformatted_Substitute__c.remove(')');
                        listObj.Listing_Phone_Unformatted_Substitute__c=listObj.Listing_Phone_Unformatted_Substitute__c.remove('-');
                        listObj.Listing_Phone_Unformatted_Substitute__c=listObj.Listing_Phone_Unformatted_Substitute__c.remove(' ');
                }
                if(listObj.Normalized_Last_Name_Business_Name__c!=null && string.isNotBlank(listObj.Normalized_Last_Name_Business_Name__c)){
                    listObj.LST_NormalisedLastName_LEFT_1_Char__c=listObj.Normalized_Last_Name_Business_Name__c.Left(1);
                }
                listObj.Li_isTriggerExecuted__c = true;
                if(listObj.Lead__c!=null){
                    listObj.Is_Lead__c=true;                    
                    if(listObj.Telco_Provider_Account_ID__c!=null){
                        listObj.Lead__r.Telco_Partner_Account__c = listObj.Telco_Provider_Account_ID__c;
                        leadMapToUpdate.put(listObj.Lead__c,listObj.Lead__r);
                    }
                }
                
                if(listObj.Account__c!=null && listObj.Telco_Provider_Account_ID__c!=null){
                    listObj.Account__r.Telco_Partner__c = listObj.Telco_Provider_Account_ID__c;
                    accMapToUpdate.put(listObj.Account__c,listObj.Account__r);
                }
                
                /*for(Order_Line_Items__c oli:listObj.Order_Line_Items__r){
                     oli.Sort_As__c = oli.Sort_As_FORMULA__c;
                     oliListToUpdate.add(oli);
                }*/
                listingListToUpdate.add(listObj);
            }
            
            if(leadMapToUpdate!=null && leadMapToUpdate.size()>0){
                update leadMapToUpdate.values();    
            }
            if(accMapToUpdate!=null && accMapToUpdate.size()>0){
                update accMapToUpdate.values();
            }
            if(listingListToUpdate!=null && listingListToUpdate.size()>0){
                update listingListToUpdate;
            }
            /*if(oliListToUpdate!=null && oliListToUpdate.size()>0){
                update oliListToUpdate;
            }*/
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch trigger for Listing insert');
        } 
    }
    
    global void finish(Database.BatchableContext bc) {
        if(bolStopExecution != true) {
            ListingTriggerForDataMigrBatch objBatch = new ListingTriggerForDataMigrBatch(filtercondition, true);
            Database.executeBatch(objBatch, 500);
        }
    }

}