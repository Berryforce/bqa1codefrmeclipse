@IsTest(SeeAllData = true)
public class Product2AIUDTest {
    static testMethod void Product2Test(){
        Test.startTest();
        //Product2 objProduct = new Product2(ProductCode = '001', Name = 'Unit Test Product', IsActive = true, Billing_Duration__c = 6);
        //insert objProduct;
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Inventory_Tracking_Group__c = 'Spine';
		insert newProduct;
		newProduct.Inventory_Tracking_Group__c = 'Spine - Half';
        update newProduct;
        Test.stopTest();
    }
}