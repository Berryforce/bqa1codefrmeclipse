public with sharing class Rev_Case_SCN_controller {
    //Getters setters
    
    public string caseID{get;set;}
    public Case CaseObj{get; set;}
    public list <Order_Line_Item_Discount__c> DOLIs{get; set;}
    public list <c2g__codaCreditNote__c> Invoices{get; set;}
    public list <c2g__codaCreditNote__c> PaidInvoices{get; set;}
    public list <c2g__codaCreditNote__c> validSCN{get; set;}
    public list <c2g__codaCreditNoteLineItem__c> SCNlneitems{get; set;}
    public list <c2g__codaCreditNoteLineItem__c> SCNlneitemsnb{get; set;}
    public list <c2g__codaCreditNoteLineItem__c> SCNlneitemall{get; set;}
    public list <String> SCNIDlist1{get; set;}
    public c2g__codaCreditNoteLineItem__c[] doomedLI{get; set;}
    public c2g__codaCreditNote__c[] doomedSCN{get; set;}
    public set<Id> doomedLIinf{get;set;}
    public Integer SCNlneitemallCount{get; set;}
    
    public Rev_Case_SCN_controller(ApexPages.StandardController controller) {
       caseID = ApexPages.currentPage().getParameters().get('caseid'); 
       system.debug('************ case Id is= '+ caseID);
       if(caseID != Null){
            CaseObj = [SELECT Id, CaseNumber, ContactId, AccountId, SuppliedName, SuppliedEmail, SuppliedPhone, SuppliedCompany, Type, RecordTypeId,
                      Status, Reason, Origin, Subject, Priority, Description, IsClosed, ClosedDate, HasCommentsUnreadByOwner, HasSelfServiceComments,  
                      CreatedDate, CreatedById, Case_Category__c, Case_Source__c, Customer_Satisfied__c, Main_Listed_Phone_Number__c, Received_From__c, 
                      Resolution_Note__c, Total_Credit_Note__c, User_Responsible_del__c, Digital_Product_Requirement__c, Account.Name, 
                      account.account_manager__c,account.Account_Manager__r.Name, account.account_number__c, account.phone, account.primary_canvass__c, 
                      Additional_BCC__c, Contracted_Total_Value__c, Risk_Dollars_Adjusted__c, Sub_Credit__c, Total_BCC__c, Total_Credit__c, Total_Max_BCC__c,
                      Total_Monthly_BCC__c, recordType.developername, Total_Contract_Value__c, Adjustment_Against_Contract__c, of_Contract_Adjusted__c
                      FROM Case 
                      WHERE Id = :caseID Limit 1];
                   
                      
           DOLIs = [SELECT Id, Name,Order_Group__c ,Line_item_total_value__c, Order_Line_Item__c, Discount_Percent__c, Max_Credit__c, date_of_issuance__c, 
                   checked__c, Discount_Per_period__c,  Number_of_Billing_Periods_to_discount__c, total_Credit_amount__c, BCC_Amount__c, BCC_Duraton__c, Case__c, 
                   Effective_Date__c, Total_BCC__c,Discount_Type__c,reason__c, Source__c, User_Responsible__c, Max_BCC__c, Issue_as_Single_Credit__c, 
                   User_Responsible__r.Name, order_line_item__r.Evergreen_Payments__c, 
                   order_line_item__r.Opportunity__c, order_line_item__r.checked__c, Credit_Amount_Reversed__c, Order_Line_Item_Canvass__c, 
                   Canvass_Primary_Telco__c, Overcompensated__c, Maxbcc_Num__c, Total_Credit_num__c, Client_Tier__c, OLI_Billing_Partner_Code__c, Directory_Edition__c
                   FROM Order_Line_Item_Discount__c 
                   WHERE Case__c= :CaseID 
                   AND Total_Credit_num__c >0                   
                   
                   ORDER BY order_line_item__r.Order_Group__c];
                   system.debug('************ DOLIs size is= '+ DOLIs.size());  
                   
   SCNlneitemall = [SELECT Id,  Name, c2g__CreditNote__c, Is_Billed__c, Order_Line_Item__c, c2g__CreditNote__r.Id, c2g__CreditNote__r.Name, c2g__CreditNote__r.c2g__Account__c, c2g__CreditNote__r.c2g__CreditNoteCurrency__c, c2g__CreditNote__r.c2g__CreditNoteDate__c, c2g__CreditNote__r.c2g__CreditNoteDescription__c, c2g__CreditNote__r.c2g__CreditNoteReason__c, 
                   c2g__CreditNote__r.c2g__CreditNoteStatus__c, c2g__CreditNote__r.c2g__CreditNoteTotal__c, c2g__CreditNote__r.c2g__CustomerReference__c, c2g__CreditNote__r.c2g__Dimension1__c, c2g__CreditNote__r.c2g__Dimension2__c, c2g__CreditNote__r.c2g__Dimension3__c, 
                   c2g__CreditNote__r.c2g__Dimension4__c, c2g__CreditNote__r.c2g__DiscardReason__c, c2g__CreditNote__r.c2g__DueDate__c, c2g__CreditNote__r.c2g__ExternalId__c, c2g__CreditNote__r.c2g__InvoiceDate__c, c2g__CreditNote__r.c2g__Invoice__c, c2g__CreditNote__r.c2g__Opportunity__c, 
                   c2g__CreditNote__r.c2g__OutstandingValue__c, c2g__CreditNote__r.c2g__OwnerCompany__c, c2g__CreditNote__r.c2g__PaymentStatus__c, c2g__CreditNote__r.c2g__Period__c, c2g__CreditNote__r.c2g__PrintStatus__c, 
                   c2g__CreditNote__r.c2g__TaxTotal__c, c2g__CreditNote__r.c2g__Transaction__c, c2g__CreditNote__r.c2g__UnitOfWork__c, c2g__CreditNote__r.c2g__Year__c, c2g__CreditNote__r.c2g__NetTotal__c, 
                   c2g__CreditNote__r.c2g__Tax1Total__c, c2g__CreditNote__r.c2g__Tax2Total__c, c2g__CreditNote__r.c2g__Tax3Total__c, c2g__CreditNote__r.ffbext__Approved__c, c2g__CreditNote__r.ffbilling__CopyAccountValues__c, 
                   c2g__CreditNote__r.ffbilling__CopyDefaultPrintedTextDefinitions__c, c2g__CreditNote__r.ffbilling__DeriveCurrency__c, c2g__CreditNote__r.ffbilling__DeriveDueDate__c, c2g__CreditNote__r.ffbilling__DerivePeriod__c, 
                   c2g__CreditNote__r.Case__c, c2g__CreditNote__r.Order_Line_Item_Discount__c, c2g__CreditNote__r.CMR_Number__c, c2g__CreditNote__r.National_Client_Number__c, c2g__CreditNote__r.ISS_Number__c, c2g__CreditNote__r.PAN1__c, c2g__CreditNote__r.Billing_Address__c, c2g__CreditNote__r.Transaction_Type__c, 
                   c2g__CreditNote__r.Telco_Customer__c, Sales_Invoice_Line_Item__c,c2g__CreditNote__r.Shipping_Address__c, c2g__CreditNote__r.OpCo__c,  c2g__CreditNote__r.Auto_Number__c
                   FROM c2g__codaCreditNoteLineItem__c
                   WHERE c2g__CreditNote__r.Case__c = :CaseID  
                   AND c2g__CreditNote__r.c2g__CreditNoteStatus__c != 'Complete'
                   AND c2g__CreditNote__r.c2g__CreditNoteStatus__c != 'Discarded'
                   AND c2g__CreditNote__r.c2g__PaymentStatus__c != 'Paid'
                   AND c2g__CreditNote__r.c2g__NetTotal__c >0
                   order by c2g__CreditNote__r.name  
                   ]; 
                   SCNlneitemallCount = SCNlneitemall.size();       
        system.debug('************ SCNlneitemall size is= '+ SCNlneitemall.size()); 
        doomedLIinf = new set<id>(); 
         for(c2g__codaCreditNoteLineItem__c SCNLI: SCNlneitemall){
                      doomedLIinf.add(SCNLI.Id);
                   }
                   doomedLI = [SELECT Id, Name FROM c2g__codaCreditNoteLineItem__c WHERE id in : doomedLIinf]; 
       system.debug('************ doomedLI size is= '+ doomedLI.size());   
                   // load the SCN
                   
                   
                    
        validSCN = [SELECT Id, Name, c2g__Account__c, c2g__CreditNoteCurrency__c, c2g__CreditNoteDate__c, c2g__CreditNoteDescription__c, c2g__CreditNoteReason__c, 
                   c2g__CreditNoteStatus__c, c2g__CreditNoteTotal__c, c2g__CustomerReference__c, c2g__Dimension1__c, c2g__Dimension2__c, c2g__Dimension3__c, 
                   c2g__Dimension4__c, c2g__DiscardReason__c, c2g__DueDate__c, c2g__ExternalId__c, c2g__InvoiceDate__c, c2g__Invoice__c, c2g__Opportunity__c, 
                   c2g__OutstandingValue__c, c2g__OwnerCompany__c, c2g__PaymentStatus__c, c2g__Period__c, c2g__PrintStatus__c, 
                   c2g__TaxTotal__c, c2g__Transaction__c, c2g__UnitOfWork__c, c2g__Year__c, c2g__NetTotal__c, 
                   c2g__Tax1Total__c, c2g__Tax2Total__c, c2g__Tax3Total__c, ffbext__Approved__c, ffbilling__CopyAccountValues__c, 
                   ffbilling__CopyDefaultPrintedTextDefinitions__c, ffbilling__DeriveCurrency__c, ffbilling__DeriveDueDate__c, ffbilling__DerivePeriod__c, 
                   Case__c, Order_Line_Item_Discount__c, CMR_Number__c, National_Client_Number__c, ISS_Number__c, PAN1__c, Billing_Address__c, Transaction_Type__c, 
                   Telco_Customer__c, Shipping_Address__c, OpCo__c, Auto_Number__c
                   FROM c2g__codaCreditNote__c 
                   WHERE Case__c = :CaseID
                   AND c2g__CreditNoteStatus__c != 'Complete'
                   AND c2g__CreditNoteStatus__c != 'Discarded'
                   AND c2g__PaymentStatus__c != 'Paid'
                   AND c2g__NetTotal__c >0];
       system.debug('************ validSCN size is= '+ validSCN.size());             
      list<String> SCNIDlist0 = new list<String>();
                   for(c2g__codaCreditNote__c SCN: validSCN){
                      SCNIDlist0.add(SCN.Id);
                   }
       system.debug('************ SCNIDlist0 size is= '+ SCNIDlist0.size());                     
                   

    SCNlneitems = [SELECT Id,  Name, c2g__CreditNote__c, Is_Billed__c ,Order_Line_Item__c 
                    FROM c2g__codaCreditNoteLineItem__c            
                    WHERE c2g__CreditNote__c in :SCNIDlist0
                    AND is_billed__c = True];
    system.debug('*********Billed *** SCNlneitems size is= '+ SCNlneitems.size()); 
    list<String> SCNIDlist = new list<String>();                    
                   for(c2g__codaCreditNoteLineItem__c SCNLI: SCNlneitems){
                      SCNIDlist.add(SCNLI.c2g__CreditNote__c);
                   } 
    system.debug('********billed**** SCNIDlist size is= '+ SCNIDlist.size());                
                   
    PaidInvoices = [SELECT Id, Name, c2g__Account__c, c2g__CreditNoteCurrency__c, c2g__CreditNoteDate__c, c2g__CreditNoteDescription__c, c2g__CreditNoteReason__c, 
                   c2g__CreditNoteStatus__c, c2g__CreditNoteTotal__c, c2g__CustomerReference__c, c2g__Dimension1__c, c2g__Dimension2__c, c2g__Dimension3__c, 
                   c2g__Dimension4__c, c2g__DiscardReason__c, c2g__DueDate__c, c2g__ExternalId__c, c2g__InvoiceDate__c, c2g__Invoice__c, c2g__Opportunity__c, 
                   c2g__OutstandingValue__c, c2g__OwnerCompany__c, c2g__PaymentStatus__c, c2g__Period__c, c2g__PrintStatus__c, 
                   c2g__TaxTotal__c, c2g__Transaction__c, c2g__UnitOfWork__c, c2g__Year__c, c2g__NetTotal__c, 
                   c2g__Tax1Total__c, c2g__Tax2Total__c, c2g__Tax3Total__c, ffbext__Approved__c, ffbilling__CopyAccountValues__c, 
                   ffbilling__CopyDefaultPrintedTextDefinitions__c, ffbilling__DeriveCurrency__c, ffbilling__DeriveDueDate__c, ffbilling__DerivePeriod__c, 
                   Case__c, Order_Line_Item_Discount__c, CMR_Number__c, National_Client_Number__c, ISS_Number__c, PAN1__c, Billing_Address__c, Transaction_Type__c, 
                   Telco_Customer__c, Shipping_Address__c, OpCo__c, Auto_Number__c 
                   FROM c2g__codaCreditNote__c
                   WHERE ID in :SCNIDlist];
       system.debug('********billed**** PaidInvoices size is= '+ PaidInvoices.size());
                                       
    SCNlneitemsnb = [SELECT Id,  Name, c2g__CreditNote__c, Is_Billed__c 
                    FROM c2g__codaCreditNoteLineItem__c            
                    WHERE c2g__CreditNote__c in :SCNIDlist0
                    AND is_billed__c != True];           
 system.debug('********NOTbilled**** SCNlneitemsnb size is= '+ SCNlneitemsnb.size());
              list<String> SCNIDlist1 = new list<String>();
                   for(c2g__codaCreditNoteLineItem__c SCNLI: SCNlneitems){
                      SCNIDlist1.add(SCNLI.c2g__CreditNote__c);
                   }        
                   
                   
        Invoices = [SELECT Id, Name, c2g__Account__c, c2g__CreditNoteCurrency__c, c2g__CreditNoteDate__c, c2g__CreditNoteDescription__c, c2g__CreditNoteReason__c, 
                   c2g__CreditNoteStatus__c, c2g__CreditNoteTotal__c, c2g__CustomerReference__c, c2g__Dimension1__c, c2g__Dimension2__c, c2g__Dimension3__c, 
                   c2g__Dimension4__c, c2g__DiscardReason__c, c2g__DueDate__c, c2g__ExternalId__c, c2g__InvoiceDate__c, c2g__Invoice__c, c2g__Opportunity__c, 
                   c2g__OutstandingValue__c, c2g__OwnerCompany__c, c2g__PaymentStatus__c, c2g__Period__c, c2g__PrintStatus__c, 
                   c2g__TaxTotal__c, c2g__Transaction__c, c2g__UnitOfWork__c, c2g__Year__c, c2g__NetTotal__c, 
                   c2g__Tax1Total__c, c2g__Tax2Total__c, c2g__Tax3Total__c, ffbext__Approved__c, ffbilling__CopyAccountValues__c, 
                   ffbilling__CopyDefaultPrintedTextDefinitions__c, ffbilling__DeriveCurrency__c, ffbilling__DeriveDueDate__c, ffbilling__DerivePeriod__c, 
                   Case__c, Order_Line_Item_Discount__c, CMR_Number__c, National_Client_Number__c, ISS_Number__c, PAN1__c, Billing_Address__c, Transaction_Type__c, 
                   Telco_Customer__c, Shipping_Address__c, OpCo__c, Auto_Number__c 
                   FROM c2g__codaCreditNote__c
                   WHERE  ID in :SCNIDlist1];
                   
                   
                   
    
       }
    }

    public pageReference RevClaim(){
      pageReference pr;
      pr = new pageReference('/'+caseID);
      
      if(PaidInvoices.size() >0 || test.isRunningTest() == true){
      system.debug('********inside RevClaim()**** PaidInvoices size is= '+ PaidInvoices.size());
          // create new scn from old SCN that have been posted
          List <c2g__codaCreditNoteLineItem__c> SCNLIS = new List <c2g__codaCreditNoteLineItem__c>();
          List < c2g__codaCreditNote__c > SCNS = new List < c2g__codaCreditNote__c >();
          
          for (c2g__codaCreditNote__c  PSCN : PaidInvoices){
              //build new credit note here 
              c2g__codaCreditNote__c  clonedSCN  = new c2g__codaCreditNote__c();
              sObject originalSObject1 = (sObject) PSCN;
              List<sObject> originalSObjects1 = new List<sObject> {originalSObject1};
              List<sObject> clonedSObjects1 = SObjectAllFieldCloner.cloneObjects(originalSobjects1, originalSobject1.getsObjectType());
              clonedSCN = (c2g__codaCreditNote__c)clonedSObjects1.get(0); 
              clonedSCN.c2g__ExternalId__c = PSCN.id;
              SCNS.add(clonedSCN);
             system.debug('********inside RevClaim()**** adding new SCN*****************');
              for(c2g__codaCreditNoteLineItem__c SCNLI :SCNlneitems){
                  
                  if(SCNLI.c2g__CreditNote__c  == PSCN.id){
                     //build the scn line items here 
                      c2g__codaCreditNoteLineItem__c clonedLI = new c2g__codaCreditNoteLineItem__c();
                      sObject originalSObject = (sObject) SCNLI;
                      List<sObject> originalSObjects = new List<sObject> {originalSObject};
                      List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects( originalSobjects, originalSobject.getsObjectType());
                      clonedLI = (c2g__codaCreditNoteLineItem__c )clonedSObjects.get(0); 
                      clonedLI.c2g__ExternalId__c = PSCN.id;
                      clonedLI.c2g__UnitPrice__c = (clonedLI.c2g__UnitPrice__c *-1);
                      SCNLIS.add(clonedLI);
                      system.debug('********inside RevClaim()**** adding new SCN Line item*****************');
                             
                  }
                  
              }
              
          }
            system.debug('********inside RevClaim()**** SCNS size is= '+ SCNS.size());
          if(SCNS.size() >0){
          system.debug('********inside RevClaim()**** inserting new SCNs*****************');
              insert SCNS;
              for(c2g__codaCreditNote__c SCN :SCNS){
                  for(c2g__codaCreditNoteLineItem__c SCNli :SCNLIS){
                      if(SCNli.c2g__ExternalId__c == SCN.c2g__ExternalId__c){
                          SCNli.c2g__CreditNote__c = SCN.id;
                      }
                  }          
              }
              system.debug('********inside RevClaim()**** inserting new SCN Line Items *****************');     
              insert SCNLIS;
          }
      } else {
          system.debug('********inside RevClaim()**** PaidInvoices size is= '+ PaidInvoices.size());
      }
      system.debug('********inside RevClaim()**** deleting scn line items*****************');
      delete doomedLI;
      
      for(Order_Line_Item_Discount__c DOLI :DOLIs){
          DOLI.Date_of_Reversal__c = date.today();
          DOLI.claim_reversed__c = true;
      }
     system.debug('********inside RevClaim()**** Updating DOLI items*****************'); 
     update DOLIs;
    return pr;
    }








}