global class TBCcUnsuppressionScopedListingBatch implements Database.Batchable<sObject>,Database.Stateful {
    private set<Id> setDirId = new set<Id>();
    global TBCcUnsuppressionScopedListingBatch(set<Id> setId) {
        if(setId.size()> 0 && setId != null) {
            setDirId.addAll(setId);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        string bookStatus = 'NI';
        return database.getQuerylocator('select id,Suppressed_By_OLI__c,Suppressed_By_OLI__r.Directory_Edition_Book_Status__c,Suppressed__c,Directory__c  from Directory_Listing__c where Directory__c IN : setDirId AND Suppressed_By_OLI__c != Null AND Suppressed__c = True AND Suppressed_By_OLI__r.Directory_Edition_Book_Status__c != :bookStatus');
    }
    global void execute(Database.BatchableContext bc, List<Directory_Listing__c> dlList) {
        list<Directory_Listing__c> lstDL = new list<Directory_Listing__c>();
        if(dlList.size() > 0) {
            for(Directory_Listing__c iterator : dlList) {
                iterator.Suppressed__c = False;
                iterator.Suppressed_By_OLI__c = null;
                lstDL.add(iterator);
            }
            if(lstDL.size() > 0) {
                update lstDL;
            }
        }
    }
    global void finish(Database.BatchableContext bc){}
}