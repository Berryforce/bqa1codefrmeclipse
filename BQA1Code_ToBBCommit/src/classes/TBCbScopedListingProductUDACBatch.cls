global class TBCbScopedListingProductUDACBatch implements Database.Batchable<sObject>,Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        string Id = '005G0000005hhf4';
        return database.getQuerylocator('SELECT Id,Product__c,Original_Product__c,Caption_Header_Enhanced_By__c,Enhanced_By_OLI_Art_URN__c,Enhanced_By_OLI_Con__c,(Select Id,Product2__c,Name,Digital_Product_Requirement__r.URN_number__c FROM Order_Line_Items__r where isCanceled__c = false AND ListPrice__c != null AND Is_Child__c= False ORDER BY ListPrice__c DESC Limit 1) from Directory_Listing__c where createdbyId=:Id AND Caption_Header__c = True');          
    }
    global void execute(Database.BatchableContext bc, List<Directory_Listing__c > dlList) {
        list<Directory_Listing__c> DLUpdateList = new list<Directory_Listing__c>();
        for(Directory_Listing__c DLIterator : dlList) {
            Boolean bflag = false;
            if(DLIterator.Order_Line_Items__r.size() > 0) {
                Directory_Listing__c DL = new Directory_Listing__c(id = DLIterator.Id);
                if(DLIterator.Product__c != DLIterator.Order_Line_Items__r[0].Product2__c) {
                    bflag=true;
                    DL.Product__c = DLIterator.Order_Line_Items__r[0].Product2__c;
                }
                if(DLIterator.Order_Line_Items__r[0].Digital_Product_Requirement__r.URN_number__c != null) {
                    bflag=true;
                    DL.Caption_Header_Enhanced_By__c = DLIterator.Order_Line_Items__r[0].Id;
                    DL.Enhanced_By_OLI_Art_URN__c = string.valueOf(DLIterator.Order_Line_Items__r[0].Digital_Product_Requirement__r.URN_number__c);
                    DL.Enhanced_By_OLI_Con__c = DLIterator.Order_Line_Items__r[0].Name;
                }
                if(bflag) {
                    DLUpdateList.add(DL);
                }
            }
            else {
               if(DLIterator.Product__c != DLIterator.Original_Product__c) {
                    bflag=true;
                    DLIterator.Product__c = DLIterator.Original_Product__c;
                }
                if(DLIterator.Caption_Header_Enhanced_By__c != null || DLIterator.Enhanced_By_OLI_Art_URN__c != null ||  DLIterator.Enhanced_By_OLI_Con__c != null ) {
                    bflag=true;
                    DLIterator.Caption_Header_Enhanced_By__c = null;
                    DLIterator.Enhanced_By_OLI_Art_URN__c = null;
                    DLIterator.Enhanced_By_OLI_Con__c = null;
                }
                if(bflag) {
                    DLUpdateList.add(DLIterator);
                }
            }
        }
        if(DLUpdateList.size() > 0) {
            update DLUpdateList;
        }
    }
    global void finish(Database.BatchableContext bc){}
}