/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public with sharing class FFA_SCRTelcoMatchingStagingQueueable implements Queueable
{
    private List<String> errorMessages;
    private List<String> successMessages;
    private Map<String, List<FFA_TelcoTransactionLineWrapper>> negativeInvoiceLines;
    private Map<String, List<FFA_TelcoTransactionLineWrapper>> postiveInvoiceLines;
    private Map<String, Decimal> creditNoteOutstandingBalance;
    private Id accountId;
    private Id customerName;
    private boolean callFromBatchClass;
    private Map<Integer, List<c2g__codaTransactionLineItem__c>> scope;
    private Integer counter;
    
    public FFA_SCRTelcoMatchingStagingQueueable(boolean callFromBatchClass, Id accountId, Id customerName) 
    {
        this.callFromBatchClass = callFromBatchClass;
        this.accountId = accountId;
        this.customerName = customerName;
        this.scope = null;
        this.counter = 0;
        this.errorMessages = new List<String>();
        this.successMessages = new List<String>();
    }

    public FFA_SCRTelcoMatchingStagingQueueable(boolean callFromBatchClass, Map<Integer, List<c2g__codaTransactionLineItem__c>> scope,  Integer counter) 
    {
        this.callFromBatchClass = callFromBatchClass;
        this.accountId = accountId;
        this.customerName = customerName;
        this.scope = scope;
        this.counter = counter;
        this.errorMessages = new List<String>();
        this.successMessages = new List<String>();
    }

    public void execute(QueueableContext context) 
    {
        if(callFromBatchClass) 
        {
            ffps_bmatching.AppTriggerSwitch.turnOffTrigger();
            scope = getUnmatchedSalesCreditNoteLines();
            System.enqueueJob(new FFA_SCRTelcoMatchingStagingQueueable(false, scope, counter));
        }
        else
        {
            ffps_bmatching.AppTriggerSwitch.turnOffTrigger();

            if(Test.isRunningTest()) scope = getUnmatchedSalesCreditNoteLines();
            
            List<c2g__codaTransactionLineItem__c> lines = scope.get(counter);
            if(!lines.isEmpty()) createStagingRecords(scope.get(counter));

            counter++;
            if(scope.containsKey(counter))
            {
                System.enqueueJob(new FFA_SCRTelcoMatchingStagingQueueable(false, scope, counter));                
            }  
        }
    }

    private Map<Integer, List<c2g__codaTransactionLineItem__c>> getUnmatchedSalesCreditNoteLines()
    {
        Date matchingDate = System.today();
        
        List<c2g__codaTransactionLineItem__c> listOfTransactions = new List<c2g__codaTransactionLineItem__c>();

        Map<Integer, List<c2g__codaTransactionLineItem__c>> rtnMap = new Map<Integer, List<c2g__codaTransactionLineItem__c>>();
        //Find all Cash Entry Account Lines 
        String query = FFA_SCRTelcoMatchingStagingSOQL.getStartQuery(accountId, customerName, matchingDate);
        
        List<c2g__codaTransactionLineItem__c> scrAnalysisLines = database.query(query);
        //Single map should not exceed 100 lines to stay within heapsize limit
        Integer countery = 0;
        Integer counterx = 0;
        Id lastAccId = null;
        for(c2g__codaTransactionLineItem__c line :scrAnalysisLines)
        {
            counterx++;
            listOfTransactions.add(line);

            //if counterx reaches 100, then add map to rtnList and reset rtnMap and counterx
            if(counterx == 100) 
            {
                errorMessages.add(countery +'' + line.c2g__Account__c);
                rtnMap.put(countery, listOfTransactions);
                listOfTransactions = new List<c2g__codaTransactionLineItem__c>();
                counterx = 0;
                countery++;
            }
        }
        if(!listOfTransactions.isEmpty()) rtnMap.put(countery, listOfTransactions);

        return rtnMap;
    }

    private void createStagingRecords(List<c2g__codaTransactionLineItem__c> lines)
    {
        Integer recordCounter = 0;
        //Internal Custom Cash Matching History Headers
        Map<Integer, ffps_bmatching__Custom_Cash_Matching_History__c> cashMatchingHeader = new Map<Integer, ffps_bmatching__Custom_Cash_Matching_History__c>();
        //Internal Custom Cash Matching History Lines        
        Map<Integer, List<ffps_bmatching__Custom_Cash_Matching_History_Line__c>> cashMatchingLines = new Map<Integer, List<ffps_bmatching__Custom_Cash_Matching_History_Line__c>>();

        Set<id> transIds = createWrapperClassData(lines);
        Id currentPeriod = FFA_Util.getPeriodIDByDate(System.today());

        //Find all Sales Credit Note Account Lines that are still outstanding to be matched
        Map<id, c2g__codaTransactionLineItem__c> creditNoteAccountLineMapping = FFA_SCRTelcoMatchingStagingSOQL.getSCRTransactionAccountLines(transIds);
        for (c2g__codaTransactionLineItem__c line :lines)
        {
            try
            {
                String key = line.c2g__Account__c + '' + line.ffps_bmatching__OLI_Line_Number__c;
                //Find related Sales Credit Note Account Line
                c2g__codaTransactionLineItem__c creditNoteAccLine = creditNoteAccountLineMapping.get(line.c2g__Transaction__c);
                //Credit Note Amount need matching
                Decimal creditNoteAmount = line.ffps_bmatching__Balance_Due__c;
                //loop through the list until SCR balance is 0 or list is completed
                Decimal currentInvoiceTotalBalance = 0;
                List<FFA_TelcoTransactionLineWrapper> transLines = new List<FFA_TelcoTransactionLineWrapper>();
                List<FFA_TelcoTransactionLineWrapper> newTransLines = new List<FFA_TelcoTransactionLineWrapper>();

                if(creditNoteAmount > 0)
                {
                    if(negativeInvoiceLines.containskey(key))
                    {
                        transLines = negativeInvoiceLines.get(key);
                        if(transLines.isEmpty())
                            throw new FFA_AppException (line.name +' has no related Invoice Line to be Matched to: Document Reference '+ line.ffps_bmatching__Line_Reference__c);
                    }
                    else
                    {
                        throw new FFA_AppException (line.name +' has no related Invoice Line to be Matched to: Document Reference '+ line.ffps_bmatching__Line_Reference__c);
                    }
                }
                else if(creditNoteAmount < 0)
                {
                    if(postiveInvoiceLines.containskey(key))
                    {
                        transLines = postiveInvoiceLines.get(key);
                        if(transLines.isEmpty())
                            throw new FFA_AppException (line.name +' has no related Invoice Line to be Matched to: Document Reference '+ line.ffps_bmatching__Line_Reference__c);
                        }
                    else
                    {
                        throw new FFA_AppException (line.name +' has no related Invoice Line to be Matched to: Document Reference '+ line.ffps_bmatching__Line_Reference__c);
                    }
                }
                cashMatchingHeader.put(recordCounter, createInternalReference(line.c2g__Account__c, (id)line.getsObject('c2g__Transaction__r').get('Customer_Name__c'), currentPeriod));
                for(FFA_TelcoTransactionLineWrapper wLine :transLines)
                {
                    if(math.abs(currentInvoiceTotalBalance) < math.abs(creditNoteAmount)) 
                    {
                        if(wLine.balance != 0)
                        {
                            Decimal indInvoiceAmount = wLine.balance;
                            //if line outstanding is greater than document outstanding total, override indInvoiceAmount with c2g__DocumentOutstandingTotal__c
                            //line outstanding balance will not change through Standard Cash Matching UI
                            if (indInvoiceAmount > wLine.transLine.c2g__Transaction__r.c2g__DocumentOutstandingTotal__c)
                                indInvoiceAmount = wLine.transLine.c2g__Transaction__r.c2g__DocumentOutstandingTotal__c;

                            currentInvoiceTotalBalance += indInvoiceAmount;

                            if(math.abs(currentInvoiceTotalBalance) > math.abs(creditNoteAmount))
                            {
                                if(indInvoiceAmount > 0 )
                                    indInvoiceAmount -= (math.abs(currentInvoiceTotalBalance) - math.abs(creditNoteAmount));
                                else
                                    indInvoiceAmount += (math.abs(currentInvoiceTotalBalance) - math.abs(creditNoteAmount));
                            }
                            wLine.selected = true;
                            wLine.amountToBeMatched = indInvoiceAmount;
                            wLine.balance -= indInvoiceAmount;
                            newTransLines.add(wLine);
                        }
                    }
                }
                //Exception here!
                FFA_TelcoTransactionLineWrapper wrapper = new FFA_TelcoTransactionLineWrapper();
                wrapper.transLine = line;
                wrapper.accountLineId = creditNoteAccLine.id;
                wrapper.amountToBeMatched = creditNoteAmount;               
                newTransLines.add(wrapper);

                if(creditNoteAmount > 0)
                {
                    cashMatchingLines.put(recordCounter, createInternalReferenceLine(newTransLines));
                    recordCounter++;
                }
                else if(creditNoteAmount < 0)
                {
                    cashMatchingLines.put(recordCounter, createInternalReferenceLine(newTransLines));
                    recordCounter++;
                }
            }  
            catch (Exception e)
            {
                errorMessages.add( 'Transaction line : ' + line.Name + ' was not matched because : ' + e.getMessage() + '(' + e.getLineNumber() + ')' + '\n\n');
            }
        }
        System.Savepoint sp = Database.setSavepoint();
        try
        {
            if( cashMatchingHeader.keySet().size() > 0 && cashMatchingLines.keySet().size() > 0)
            {
                insert cashMatchingHeader.values();
                
                List<ffps_bmatching__Custom_Cash_Matching_History_Line__c> allLines = new List<ffps_bmatching__Custom_Cash_Matching_History_Line__c>();
                for( Integer index : cashMatchingHeader.keySet() )
                {
                    Id headerId = cashMatchingHeader.get( index ).Id;
                    for(ffps_bmatching__Custom_Cash_Matching_History_Line__c line :cashMatchingLines.get( index ))
                    {
                        line.ffps_bmatching__Custom_Cash_Matching_History__c = headerId;
                        allLines.add( line );
                    }
                }
                insert allLines;
            }
        }
        catch (Exception e)
        {
            Database.rollback(sp);
            errorMessages.add( 'Insert Failed : ' + e.getMessage() + '(' + e.getLineNumber() + ')' + '\n\n');
        } 
        if(!errorMessages.isEmpty())
        {
            //sendEmail('uravat@financialforce.com', successMessages, errorMessages);
        } 
    }

    private void sendEmail( String address, List<String>successMessages, List<String>errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = '';     
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Matching Sales Credit matched to Sales Invoices (CreditNoteMatchedToInvoiceBatch)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }     

    private Set<id> createWrapperClassData(List<c2g__codaTransactionLineItem__c> scrTransactionLines)
    {
        creditNoteOutstandingBalance = new Map<String, Decimal>();
        //Wrapper class that contains all negative Sales Invoice Lines
        negativeInvoiceLines = new Map<String, List<FFA_TelcoTransactionLineWrapper>>();
        //Wrapper class that contains all postive Sales Invoice Lines
        postiveInvoiceLines = new Map<String, List<FFA_TelcoTransactionLineWrapper>>();
        Set<id> accIds = new Set<id>();
        Set<id> customerNameIds = new Set<id>();
        Set<id> transIds = new Set<id>();
        Set<String> oliLineNumbers = new Set<String>();
        for (c2g__codaTransactionLineItem__c line :scrTransactionLines)
        {
            String key = line.c2g__Account__c + '' + line.ffps_bmatching__OLI_Line_Number__c;
            transIds.add(line.c2g__Transaction__c);
            accIds.add(line.c2g__Account__c);
            customerNameIds.add((id)line.getsObject('c2g__Transaction__r').get('Customer_Name__c'));
            oliLineNumbers.add(line.ffps_bmatching__OLI_Line_Number__c);
            if(creditNoteOutstandingBalance.containskey(key))
                creditNoteOutstandingBalance.put(key, creditNoteOutstandingBalance.get(key) + line.ffps_bmatching__Balance_Due__c);
            else
                creditNoteOutstandingBalance.put(key, line.ffps_bmatching__Balance_Due__c);
        }
        //Keep track how balance for each Account.
        Map<String, Decimal> invoiceOutstandingBalance = new Map<String, Decimal>();
        //Find all Invoice Transaction Lines that are still avaliable to be matched 
        Map<Id, List<c2g__codaTransactionLineItem__c>> outstandingInvoiceLines = FFA_SCRTelcoMatchingStagingSOQL.getTransAnalysisLines(oliLineNumbers, accIds, customerNameIds, system.today());
        //Find all Invoice Account Lines that are still outstanding to be matched
        Map<Id, c2g__codaTransactionLineItem__c> invoiceAccountLineMapping = FFA_SCRTelcoMatchingStagingSOQL.getTransactionInvoiceAccLines(accIds, customerNameIds, outstandingInvoiceLines.keySet(), system.today());
        //Update AccountId, No point selecting Accounts which doesnt have related Invoices
        for(id tranHeaderId :outstandingInvoiceLines.keyset())
        { 
            for(c2g__codaTransactionLineItem__c line :outstandingInvoiceLines.get(tranHeaderId))
            {
                String key = line.c2g__Account__c + '' + line.ffps_bmatching__OLI_Line_Number__c;
                FFA_TelcoTransactionLineWrapper wrapper = new FFA_TelcoTransactionLineWrapper();
                wrapper.transLine = line;
                wrapper.accountLineId = invoiceAccountLineMapping.get(line.c2g__Transaction__c).id;
                wrapper.balance = line.ffps_bmatching__Balance_Due__c;

                if(line.ffps_bmatching__Balance_Due__c > 0)
                {
                    if(postiveInvoiceLines.containskey(key))
                    {
                        postiveInvoiceLines.get(key).add(wrapper);
                    }
                    else
                    {
                        postiveInvoiceLines.put((key), new List <FFA_TelcoTransactionLineWrapper> { wrapper });
                    }
                }
                else if(line.ffps_bmatching__Balance_Due__c < 0)
                {
                    if(negativeInvoiceLines.containskey(key))
                    {
                        negativeInvoiceLines.get(key).add(wrapper);
                    }
                    else
                    {
                        negativeInvoiceLines.put((key), new List <FFA_TelcoTransactionLineWrapper> { wrapper });
                    }
                }
                if(invoiceOutstandingBalance.containskey(key))
                {
                    if(invoiceOutstandingBalance.containskey(key) && creditNoteOutstandingBalance.containskey(key) && (Math.abs(invoiceOutstandingBalance.get(key)) > Math.abs(creditNoteOutstandingBalance.get(key)))) 
                        invoiceOutstandingBalance.put(key, invoiceOutstandingBalance.get(key) + line.ffps_bmatching__Balance_Due__c);
                }
                else
                {
                    invoiceOutstandingBalance.put(key, line.ffps_bmatching__Balance_Due__c);
                }  
            }
        }

        return transIds;
    }

    private static ffps_bmatching__Custom_Cash_Matching_History__c createInternalReference(Id accId, Id customerNameId, Id currentPeriod)
    {
        ffps_bmatching__Custom_Cash_Matching_History__c cmr = new ffps_bmatching__Custom_Cash_Matching_History__c();
        cmr.ffps_bmatching__Account__c = accId;
        cmr.ffps_bmatching__Period__c = currentPeriod;
        cmr.Customer_Name__c = customerNameId;
        cmr.ffps_bmatching__Matching_Date__c = system.today();
        return cmr;
    }

    private List<ffps_bmatching__Custom_Cash_Matching_History_Line__c> createInternalReferenceLine(List<FFA_TelcoTransactionLineWrapper> newTransLines)
    {
        Decimal totalCashAmount = 0;

        //Create the lines
        List<ffps_bmatching__Custom_Cash_Matching_History_Line__c> referenceLines = new List<ffps_bmatching__Custom_Cash_Matching_History_Line__c>();
        for(FFA_TelcoTransactionLineWrapper alwLine :newTransLines)
        {
            if(alwLine.transLine.c2g__Transaction__r.c2g__TransactionType__c == 'Invoice')
            {
                ffps_bmatching__Custom_Cash_Matching_History_Line__c referenceLine = new ffps_bmatching__Custom_Cash_Matching_History_Line__c(
                    ffps_bmatching__Transaction_Line_Item__c = alwLine.transLine.id,
                    ffps_bmatching__Transaction_Line_Id__c = alwLine.accountLineId,
                    ffps_bmatching__Amount_Matched__c = alwLine.amountToBeMatched,
                    ffps_bmatching__Action__c = 'Match');
                referenceLines.add(referenceLine);
                totalCashAmount += alwLine.amountToBeMatched;
            }
        }
        for(FFA_TelcoTransactionLineWrapper alwLine :newTransLines)
        {
            if(alwLine.transLine.c2g__Transaction__r.c2g__TransactionType__c == 'Credit Note')
            {
                ffps_bmatching__Custom_Cash_Matching_History_Line__c referenceLine = new ffps_bmatching__Custom_Cash_Matching_History_Line__c(
                    ffps_bmatching__Transaction_Line_Item__c = alwLine.transLine.id,
                    ffps_bmatching__Transaction_Line_Id__c = alwLine.accountLineId,
                    ffps_bmatching__Amount_Matched__c = totalCashAmount * -1,
                    ffps_bmatching__Action__c = 'Match');
                referenceLines.add(referenceLine);
            }
        }

        return referenceLines;
    }
}