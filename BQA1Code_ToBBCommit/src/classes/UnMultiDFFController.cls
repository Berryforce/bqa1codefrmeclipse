public class UnMultiDFFController {
	private id dffId;
	public Id getdffId() {
		return dffId;
	}
	public void setdffId(String value) {
		if(dffId != value) {
			dffId = value;
			onLoad();
		}
	}
	
	public list<Digital_Product_Requirement__c> lstDFFCompleted {get;set;}
    public list<Digital_Product_Requirement__c> lstDFF {get;set;}
    
    public UnMultiDFFController() {
	}
	
	public void onLoad() {
		lstDFF = [Select Id,Name,Account__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,URN_number__c,Issue_Number__c,DFF_Proof_Contact_Email_Address__c,Previous_URN_Number__c,Paper_or_Email__c,Proof_Contact_Title__c,Heading__c,Directory_Edition__c,Directory_Section_Columns__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,Proof__c,Proof_Contact__c,DFF_Proof_Contact_Mailing_Address__c from Digital_Product_Requirement__c where Id = :dffId limit 1];
		set<Id> actId = new set<Id>();
        set<String> setUdac = new set<String>();
        set<string> setcanvassId = new set<string>();
        String dirSecCols;
        decimal URN;
        String editionCode;
        if(lstDFF.size() > 0) {
            for(Digital_Product_Requirement__c objDFF : lstDFF) {
                if(objDFF.Account__c!= null) {
                    actId.add(objDFF.Account__c);
                }
                if(objDFF.UDAC__c != null) {
                    setUdac.add(objDFF.UDAC__c);
                }
                if(objDFF.URN_number__c != null) {
                    URN = objDFF.URN_number__c ;
                }
                if(objDFF.Directory_Section_Columns__c != null){
                    dirSecCols = objDFF.Directory_Section_Columns__c;
                }
                if(objDFF.Canvass__c != null){
                    setcanvassId.add(objDFF.Canvass__c);
                }
                if(objDFF.Issue_Number__c != null){
                    editionCode = objDFF.Issue_Number__c;
                }
            }
        }
		lstDFFCompleted = [Select Id,Name,Account__c,URN_number__c,Previous_URN_Number__c,Is_Multied__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Product_Type__c,Directory_Section_Columns__c,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,Heading__c,Directory_Edition__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,sequence__c from Digital_Product_Requirement__c where Issue_Number__c =:editionCode AND DFF_Cancel_Status__c = false AND Account__c IN :actId and UDAC__c =:setUdac and Id NOT IN:lstDFF and Directory_Section_Columns__c =:dirSecCols and Canvass__c IN:setcanvassId  and URN_number__c =: URN and Is_Multied__c = true];
	}
	
	public pagereference saveSelectedDFF() {
		list<Digital_Product_Requirement__c> lstUpdateDFF = new list<Digital_Product_Requirement__c>();
		Integer iCount = 1;
		for(Digital_Product_Requirement__c iterator : lstDFFCompleted) {
			system.debug(iterator.Is_Multied__c);
            if(!iterator.Is_Multied__c) {
            	if(iterator.Previous_URN_Number__c != null) {
            		iterator.URN_number__c = iterator.Previous_URN_Number__c;
            	}
                iterator.Previous_URN_Number__c = null;
                iterator.Sequence__c = 1;
                iterator.Production_Notes__c = '';
                if(String.isNotEmpty(iterator.OrderLineItemID__r.Product_Type__c)) {
                    iterator.RecordtypeId = CommonMethods.getRedordTypeIdByName(iterator.OrderLineItemID__r.Product_Type__c, CommonMessages.dffObjectName);
                }
                lstUpdateDFF.add(iterator);
            }
            else {
            	iCount++;
            	iterator.Sequence__c = iCount;
                lstUpdateDFF.add(iterator);
            }
        }
        
        for(Digital_Product_Requirement__c iterator : lstDFF) {
            iterator.Sequence__c = 1;
            if(iCount == 1) {
            	iterator.Is_Multied__c = false;
            }
            lstUpdateDFF.add(iterator);
		}
		
        if(lstUpdateDFF.size() > 0) {
        	update lstUpdateDFF;
        	return new pagereference('/'+dffId);
        }
		return null;
	}
}