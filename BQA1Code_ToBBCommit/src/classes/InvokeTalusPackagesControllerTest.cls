@isTest
public class InvokeTalusPackagesControllerTest{
    static testmethod void classmet(){

        Package_ID__c pk = new Package_ID__c(Name='PKG_IYP0001', Resource_URI__c='test');
        insert pk;
        
        Package_Product__c prdts = new Package_Product__c(Name='IYP0001_METAL', Package_ID__c=pk.id);
        insert prdts;
        
        test.startTest();
        
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStaticResource('https://stg-nigel.colonylogic.com/catalog/api/v2/packages/q?param=1 & param=2', 'Talus_UDAC_Packages');
        mock.setStaticResource('https://stg-nigel.colonylogic.com/catalog/api/v2/packages/PKG_MCM0001/'+ pk.Resource_URI__c, 'Talus_UDAC_Products');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');

        //Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);  
        
        ApexPages.StandardController sc = new ApexPages.StandardController(pk);
        InvokeTalusPackagesController ITP = new InvokeTalusPackagesController(sc);
        ITP.invkPkgs();        
    
        test.stopTest();
    }
}