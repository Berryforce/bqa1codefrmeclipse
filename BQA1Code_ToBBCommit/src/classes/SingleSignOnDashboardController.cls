public with sharing class SingleSignOnDashboardController 
{

  public String dashboardURL {get; private set;}
  public Boolean isPortal{get; private set;}
  
  public static final String BERRY_ID_PARAM = 'id';
  public static final String OAUTH_SERVICE_NAME = 'Talus Dashboard';
  
  //this is an interface to allow testing of the callout
  public interface calloutDevice
  {
    String doCallout(HTTPrequest req);
  }
  
  private class NormalCalloutDevice implements calloutDevice
  {
    public string doCallout(HTTPrequest req)
    {
      Http http = new Http();
      HttpResponse res = http.send(req);
      return res.getBody();
    }
  }
  
  private calloutDevice callerDev;

  public static String getUrlFromBerryID(string berryId,calloutDevice dev,string url_parameter)
  {
    OAuth_Service__c service = OAuth_Service__c.getInstance(OAUTH_SERVICE_NAME);
    
    string URL = service.URL__c + 'accounts/?external_id=' + berryId;
    URL += '&' + url_parameter+'=1' + '&format=json';
    
    HTTPRequest req = new HTTPRequest();
    req.setEndpoint(URL);
    req.setMethod('GET');
    
    OAuth signer = new OAuth(service);
    signer.sign(req);
    string jsonBody = dev.doCallout(req);
    
    //Debug prints to determine if data is being returned --
    system.debug('URL=========='+URL);
    system.debug('req==========='+req);
    system.debug('jsonBody============='+jsonBody);
    //-------------------------------------------------------
    
    return extractDashboardUrl(jsonBody,0);
  }
  
  public static String extractDashboardURL(String json, Integer index)
  {
    //if a blank string is passed in a salesforce system error will occur, so we must blank check the string
    if('' == json)
    {
      return null;
    }
    
    try
    {
      Map<String, Object> deserializedResponse = (Map<String, Object>)System.JSON.deserializeUntyped(json);
      List<Object> dashboardObjects = (List<Object>) deserializedResponse.get('objects');
      Map<String, Object> desiredDashboardObject = (Map<String,Object>)dashboardObjects[index];
      string URL = (String)desiredDashboardObject.get('dashboard_url');
      
      return URL;
    }
    catch(exception e)
    {
      return null;
    }
  }
  
  public string queryBerryIdFromUser()
  {
    Id UserId = UserInfo.getUserId();
    system.debug('UserId========'+UserId);
    List<User> queriedUsers = [SELECT Id, Contact.Account.Enterprise_Customer_ID__c FROM User WHERE Id = :UserId];
    
    if(queriedUsers.size() != 1)
    {
      return null;
    }
    
    return queriedUsers[0].Contact.Account.Enterprise_Customer_ID__c;
  }
  
  private string queryBerryIdFromAccountId(Id AccountId)
  {
    List<Account> accs = [SELECT Enterprise_Customer_ID__c FROM Account WHERE Id = :AccountId];
    return accs[0].Enterprise_Customer_ID__c;
  }
  
  public void populateDashboardUrl()
  {
    //ok check the url for a berry_id param, this indicates that we have come direcly from an account
    Map<String,String> params = ApexPages.currentPage().getParameters(); 
    String berryId; 
    
    if(params.containsKey(BERRY_ID_PARAM))
    {
      //get our url using this berry id
      berryId = queryBerryIdFromAccountId(params.get(BERRY_ID_PARAM));
      isPortal = false;
    }
    else
    {
      //get the account info from the user info
      berryId = queryBerryIdFromUser();
      isPortal = true;
    }
    system.debug('berryId========'+berryId);    
    if(berryID == null )
    {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,system.label.SingleSignOnDashboard_no_BerryId));
      return;
    }
    
    //populate the URL from a callout to the talus service.
    try
    {
      string param = 'dashboard_url';
      if(isPortal)
      {
        param = 'secure_url';
      }
      
      dashboardURL = getUrlFromBerryID(berryId, callerDev,param);
      System.debug('************' + dashboardURL);
    }
    catch(exception e)
    {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,system.label.SingleSignOnDashboard_Callout_Exception));
      return;
    }   
    
    if(dashboardURL == null)
    {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,system.label.SingleSignOnDashboard_Response_Error));
    }
  }
  
  //this is made as a controller extension so it can be used on the account page
  //otherwise it doesn't use the standard controller
  public SingleSignOnDashboardController(ApexPages.standardController sc)
  {
    callerDev = new normalCalloutDevice();
  }
  
  //overload for testing.
  public SingleSignOnDashboardController(calloutDevice devOverride)
  {
    callerDev = devOverride;
  }
}