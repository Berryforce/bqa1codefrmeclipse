@isTest
private Class ScopedListingABDTest
{
    static testmethod void TestScopedListingABD(){
        
        Recordtype rttelcoId = [SELECT DeveloperName,Id,Name,SobjectType FROM RecordType where Name ='Telco Provider' and SobjectType='Telco__c'];
        Account objAcc = TestMethodsUtility.generateTelcoAccount();
        insert objAcc;
        Account objAcc1 = TestMethodsUtility.generateTelcoAccount();
        insert objAcc1;
        Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
        objTelco.RecordTypeId = rttelcoId.id;
        insert objTelco;
        Telco__c objTelco1 = TestMethodsUtility.generateTelco(objAcc1.Id);
        objTelco1.RecordTypeId = rttelcoId.id;
        insert objTelco1;
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        insert objDir;
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Listing__c objListing = TestMethodsUtility.generateListing();
        objListing.phone__c = '(890)890-0987';
        insert objListing;
        list<Directory_Listing__c> objdirLst = new list<Directory_Listing__c>();
        Directory_Listing__c objDirListing = TestMethodsUtility.generateNonOverrideRT();
        objDirListing.Listing__c = objListing.Id;
        objDirListing.Directory__c = objDir.Id;
        objDirListing.Directory_section__c = objDirSec.Id;
        objDirListing.Telco_Provider__c = objTelco.Id;
        objDirListing.phone_number__c = '(890)890-0987';
        objdirLst.add(objDirListing);
        //insert objDirListing;
        Directory_Listing__c objDirListing1 = TestMethodsUtility.generateNonOverrideRT();
        objDirListing1.Listing__c = objListing.Id;
        objDirListing1.Directory__c = objDir.Id;
        objDirListing1.Directory_section__c = objDirSec.Id;
        objDirListing1.Telco_Provider__c = objTelco.Id;
        objDirListing.phone_number__c = '(890)890-0987';
        objdirLst.add(objDirListing1);
        insert objdirLst;
        set<string> setdirsecValues = new set<string>();
        set<string> setArea = new set<string>();
        Test.StartTest();
        Database.BatchableContext bc;
        ScopedListingABDPendingBatchController objSOBatch = new ScopedListingABDPendingBatchController(objDir.id,objTelco1.Id,setdirsecValues,setArea,null,null,null,true);
        objSOBatch.start(bc);
        objSOBatch.execute(bc,objdirLst);
        Test.StopTest();
   
    }

}