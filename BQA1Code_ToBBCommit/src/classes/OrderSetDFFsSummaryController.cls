/********************************************************************
Controller Class to show OrderSet Dffs and Fulfillment related fields
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/28/2015
*********************************************************************/
public with sharing class OrderSetDFFsSummaryController {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    public String flag {
        get;
        set;
    }

    private Digital_Product_Requirement__c dff;
    public Map < String, String > mpDFlds = new Map < String, String > ();
    public String sptzAdvUrl;
    public List< Digital_Product_Requirement__c > lstSptzSEODffs = new List< Digital_Product_Requirement__c > ();
    public List< Digital_Product_Requirement__c > lstSptzOPODffs = new List< Digital_Product_Requirement__c > ();
    public List< Digital_Product_Requirement__c > lstSptzOnlineDisplyDffs = new List< Digital_Product_Requirement__c > ();
    public List< Digital_Product_Requirement__c > lstSptzDffs = new List< Digital_Product_Requirement__c > ();

    //Constructor
    public OrderSetDFFsSummaryController(ApexPages.StandardController controller) {

        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
    }

    public Map < String, String > getSmrryFlds() {

        Map < String, String > mapDFFFields = CommonMethods.fetchDFFFields();
        dffId = dff.Id;

        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.DFFFieldLabelNames(orderSetId, mOrderSetId);

        System.debug('************ALLDFFS************' + allDffs.size() + allDffs);

        if (allDffs.size() > 0) {

            for (Digital_Product_Requirement__c dff: allDffs) {

                String strDFFFields = mapDFFFields.get(dff.DFF_RecordType_Name__c);
                String[] strArrDFFFields;
                String DFFFldVals;
                Set < String > lkpFlds = CommonUtility.lookupFields();
                if(Test.isRunningTest()){
                    strDFFFields = 'business_name__c';
                }
                if (String.isNotBlank(strDFFFields)) {
                    strArrDFFFields = strDFFFields.split(',');
                    for (String strField: strArrDFFFields) {
                        if (!lkpFlds.Contains(strField)) {
                            if (String.isNotBlank(DFFFldVals)) {
                                DFFFldVals += '<br/>' + strField.replace('__c', '') + ': ' + dff.get(strField);
                            } else {
                                DFFFldVals = strField.replace('__c', '') + ': ' + dff.get(strField);
                            }
                        }

                    }

                    mpDFlds.put(dff.UDAC__c, DFFFldVals);
                }
                if(dff.DFF_RecordType_Name__c == 'Spotzer Website' && String.isNotBlank(dff.Bundle__c)){
                   sptzAdvUrl = dff.business_url__c;
                   //CommonUtility.msgInfo('************Spotzer Base Advertised Url: ' + sptzAdvUrl);
                }
                if((dff.DFF_RecordType_Name__c == 'SEO') && String.isNotBlank(dff.Bundle__c)){
                    lstSptzSEODffs.add(dff);
                }
                if((dff.DFF_RecordType_Name__c == 'OPO') && String.isNotBlank(dff.Bundle__c)){
                    lstSptzOPODffs.add(dff);
                }               
                if(dff.DFF_RecordType_Name__c == 'Online Display' && String.isNotBlank(dff.Bundle__c)){
                    lstSptzOnlineDisplyDffs.add(dff);
                }

            }

        }
        
        //Show warning message to user, if there are multiple products(having same product type) with same Advertised Url in Spotzer Bundle
        if(lstSptzSEODffs.size()>1){
            //CommonUtility.msgInfo('************SEO************');        
            flag = 'S';
            for(Digital_Product_Requirement__c iterator: lstSptzSEODffs){
                if(iterator.business_url__c != sptzAdvUrl && String.isBlank(iterator.Bundle__c)){
                    flag = 'N';
                }
            }
        }
        
        if(flag != 'S'){
            if(lstSptzOPODffs.size()>1){
                //CommonUtility.msgInfo('************OPO************');                
                flag = 'S';            
                for(Digital_Product_Requirement__c iterator: lstSptzOPODffs){
                    if(iterator.business_url__c != sptzAdvUrl && String.isBlank(iterator.Bundle__c)){
                        flag = 'N';
                    }
                }            
            }
        }
        
        if(flag != 'S'){
            if(lstSptzOnlineDisplyDffs.size()>1){
                //CommonUtility.msgInfo('************Online Display************');                
                flag = 'S';
                for(Digital_Product_Requirement__c iterator: lstSptzOnlineDisplyDffs){
                    if(iterator.business_url__c != sptzAdvUrl && String.isBlank(iterator.Bundle__c)){
                        flag = 'N';
                    }
                }            
            }
        }
        
        //System.debug('************' + flag);
        if(flag == 'S'){
            CommonUtility.msgWarning('You have multiple of the same Products "SEO or OPO or Online Display" which have the same Advertised Business Url. Please update accordingly');
        }
        
        if(mpDFlds.size()>0){
            for(String s: mpDFlds.Values()){
            
            }
        }

        //System.debug('************DFFFields************' + mpDFlds.keySet());
        return mpDFlds;
    }

    public List<Digital_Product_Requirement__c> getAllDffsSmmry() {
        dffId = dff.Id;

        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.DFFFieldLabelNames(orderSetId, mOrderSetId);
        return allDffs;    
    }
    
    public PageReference submitfulfillment() {
        PageReference pg = new PageReference('/apex/CreateTalusSubscriptionNew?id=' + dffId);
        return pg;
    }

}