@isTest
public class UploadContentControllerTest {
    static testMethod void testUploadContent() {
        Test.StartTest();
        Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
		Canvass__c c = TestMethodsUtility.createCanvass(telco);   
        
        Directory__c dir = TestMethodsUtility.createDirectory(); 
        
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = c.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        
        UploadContentController obj = new UploadContentController(new ApexPages.StandardController(DE));
        obj.uploadContents();
        
        Test.StopTest();
    }
}