@isTest(seeAllData=true)
public class YPCAddonValidationControllerTest{

    public static testMethod void ypcAdnVldtn(){

        List<YPC_AddOn__c> lstYpcAddns = new List<YPC_AddOn__c>();

        Product2 prdt = CommonUtility.createPrdtTest();
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Effective_Date__c = system.Today();
        OLI.Status__c = '';
        insert OLI;

        Test.startTest();

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.Talus_Subscription_Id__c = '';
        dff.Talus_DFF_Id__c = '';
        dff.recordTypeId = Label.YPC_Record_Type_Id;
        dff.OrderLineItemID__c = OLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.Status__c = '';
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff;

        lstYpcAddns.add(CommonUtility.createYPCAddonTest(dff.Id, 'YPCP', 'New'));
        lstYpcAddns.add(CommonUtility.createYPCAddonTest(dff.Id, 'CSTLK', 'New'));
        insert lstYpcAddns;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(dff);
        YPCAddonValidationController YPCAddVal = new YPCAddonValidationController(sc);
        YPCAddVal.addnVldtn();
        
        Test.stopTest();
        
    }
}