@isTest(seeAllData=true)
public class NewSalesInvoiceControllerTest {
	static testMethod void testNewSCN() {
		Test.startTest();
		Account newAccount = TestMethodsUtility.createAccount('customer');
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.generateSalesInvoice(newAccount, newOpportunity);
        NewSalesInvoiceController obj = new NewSalesInvoiceController(new ApexPages.StandardController(newSalesInvoice));
		obj.newSI = newSalesInvoice;
		obj.savSI();
        Test.stopTest();
	}
}