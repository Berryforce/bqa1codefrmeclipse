@IsTest
public class NationalStagingLineItemHandlerTest {

  public static testMethod void NSLIHandlerControllertestMethod(){
        Test.startTest(); 
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        List<Directory_Product_Mapping__c> ListDPM=new List<Directory_Product_Mapping__c>();
        
        Directory_Product_Mapping__c dpm=new Directory_Product_Mapping__c ();
        dpm.Directory__c=objDir.id;
        dpm.Product2__c=newProduct.Id;
        ListDPM.add(dpm);
        
       /* Directory_Product_Mapping__c dpm1=new Directory_Product_Mapping__c ();
        dpm1.Directory__c=objDir.id;
        dpm1.Product2__c=objProd.Id;
        ListDPM.add(dpm1);
        */
        insert ListDPM;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        objNSOS.Is_Converted__c=true;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        
        List<National_Staging_Line_Item__c> ListNSLINew=new List<National_Staging_Line_Item__c>();
        Set<id> NSLIIds=new Set<Id>();
        for(National_Staging_Line_Item__c nsliiterator : ListNSLI)
        {
           NSLIIds.add(nsliiterator.id);
        }
        List<National_Staging_Line_Item__c> objNSLINew = NationalStagingLineItemSOQLMethods.getNationalStagingLIListByNSLIId(NSLIIds);
        for(National_Staging_Line_Item__c nsliiterator : objNSLINew )
        {
            nsliiterator.UDAC__c='GC50';
            nsliiterator.Product2__c=objProd1.Id;
            ListNSLINew.add(nsliiterator);
        }
        ListNSLINew[0].UDAC__c=null;
        ListNSLINew[1].UDAC__c='SS';
        ListNSLINew[1].Product2__c=objProd.Id;
        update ListNSLINew;
        
        
        Map<Id, National_Staging_Line_Item__c> oldMap=new Map<Id, National_Staging_Line_Item__c>();
        for(National_Staging_Line_Item__c nsliIterator : ListNSLINew)
        {
          oldMap.put(nsliIterator.id,nsliIterator);
        }
        NationalStagingLineItemHandlerController.onBeforeInsert(ListNSLI);
        NationalStagingLineItemHandlerController.onBeforeUpdate(ListNSLI,oldMap);
        list<National_Staging_Line_Item__c> lstNSLIforInsert=new list<National_Staging_Line_Item__c>();
        lstNSLIforInsert=[SELECT Action__c,Advertising_Data__c,Auto_Number__c,BAS__c,Caption_Display_Text__c,Caption_Header__c,Caption_Indent_Level__c,Caption_Indent_Order__c,Caption_Phone_Number__c,CORE_Migration_ID__c,CreatedById,CreatedDate,DAT__c,Delete__c,Directory_Code__c,Directory_Heading__c,Directory_Section__c,Directory__c,Discount__c,Full_Rate_f__c,Id,Is_Caption_Header__c,Is_Changed__c,Is_Processed__c,Line_Error_Description__c,Line_Number__c,Listing__c,Name,National_Graphics_ID__c,National_Pricing__c,National_Staging_Header__c,Needs_Review__c,New_Transaction__c,Parent_ID__c,Processed_Order_Line_Item__c,Product2__c,RAC_Date__c,Ready_for_Processing__c,Row_Type__c,SAC_Date__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Standing_Order__c,TradeMark_Parent_Id__c,Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c FROM National_Staging_Line_Item__c where id in:ListNSLI];
        
        NationalStagingLineItemHandlerController.populateProductDPM(lstNSLIforInsert);
        Test.StopTest();
      }
  }