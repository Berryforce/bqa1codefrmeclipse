global class DataLoadBatch2 implements Database.Batchable<sObject>{
    global DataLoadBatch2(){
    }
    /*global Database.QueryLocator start(Database.BatchableContext bc){
        
        //String SOQL = 'select DFF_Product__c,ModificationOrderLineItem__r.Product2__r.ProductCode,ModificationOrderLineItem__r.Product2__c, DFF_Product__r.ProductCode,Sales_Rep__r.Email,Sales_Rep__r.FirstName,Sales_Rep__r.LastName from Digital_Product_Requirement__c where UDAC__c = null';
        
        //String SOQL = 'SELECT id FROM Listing__c';
        
        //String SOQL = 'SELECT id, DP_Caption_Member_F__c,DP_Caption_Header_F__c,DP_Continuous_Service_Order_Appearance_F__c,DP_Is_Cross_Reference_F__c,DP_Trademark_Finding_Line_F__c,DP_IBUN_Type_F__c,DP_IBUN_Website_One_F__c,DP_IBUN_Website_Two_F__c,DP_Internet_Bundle_Ad_F__c,DP_Section_Code_F__c,DP_Directory_Code_F__c,DP_Banner_F__c,DP_Billboard_F__c,DP_Year_F__c,DP_Directory_Edition_Code_F__c,DP_Directory_Edition_Name_F__c,DP_Section_Type_F__c,DP_Type_F__c,DP_Scoped_Caption_Header_F__c,DP_DataLoad__c, Order_Line_Item__r.Digital_Product_Requirement__c FROM Directory_Pagination__c where DP_DataLoad__c = false limit 1000000';
        
        String SOQL = 'select id,Canvass__r.Primary_Telco__c from Order_line_Items__c where Canvass__r.Primary_Telco__c != null and Telco__c = null and media_type__c = \'Digital\'';
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Order_line_Items__c> scope){
    
        //delete scope;
    
        List<Order_line_Items__c> lstUpdt = new List<Order_line_Items__c>();
        for(Order_line_Items__c objOLI : scope) {
            //Directory_Pagination__c objDFFNew = new Directory_Pagination__c(id = objDP.id);
            //lstUpdt.add(new Directory_Pagination__c(id = objDP.id, Scoped_Caption_Header__c = objDP.DP_Scoped_Caption_Header_F__c, Caption_Member__c = objDP.DP_Caption_Member_F__c, Caption_Header__c = objDP.DP_Caption_Header_F__c, Continuous_Service_Order_Appearance__c = objDP.DP_Continuous_Service_Order_Appearance_F__c, Is_Cross_Reference__c = objDP.DP_Is_Cross_Reference_F__c, Trademark_Finding_Line__c = objDP.DP_Trademark_Finding_Line_F__c, IBUN_Type__c = objDP.DP_IBUN_Type_F__c, IBUN_Website_One__c = objDP.DP_IBUN_Website_One_F__c, IBUN_Website_Two__c = objDP.DP_IBUN_Website_Two_F__c, Internet_Bundle_Ad__c = objDP.DP_Internet_Bundle_Ad_F__c, Section_Code__c = objDP.DP_Section_Code_F__c, Directory_Code__c = objDP.DP_Directory_Code_F__c, Banner__c = objDP.DP_Banner_F__c, Billboard__c = objDP.DP_Billboard_F__c, Year__c = objDP.DP_Year_F__c, Directory_Edition_Code__c = objDP.DP_Directory_Edition_Code_F__c, Directory_Edition_Name__c = objDP.DP_Directory_Edition_Name_F__c, Section_Type__c = objDP.DP_Section_Type_F__c, Type__c = objDP.DP_Type_F__c, DP_DataLoad__c = true, Digital_Product_Requirement__c = objDP.Order_Line_Item__r.Digital_Product_Requirement__c));
            lstUpdt.add(new Order_line_Items__c(id = objOLI.id, Telco__c = objOLI.Canvass__r.Primary_Telco__c));
            
        }
        if(lstUpdt.size() > 0) {
            update lstUpdt;
        }        
    }*/
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        //String SOQL = 'select DFF_Product__c,ModificationOrderLineItem__r.Product2__r.ProductCode,ModificationOrderLineItem__r.Product2__c, DFF_Product__r.ProductCode,Sales_Rep__r.Email,Sales_Rep__r.FirstName,Sales_Rep__r.LastName from Digital_Product_Requirement__c where UDAC__c = null';
        
        //String SOQL = 'SELECT id FROM Listing__c';
        
        //String SOQL = 'SELECT id, DP_Caption_Member_F__c,DP_Caption_Header_F__c,DP_Continuous_Service_Order_Appearance_F__c,DP_Is_Cross_Reference_F__c,DP_Trademark_Finding_Line_F__c,DP_IBUN_Type_F__c,DP_IBUN_Website_One_F__c,DP_IBUN_Website_Two_F__c,DP_Internet_Bundle_Ad_F__c,DP_Section_Code_F__c,DP_Directory_Code_F__c,DP_Banner_F__c,DP_Billboard_F__c,DP_Year_F__c,DP_Directory_Edition_Code_F__c,DP_Directory_Edition_Name_F__c,DP_Section_Type_F__c,DP_Type_F__c,DP_Scoped_Caption_Header_F__c,DP_DataLoad__c, Order_Line_Item__r.Digital_Product_Requirement__c FROM Directory_Pagination__c where DP_DataLoad__c = false limit 1000000';
        
        String SOQL = 'select id, Canvass__r.Primary_Telco__c from Modification_Order_Line_Item__c where Canvass__r.Primary_Telco__c != null and Telco__c = null and completed__c = false and inactive__c = false';
        
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<Modification_Order_Line_Item__c> scope){
    
        //delete scope;
    
        List<Modification_Order_Line_Item__c> lstUpdt = new List<Modification_Order_Line_Item__c>();
        for(Modification_Order_Line_Item__c objOLI : scope) {
            //Directory_Pagination__c objDFFNew = new Directory_Pagination__c(id = objDP.id);
            //lstUpdt.add(new Directory_Pagination__c(id = objDP.id, Scoped_Caption_Header__c = objDP.DP_Scoped_Caption_Header_F__c, Caption_Member__c = objDP.DP_Caption_Member_F__c, Caption_Header__c = objDP.DP_Caption_Header_F__c, Continuous_Service_Order_Appearance__c = objDP.DP_Continuous_Service_Order_Appearance_F__c, Is_Cross_Reference__c = objDP.DP_Is_Cross_Reference_F__c, Trademark_Finding_Line__c = objDP.DP_Trademark_Finding_Line_F__c, IBUN_Type__c = objDP.DP_IBUN_Type_F__c, IBUN_Website_One__c = objDP.DP_IBUN_Website_One_F__c, IBUN_Website_Two__c = objDP.DP_IBUN_Website_Two_F__c, Internet_Bundle_Ad__c = objDP.DP_Internet_Bundle_Ad_F__c, Section_Code__c = objDP.DP_Section_Code_F__c, Directory_Code__c = objDP.DP_Directory_Code_F__c, Banner__c = objDP.DP_Banner_F__c, Billboard__c = objDP.DP_Billboard_F__c, Year__c = objDP.DP_Year_F__c, Directory_Edition_Code__c = objDP.DP_Directory_Edition_Code_F__c, Directory_Edition_Name__c = objDP.DP_Directory_Edition_Name_F__c, Section_Type__c = objDP.DP_Section_Type_F__c, Type__c = objDP.DP_Type_F__c, DP_DataLoad__c = true, Digital_Product_Requirement__c = objDP.Order_Line_Item__r.Digital_Product_Requirement__c));
            lstUpdt.add(new Modification_Order_Line_Item__c(id = objOLI.id, Telco__c = objOLI.Canvass__r.Primary_Telco__c));
            /*if(objDFFNew.DFF_Product__c == null && objDFF.ModificationOrderLineItem__c != null) {
                objDFFNew.DFF_Product__c = objDFF.ModificationOrderLineItem__r.Product2__c;
                objDFFNew.UDAC__c = objDFF.ModificationOrderLineItem__r.Product2__r.ProductCode;
                lstUpdtDFF.add(objDFFNew);
            }*/
        }
        if(lstUpdt.size() > 0) {
            update lstUpdt;
        }        
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }

}