global class BillingSubsequentPrintSchedulerHndlr implements BillingSubsequentPrintScheduler.BillingSubsequentPrintSchedulerInterface{
     global void execute(SchedulableContext SC) {
        Database.executeBatch(new PrintMonthlyBilling(system.today(), CommonMessages.printMediaType), 20);
    } 
}