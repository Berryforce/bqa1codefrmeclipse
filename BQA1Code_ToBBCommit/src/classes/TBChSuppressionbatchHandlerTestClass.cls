@isTest
private class TBChSuppressionbatchHandlerTestClass {
    static testMethod void TBChSuppressionbatchHandlerTest() {

        SchedulableContext sc;
        Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Dir_BOTS_Suppress_UnSuppress_OLI__c = system.today().adddays(2);
        insert dir;
        Directory_Edition__c dirEdition = TestMethodsUtility.generateDirectoryEdition(dir);
        dirEdition.Book_Status__c = 'BOTS';
        dirEdition.DE_Suppress_UnSuppress_OLI__c = system.today().adddays(2);
        insert dirEdition;
        //system.assertEquals(null,dir.Dir_BOTS_Suppress_UnSuppress_OLI__c);
        listing__c lst = TestMethodsUtility.createListing('Main Listing');
        Directory_listing__c dlist = TestMethodsUtility.generateDirectoryListing();
        dlist.Listing__c=lst.Id;
        dlist.Directory__c = dir.Id;
        insert dlist;
        Account acct = TestMethodsUtility.generateCustomerAccount();
        insert acct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);
        oln.Scoped_Suppression_Processing_Complete__c = false;
        oln.Requires_Scoped_Suppression_Processing__c = true;
        oln.Directory_Edition__c = dirEdition.Id;
        oln.Directory__c = dir.id;
        oln.Media_Type__c = 'print';
        oln.Listing__c = lst.id;
        insert oln;
        TBChSuppressionbatchHandler tbsSuppress = new TBChSuppressionbatchHandler();
        tbsSuppress.execute(sc);
        TBCscSupressionBatchScheduler tbsSuppressSch = new TBCscSupressionBatchScheduler();
        tbsSuppressSch.execute(sc);
        
    }
     static testMethod void TBChUnSuppressionbatchHandlerTest() {
        SchedulableContext sc;
        Directory__c dir1 = TestMethodsUtility.generateDirectory();
        dir1.Dir_BOTS_Suppress_UnSuppress_OLI__c = system.today().adddays(-4);
        insert dir1;
        Directory_Edition__c dirEdition = TestMethodsUtility.generateDirectoryEdition(dir1);
        dirEdition.Book_Status__c = 'BOTS';
        dirEdition.DE_Suppress_UnSuppress_OLI__c = system.today().adddays(-4);
        insert dirEdition;
        //system.assertEquals(null,dir.Dir_BOTS_Suppress_UnSuppress_OLI__c);
        listing__c lst = TestMethodsUtility.createListing('Main Listing');
        Directory_listing__c dlist1 = TestMethodsUtility.generateDirectoryListing();
        dlist1.Listing__c=lst.Id;
        dlist1.Directory__c = dir1.Id;
        insert dlist1;
        Account acct = TestMethodsUtility.generateCustomerAccount();
        insert acct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);
        oln.Scoped_Suppression_Processing_Complete__c = false;
        oln.Requires_Scoped_Suppression_Processing__c = true;
        oln.Directory_Edition__c = dirEdition.Id;
        oln.Directory__c = dir1.id;
        oln.Media_Type__c = 'print';
        oln.Listing__c = lst.id;
        insert oln;
        dlist1.Suppressed_By_OLI__c = oln.Id;
        dlist1.Suppressed__c = true;
        //dlist1.Telco_Provider__c = null;
        update dlist1;
        TBChUnSuppressionbatchHandler tbsUnSuppress = new TBChUnSuppressionbatchHandler();
        tbsUnSuppress.execute(sc);
        TBCscUnSupressionBatchScheduler tbsUnSuppressSch = new TBCscUnSupressionBatchScheduler();
        tbsUnSuppressSch.execute(sc);
        
    }
}