global class ScheduleDummyObjRecordDeletionBatch implements Schedulable {
   public Interface ScheduleDummyObjRecordDeletionBatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ScheduleDummyObjRecordDeletionBatchHndlr');
        if(targetType != null) {
            ScheduleDummyObjRecordDeletionBatchInterface obj = (ScheduleDummyObjRecordDeletionBatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}