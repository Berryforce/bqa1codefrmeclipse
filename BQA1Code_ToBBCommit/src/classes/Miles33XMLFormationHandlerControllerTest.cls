@isTest
private class Miles33XMLFormationHandlerControllerTest  {

    static testMethod void generateXMLForYPCTest() {
        Test.startTest();
        Account newAccount = TestMethodsUtility.generateAccount('customer');
        newAccount.name = 'Test&Account';
        insert newAccount;
      
       Digital_Product_Requirement__c DigDFF = TestMethodsUtility.generateiYPGold();
       DigDFF.Account__c = newAccount.id;
       insert DigDFF;
       List<User> lstUser = new List<User>();
       lstUser = [Select Id From User where Id IN(Select ownerid from Digital_Product_Requirement__c where id =:DigDFF.Id)];     
     

       DigDFF.Production_Notes_for_Logo__c = 'NEW Notes';
       DigDFF.Production_Notes_for_Banner_Graphic__c = 'New Notes';
       DigDFF.Production_Notes_for_Diamond_Graphic__c= 'New Notes';
       DigDFF.Production_Notes_for_PrintAdOrderGraphic__c = 'New Notes';
       DigDFF.Heading__c = 'Test&Heading';
       DigDFF.Paper_or_Email__c = 'Test';
       DigDFF.URN_number__c = 367;
       DigDFF.ownerid=lstUser[0].Id; 
       DigDFF.Surname__c='TestSurName1234567890';
       update DigDFF;
       
    
               
       Set<Id> setDigdff=new Set<Id>();
       setDigdff.add(DigDFF.Id);
       Map<Id,String> mapMps = new Map<Id,String>();
        mapMps.put(DigDFF.id,DigDFF.Production_Notes_for_PrintAdOrderGraphic__c);
       list<YPC_Graphics__c> lstYPC = new list<YPC_Graphics__c>();
       
       YPC_Graphics__c YPC = new YPC_Graphics__c(Miles_Status__c = '',DFF__c = DigDFF.Id,Production_Notes_for_YPC_Graphic__c = DigDFF.Production_Notes_for_Logo__c,YPC_Graphics_Type__c = 'Logo');
       lstYPC.add(YPC);
       YPC = new YPC_Graphics__c(Miles_Status__c = '', DFF__c = DigDFF.Id,Production_Notes_for_YPC_Graphic__c = DigDFF.Production_Notes_for_Logo__c,YPC_Graphics_Type__c = 'Diamond Logo');
       lstYPC.add(YPC);
       YPC = new YPC_Graphics__c(Miles_Status__c = '', DFF__c = DigDFF.Id,Production_Notes_for_YPC_Graphic__c = DigDFF.Production_Notes_for_Logo__c,YPC_Graphics_Type__c = 'Diamond Distribution');
       lstYPC.add(YPC);
       insert lstYPC;
       Miles33XMLFormationHandlerController.generateXMLForYPC(setDigdff,mapMps);
       Miles33XMLFormationHandlerController.generateXMLForPrint(DigDFF.Id);
    
       
       Test.stopTest(); 
    }
}