/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public with sharing class FFA_TelcoCashMatchingByGroupQueueable implements Queueable
{
    private List<ffps_bmatching__Custom_Cash_Matching_History__c> matchingHistories;
    
    private Id groupingId;
    private List<String> errorMessages;
    
    public FFA_TelcoCashMatchingByGroupQueueable(Id groupId) 
    {
        errorMessages = new List<String>();
        groupingId = groupId;
    }
    
    public FFA_TelcoCashMatchingByGroupQueueable(Id groupId, List<String> errors) 
    {
        errorMessages = errors;
        groupingId = groupId;
    }

    public void execute(QueueableContext context) 
    {
        ffps_bmatching.AppTriggerSwitch.turnOffTrigger();

        Set<id> transactionLineIds = new Set<id>();
        
        List<ffps_bmatching__Custom_Cash_Matching_History__c> matchingHistories = 
            [SELECT
                id,
                name,
                ffps_bmatching__Matching_Reference__c,
                ffps_bmatching__Period__c,
                ffps_bmatching__Completed__c,
                ffps_bmatching__Account__c,             
                ffps_bmatching__Matching_Date__c,
                ffps_bmatching__Error_Count__c,
                (SELECT
                    id,
                    ffps_bmatching__Transaction_Line_Id__c,
                    ffps_bmatching__Transaction_Line_Item__c,
                    ffps_bmatching__Amount_Matched__c
                FROM
                    ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
            FROM 
                ffps_bmatching__Custom_Cash_Matching_History__c
            WHERE
                 ffps_bmatching__Completed__c = false 
            AND
                ffps_bmatching__Custom_Cash_Matching_History_Group__c = :groupingId
            AND 
                (ffps_bmatching__Error_Count__c < 3 OR ffps_bmatching__Error_Count__c = null)
            ORDER BY
                ffps_bmatching__Error_Count__c
            LIMIT 3];

        for(ffps_bmatching__Custom_Cash_Matching_History__c so :matchingHistories)
        {
            System.Savepoint sp = Database.setSavepoint();
            try
            {
                c2g.CODAAPICommon_7_0.Context apiContext = null;
                c2g.CODAAPICashMatchingTypes_7_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_7_0.Configuration();
                configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_7_0.enumMatchingCurrencyMode.Document; 
                configuration.MatchingDate = so.ffps_bmatching__Matching_Date__c;
                List<c2g.CODAAPICashMatchingTypes_7_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_7_0.Item>();

                configuration.Account = c2g.CODAAPICommon.getRef(so.ffps_bmatching__Account__c, null);
                configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(so.ffps_bmatching__Period__c, null);

                for(ffps_bmatching__Custom_Cash_Matching_History_Line__c line :so.ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
                {
                    transactionLineIds.add(line.ffps_bmatching__Transaction_Line_Item__c);
                    c2g.CODAAPICashMatchingTypes_7_0.Item item = new c2g.CODAAPICashMatchingTypes_7_0.Item();
                    item.TransactionLineItem = c2g.CODAAPICommon.getRef( line.ffps_bmatching__Transaction_Line_Id__c, null );
                    item.Paid = line.ffps_bmatching__Amount_Matched__c;
                    item.Discount = 0; 
                    item.WriteOff = 0; 
                    items.add( item );
                }

                if (items.size() > 0)
                {
                    c2g.CODAAPICashMatchingTypes_7_0.Analysis analisysInfo = new c2g.CODAAPICashMatchingTypes_7_0.Analysis();
                    c2g.CODAAPICommon.Reference matchReference = c2g.CODAAPICashMatching_7_0.Match(apiContext, configuration, items, analisysInfo);
                    so.ffps_bmatching__Matching_Reference__c = matchReference.id;
                    so.ffps_bmatching__Completed__c = true;
                    so.ffps_bmatching__Update_Account__c = true;
                }
            }
            catch (Exception e)
            {
                Database.rollback(sp);
                errorMessages.add( 'Custom Cash Matching Reference : ' + so.Name + ' was not matched because : ' + e.getMessage() + ' for grouping '+groupingId +' (' + e.getLineNumber() + ')' + '\n\n');
                if(so.ffps_bmatching__Error_Count__c == null)
                {
                    so.ffps_bmatching__Error_Count__c = 1;
                }
                else
                {
                    so.ffps_bmatching__Error_Count__c+= 1;
                }
            }
        }
        update matchingHistories;
        
        update [SELECT id FROM c2g__codaTransactionLineItem__c WHERE id IN :transactionLineIds];
        
        List<ffps_bmatching__Custom_Cash_Matching_History__c> availableMatchHistories = 
            [SELECT
                id,
                name
            FROM 
                ffps_bmatching__Custom_Cash_Matching_History__c
            WHERE
                 ffps_bmatching__Completed__c = false 
            AND
                ffps_bmatching__Custom_Cash_Matching_History_Group__c = :groupingId
            AND
                (ffps_bmatching__Error_Count__c < 3 OR ffps_bmatching__Error_Count__c = null)
            LIMIT 1];

        if(availableMatchHistories.size() != 0)
        {
            System.enqueueJob(new FFA_TelcoCashMatchingByGroupQueueable(groupingId, errorMessages));
        }
        else
        {
            delete [Select Id From ffps_bmatching__Custom_Cash_Matching_History_Group__c Where Id = :groupingId];
        }
        //if(!errorMessages.isEmpty()) //sendEmail('uravat@financialforce.com', errorMessages);
    }

    private static void sendEmail( String address, List<String> errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Cash Matching Queueable (FFA_TelcoCashMatchingByGroupQueueable):\n\n';
        
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Cash Matching By Account Grouping (FFA_TelcoCashMatchingByGroupQueueable)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}