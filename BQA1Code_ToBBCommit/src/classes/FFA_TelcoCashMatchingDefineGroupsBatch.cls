/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/
public class FFA_TelcoCashMatchingDefineGroupsBatch implements Database.Batchable<SObject>, Database.Stateful, Schedulable
{
    public Set<Id> accountIds = new Set<Id>();
    
    public String previousAccountKey;
    public ffps_bmatching__Custom_Cash_Matching_History_Group__c currentGroup;
    public Integer groupSizeCount = 0;
    public Integer groupingNumber = 1;
    
    public void execute (SchedulableContext ctx)
    {
        List<String> classNames = new List<String>{'FFA_SCRTelcoMatchingStagingQueueable'}; 
        List<String> batchStatus = new List<String>{'Queued', 'Holding', 'Preparing', 'Processing'};
        Integer jobs = [SELECT count() FROM AsyncApexJob WHERE ApexClass.Name IN :classNames AND Status IN :batchStatus];

        if(jobs > 0)
        {
            Datetime sysTime = System.now().addminutes( 10 );
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            FFA_TelcoCashMatchingDefineGroupsBatch batch = new FFA_TelcoCashMatchingDefineGroupsBatch();
            System.schedule('FFA_TelcoCashMatchingDefineGroupsBatch' + sysTime.getTime(), chron_exp, batch);
        }
        else
        {
            Database.executeBatch( new FFA_TelcoCashMatchingDefineGroupsBatch() , 100);
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date matchingDate = system.today().adddays(-3);
        Id userId = UserInfo.getUserId();

        String query = 
            'SELECT '+
            '    Id, '+
            '    Customer_Name__c, '+
            '    ffps_bmatching__Account__c, '+
            '    ffps_bmatching__Custom_Cash_Matching_History_Group__c '+
            'FROM '+
            '    ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '    ffps_bmatching__Completed__c = false '+
            'AND '+
            '    ffps_bmatching__Custom_Cash_Matching_History_Group__c = null '+
            'AND '+
            '   CreatedDate >= :matchingDate '+
            'AND '+
            '   CreatedById = :userId '+
            'ORDER BY '+
            '    ffps_bmatching__Account__c';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SObject> unTypedScope) 
    {
        Map<Id, ffps_bmatching__Custom_Cash_Matching_History_Group__c> historyByItem = new Map<Id,ffps_bmatching__Custom_Cash_Matching_History_Group__c>();
        List<ffps_bmatching__Custom_Cash_Matching_History_Group__c> groupsToInsert = new List<ffps_bmatching__Custom_Cash_Matching_History_Group__c>();
        
        for(SObject scopeItem : unTypedScope)
        {
            if((Id)scopeItem.get('ffps_bmatching__Account__c') != null && (Id)scopeItem.get('Customer_Name__c') != null)
            {
                String currentKey = (String)scopeItem.get('ffps_bmatching__Account__c') +'-'+(String)scopeItem.get('Customer_Name__c');
                if(currentGroup == null || groupSizeCount >= 15 || previousAccountKey != currentKey)
                {    
                    if(previousAccountKey != currentKey)
                    {
                        groupingNumber=1;
                    }
                    currentGroup = new ffps_bmatching__Custom_Cash_Matching_History_Group__c(ffps_bmatching__Group_Number__c = groupingNumber, ffps_bmatching__Account__c = (Id)scopeItem.get('ffps_bmatching__Account__c'));
                    groupsToInsert.add(currentGroup);
                    groupSizeCount = 0;
                    groupingNumber++;
                }
                historyByItem.put(scopeItem.Id, currentGroup);
                groupSizeCount++;
                currentGroup.ffps_bmatching__Group_Size__c = groupSizeCount;
                previousAccountKey = currentKey;
            }
        }
        if(groupsToInsert.size() > 0)
        {
            insert groupsToInsert;
        }
        
        List<SObject> historiesList = new List<SObject>();
        for(SObject scopeItem : unTypedScope)
        {
            if(historyByItem.containsKey(scopeItem.Id))
            {
                scopeItem.put('ffps_bmatching__Custom_Cash_Matching_History_Group__c',historyByItem.get(scopeItem.Id).Id);
                historiesList.add(scopeItem);
            }
        }
        
        if(historiesList.size() > 0)
        {
            update historiesList;
        }
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        FFA_TelcoCashMatchingEnqueueByGroup batch = new FFA_TelcoCashMatchingEnqueueByGroup();       
        Database.executeBatch(batch,1);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { 'uravat@financialforce.com' } );
        String body = 'Account Ids picked up ';

        mail.setPlainTextBody( body );
        mail.setSubject( 'Results of Account Staging batch' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}