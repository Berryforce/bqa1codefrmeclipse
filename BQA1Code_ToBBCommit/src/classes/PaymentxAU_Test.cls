@isTest(SeeAllData = true)
private class PaymentxAU_Test {
    static testMethod void PaymentXAUMethod1() 
    {
        Test.Starttest();
        List<c2g__codaDimension1__c> lstDimension1 = new List<c2g__codaDimension1__c>();
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        insert newOpportunity;  
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        newProduct.Exclude_Dimension_1_3__c = true;
        insert newProduct;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        system.assertEquals(newAccount.Id, newOrLI.Account__c);
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.createDirectoryEdition(newDirectory.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);
        Canvass__c soqlCanvass = [Select Id, Name, Canvass_Code__c From Canvass__c Where Id =: newCanvass.Id Limit 1];
        
        c2g__codaDimension1__c Dimension1ForDirectory = TestMethodsUtility.generateDimension1(newDirectory);
        c2g__codaDimension1__c Dimension1ForDirectoryEdition = TestMethodsUtility.generateDimension1(newDirectoryEdition);
        c2g__codaDimension1__c Dimension1ForAccount = TestMethodsUtility.generateDimension1(newCanvass.Name, soqlCanvass.Canvass_Code__c);
        lstDimension1.add(Dimension1ForDirectory);
        lstDimension1.add(Dimension1ForDirectoryEdition);
        lstDimension1.add(Dimension1ForAccount);
        insert lstDimension1;
        c2g__codaDimension2__c Dimension2ForProduct = TestMethodsUtility.generateDimension2(newProduct);
        c2g__codaDimension3__c Dimension3ForOrLI = TestMethodsUtility.generateDimension3(newOrLI);
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        c2g__codaCompany__c newCompany = TestMethodsUtility.createCompany('VAT');
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.generateSalesInvoice(newAccount, newOpportunity);
        newSalesInvoice.c2g__OwnerCompany__c = newCompany.Id;
        insert newSalesInvoice;
        
        c2g__codaInvoiceLineItem__c newSalesInvoiceLI=TestMethodsUtility.createSalesInvoiceLineItem(newOrLI,newSalesInvoice, newProduct,Dimension1ForDirectory ,Dimension2ForProduct ,Dimension3ForOrLI ,dimension4);
       
            
c2g__codaInvoiceLineItem__c newSalesInvoiceLITest=TestMethodsUtility.createSalesInvoiceLineItem(newOrLI,newSalesInvoice, newProduct,Dimension1ForDirectory ,Dimension2ForProduct ,Dimension3ForOrLI ,dimension4);
       newSalesInvoiceLITest.c2g__UnitPrice__c=100;
       update newSalesInvoiceLITest;
       list<c2g__codaInvoiceLineItem__c> lstILI=new list<c2g__codaInvoiceLineItem__c>();
       lstILI.add(newSalesInvoiceLITest);      
        
       // insert newSalesInvoiceLI;
       // system.assertEquals(newAccount.Id, newSalesInvoiceLI.Order_Line_Item__r.Account__c);
        system.assertNotEquals(newSalesInvoiceLI.c2g__Invoice__c, null);
        pymt__PaymentX__c newPaymentX = TestMethodsUtility.generatePaymentX(newSalesInvoice, newOpportunity);
        newPaymentX.pymt__Billing_Email__c = 'test@test.test';
        newPaymentX.pymt__Status__c = 'Completed';
        newPaymentX.pymt__Amount__c = 10.00;
        newPaymentX.pymt__Payment_Type__c = 'Credit Card';
        newPaymentX.pymt__Contact__c = newContact.Id;
        insert newPaymentX;
        system.assertNotEquals(newPaymentX.p2f__FF_Sales_Invoice__c, null);
                
        pymt__PaymentX__c newPaymentX1 = TestMethodsUtility.generatePaymentX(newSalesInvoice, newOpportunity);
        newPaymentX1.pymt__Billing_Email__c = 'test@test.test';
        newPaymentX1.pymt__Status__c = 'Completed';
        newPaymentX1.pymt__Amount__c = 10.00;
        newPaymentX1.pymt__Payment_Type__c = 'ECheck';
        newPaymentX1.pymt__Contact__c = newContact.Id;
        newPaymentX1.p2f__FF_Sales_Invoice__c=newSalesInvoice.Id;
        insert newPaymentX1;
        Test.Stoptest();
        //newPaymentX.pymt__Status__c = 'Declined';
        //update newPaymentX;
        
        
        PaymentxAUtrigger_v1 pymtController=new PaymentxAUtrigger_v1();
        Messaging.SingleEmailMessage msg=pymtController.CreatePDFAttachment(newAccount,newContact,newPaymentX1,newSalesInvoice,lstILI,Date.parse('3/3/2013') ,false,true);
    }
    static testMethod void PaymentXAUMethod2() {
        Test.Starttest();
        Set<Id> SalesInvoiceIds = new Set<Id>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        insert newOpportunity;  
        
        c2g__codaCompany__c newCompany = TestMethodsUtility.createCompany('VAT');
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.generateSalesInvoice(newAccount, newOpportunity);
        newSalesInvoice.c2g__OwnerCompany__c = newCompany.Id;
        insert newSalesInvoice;
        SalesInvoiceIds.add(newSalesInvoice.Id);
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Exclude_Dimension_1_3__c = true;
        insert newProduct;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        c2g__codaDimension1__c Dimension1ForDirectory = TestMethodsUtility.generateDimension1(newDirectory);
         c2g__codaDimension2__c Dimension2ForProduct = TestMethodsUtility.generateDimension2(newProduct);
        c2g__codaDimension3__c Dimension3ForOrLI = TestMethodsUtility.generateDimension3(newOrLI);
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        
        c2g__codaInvoiceLineItem__c newSalesInvoiceLI=TestMethodsUtility.createSalesInvoiceLineItem(newOrLI,newSalesInvoice, newProduct,Dimension1ForDirectory ,Dimension2ForProduct ,Dimension3ForOrLI ,dimension4);
       
       // c2g__codaInvoiceLineItem__c newSalesInvoiceLI = TestMethodsUtility.generateSalesInvoiceLineItem(newSalesInvoice, newProduct);
       // insert newSalesInvoiceLI;
        pymt__PaymentX__c newPaymentX = TestMethodsUtility.generatePaymentX(newSalesInvoice, newOpportunity);
        newPaymentX.pymt__Billing_Email__c = 'test@test.test';
        newPaymentX.pymt__Status__c = 'Declined';
        newPaymentX.pymt__Amount__c = 10.00;
        newPaymentX.pymt__Payment_Type__c = 'Credit Card';
        newPaymentX.pymt__Contact__c = newContact.Id;
        newPaymentX.pymt__Card_Type__c = 'Visa';
        newPaymentX.pymt__Last_4_Digits__c = '9876';
        newPaymentX.pymt__Log__c = 'Description';
        insert newPaymentX;
        PaymentxAUtrigger_v1.generatePDF(SalesInvoiceIds);
        Test.Stoptest();
    }
    static testMethod void PaymentXAUMethod3() {
        Test.Starttest();
        Set<Id> SalesInvoiceIds = new Set<Id>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        insert newOpportunity;  
        c2g__codaCompany__c newCompany = TestMethodsUtility.createCompany('VAT');
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.generateSalesInvoice(newAccount, newOpportunity);
        newSalesInvoice.c2g__OwnerCompany__c = newCompany.Id;
        insert newSalesInvoice;
        SalesInvoiceIds.add(newSalesInvoice.Id);
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Exclude_Dimension_1_3__c = true;
        insert newProduct;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        c2g__codaDimension1__c Dimension1ForDirectory = TestMethodsUtility.generateDimension1(newDirectory);
         c2g__codaDimension2__c Dimension2ForProduct = TestMethodsUtility.generateDimension2(newProduct);
        c2g__codaDimension3__c Dimension3ForOrLI = TestMethodsUtility.generateDimension3(newOrLI);
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        
        c2g__codaInvoiceLineItem__c newSalesInvoiceLI=TestMethodsUtility.createSalesInvoiceLineItem(newOrLI,newSalesInvoice, newProduct,Dimension1ForDirectory ,Dimension2ForProduct ,Dimension3ForOrLI ,dimension4);
       
        //c2g__codaInvoiceLineItem__c newSalesInvoiceLI = TestMethodsUtility.generateSalesInvoiceLineItem(newSalesInvoice, newProduct);
        //insert newSalesInvoiceLI;
        pymt__PaymentX__c newPaymentX = TestMethodsUtility.generatePaymentX(newSalesInvoice, newOpportunity);
        newPaymentX.pymt__Billing_Email__c = 'test@test.test';
        newPaymentX.pymt__Status__c = 'Declined';
        newPaymentX.pymt__Amount__c = 10.00;
        newPaymentX.pymt__Payment_Type__c = 'ECheck';
        newPaymentX.pymt__Contact__c = newContact.Id;
        newPaymentX.pymt__Card_Type__c = 'Visa';
        newPaymentX.pymt__Last_4_Digits__c = '9876';
        newPaymentX.pymt__Log__c = 'Description';
        insert newPaymentX;
        PaymentxAUtrigger_v1.generatePDF(SalesInvoiceIds);
        Test.Stoptest();
    }
}