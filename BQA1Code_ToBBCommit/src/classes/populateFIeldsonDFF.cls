global class populateFIeldsonDFF implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string recordType=Label.TestOLIRTLocal;
        string mediaType=CommonMessages.printMediaType;
        //String query = 'SELECT Id, Name, recordtypeid, Media_Type__c, Listing__c FROM Order_Line_Items__c where (Listing__c!=null and Media_Type__c=:mediaType) and recordtypeid=:recordType';
        String query='SELECT Id, Name, recordtypeid, Media_Type__c, Digital_Product_Requirement__c,'+
                    'Listing__c,Listing__r.Phone__c, Listing__r.Name,Listing__r.Listing_Postal_Code__c,'+
                    'Listing__r.Listing_City__c, Listing__r.Area_Code__c, Listing__r.Listing_State__c,'+
                    'Listing__r.Listing_Street_Number__c,Listing__r.Listing_Street__c,Listing__r.Listing_PO_Box__c '+
                    'FROM Order_Line_Items__c where Listing__c!=null and Media_Type__c=:mediaType and recordtypeid=:recordType limit 10';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Order_Line_Items__c> oliList) {
        list<Digital_Product_Requirement__c> dffListUpdate=new list<Digital_Product_Requirement__c>();
        //looping orderlineitems list
        Savepoint sp = Database.setSavepoint();
        try {
	        for(Order_Line_Items__c oli : oliList) {
	            Digital_Product_Requirement__c d=new Digital_Product_Requirement__c(Id=oli.Digital_Product_Requirement__c);
	            d.business_phone_number_office__c=oli.Listing__r.Phone__c;
	            d.business_name__c=oli.Listing__r.Name;
	            d.business_postal_code__c=oli.Listing__r.Listing_Postal_Code__c;
	            d.business_city__c=oli.Listing__r.Listing_City__c;
	            d.Advertised_Phone_Area_Code__c=oli.Listing__r.Area_Code__c;
	            d.business_state__c=oli.Listing__r.Listing_State__c;
	            d.business_address1__c=oli.Listing__r.Listing_Street_Number__c+oli.Listing__r.Listing_Street__c+oli.Listing__r.Listing_PO_Box__c;
	            dffListUpdate.add(d);
	        }
	        system.debug('listt to update dff----'+dffListUpdate);
	        update dffListUpdate;
        }
        catch(Exception e) {
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Relinking Listing PopulateFieldsonDFF');
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}