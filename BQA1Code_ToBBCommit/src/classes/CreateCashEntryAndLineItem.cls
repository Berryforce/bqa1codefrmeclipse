global class CreateCashEntryAndLineItem implements database.batchable<sObject>{
    String strWhere;
    String strTransactionType;
    public CreateCashEntryAndLineItem(string strWhere,String strTransactionType) {
        this.strWhere = strWhere;
        this.strTransactionType = strTransactionType;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String idAcc = '001Z000000z8VB3';
        String strQuery = 'Select Id from Account where ';// where Cash_batching_3__c = TRUE';
        strQuery = strQuery + strWhere;
        //String strQuery = 'Select Id from Account where ID =:idAcc';
        return Database.getQueryLocator(strQuery);
    } 
    
    global void execute(Database.BatchableContext BC, List<Account> lstAccount)  {
    
        
        list<c2g__codaCashEntryLineItem__c> lstChildCE = new list<c2g__codaCashEntryLineItem__c>();
        
        if(strTransactionType != 'PIA') {
            c2g__codaCashEntry__c objCE = createCashEntry('P - Payment');
            insert objCE;
            for(Account objAccount : lstAccount) {
                lstChildCE.add(createCashEntryLineItem(objAccount, objCE));
            }
            insert lstChildCE;
        }
        else {
            c2g__codaCashEntry__c objCE = createCashEntry('PIA – Payment In Advance');
            insert objCE;
            map<Id,c2g__codaInvoiceLineItem__c> mapActIdSILI = new map<Id,c2g__codaInvoiceLineItem__c>();
            set<Id> setAcctId = new set<Id>();
            for(Account objAccount : lstAccount) {
                setAcctId.add(objAccount.Id);
            }
            if(setAcctId.size()> 0) {
                list<c2g__codaInvoiceLineItem__c> lstSILI = new list<c2g__codaInvoiceLineItem__c>();
                lstSILI = [Select Id,c2g__Invoice__r.c2g__Account__c,c2g__Dimension1__c,Directory__c,Directory_Edition__c from c2g__codaInvoiceLineItem__c where c2g__Invoice__r.c2g__Account__c IN :setAcctId AND c2g__Invoice__r.c2g__InvoiceStatus__c ='Complete' AND c2g__Invoice__r.SI_Payment_Method__c != 'Telco Billing'
                        AND c2g__Invoice__r.c2g__InvoiceTotal__c > 0 AND c2g__Invoice__r.c2g__Account__r.RecordTypeId=:Label.TestAccountCustomerRT AND c2g__Invoice__r.c2g__InvoiceDate__c<= Today AND c2g__Invoice__r.c2g__OutstandingValue__c > 0 AND c2g__Invoice__r.c2g__Account__r.AR_Balance__c > 20];
                if(lstSILI.size()> 0) {
                    for(c2g__codaInvoiceLineItem__c iteratorSILI : lstSILI) {
                        if(!mapActIdSILI.containskey(iteratorSILI.c2g__Invoice__r.c2g__Account__c)) {
                            mapActIdSILI.put(iteratorSILI.c2g__Invoice__r.c2g__Account__c,iteratorSILI);
                        }
                    }
                }
                for(Account objAccount : lstAccount) {
                    if(mapActIdSILI.get(objAccount.Id) != null) {
                        lstChildCE.add(createPIACashEntryLineItem(objAccount, objCE,mapActIdSILI.get(objAccount.Id)));
                    }
                }
            }
            if(lstChildCE.size()> 0) {
                insert lstChildCE;
            }
        }
        
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
    } 
    
    
    private c2g__codaCashEntry__c createCashEntry(String strTranType) {
        Date objDate = System.today().addDays(-1);
        return new c2g__codaCashEntry__c(Transaction_Type__c = strTranType, Payment_Type__c = 'Check', c2g__BankAccount__c = 'a0HG000000LYRkB', 
        c2g__Date__c = objDate, c2g__Type__c = 'Receipt', c2g__PaymentMethod__c = 'Lockbox', c2g__Status__c = 'In Progress', ffcash__DerivePeriod__c = true, 
        ffcash__DeriveBankAccount__c = true, ffcash__DeriveBankAccountDimensions__c = true, ffcash__DeriveBankChargesAnalysis__c = true);
    }
    
    private c2g__codaCashEntryLineItem__c createCashEntryLineItem(Account objAcc, c2g__codaCashEntry__c objCE) {
        return new c2g__codaCashEntryLineItem__c(c2g__CashEntry__c = objCE.Id, c2g__Account__c = objAcc.Id, c2g__CashEntryValue__c = 100.00,
        Transaction_Types__c = 'P - Payment', Payment_Type__c = 'Check', Payment_Reason__c = 'P - Payment', c2g__AccountPaymentMethod__c = 'Lockbox');
    }
    private c2g__codaCashEntryLineItem__c createPIACashEntryLineItem(Account objAcc, c2g__codaCashEntry__c objCE,c2g__codaInvoiceLineItem__c objSILI) {
        return new c2g__codaCashEntryLineItem__c(c2g__CashEntry__c = objCE.Id, c2g__Account__c = objAcc.Id, c2g__CashEntryValue__c = 100.00,
        Transaction_Types__c = 'PIA – Payment In Advance', Payment_Type__c = 'Check', Payment_Reason__c = 'P - Payment', c2g__AccountPaymentMethod__c = 'Lockbox',
        c2g__AccountDimension1__c = objSILI.c2g__Dimension1__c,Directory__c = objSILI.Directory__c,Directory_Edition__c = objSILI.Directory_Edition__c);
    }
}