<apex:component controller="NationalBillingSendISS" allowDML="true"> 
    <apex:pageBlock id="pgBlkSendISS">
    <apex:pageMessages />  
    <apex:pageBlockSection title="Send ISS" collapsible="false">
        <apex:panelGrid columns="2">            
            <apex:outputLabel >Publication Company:</apex:outputLabel>
            <apex:inputtext value="{!strPubCodeSendISS}"/>
            <apex:outputLabel >Edition:</apex:outputLabel>
            <apex:panelGroup >
            	<apex:inputtext value="{!strEditionCodeSendISS}"/>      
               <apex:commandButton value="Go" action="{!fetchISS}" rerender="pgBlkSendISS" status="actStatus"/> 
            </apex:panelGroup>               
        </apex:panelGrid>
    </apex:pageBlockSection>                            
    <apex:pageBlockSection id="pgBlkSecSendISSData" collapsible="false" rendered="{!bolFetchDir}" columns="1">
        <apex:commandLink value="ISS for Pub Co {!strPubCodeSendISS}" action="{!displayISS}" rerender="pgBlkSendISS, ISSs" status="actStatus"/>
        <apex:panelGrid style="align:left" columns="6" width="60%" border="1">
            <apex:outputLabel >Total Gross Amount</apex:outputLabel>
            <apex:outputLabel >Total ADJ Amount</apex:outputLabel>
            <apex:outputLabel >Total Tax Amount</apex:outputLabel>
            <apex:outputLabel >Total Commission</apex:outputLabel>
            <apex:outputLabel >Net Amount</apex:outputLabel>
            <apex:outputLabel >Total Number of ISS</apex:outputLabel>
            <apex:outputText value="${0, number, ###,###,###,##0.00}">
	            <apex:param value="{!ISSAmt}" />
	        </apex:outputText> 
            <apex:outputText value="${0, number, ###,###,###,##0.00}">
	            <apex:param value="{!TotalADJAmount}" />
	        </apex:outputText> 
	        <apex:outputText value="${0, number, ###,###,###,##0.00}">
	            <apex:param value="{!ISSTaxAmt}" />
	        </apex:outputText> 
	        <apex:outputText value="${0, number, ###,###,###,##0.00}">
	            <apex:param value="{!TotalCommission}" />
	        </apex:outputText> 
	        <apex:outputText value="${0, number, ###,###,###,##0.00}">
	            <apex:param value="{!ISSNetAmt}" />
	        </apex:outputText> 
            <apex:outputtext value="{!intISSCount}" />
        </apex:panelGrid>       
        <!--<apex:commandButton value="Send to Elite" id="btnSendElite" disabled="{! NOT(chkISSComplete)}" action="#"/>-->
        <apex:panelGrid columns="1">
            <apex:panelGroup >
                <apex:inputCheckbox value="{!balancedBool}"/>
                <apex:outputText >Balanced</apex:outputText>
            </apex:panelGroup>
            <apex:panelGroup >
                <apex:commandButton value="Send To Elite" disabled="{!sendToEliteBool}" action="{!sendToElite}" status="actStatus" reRender="pgBlkSendISS"/>
                <!-- <apex:commandButton value="SAC/RAC Error Report"/> -->
            </apex:panelGroup>
            <apex:panelGroup >
                <apex:inputCheckbox value="{!natInvCompleteBool}" disabled="true"/> 
                <apex:outputText >ISS Sent to Elite</apex:outputText>
            </apex:panelGroup>
        </apex:panelGrid>   
        <apex:outputLink target="_blank" style="align:right" title="SAC Error Report" value="/{!SACRACreportId}">SAC Error Report</apex:outputLink>
        <apex:outputLink target="_blank" style="align:right" title="ISS/CMR Validation Report" value="/{!ISSToCMRReportId}?pv0={!strPubCodeSendISS}&pv1={!strEditionCodeSendISS}">ISS/CMR Validation Report</apex:outputLink>        
    </apex:pageBlockSection>
    <apex:pageBlockButtons location="bottom" rendered="{!bolFetchDir}">
        <apex:commandButton value="Save To Status Table" action="{!updateStatusTable}" status="actStatus" rerender="pgBlkSendISS"/>
            <apex:commandButton value="Refresh From Status Table" action="{!fetchISS}" status="actStatus" reRender="pgBlkSendISS"/>
    </apex:pageBlockButtons>
    </apex:pageBlock>
    
	<apex:outputPanel id="ISSs">
		<apex:outputPanel layout="block" styleClass="popupBg" id="opISS" rendered="{!bolISS}">
			<apex:outputPanel styleClass="popup" layout="block" rendered="{!bolISS}">
				<apex:pageBlock id="pgBlkDisplayISS">
					<apex:pageBlockButtons location="bottom">
						<apex:commandButton action="{!hideISS}" value="Close"/>
					</apex:pageBlockButtons>
					<apex:pageBlockSection title="ISS" columns="1" id="pbSection" collapsible="false">
						<apex:pageBlockTable border="1" value="{!lstISS}" var="objISS">                         
							<apex:column headerValue="ISS" headerClass="colHeadr">
								<apex:outputLink target="_blank" value="/{!objISS.Id}">{!objISS.Name}</apex:outputLink>
							</apex:column>
							<apex:column headerValue="CMR Number" headerClass="colHeadr" width="10px" value="{!objISS.CMR_Number__c}" dir="LTR"/>
							<apex:column headerValue="Total Gross Amount" headerClass="colHeadr" width="10px" value="{!objISS.Total_Gross_Amount__c}" styleClass="rightAlign">
								<apex:facet name="footer">
		                        	<apex:outputText value="${0, number, ###,###,###,##0.00}" style="float:right;">
									    <apex:param value="{!totalISSGrossAmount}" />
									</apex:outputText>
	                        	</apex:facet>
							</apex:column>
							<apex:column headerValue="Total ADJ Amount" headerClass="colHeadr" width="10px" value="{!objISS.Total_ADJ_Amount__c}" styleClass="rightAlign">
								<apex:facet name="footer">
		                        	<apex:outputText value="${0, number, ###,###,###,##0.00}" style="float:right;">
									    <apex:param value="{!totalISSADJAmount}" />
									</apex:outputText>
                        		</apex:facet>
							</apex:column>
							<apex:column headerValue="Total Commission" headerClass="colHeadr" width="10px" value="{!objISS.TotalCommission__c}" styleClass="rightAlign">
								<apex:facet name="footer">
		                        	<apex:outputText value="${0, number, ###,###,###,##0.00}" style="float:right;">
									    <apex:param value="{!totalISSCommissionAmount}" />
									</apex:outputText>
	                        	</apex:facet>
							</apex:column>
							<apex:column headerValue="Total Tax" headerClass="colHeadr" width="10px" value="{!objISS.Total_Tax__c}" styleClass="rightAlign">
								<apex:facet name="footer">
		                        	<apex:outputText value="${0, number, ###,###,###,##0.00}" style="float:right;">
									    <apex:param value="{!totalISSTaxAmount}" />
									</apex:outputText>
	                        	</apex:facet>
							</apex:column>
							<apex:column headerValue="Net Amount" headerClass="colHeadr" width="10px" value="{!objISS.NetAmount__c}" styleClass="rightAlign">
								<apex:facet name="footer">
		                        	<apex:outputText value="${0, number, ###,###,###,##0.00}" style="float:right;">
									    <apex:param value="{!totalISSNetAmount}" />
									</apex:outputText>
	                        	</apex:facet>
							</apex:column>
						</apex:pageBlockTable>
					</apex:pageBlockSection>
				</apex:pageBlock>
			</apex:outputPanel>
		</apex:outputPanel>
	</apex:outputPanel>
</apex:component>