<apex:component controller="LocalBillingPrintFirstMonthController" allowDML="true">
    <style>
    .ct{
        text-align:right;        
    }
    </style>
    <apex:form >
        <apex:pageMessages ></apex:pageMessages>
        <apex:actionstatus id="fetchStatus">
            <apex:facet name="start">
            <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb;height:100%;opacity:0.65;width:100%;"> 
                <div class="waitingHolder" style="width: 91px;">
                    <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                    <span class="waitingDescription">Loading...</span>
                </div>
            </div>
            </apex:facet>
        </apex:actionstatus>
        <apex:pageBlock >           
            <apex:pageBlockSection title="First Month Print" columns="1" id="pbsMain">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Select Bill Prep Date"></apex:outputLabel>
                    <apex:inputField value="{!objDummy.Date__c}" id="billdate">
                        <apex:actionSupport event="onblur" action="{!fetchDirectoryEdition}" status="fetchStatus" reRender="pbsMain"/>
                    </apex:inputField>                  
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Directory Edition"></apex:outputLabel>
                    <apex:outputPanel >
                        <apex:selectList value="{!strSelectedDirectoryEdition}" multiselect="false" size="1">
                            <apex:selectOptions value="{!lstDirectoryEdition}"/>
                        </apex:selectList>
                        &nbsp;&nbsp;<apex:commandButton value="Go" action="{!clickGo}" reRender="opLOB,pbsMain,opPostFF,opCreateTelco" status="fetchStatus"/>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputPanel >
                        <apex:outputLabel value="{!strNoofAccountAndInvoice}" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            <apex:outputPanel id="opLOB">
                <apex:pageBlockSection title="List of Business Report" columns="1" id="pbsLOB" rendered="{!lstLOB.size > 0}">
                    <apex:pageBlockTable value="{!lstLOB}" var="item">
                        <apex:column headerValue="" value="{!item.strName}"/>
                        <apex:column headerValue="# of Account" value="{!item.iAccCount}" headerClass="ct" style="text-align: right;"/>
                        <apex:column headerValue="# of Invoice" value="{!item.iSICount}" headerClass="ct" style="text-align: right;"/>
                        <apex:column headerValue="OLI Total" value="{!item.dOLITotal}" headerClass="ct" style="text-align: right;"/>
                        <apex:column headerValue="Invoice Total" value="{!item.dSITotal}" headerClass="ct" style="text-align: right;"/>
                        <apex:column headerValue="Variance" value="{!item.dVariance}" headerClass="ct" style="text-align: right;"/>
                    </apex:pageBlockTable>
                    <apex:outputPanel >                        
                        <apex:commandLink action="/{!LOBMatchingReportId}?pv0={!objDEValues.Directory_Code__c}&pv1={!objDEValues.Edition_Code__c}&colDt_s={!objDummy.Date__c}&colDt_e={!objDummy.Date__c}" target="_blank" value="LOB Matching Report" id="btnLOBMatchingReport"/>&nbsp;&nbsp;&nbsp;&nbsp; 
                        <apex:commandLink action="/{!LOBReportId}?pv0={!objDEValues.Directory_Code__c}&pv1={!objDEValues.Edition_Code__c}" target="_blank" value="LOB Report"/>&nbsp;&nbsp;&nbsp;&nbsp;
                        <apex:commandLink action="/{!UDACSumReportId}?pv0={!objDEValues.Directory_Code__c}&pv1={!objDEValues.Edition_Code__c}" target="_blank" value="UDAC Summary Report"/>&nbsp;&nbsp;&nbsp;&nbsp;                        
                        <apex:outputPanel layout="block" style="float:right">
                            <apex:commandButton id="refreshreportbtn" value="Refresh" action="{!clickGo}" status="fetchStatus" reRender="opLOB,pbsMain,opPostFF,opCreateTelco"/> 
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <apex:outputPanel >
                        <apex:inputCheckbox id="chkReconciled" value="{!bReconciled}"><apex:actionSupport event="onchange" status="fetchStatus" reRender="opLOB"/></apex:inputCheckbox><apex:outputLabel value="Reconciled" for="chkReconciled"/>&nbsp;&nbsp;
                        <apex:inputCheckbox id="chkMIRT" value="{!bMIRT}"><apex:actionSupport event="onchange" status="fetchStatus" reRender="opLOB"/></apex:inputCheckbox><apex:outputLabel value="MIRT" for="chkMIRT"/>&nbsp;&nbsp;
                        <apex:commandButton id="updateSve" value="Save" action="{!updateSIwithReconciledCheckBox}" disabled="{!IF((bReconciled && bMIRT),false,true)}" reRender="opPostFF,opReconciliationPanel" status="fetchStatus"/>                   
                    </apex:outputPanel>
                    <apex:outputPanel id="opReconciliationPanel">
                        <apex:actionPoller action="{!apexReconciliationJobStatus}" enabled="{!bReconciliationEnabledPoller}" interval="5" reRender="opReconciliationPanel,opPostFF" status="counterStatus"/>
                        <apex:actionStatus startText="Fetching Status..." id="counterStatus"/>
                        <br/>
                        <apex:outputLabel value="{!strSIReconcileBatchStatus}" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </apex:outputPanel>
            <apex:outputPanel id="opPostFF">
                <apex:pageBlockSection title="Post to Financial Force" columns="1" id="pbsPostFF"  rendered="{!bShowPostFF}">
                    <apex:outputPanel >
                        <apex:commandLink action="/{!printFirstMonthPostingReportId}?pv1={!objDEValues.Directory_Code__c}&pv2={!objDEValues.Edition_Code__c}" target="_blank" value="Invoice Posting Report" id="clInvoicePostingReport"/>&nbsp;&nbsp;&nbsp;
                    </apex:outputPanel>
                    <apex:outputPanel >
                        <apex:inputCheckbox id="chkPostSI" value="{!bPostAllInvoice}"><apex:actionSupport event="onchange" status="fetchStatus" reRender="pbsPostFF"/></apex:inputCheckbox><apex:outputLabel value="Post All Invoice" for="chkPostSI"/>
                    </apex:outputPanel>
                    <apex:outputPanel id="opPostFF">
                        <apex:commandButton id="btnPostingInvoice" value="Post to FF" action="{!postFFInvoice}" disabled="{!IF((bShowPostFF && bPostAllInvoice),false,true)}" reRender="pbsPostFF" status="fetchStatus"/>&nbsp;
                        <apex:actionPoller action="{!apexJobStatus}" enabled="{!bEnabledPoller}" interval="5" reRender="opPostFF,opCreateTelco"/>
                        <apex:outputLabel value="{!strBatchStatus}" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </apex:outputPanel>
            <apex:outputPanel id="opCreateTelco">
                <apex:pageBlockSection title="Create Telco File" columns="1" id="pbsCreateTelco" rendered="{!bShowCreateTelco}">                    
                    <apex:panelGrid columns="2" width="75%">
                        <apex:panelGroup >
                            <apex:commandButton value="Create & Send Telco File" status="fetchStatus" action="{!createTelcoFile}" disabled="{!!objDEValues.Telco_Recives_Electronice_File__c}" reRender="opCreateTelco" />
                            <apex:actionPoller action="{!checkTelcoXMLOutput}" enabled="{!bEnabledPollerForTelco}" interval="5" reRender="opCreateTelco"/>
                            &nbsp;&nbsp;<apex:outputLabel value="{!strBatchStatusTelco}" />
                        </apex:panelGroup>
                        <apex:panelGroup >
                            <apex:outputLabel value="Telco Invoice Total : "/>
                            <apex:outputText value="{0,number,$ 0.00}">
                                <apex:param value="{!dTelcoInvoiceTotal}"/>
                            </apex:outputText>
                        </apex:panelGroup>
                        <apex:panelGroup >
                            <apex:outputLabel style="float: left;width: 200px;">Telco Received Electronic File</apex:outputLabel>
                            <apex:inputCheckbox id="elcetronicchkbox" value="{!objDEValues.Telco_Recives_Electronice_File__c}" disabled="true"/>
                        </apex:panelGroup>
                        <apex:panelGroup >
                            <apex:outputLabel value="XML Telco Invoice Total : "/>
                            <apex:outputText value="{0,number,$ 0.00}">
                                <apex:param value="{!objDEValues.XML_Output_Total_Amount__c}"/>
                            </apex:outputText>
                        </apex:panelGroup>
                    </apex:panelGrid>
                </apex:pageBlockSection>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
</apex:component>