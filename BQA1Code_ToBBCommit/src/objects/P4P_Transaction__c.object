<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object used to store the data from Media Trax for P4P that is transformed into Records by Informatica. The records in this object are queried by the associated OLI for the P4P Current Billing Period Clicks/Leads and P4P Current Month Clicks/Leads.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Audio_URL__c</fullName>
        <externalId>false</externalId>
        <label>Audio URL</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>Call_Result__c</fullName>
        <description>P4P Media Trax Call Result: Connected/No Answer 
Abandoned calls are being dropped from the daily file.</description>
        <externalId>false</externalId>
        <label>Call Result</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Call_Time__c</fullName>
        <description>Tracks the time a call was received</description>
        <externalId>false</externalId>
        <label>Call Time</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Caller_Number__c</fullName>
        <description>Stores the number that called the Media Trax Tracking Number</description>
        <externalId>false</externalId>
        <label>Caller Number</label>
        <length>15</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cost_Per_Call__c</fullName>
        <externalId>false</externalId>
        <label>Cost Per Call</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Credit</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Date_of_Click_Call_Lead__c</fullName>
        <description>The date the Click, Call, or Lead occurred</description>
        <externalId>false</externalId>
        <inlineHelpText>The date the Click, Call, or Lead occurred</inlineHelpText>
        <label>Date of Click/Call/Lead</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Duration__c</fullName>
        <externalId>false</externalId>
        <label>Duration in seconds</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The P4P Order Line Item the recorded Click/Lead/Call is related to.</description>
        <externalId>false</externalId>
        <inlineHelpText>This P4P event log is related to this Order Line item.</inlineHelpText>
        <label>Order Line Item</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>This is not an Order Line Item with P4P Billing.</errorMessage>
            <filterItems>
                <field>Order_Line_Items__c.Is_P4P__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Order_Line_Items__c</referenceTo>
        <relationshipLabel>P4P Transaction Log</relationshipLabel>
        <relationshipName>P4P_Transactions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>P4P_Tracking_Number__c</fullName>
        <externalId>false</externalId>
        <label>P4P Tracking Number</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Verified Billable</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Solicitation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Job Seeker</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wrong Number</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Out of Area</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Non Business</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Total_Billable_Calls__c</fullName>
        <description>The number of billable Clicks/Calls/Leads that the P4P transaction is worth</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of billable Clicks/Calls/Leads that the P4P transaction is worth</inlineHelpText>
        <label>Total Billable Clicks/Calls/Leads</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>P4P Transaction</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Caller_Number__c</columns>
        <columns>Call_Result__c</columns>
        <columns>Call_Time__c</columns>
        <columns>Cost_Per_Call__c</columns>
        <columns>Date_of_Click_Call_Lead__c</columns>
        <columns>Duration__c</columns>
        <columns>Order_Line_Item__c</columns>
        <columns>Audio_URL__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>P4P Transaction Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>P4P Transactions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
