<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Auto_Number__c</fullName>
        <externalId>false</externalId>
        <label>Auto Number</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>CMR Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National History Order Sets (CMR Name)</relationshipLabel>
        <relationshipName>National_History_Order_Sets1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CMR_Notice__c</fullName>
        <externalId>false</externalId>
        <formula>IF( CMR_Name__r.Non_Elite__c =TRUE, IMAGE(&quot;/img/samples/flag_red.gif&quot;, &quot;Non-Elite&quot;, 32,32), 
IMAGE(&quot;/img/samples/flag_green.gif&quot;, &quot;Elite CMR&quot;, 32,32))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CMR Notice</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_Number__c</fullName>
        <externalId>false</externalId>
        <label>CMR #</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CORE_Migration_ID__c</fullName>
        <description>This is needed for migration of data from CORE</description>
        <externalId>false</externalId>
        <label>CORE Migration ID</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Name_Text__c</fullName>
        <externalId>false</externalId>
        <label>Client Name Text</label>
        <length>53</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Client Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National History Order Sets (Client Name)</relationshipLabel>
        <relationshipName>National_History_Order_Sets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client_Number__c</fullName>
        <externalId>false</externalId>
        <label>Client #</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Directory_Edition_Number__c</fullName>
        <externalId>false</externalId>
        <label>Directory Edition #</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Edition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory_Number__c</fullName>
        <externalId>false</externalId>
        <label>Directory #</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Version__c</fullName>
        <externalId>false</externalId>
        <label>Directory Version</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>National Staging Order Sets</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>From_Number__c</fullName>
        <externalId>false</externalId>
        <label>From #</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>From_Type__c</fullName>
        <externalId>false</externalId>
        <label>From Type</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Header_Error_Description__c</fullName>
        <externalId>false</externalId>
        <label>Header Error Description</label>
        <length>5000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Is_Converted__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Converted to Opportunity and Order OR not</description>
        <externalId>false</externalId>
        <label>Is Converted?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Ready__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Ready?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Late_Order__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Late Order</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>NAT_Client_Id__c</fullName>
        <externalId>false</externalId>
        <label>NAT Client Id</label>
        <length>25</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NAT__c</fullName>
        <externalId>false</externalId>
        <label>NAT</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>National_Staging_Order_Set__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>National Staging Order Set</label>
        <referenceTo>National_Staging_Order_Set__c</referenceTo>
        <relationshipLabel>National History Order Sets</relationshipLabel>
        <relationshipName>National_History_Order_Sets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Non_Elite_CMR__c</fullName>
        <externalId>false</externalId>
        <formula>IF( CMR_Name__r.Non_Elite__c =TRUE, &quot;Y&quot;, &quot;N&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Non-Elite CMR</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Code__c</fullName>
        <externalId>false</externalId>
        <label>Publication Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Publication_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Publication Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>National Staging Order Sets (Publication Company)</relationshipLabel>
        <relationshipName>National_Staging_Order_Sets3</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Publication_Date__c</fullName>
        <externalId>false</externalId>
        <label>Publication Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RAC_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Receipt acknowledged date from Elite - set by integration</inlineHelpText>
        <label>RAC Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Reference_Date__c</fullName>
        <externalId>false</externalId>
        <label>Reference Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>SAC_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Date sent to Elite - set by integration</inlineHelpText>
        <label>SAC Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>State__c</fullName>
        <externalId>false</externalId>
        <label>State</label>
        <length>2</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TRANS_Code__c</fullName>
        <externalId>false</externalId>
        <label>TRANS Code</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>To_Number__c</fullName>
        <externalId>false</externalId>
        <label>To #</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>To_Type__c</fullName>
        <externalId>false</externalId>
        <label>To Type</label>
        <length>1</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_ID__c</fullName>
        <externalId>true</externalId>
        <label>Transaction ID</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Unique_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Unique ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Version_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Version ID</label>
        <length>21</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Version__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Version</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>National History Order Set</label>
    <nameField>
        <label>National History Order Set Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>National History Order Sets</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
