<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>MSC_Area_Code__c</fullName>
        <externalId>false</externalId>
        <label>Area Code</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MSC_Area_Exchange_Dir_Dir_Sec_Combo__c</fullName>
        <caseSensitive>true</caseSensitive>
        <externalId>false</externalId>
        <label>Area Exchange Dir Dir Sec Combo</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>MSC_Directory_Section__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Section</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Directory_Section__c.Directory__c</field>
                <operation>equals</operation>
                <valueField>$Source.MSC_Directory__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Directory_Section__c</referenceTo>
        <relationshipLabel>Multi Scoping Children</relationshipLabel>
        <relationshipName>Multi_Scoping_Children</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>MSC_Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>Multi Scoping Children</relationshipLabel>
        <relationshipName>Multi_Scoping_Children</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>MSC_Exchange_Code__c</fullName>
        <externalId>false</externalId>
        <label>Exchange Code</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MSC_Multi_Scoping_Header__c</fullName>
        <externalId>false</externalId>
        <label>Multi Scoping Header</label>
        <referenceTo>Multi_Scoping_Header__c</referenceTo>
        <relationshipLabel>Multi Scoping Children</relationshipLabel>
        <relationshipName>Multi_Scoping_Children</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <label>Multi Scoping Child</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>MSC_Multi_Scoping_Header__c</columns>
        <columns>MSC_Area_Code__c</columns>
        <columns>MSC_Exchange_Code__c</columns>
        <columns>MSC_Directory__c</columns>
        <columns>MSC_Directory_Section__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>MSC - {0}</displayFormat>
        <label>Multi Scoping Child Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Multi Scoping Children</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Area_Exchange_Code_should_match_header</fullName>
        <active>true</active>
        <errorConditionFormula>OR(
 MSC_Area_Code__c  &lt;&gt;  MSC_Multi_Scoping_Header__r.MSH_Area_Code__c ,
 MSC_Exchange_Code__c  &lt;&gt;  MSC_Multi_Scoping_Header__r.MSH_Exchange_Code__c 
)</errorConditionFormula>
        <errorMessage>Area and Exchange Codes should match with header</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Section_Type_should_match_with_header</fullName>
        <active>true</active>
        <errorConditionFormula>IF(
 TEXT(MSC_Directory_Section__r.Section_Page_Type__c) !=  TEXT(MSC_Multi_Scoping_Header__r.MSH_Directory_Section__r.Section_Page_Type__c) ,
true,
false
)</errorConditionFormula>
        <errorDisplayField>MSC_Directory_Section__c</errorDisplayField>
        <errorMessage>Section Type should match with header record&apos;s Directory Section Page Type</errorMessage>
    </validationRules>
</CustomObject>
