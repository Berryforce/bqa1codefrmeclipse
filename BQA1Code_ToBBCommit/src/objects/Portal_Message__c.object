<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active_Message__c</fullName>
        <externalId>false</externalId>
        <formula>IF(AND(TODAY()&gt;=Publish_Date__c,TODAY()&lt;=End_Date__c), &quot;Active&quot;, &quot;Inactive&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Active Message</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Apply_to_the_following_portals__c</fullName>
        <externalId>false</externalId>
        <label>Which Portal(s)?</label>
        <picklist>
            <picklistValues>
                <fullName>Local Advertising</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Berry PR Customer Portal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Dominican Republic Advertising Portal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Frontier Customer Portal</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Message_English__c</fullName>
        <externalId>false</externalId>
        <label>Message (English)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Message_Spanish__c</fullName>
        <externalId>false</externalId>
        <label>Message (Spanish)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Portal_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Portal User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Portal_Messages</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Publish_Date__c</fullName>
        <externalId>false</externalId>
        <label>Publish Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Portal Message</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Active_Message__c</columns>
        <columns>End_Date__c</columns>
        <columns>Message_English__c</columns>
        <columns>Message_Spanish__c</columns>
        <columns>Portal_User__c</columns>
        <columns>Publish_Date__c</columns>
        <columns>Apply_to_the_following_portals__c</columns>
        <columns>RECORDTYPE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>PM-{00000}</displayFormat>
        <label>Portal Message Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Portal Messages</pluralLabel>
    <recordTypes>
        <fullName>Broadcast_Message</fullName>
        <active>true</active>
        <label>Broadcast Message</label>
        <picklistValues>
            <picklist>Apply_to_the_following_portals__c</picklist>
            <values>
                <fullName>Berry PR Customer Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dominican Republic Advertising Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Frontier Customer Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Local Advertising</fullName>
                <default>true</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Individual_Message</fullName>
        <active>true</active>
        <label>Individual Message</label>
        <picklistValues>
            <picklist>Apply_to_the_following_portals__c</picklist>
            <values>
                <fullName>Berry PR Customer Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dominican Republic Advertising Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Frontier Customer Portal</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Local Advertising</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Create_Broadcast_Portal_Messages</fullName>
        <active>true</active>
        <errorConditionFormula>OR( AND( $User.Create_Individual_Portal_Message__c=FALSE, RecordType.Name = &quot;Individual Message&quot;, $Profile.Name &lt;&gt; &quot;System Administrator&quot;) , AND($User.Create_Broadcast_Portal_Messages__c=FALSE, RecordType.Name = &quot;Broadcast Message&quot;, $Profile.Name &lt;&gt; &quot;System Administrator&quot;) , AND( $User.Create_Individual_Portal_Message__c=FALSE, $User.Create_Broadcast_Portal_Messages__c=FALSE, $Profile.Name &lt;&gt; &quot;System Administrator&quot;) )</errorConditionFormula>
        <errorMessage>You are not Authorized to create this type of record.  Please contact your system administrator.</errorMessage>
    </validationRules>
</CustomObject>
