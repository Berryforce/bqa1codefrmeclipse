trigger DirectoryListing_BIUAIUD on Directory_Listing__c (before insert, before update, after delete, after insert, after update) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.DLObjectName)) {
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                DirectoryListingHandlerController.onBeforeInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                DirectoryListingHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                DirectoryListingHandlerController.onAfterInsert(trigger.new);
            }
            if(Trigger.isUpdate){
                DirectoryListingHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
            }
        }
    
    }
}