trigger COREHistCustServiceLogARNotesTrigger on CORE_Hist_Cust_Service_Log_AR_Notes__c (before insert, before update) {
	Set<String> setCoreAcctIds = new Set<String>();
	Map<String, CORE_Hist_Cust_Service_Log_AR_Notes__c> mapCoreAcctIdCoreHistCust = new Map<String, CORE_Hist_Cust_Service_Log_AR_Notes__c>();
	Map<String, Account> mapExtIdAcct = new Map<String, Account>();
	
	for(CORE_Hist_Cust_Service_Log_AR_Notes__c iterator : trigger.new) {
		if(String.isNotBlank(iterator.Core_Account_ID__c)) {
			setCoreAcctIds.add(iterator.Core_Account_ID__c);
			mapCoreAcctIdCoreHistCust.put(iterator.Core_Account_ID__c, iterator);
		}
	}
	
	if(setCoreAcctIds.size() > 0) {
		List<Account> listAcct = AccountSOQLMethods.getAccountsByExternalId(setCoreAcctIds);	
		
		if(listAcct.size() > 0) {
			for(Account acct : listAcct) {
				if(String.isNotBlank(acct.X3l_External_ID__c)) {
					mapExtIdAcct.put(acct.X3l_External_ID__c, acct);
				} else if(String.isNotBlank(acct.Parent_3L_External_ID__c)) {
					mapExtIdAcct.put(acct.Parent_3L_External_ID__c, acct);
				}
			}
		}
		
		for(CORE_Hist_Cust_Service_Log_AR_Notes__c iterator : trigger.new) {
			if(String.isNotBlank(iterator.Core_Account_ID__c)) {
				if(mapExtIdAcct.containsKey(iterator.Core_Account_ID__c)) {
					iterator.AccountLookup__c = mapExtIdAcct.get(iterator.Core_Account_ID__c).Id;
				}
			}
		}
	}
}